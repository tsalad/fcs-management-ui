import { Api, Linq2 } from 'o8-common';
import ConfigurationManager from '../Services/ConfigurationManager';
import OrdersFlaggedService from '../Services/OrdersFlaggedService';
import ReleaseService from '../Services/ReleaseService';
import { IDispatch } from '../Typings/IDispatch';
import { IDispatchStatus } from '../Typings/IDispatchStatus';
import { StateEnum } from '../Typings/StateEnum';
import FCSConstants from '../Utils/FCSConstants';

// Fetches all dispatches
const fetchDispatches = () => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/fulfillment/dispatch?shipments=true&groups=true');
            const { dispatches } = (await Api.get(url)).body;
            const payload = Linq2.toDictionary(dispatches, (d: IDispatch) => d.dispatchCode);
            dispatch({ type: FCSConstants.FETCH_DISPATCHES_SUCCESS, payload });
        } catch (err) {
            dispatch ({ type: FCSConstants.FETCH_DISPATCHES_FAILURE });
            throw new Error(err);
        }
    };
};

// Updates an existing dispatch
const updateDispatch = (d: Partial<IDispatch>) => {
    const { updatedAt, dispatchCode, productionCode, state } = d;
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/dispatch/${dispatchCode}`);
            const payload: IDispatch = (await Api.put(url, { updatedAt, productionCode, state })).body;
            dispatch({ type: FCSConstants.UPDATE_DISPATCH_SUCCESS, payload });
            return payload;
        } catch (err) {
            dispatch({ type: FCSConstants.UPDATE_DISPATCH_FAILURE });
            throw new Error(err);
        }
    };
};

// Deletes existing dispatch
const deleteDispatch = (d: Partial<IDispatch>) => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/dispatch/${d.dispatchCode}`);
            const payload: IDispatch = (await Api.delete(url)).body;
            dispatch({ type: FCSConstants.DELETE_DISPATCH_SUCCESS, payload });
        } catch (err) {
            dispatch({ type: FCSConstants.DELETE_DISPATCH_FAILURE });
            throw new Error(err);
        }
    };
};

// Ships a dispatch by dispatch code
const shipDispatch = (d: Partial<IDispatch>) => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/dispatch/${d.dispatchCode}/ship`);
            const payload: IDispatch = (await Api.post(url)).body;
            dispatch({ type: FCSConstants.UPDATE_DISPATCH_SUCCESS, payload });
            return payload;
        } catch (err) {
            dispatch({ type: FCSConstants.UPDATE_DISPATCH_FAILURE });
            throw new Error(err);
        }
    };
};

// Generates dispatch groups
const createDispatchGroups = (d: IDispatch, space: number, slots: number) => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/dispatch/${d.dispatchCode}/group/auto`);
            const FETCH_URL = await ConfigurationManager.buildPorterUrl(`/fulfillment/dispatch/${d.dispatchCode}`);
            await Api.post(url, { space, slots });
            // Fetch updated dispatch with groups to add to state.
            const payload = (await Api.get(FETCH_URL)).body;
            dispatch({ type: FCSConstants.UPDATE_DISPATCH_SUCCESS, payload });
        } catch (err) {
            dispatch({ type: FCSConstants.UPDATE_DISPATCH_FAILURE });
            throw new Error(err);
        }
    };
};

// Updates a dispatch group
const updateDispatchGroup = (group: IDispatchGroup) => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/dispatch/${group.dispatchCode}/group/${group.groupCode}`);
            const FETCH_URL = await ConfigurationManager.buildPorterUrl(`/fulfillment/dispatch/${group.dispatchCode}`);
            await Api.put(url, group);
            const payload = (await Api.get(FETCH_URL)).body;
            dispatch({ type: FCSConstants.UPDATE_DISPATCH_SUCCESS, payload });
            return payload;
        } catch (err) {
            dispatch({ type: FCSConstants.UPDATE_DISPATCH_FAILURE });
            throw new Error(err);
        }
    };
};

// Fetches order flags
const fetchOrdersFlagged = () => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const payload = Linq2.toDictionary(await OrdersFlaggedService.fetchOrdersFlagged() || [], f => f.id);
            dispatch({ type: FCSConstants.FETCH_ORDERS_FLAGGED_SUCCESS, payload });
            return payload;
        } catch (err) {
            dispatch({ type: FCSConstants.FETCH_ORDERS_FLAGGED_FAILURE });
            throw new Error(err);
        }
    };
};

// Fetches all releases EXCEPT archived.  This state is handled separately because it could be a large data set and therefore needs additional filtering
const fetchReleases = () => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const releases = await ReleaseService.fetchReleases([ StateEnum.READY, StateEnum.PENDING, StateEnum.SHIPPABLE, StateEnum.FULFILLMENT, StateEnum.COMPLETED ], true);
            const payload = Linq2.toDictionary(releases, r => r.orderId);
            dispatch({ type: FCSConstants.FETCH_RELEASES_SUCCESS, payload });
        } catch (err) {
            dispatch({ type: FCSConstants.FETCH_RELEASES_FAILURE });
            throw new Error(err);
        }
    };
};

// Fetches the currently processing dispatch
const fetchActiveDispatchStatus = () => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/fulfillment/dispatch/queryAsyncDispatch?activeOnly=true');
            const { dispatchCreationJobMap } = (await Api.get(url)).body;
            const payload = Linq2.toDictionary(Object.values(dispatchCreationJobMap), (d: IDispatchStatus) => d.dispatchCode);
            dispatch({ type: FCSConstants.FETCH_ACTIVE_DISPATCH_STATUS_SUCCESS, payload });
        } catch (err) {
            dispatch({ type: FCSConstants.FETCH_ACTIVE_DISPATCH_STATUS_FAILURE });
            throw new Error(err);
        }
    };
};

export {
    createDispatchGroups,
    fetchDispatches,
    updateDispatch,
    updateDispatchGroup,
    deleteDispatch,
    shipDispatch,
    fetchOrdersFlagged,
    fetchReleases,
    fetchActiveDispatchStatus
};
