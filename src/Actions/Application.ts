import { v4 } from 'uuid';
import { SnackbarActionStateEnum } from '../Components/SnackbarAction';
import FCSConstants from '../Utils/FCSConstants';

const setSuccessMessage = (message?: string) => {
    return (dispatch: Redux.Dispatch<any>) => {
        dispatch({ type: FCSConstants.SET_SNACKBAR_MESSAGE, payload: { id: v4(), type: SnackbarActionStateEnum.SUCCESS, message } });
    };
};

const clearSnackbarMessage = (key: string) => {
    return (dispatch: Redux.Dispatch<any>) => {
        dispatch({ type: FCSConstants.CLEAR_SNACKBAR_MESSAGE, payload: key });
    };
};

const setErrorMessage = (message?: string) => {
    return (dispatch: Redux.Dispatch<any>) => {
        dispatch({ type: FCSConstants.SET_SNACKBAR_MESSAGE, payload: { id: v4(), type: SnackbarActionStateEnum.ERROR, message } });
    };
};

const toggleManifestImportModal = (state: boolean) => {
    return (dispatch: Redux.Dispatch<any>) => {
        dispatch({ type: FCSConstants.MODAL_MANIFEST_IMPORT_TOGGLE, payload: state });
    };
};

export {
    setSuccessMessage,
    clearSnackbarMessage,
    setErrorMessage,
    toggleManifestImportModal
};
