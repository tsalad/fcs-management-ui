import { Api, Linq2 } from 'o8-common';
import Catalog from '../Services/Catalog';
import ConfigurationManager from '../Services/ConfigurationManager';
import FCSConstants from '../Utils/FCSConstants';

// Fetches all existing product items
const fetchItems = () => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/catalog/item?suppliers=true');
            const items: IProductItem[] = ((await Api.get(url)).body).items;
            // Create dictionary with sku as keys
            const payload = Linq2.toDictionary(items, i => i.sku);
            dispatch({ type: FCSConstants.FETCH_ITEMS_SUCCESS, payload });
        } catch (err) {
            dispatch({ type: FCSConstants.FETCH_ITEMS_FAILURE });
            throw new Error(err);
        }
    };
};

// Fetches all existing product categories
const fetchCategories = () => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/catalog/category');
            const categories: ICategory[] = ((await Api.get(url)).body).categories;
            // Create dictionary with categoryCode as keys
            const payload = Linq2.toDictionary(categories, (item) => item.categoryCode);
            dispatch({ type: FCSConstants.FETCH_CATEGORIES_SUCCESS, payload });
        } catch (err) {
            dispatch({ type: FCSConstants.FETCH_CATEGORIES_FAILURE });
            throw new Error(err);
        }
    };
};

// Fetches all of the existing products
const fetchProducts = () => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/catalog/product');
            const products: IProduct[] = ((await Api.get(url)).body).products;
            // Create dictionary with productCode as keys
            const payload = Linq2.toDictionary(products, (item) => item.productCode);
            dispatch({ type: FCSConstants.FETCH_PRODUCTS_SUCCESS, payload });
        } catch (err) {
            dispatch({ type: FCSConstants.FETCH_PRODUCTS_FAILURE });
            throw new Error(err);
        }
    };
};

// Fetches all of the existing product tags
const fetchTags = () => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/catalog/tag');
            const tags: ITagLabel[] = ((await Api.get(url)).body).labels;
            const payload = Linq2.toDictionary(tags, t => t.label);
            dispatch({ type: FCSConstants.FETCH_PRODUCT_TAGS_SUCCESS, payload });
        } catch (err) {
            dispatch({ type: FCSConstants.FETCH_PRODUCT_TAGS_FAILURE });
            throw new Error(err);
        }
    };
};

// Creates a new product category
const createCategory = (category: Partial<ICategory>) => {
    return async (dispatch: Redux.Dispatch<any>): Promise<ICategory> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/catalog/category');
            const payload: ICategory = (await Api.post(url, category)).body;
            dispatch({ type: FCSConstants.CREATE_CATEGORY_SUCCESS, payload });
            return payload;
        } catch (err) {
            dispatch({ type: FCSConstants.CREATE_CATEGORY_FAILURE });
            throw new Error(err);
        }
    };
};

// Creates a new product item
const createProductItem = (item: Partial<IProductItem>) => {
    return async (dispatch: Redux.Dispatch<any>): Promise<IProductItem> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/catalog/item');
            // need to remove category code as it's not accepted on the create product item input
            const { categoryCode, ...rest } = item;
            const payload: IProductItem = (await Api.post(url, rest)).body;
            dispatch({ type: FCSConstants.CREATE_PRODUCT_ITEM_SUCCESS, payload });
            return payload;
        } catch (err) {
            dispatch({ type: FCSConstants.CREATE_PRODUCT_ITEM_FAILURE });
            throw new Error(err);
        }
    };
};

// Creates an image for a given product item.
const createProductItemImage = (sku: string, image: Partial<IProductItemImage>) => {
    return async (dispatch: Redux.Dispatch<any>): Promise<IProductItemImage> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/catalog/item/${sku}/image`);
            return (await Api.post(url, image)).body;
        } catch (err) {
            throw new Error(err);
        }
    };
};

// Creates an image for a given product item using multi part upload.
const createProductItemImageMultipart = (file: File, sku: string) => {
    return async (dispatch: Redux.Dispatch<any>): Promise<IProductItemImage> => {
        try {
            const body = new FormData();

            body.append('binaryData', file);
            body.append('metadata', new Blob([ JSON.stringify({ contentType: file.type }) ], { type: 'application/json' }));

            const url = await ConfigurationManager.buildPorterUrl(`/catalog/item/${sku}/imagemultipart`);
            return (await Api.post(url, body)).body;
        } catch (err) {
            throw new Error(err);
        }
    };
};

// Deletes an image from a product item
const deleteProductItemImage = (sku: string) => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/catalog/item/${sku}/image`);
            await Api.delete(url);
            dispatch({ type: FCSConstants.DELETE_PRODUCT_ITEM_IMAGE_SUCCESS });
        } catch (err) {
            throw new Error(err);
            dispatch({ type: FCSConstants.DELETE_PRODUCT_ITEM_IMAGE_FAILURE });
        }
    };
};

// Updates an existing product item
const updateProductItem = (item: Partial<IProductItem>) => {
    return async (dispatch: Redux.Dispatch<any>): Promise<IProductItem> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/catalog/item/${item.sku}`);
            // need to remove category code as it's not accepted on the update product item input
            const current = Catalog.fetchProductItem(item.sku);
            const productItem = { ...current, ...item };
            const { categoryCode, ...rest } = productItem;
            const payload: IProductItem = (await Api.put(url, rest)).body;
            dispatch({ type: FCSConstants.UPDATE_PRODUCT_ITEM_SUCCESS, payload });
            return payload;
        } catch (err) {
            dispatch({ type: FCSConstants.UPDATE_PRODUCT_ITEM_FAILURE });
            throw new Error(err);
        }
    };
};

// Creates a new product
const createProduct = (product: Partial<IProduct>) => {
    return async (dispatch: Redux.Dispatch<any>): Promise<IProduct> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/catalog/product`);
            const payload: IProduct = (await Api.post(url, product)).body;
            dispatch({ type: FCSConstants.CREATE_PRODUCT_SUCCESS, payload });
            return payload;
        } catch (err) {
            dispatch({ type: FCSConstants.CREATE_PRODUCT_FAILURE });
            throw new Error(err);
        }
    };
};

// Creates a new tag label
const createTag = (label: string) => {
    return async (dispatch: Redux.Dispatch<any>): Promise<ITagLabel> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/catalog/tag`);
            const payload = (await Api.post(url, { label })).body;
            dispatch({ type: FCSConstants.CREATE_TAG_LABEL_SUCCESS, payload });
            return payload;
        } catch (err) {
            dispatch({ type: FCSConstants.CREATE_TAG_LABEL_FAILURE });
            throw new Error(err);
        }
    };
};

export {
    createCategory,
    createProductItem,
    createProduct,
    createTag,
    createProductItemImage,
    createProductItemImageMultipart,
    deleteProductItemImage,
    updateProductItem,
    fetchItems,
    fetchCategories,
    fetchProducts,
    fetchTags
};
