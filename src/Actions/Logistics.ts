import { Api, Linq2 } from 'o8-common';
import { v4 } from 'uuid';
import ConfigurationManager from '../Services/ConfigurationManager';
import { IStockEvent } from '../Typings/IStockEvent';
import { IStockInput } from '../Typings/IStockInput';
import FCSConstants from '../Utils/FCSConstants';

// Fetches all clients and creates a menu item for each client
const fetchClientList = () => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/logistics/client');
            const { clients } = (await Api.get(url)).body;
            const payload: Map<string, IClient> = Linq2.toDictionary(clients, (c: IClient) => c.clientCode);
            const menus = Linq2.select(clients.values(), (c: IClient) => ({ menuId: c.clientCode, title: c.name })).toArray();
            dispatch({ type: FCSConstants.FETCH_CLIENTS_SUCCESS, payload });
            dispatch({ type: FCSConstants.SET_CLIENT_MENUS, payload: menus });
        } catch (err) {
            dispatch({ type: FCSConstants.FETCH_CLIENTS_FAILURE });
            throw new Error(err);
        }
    };
};

// Fetches all existing shipping methods
const fetchShippingMethods = () => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/logistics/carrier/method?carrierCode=');
            const { methods } = (await Api.get(url)).body;
            const payload: Map<string, IShippingMethod> = Linq2.toDictionary(methods, (m: IShippingMethod) => m.serviceCode);
            dispatch({ type: FCSConstants.FETCH_SHIPPING_METHODS_SUCCESS, payload });
        } catch (err) {
            dispatch({ type: FCSConstants.FETCH_SHIPPING_METHODS_FAILURE });
            throw new Error(err);
        }
    };
};

// Fetches all depots or locations
const fetchDepots = () => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/logistics/depot');
            const { depots } = (await Api.get(url)).body;
            const payload: Map<string, IClient> = Linq2.toDictionary(depots, (d: IDepot) => d.depotCode);
            dispatch({ type: FCSConstants.FETCH_DEPOTS_SUCCESS, payload });
        } catch (err) {
            dispatch({ type: FCSConstants.FETCH_DEPOTS_FAILURE });
            throw new Error(err);
        }
    };
};

// Fetches all storage locations
const fetchStorage = () => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/logistics/storage?stock=true');
            const { storage } = (await Api.get(url)).body;
            const payload: Map<string, IStorage> = Linq2.toDictionary(storage, (d: IStorage) => d.storageCode);
            dispatch({ type: FCSConstants.FETCH_STORAGE_SUCCESS, payload });
        } catch (err) {
            dispatch({ type: FCSConstants.FETCH_STORAGE_FAILURE });
            throw new Error(err);
        }
    };
};

// Fetches all workstations
const fetchWorkstations = () => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/logistics/workstation?stock=true');
            const { workstations } = (await Api.get(url)).body;
            const payload: Map<string, IWorkstation> = Linq2.toDictionary(workstations, (w: IWorkstation) => w.storageCode);
            dispatch({ type: FCSConstants.FETCH_WORKSTATION_SUCCESS, payload });
        } catch (err) {
            dispatch({ type: FCSConstants.FETCH_WORKSTATION_FAILURE });
            throw new Error(err);
        }
    };
};

// Fetches stock for all products
const fetchStock = () => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/logistics/stock/search');
            const { stock } = (await Api.get(url)).body as { stock: IStock[] };
            const payload = Linq2.groupBy(stock, s => s.sku).toDictionary();
            dispatch({ type: FCSConstants.FETCH_STOCK_SUCCESS, payload });
        } catch (err) {
            dispatch({ type: FCSConstants.FETCH_STOCK_FAILURE });
            throw new Error(err);
        }
    };
};

// Fetches events for all products
const fetchEvents = () => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/logistics/stock/event');
            const { events } = (await Api.get(url)).body as { events: IStockEvent[] };
            const payload = Linq2.groupBy(events, s => s.sku).toDictionary();
            dispatch({ type: FCSConstants.FETCH_EVENTS_SUCCESS, payload });
        } catch (err) {
            dispatch({ type: FCSConstants.FETCH_EVENTS_FAILURE });
            throw new Error(err);
        }
    };
};

// Fetches production line locations
const fetchProductionLines = () => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/logistics/production');
            const { lines } = (await Api.get(url)).body as { lines: IProductionLine[] };
            const payload = Linq2.toDictionary(lines, l => l.productionCode);
            dispatch({ type: FCSConstants.FETCH_PRODUCTION_LINES_SUCCESS, payload });
        } catch (err) {
            dispatch({ type: FCSConstants.FETCH_PRODUCTION_LINES_FAILURE });
            throw new Error(err);
        }
    };
};

// Fetches all shipping suppliers
const fetchSuppliers = () => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/logistics/supplier');
            const { suppliers } = (await Api.get(url)).body as { suppliers: ISupplier[] };
            const payload = Linq2.toDictionary(suppliers, s => s.supplierCode);
            dispatch({ type: FCSConstants.FETCH_SUPPLIERS_SUCCESS, payload });
        } catch (err) {
            dispatch({ type: FCSConstants.FETCH_SUPPLIERS_FAILURE });
            throw new Error(err);
        }
    };
};

// Creates a new stock for a product
const createStock = (stock: IStockInput) => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/logistics/stock/adjust');
            const payload = (await Api.post(url, stock)).body;
            dispatch({ type: FCSConstants.CREATE_STOCK_SUCCESS, payload });
            // Note this doesn't return the event so we need to refetch
            fetchEvents()(dispatch);
        } catch (err) {
            dispatch({ type: FCSConstants.CREATE_STOCK_FAILURE });
            throw new Error(err);
        }
    };
};

// Creates a new storage depot
const createDepot = (depot: Partial<IDepot>) => {
    const depotCode = v4().toUpperCase();
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/logistics/depot');
            const payload = (await Api.post(url, { ...depot, depotCode })).body;
            dispatch({ type: FCSConstants.CREATE_DEPOT_SUCCESS, payload });
            createStorage({ depotCode, storageCode: v4().toUpperCase(), name: `${depot.name} Storage` })(dispatch);
            return payload;
        } catch (err) {
            dispatch({ type: FCSConstants.CREATE_DEPOT_FAILURE });
            throw new Error(err);
        }
    };
};

// Creates a new storage location for a depot
const createStorage = (storage: IStorage) => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/logistics/storage');
            const payload =  (await Api.post(url, storage)).body;
            dispatch({ type: FCSConstants.CREATE_STORAGE_SUCCESS, payload });
            return payload;
        } catch (err) {
            dispatch({ type: FCSConstants.CREATE_STORAGE_FAILURE });
            throw new Error(err);
        }
    };
};

// Creates a new supplier
const createSupplier = (supplier: ISupplier) => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/logistics/supplier');
            const payload = (await Api.post(url, supplier)).body;
            dispatch({ type: FCSConstants.CREATE_SUPPLIER_SUCCESS, payload });
            return payload;
        } catch (err) {
            dispatch({ type: FCSConstants.CREATE_SUPPLIER_FAILURE });
            throw new Error(err);
        }
    };
};

// Creates a new client
const createClient = (client: IClient) => {
    return async (dispatch: Redux.Dispatch<any>) => {
        client.clientCode = v4().toUpperCase();
        try {
            const url = await ConfigurationManager.buildPorterUrl('/logistics/client');
            const payload = (await Api.post(url, client)).body;
            dispatch({ type: FCSConstants.CREATE_CLIENT_SUCCESS, payload });
            return payload;
        } catch (err) {
            dispatch({ type: FCSConstants.CREATE_CLIENT_FAILURE });
            throw new Error(err);
        }
    };
};

// Updates existing client
const updateClient = (client: IClient) => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/logistics/client/${client.clientCode}`);
            const payload = (await Api.put(url, client)).body;
            dispatch({ type: FCSConstants.UPDATE_CLIENT_SUCCESS, payload });
            return payload;
        } catch (err) {
            dispatch({ type: FCSConstants.UPDATE_CLIENT_FAILURE });
            throw new Error(err);
        }
    };
};

// Updates existing supplier
const updateSupplier = (supplier: ISupplier) => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/logistics/supplier/${supplier.supplierCode}`);
            const payload = (await Api.put(url, supplier)).body;
            dispatch({ type: FCSConstants.UPDATE_SUPPLIER_SUCCESS, payload });
            return payload;
        } catch (err) {
            dispatch({ type: FCSConstants.UPDATE_SUPPLIER_FAILURE });
            throw new Error(err);
        }
    };
};

// Updates existing depot
const updateDepot = (depot: Partial<IDepot>) => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/logistics/depot/${depot.depotCode}`);
            const payload = (await Api.put(url, depot)).body;
            dispatch({ type: FCSConstants.UPDATE_DEPOT_SUCCESS, payload });
            return payload;
        } catch (err) {
            dispatch({ type: FCSConstants.UPDATE_DEPOT_FAILURE });
            throw new Error(err);
        }
    };
};

// Deletes existing depot
const deleteDepot = (key: string) => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/logistics/depot/${key}`);
            await Api.delete(url);
            dispatch({ type: FCSConstants.DELETE_DEPOT_SUCCESS, payload: key });
        } catch (err) {
            dispatch({ type: FCSConstants.DELETE_DEPOT_FAILURE });
            throw new Error(err);
        }
    };
};

// Deletes supplier by code
const deleteSupplier = (key: string) => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/logistics/supplier/${key}`);
            await Api.delete(url);
            dispatch({ type: FCSConstants.DELETE_SUPPLIER_SUCCESS, payload: key });
        } catch (err) {
            dispatch({ type: FCSConstants.DELETE_SUPPLIER_FAILURE });
            throw new Error(err);
        }
    };
};

// Deletes storage by storageCode
const deleteStorage = (key: string) => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/logistics/storage/${key}`);
            await Api.delete(url);
            dispatch({ type: FCSConstants.DELETE_STORAGE_SUCCESS, key });
        } catch (err) {
            dispatch({ type: FCSConstants.DELETE_STORAGE_FAILURE });
            throw new Error(err);
        }
    };
};

// Moves stock
const moveStock = (stockMove: IStockMoveInput) => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/logistics/stock/move');
            const payload = (await Api.post(url, stockMove)).body;
            dispatch({ type: FCSConstants.STOCK_MOVE_SUCCESS, payload });
        } catch (err) {
            dispatch({ type: FCSConstants.STOCK_MOVE_FAILURE });
            throw new Error(err);
        }
    };
};

export {
    fetchClientList,
    fetchDepots,
    fetchStorage,
    fetchWorkstations,
    fetchStock,
    fetchEvents,
    fetchProductionLines,
    fetchSuppliers,
    fetchShippingMethods,
    createStock,
    createDepot,
    createStorage,
    createSupplier,
    createClient,
    updateClient,
    updateDepot,
    updateSupplier,
    deleteDepot,
    deleteSupplier,
    deleteStorage,
    moveStock
};
