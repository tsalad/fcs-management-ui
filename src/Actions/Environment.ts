import { Api } from 'o8-common';
import FCSConstants from '../Utils/FCSConstants';

const fetchEnvironment = () => {
    return async (dispatch: Redux.Dispatch<any>) => {
        try {
            const envUrl = `rest/v1.0/status/env`;
            const payload = (await Api.get(envUrl)).body;
            dispatch({ type: FCSConstants.FETCH_ENVIRONMENT_SUCCESS, payload });
        } catch (err) {
            dispatch({ type: FCSConstants.FETCH_ENVIRONMENT_FAILURE });
        }
    };
};

export {
    fetchEnvironment
};
