import { Linq2 } from 'o8-common';
import meta from '../Decorators/meta';
import Store from '../Services/Store';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import FCSConstants from '../Utils/FCSConstants';
import Validators from '../Utils/Validators';

export interface IClientValidation {
    name: MC.IValidationResult;
    addressStreet: MC.IValidationResult;
    addressLocality: MC.IValidationResult;
    addressRegion: MC.IValidationResult;
    addressPostalCode: MC.IValidationResult;
    contactPhone: MC.IValidationResult;
    contactEmail: MC.IValidationResult;
}

export default class Client implements IClient {
    public static readonly TYPE: string = 'Client';
    public static readonly defaultProps: IClient = Object.freeze({ ...new Client() });

    @meta({ displayName: 'Last Updated' })
    public updatedAt: string = null;

    @meta({ displayName: 'Client Code' })
    public clientCode: string = null;

    @meta({ displayName: 'Name' })
    public name: string = null;

    @meta({ displayName: 'Address' })
    public address: IAddress = {
        name: '',
        phone: '',
        email: '',
        street: '',
        unit: '',
        locality: '',
        region: '',
        postalCode: '',
        country: 'US'
    };

    public static create(item: Partial<IClient>): Client {
        if (typeof item === 'object' && Object.getPrototypeOf(item) !== Client.prototype) {
            Object.setPrototypeOf(item, Client.prototype);
        }
        return item as Client;
    }

    public static validateName = ({ name, updatedAt }: IClient): MC.IValidationResult => {
        // Ensure name exists as this is a required field.
        if (name === null || name === undefined || !name.length) {
            return { valid: false, message: 'Name is required.' };
        }

        // Name must be unique.
        const { app: { modules } } = Store.getState();
        const pluginState = modules && modules.get(FCSConstants.PLUGIN_ID) as IFCSReduxState;
        const items = pluginState && pluginState.logistics && pluginState.logistics.clients.values();
        const match = Linq2.firstOrDefault(items, undefined, i => i.name.toLowerCase() === name.toLowerCase());
        return { valid: match === undefined || (updatedAt === null ? false : match.updatedAt === updatedAt), message: 'Client name already exists.' };
    }

    public static validateAddressRegion = ({ address: { region } }: IClient): MC.IValidationResult => {
        const requiredValidator = Validators.isRequired(region);
        if (!requiredValidator.valid) {
            return requiredValidator;
        }
        return { valid: FCSConstants.stateList.some(s => s === region), message: 'Please enter a valid region' };
    }

    public static validateAddressPostalCode = ({ address: { postalCode } }: IClient): MC.IValidationResult => {
        const requiredValidator = Validators.isRequired(postalCode);
        if (!requiredValidator.valid) {
            return requiredValidator;
        }

        return { valid: (/^[0-9]{5}(?:-[0-9]{4})?$/).test(postalCode), message: 'Please enter a valid postal code.' };
    }

    public static validate = (client: IClient): IClientValidation => {
        const { address: { street, locality, phone, email } } = client;
        return {
            name: Client.validateName(client),
            addressStreet: Validators.isRequired(street),
            addressLocality: Validators.isRequired(locality),
            addressRegion: Client.validateAddressRegion(client),
            addressPostalCode: Client.validateAddressPostalCode(client),
            contactPhone: Validators.isValidPhoneNumber(phone),
            contactEmail: Validators.isValidEmailAddress(email)
        };
    }
}
