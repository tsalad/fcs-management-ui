import { Linq2 } from 'o8-common';
import Store from '../Services/Store';
import { IDispatch } from '../Typings/IDispatch';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import { IShipment } from '../Typings/IShipment';
import { StateEnum } from '../Typings/StateEnum';
import FCSConstants from '../Utils/FCSConstants';
import Validators from '../Utils/Validators';

export interface IDispatchValidation {
    dispatchCode: MC.IValidationResult;
    productionCode: MC.IValidationResult;
}

export default class Dispatch implements IDispatch {
    public static readonly TYPE: string = 'Dispatch';
    public static readonly defaultProps: IDispatch = Object.freeze({ ...new Dispatch() });

    public updatedAt: string = null;

    public beganAt: string = null;

    public completedAt: string = null;

    public archivedAt: string = null;

    public dispatchCode: string = null;

    public productionCode: string = null;

    public clientCode: string = null;

    public state: StateEnum = StateEnum.READY;

    public shipments: IShipment[] = [];

    public groups: IDispatchGroup[] = [];

    public static create(item: Partial<IDispatch>): Dispatch {
        if (typeof item === 'object' && Object.getPrototypeOf(item) !== Dispatch.prototype) {
            Object.setPrototypeOf(item, Dispatch.prototype);
        }
        return item as Dispatch;
    }

    public static validateDispatchCode = (item: Partial<IDispatch>): MC.IValidationResult => {
        const { dispatchCode, updatedAt } = item;

        // Ensure name exists as this is a required field.
        if (dispatchCode === null || dispatchCode === undefined || !dispatchCode.length) {
            return { valid: false, message: 'Name is required.' };
        }

        const { app: { modules } } = Store.getState();
        const pluginState = modules && modules.get(FCSConstants.PLUGIN_ID) as IFCSReduxState;
        const items = pluginState && pluginState.logistics && pluginState.fulfillment.dispatches.values();
        const match = Linq2.firstOrDefault(items, undefined, i => i.dispatchCode.toLowerCase() === dispatchCode.toLowerCase());
        const valid = match === undefined || (updatedAt === null ? false : match.updatedAt === updatedAt);
        if (!valid) {
            return { valid, message: 'Dispatch already exists.'};
        }
        
        return { valid: (/^[A-Z0-9][A-Z0-9-_\.]{2,}$/).test(dispatchCode), message: 'Must contain only capital letters, numbers, "-" or "_"' };
    }

    public static validateProductionCode = (productionCode: string): MC.IValidationResult => {
        // Ensure name exists as this is a required field.
        if (productionCode === null || productionCode === undefined || !productionCode.length) {
            return { valid: false, message: 'Production line is required.' };
        }

        const { app: { modules } } = Store.getState();
        const pluginState = modules && modules.get(FCSConstants.PLUGIN_ID) as IFCSReduxState;
        const items = pluginState && pluginState.logistics && pluginState.logistics.productionLines;
        return { valid: items.has(productionCode), message: 'Production line does not exist.' };
    }

    public static validate = (item: Partial<IDispatch>): IDispatchValidation => ({
        dispatchCode: Dispatch.validateDispatchCode(item),
        productionCode: Dispatch.validateProductionCode(item.productionCode)
    })
}
