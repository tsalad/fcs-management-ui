import { Linq2, Util } from 'o8-common';
import meta from '../Decorators/meta';
import Store from '../Services/Store';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import FCSConstants from '../Utils/FCSConstants';
import Validators from '../Utils/Validators';

export interface ISupplierValidation {
    supplierCode: MC.IValidationResult;
    name: MC.IValidationResult;
    contactPhone: MC.IValidationResult;
    contactEmail: MC.IValidationResult;
}

export default class Supplier implements ISupplier {
    public static readonly TYPE: string = 'Supplier';
    public static readonly defaultProps: ISupplier = Object.freeze({ ...new Supplier() });

    @meta({ displayName: 'Last Updated' })
    public updatedAt: string = null;

    @meta({ displayName: 'Code' })
    public supplierCode: string = null;

    @meta({ displayName: 'Name' })
    public name: string = null;

    @meta({ displayName: 'Address' })
    public address: IAddress = {
        name: null,
        phone: null,
        email: null,
        street: null,
        unit: null,
        locality: null,
        region: null,
        postalCode: null
    };

    public static create(item: Partial<ISupplier>): Supplier {
        if (typeof item === 'object' && Object.getPrototypeOf(item) !== Supplier.prototype) {
            Object.setPrototypeOf(item, Supplier.prototype);
        }
        return item as Supplier;
    }

    public static validateCode = (supplier: ISupplier): MC.IValidationResult => {
        const { supplierCode, updatedAt } = supplier;

        if (supplierCode === null || supplierCode === undefined || !supplierCode.length) {
            return { valid: false, message: 'Supplier Code is required' };
        }

        const isValid = Validators.isValidCode(supplierCode);
        if (!isValid.valid) {
            return isValid;
        }

        const { app: { modules } } = Store.getState();
        const pluginState = modules && modules.get(FCSConstants.PLUGIN_ID) as IFCSReduxState;
        const suppliers = pluginState && pluginState.logistics && pluginState.logistics.suppliers;
        const match = suppliers && suppliers.get(supplierCode);
        const valid = match !== undefined ? (updatedAt === null ? false : match.updatedAt === updatedAt) : true;

        return { valid, message: 'Supplier Code must be unique.' };
    }

    public static validateName = (supplier: ISupplier): MC.IValidationResult => {
        const { name, updatedAt } = supplier;

        if (name === null || name === undefined || !name.length) {
            return { valid: false, message: 'Supplier Name is required' };
        }

        const { app: { modules } } = Store.getState();
        const pluginState = modules && modules.get(FCSConstants.PLUGIN_ID) as IFCSReduxState;
        const suppliers = pluginState && pluginState.logistics && pluginState.logistics.suppliers;
        const match = suppliers && Linq2.firstOrDefault([ ...suppliers.values() ], undefined, s => s.name === name);
        const valid = match !== undefined ? (updatedAt === null ? false : match.updatedAt === updatedAt) : true;

        return { valid, message: 'Supplier Name must be unique.' };
    }

    public static validate = (supplier: ISupplier): ISupplierValidation => {
        const { address } = supplier;
        return {
            supplierCode: Supplier.validateCode(supplier),
            name: Supplier.validateName(supplier),
            contactPhone: Boolean(address) && address.phone !== null ? Validators.isValidPhoneNumber(address.phone) : { valid: true, message: '' },
            contactEmail: Boolean(address) && address.email !== null ? Validators.isValidEmailAddress(address.email) : { valid: true, message: '' }
        };
    }
}
