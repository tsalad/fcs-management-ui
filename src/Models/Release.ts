import { Api, Linq2, Util } from 'o8-common';
import meta from '../Decorators/meta';
import ConfigurationManager from '../Services/ConfigurationManager';
import Store from '../Services/Store';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import { IRelease } from '../Typings/IRelease';
import { IShipment } from '../Typings/IShipment';
import { StateEnum } from '../Typings/StateEnum';
import FCSConstants from '../Utils/FCSConstants';

export interface IReleaseValidation {
    shipBy: MC.IValidationResult;
    depotCode: MC.IValidationResult;
    items: MC.IValidationResult;
    orderId: MC.IValidationResult;
}

export default class Release implements IRelease {
    public static readonly TYPE: string = 'Release';
    public static readonly defaultProps: IRelease = Object.freeze({ ...new Release() });
    
    @meta({displayName: 'Last Updated'})
    public updatedAt: string = null;

    @meta({displayName: 'Order ID'})
    public orderId: string = '';

    @meta({displayName: 'Client Code'})
    public clientCode: string = null;

    @meta({displayName: 'Depot Code'})
    public depotCode: string = null;

    @meta({displayName: 'Depot Name'})
    public depotName: string = null;

    @meta({displayName: 'Client'})
    public clientName: string = null;

    @meta({displayName: 'Dispatch Code'})
    public dispatchCode: string = null;

    @meta({displayName: 'Ship By'})
    public shipBy: string = null;

    @meta({displayName: 'Arrive Earliest'})
    public arriveEarliest: string = null;

    @meta({displayName: 'Arrive Latest'})
    public arriveLatest: string = null;

    @meta({displayName: 'Allow Expedite'})
    public allowExpedite: boolean = false;

    @meta({displayName: 'State'})
    public state: StateEnum = StateEnum.PENDING;

    @meta({displayName: 'Address'})
    public address: IAddress = {
        name: null,
        phone: null,
        email: null,
        street: null,
        unit: null,
        locality: null,
        region: null,
        postalCode: null
    };

    @meta({displayName: 'Release Items'})
    public items: IReleaseItem[] = [];

    @meta({displayName: 'Shipments'})
    public shipments: IShipment[] = null;

    public static create(item: Partial<IRelease>): Release {
        if (typeof item === 'object' && Object.getPrototypeOf(item) !== Release.prototype) {
            Object.setPrototypeOf(item, Release.prototype);
        }
        return item as Release;
    }

    public static validateShipBy = async (shipBy: string): Promise<MC.IValidationResult> => {
        return { valid: (shipBy !== undefined && shipBy !== null && shipBy.length > 0), message: 'Ship By is required.' };
    }

    public static validateDepotCode = async (depotCode: string): Promise<MC.IValidationResult> => {
        return { valid: (depotCode !== undefined && depotCode !== null && depotCode.length > 0), message: 'Shipping Location is required.' };
    }

    public static validateItems = async (items: Partial<IReleaseItem>[]): Promise<MC.IValidationResult> => {
        return { valid: Util.isNonEmptyArray(items), message: 'Please add at least one product to this release.' };
    }

    public static validateOrderId = async (release: IRelease): Promise<MC.IValidationResult> => {
        const { orderId, updatedAt } = release;

        if (orderId === null || orderId === undefined || !orderId.length) {
            return { valid: false, message: 'Order ID is required' };
        }

        try {
            // order ID must be unique.
            const { app: { modules } } = Store.getState();
            const pluginState = modules && modules.get(FCSConstants.PLUGIN_ID) as IFCSReduxState;
            const items = pluginState && pluginState.fulfillment && pluginState.fulfillment.nonArchivedReleases;
            const match = Boolean(items) && items.has(orderId) && items.get(orderId);
            const valid = Boolean(match) ? (!Boolean(updatedAt) ? false : match.updatedAt === updatedAt) : true;
            return { valid, message: 'Order ID must be unique.' };
        } catch (err) {
            return { valid: true, message: 'Order ID must be unique' };
        }
    }

    public static validate = async (release: IRelease): Promise<IReleaseValidation> => {
        const { shipBy, depotCode, items } = release;
        return {
            shipBy: await Release.validateShipBy(shipBy),
            depotCode: await Release.validateDepotCode(depotCode),
            items: await Release.validateItems(items),
            orderId: await Release.validateOrderId(release)
        };
    }
}
