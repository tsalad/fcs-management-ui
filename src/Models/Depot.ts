import { Linq2 } from 'o8-common';
import { meta } from '../Decorators';
import Store from '../Services/Store';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import FCSConstants from '../Utils/FCSConstants';
import Validators from '../Utils/Validators';

export interface IDepotValidation {
    name: MC.IValidationResult;
    clientCode: MC.IValidationResult;
    addressStreet: MC.IValidationResult;
    addressLocality: MC.IValidationResult;
    addressRegion: MC.IValidationResult;
    addressPostalCode: MC.IValidationResult;
    contactPhone: MC.IValidationResult;
    contactEmail: MC.IValidationResult;
}

export default class Depot implements IDepot {
    public static readonly TYPE: string = 'Depot';
    public static readonly defaultProps: IDepot = Object.freeze({ ...new Depot() });

    public updatedAt: string = null;

    @meta({ displayName: 'Depot Code' })
    public depotCode: string = '';

    @meta({ displayName: 'Client Code' })
    public clientCode: string = '';

    @meta({ displayName: 'Name' })
    public name: string = '';

    @meta({ displayName: 'Address' })
    public address: IAddress = {
        name: '',
        phone: '',
        email: '',
        street: '',
        unit: '',
        locality: '',
        region: '',
        postalCode: '',
        country: 'US'
    };

    public static create(item: Partial<IDepot>): Depot {
        if (typeof item === 'object' && Object.getPrototypeOf(item) !== Depot.prototype) {
            Object.setPrototypeOf(item, Depot.prototype);
        }
        return item as Depot;
    }

    public static validateName = ({ name, updatedAt }: IDepot): MC.IValidationResult => {
        // Ensure name exists as this is a required field.
        if (name === null || name === undefined || !name.length) {
            return { valid: false, message: 'Name is required.' };
        }

        // Name must be unique.
        const { app: { modules } } = Store.getState();
        const pluginState = modules && modules.get(FCSConstants.PLUGIN_ID) as IFCSReduxState;
        const items = pluginState && pluginState.logistics && pluginState.logistics.depots.values();
        const match = Linq2.firstOrDefault(items, undefined, i => i.name.toLowerCase() === name.toLowerCase());
        return { valid: match === undefined || (updatedAt === null ? false : match.updatedAt === updatedAt), message: 'Depot name already exists.' };
    }

    public static validateClient = ({ clientCode }: IDepot): MC.IValidationResult => {
        if (clientCode === null || clientCode === undefined || !clientCode.length) {
            return { valid: false, message: FCSConstants.messageRequired };
        }

        const { app: { modules } } = Store.getState();
        const pluginState = modules && modules.get(FCSConstants.PLUGIN_ID) as IFCSReduxState;
        const { clients } = pluginState && pluginState.logistics;
        return { valid: clients.has(clientCode), message: 'Client not found.' };
    }

    public static validateAddressRegion = ({ address: { region } }: IDepot): MC.IValidationResult => {
        const requiredValidator = Validators.isRequired(region);
        if (!requiredValidator.valid) {
            return requiredValidator;
        }
        return { valid: FCSConstants.stateList.some(s => s === region), message: 'Please enter a valid region' };
    }

    public static validateAddressPostalCode = ({ address: { postalCode } }: IDepot): MC.IValidationResult => {
        const requiredValidator = Validators.isRequired(postalCode);
        if (!requiredValidator.valid) {
            return requiredValidator;
        }

        return { valid: (/^[0-9]{5}(?:-[0-9]{4})?$/).test(postalCode), message: 'Please enter a valid postal code.' };
    }

    public static validate = (depot: IDepot): IDepotValidation => {
        const { address: { street, locality, phone, email } } = depot;
        return {
            name: Depot.validateName(depot),
            clientCode: Depot.validateClient(depot),
            addressStreet: Validators.isRequired(street),
            addressLocality: Validators.isRequired(locality),
            addressRegion: Depot.validateAddressRegion(depot),
            addressPostalCode: Depot.validateAddressPostalCode(depot),
            contactPhone: Validators.isValidPhoneNumber(phone),
            contactEmail: Validators.isValidEmailAddress(email)
        };
    }
}
