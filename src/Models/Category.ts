import { Linq2, Util } from 'o8-common';
import meta from '../Decorators/meta';
import Store from '../Services/Store';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import FCSConstants from '../Utils/FCSConstants';
import Validators from '../Utils/Validators';

export interface ICategoryValidation {
    name: MC.IValidationResult;
    categoryCode: MC.IValidationResult;
}

export default class Category implements ICategory {
    public static readonly TYPE: string = 'Category';
    public static readonly defaultProps: ICategory = Object.freeze({ ...new Category() });
    
    public updatedAt: string = null;

    @meta({ displayName: 'Category Code' })
    public categoryCode: string = '';

    @meta({ displayName: 'Name' })
    public name: string = '';

    @meta({ displayName: 'Description' })
    public description: string = '';

    public static create(item: Partial<ICategory>): Category {
        if (typeof item === 'object' && Object.getPrototypeOf(item) !== Category.prototype) {
            Object.setPrototypeOf(item, Category.prototype);
        }
        return item as Category;
    }

    public static validateName = (item: Partial<ICategory>): MC.IValidationResult => {
        const { name, updatedAt } = item;

        // Ensure name exists as this is a required field.
        if (name === null || name === undefined || !name.length) {
            return { valid: false, message: 'Name is required.' };
        }
        
        // Name must be unique.
        const { app: { modules } } = Store.getState();
        const pluginState = modules && modules.get(FCSConstants.PLUGIN_ID) as IFCSReduxState;
        const items = pluginState && pluginState.catalog && pluginState.catalog.categories.values();
        const match = Linq2.firstOrDefault(items, undefined, i => i.name.toLowerCase() === name.toLowerCase());
        return { valid: match === undefined || (updatedAt === null ? false : match.updatedAt === updatedAt), message: 'Category name already exists.' };
    }

    public static validateCategoryCode = (item: Partial<ICategory>): MC.IValidationResult => {
        const { categoryCode, updatedAt } = item;

        // Ensure category code is not blank as it's a required field
        if (categoryCode === null || categoryCode === undefined || !categoryCode. length) {
            return { valid: false, message: 'Category code is required.' };
        }

        // Category code must be unique.
        const { app: { modules } } = Store.getState();
        const pluginState = modules && modules.get(FCSConstants.PLUGIN_ID) as IFCSReduxState;
        const items = pluginState && pluginState.catalog && pluginState.catalog.categories;
        const match = items && items.get(categoryCode);
        const valid = match === undefined || (updatedAt === null ? false : match.updatedAt === updatedAt);
        if (!valid) {
            return { valid, message: 'Category code already exists.'};
        }

        // Finalle check if it passes the validation regex pattern.
        return Validators.isValidCode(categoryCode);
    }

    public static validate = (item: Partial<ICategory>): ICategoryValidation => ({
        name: Category.validateName(item),
        categoryCode: Category.validateCategoryCode(item)
    })
}
