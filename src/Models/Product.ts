import { Linq2 } from 'o8-common';
import { meta } from '../Decorators';
import Store from '../Services/Store';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import FCSConstants from '../Utils/FCSConstants';
import Validators from '../Utils/Validators';

export interface IProductValidation {
    name: MC.IValidationResult;
    productCode: MC.IValidationResult;
}

export default class Product implements IProduct {
    public static readonly TYPE: string = 'Product';
    public static readonly defaultProps: IProduct = Object.freeze({ ...new Product() });

    public updatedAt: string = null;

    @meta({ displayName: 'Product Group' })
    public productCode: string = '';

    @meta({ displayName: 'Name' })
    public name: string = '';

    @meta({ displayName: 'Description' })
    public description: string = '';

    @meta({ displayName: 'Category Code' })
    public categoryCode: string = '';

    public static create(item: Partial<IProduct>): Product {
        if (typeof item === 'object' && Object.getPrototypeOf(item) !== Product.prototype) {
            Object.setPrototypeOf(item, Product.prototype);
        }
        return item as Product;
    }

    public static validateName = (item: Partial<ICategory>): MC.IValidationResult => {
        const { name, updatedAt } = item;

        // Ensure name exists as this is a required field.
        if (name === null || name === undefined || !name.length) {
            return { valid: false, message: 'Name is required.' };
        }
        
        // Name must be unique.
        const { app: { modules } } = Store.getState();
        const pluginState = modules && modules.get(FCSConstants.PLUGIN_ID) as IFCSReduxState;
        const items = pluginState && pluginState.catalog && pluginState.catalog.products.values();
        const match = Linq2.firstOrDefault(items, undefined, i => i.name.toLowerCase() === name.toLowerCase());
        return { valid: match === undefined || (updatedAt === null ? false : match.updatedAt === updatedAt), message: 'Product name already exists.' };
    }

    public static validateProductCode = (item: Partial<IProduct>): MC.IValidationResult => {
        const { productCode, updatedAt } = item;

        // Ensure category code is not blank as it's a required field
        if (productCode === null || productCode === undefined || !productCode. length) {
            return { valid: false, message: 'Product group is required.' };
        }

        // Category code must be unique.
        const { app: { modules } } = Store.getState();
        const pluginState = modules && modules.get(FCSConstants.PLUGIN_ID) as IFCSReduxState;
        const items = pluginState && pluginState.catalog && pluginState.catalog.products;
        const match = items && items.get(productCode);
        const valid = match === undefined || (updatedAt === null ? false : match.updatedAt === updatedAt);
        if (!valid) {
            return { valid, message: 'Product group already exists.'};
        }

        // Finally check if it passes the validation regex pattern.
        return Validators.isValidCode(productCode);
    }

    public static validate = (item: Partial<IProduct>): IProductValidation => ({
        name: Product.validateName(item),
        productCode: Product.validateProductCode(item)
    })
}
