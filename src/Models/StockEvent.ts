import { Linq2 } from 'o8-common';
import meta from '../Decorators/meta';
import Store from '../Services/Store';
import { EventCodeEnum } from '../Typings/EventCodeEnum';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import { IStockEvent } from '../Typings/IStockEvent';
import FCSConstants from '../Utils/FCSConstants';
import Validators from '../Utils/Validators';

export interface IStockEventValidation {
    fromStorageCode: MC.IValidationResult;
    toStorageCode?: MC.IValidationResult;
    sku: MC.IValidationResult;
    eventCode: MC.IValidationResult;
}

export default class StockEvent implements IStockEvent {
    public static readonly TYPE: string = 'StockEvent';
    public static readonly defaultProps: IStockEvent = Object.freeze({ ...new StockEvent() });

    @meta({ displayName: 'From' })
    public fromStorageCode: string = null;

    @meta({ displayName: 'To' })
    public toStorageCode: string = null;

    @meta({ displayName: 'SKU' })
    public sku: string = null;

    @meta({ displayName: 'Reason' })
    public eventCode: EventCodeEnum = null;

    @meta({ displayName: 'Created At' })
    public createdAt: string = null;

    @meta({ displayName: 'Quantity' })
    public quantity: number = 0;

    public static create(item: Partial<IStockEvent>): StockEvent {
        if (typeof item === 'object' && Object.getPrototypeOf(item) !== StockEvent.prototype) {
            Object.setPrototypeOf(item, StockEvent.prototype);
        }
        return item as StockEvent;
    }

    public static validateStorageCode = (storageCode: string) => {
        const required = Validators.isRequired(storageCode);
        if (!required.valid) {
            return required;
        }

        const { app: { modules } } = Store.getState();
        const pluginState = modules && modules.get(FCSConstants.PLUGIN_ID) as IFCSReduxState;
        const storage = pluginState && pluginState.logistics && pluginState.logistics.storage;
        return { valid: storage.has(storageCode), message: 'Please enter a valid storage code' };
    }

    public static validateSku = (sku: string) => {
        const required = Validators.isRequired(sku);
        if (!required.valid) {
            return required;
        }

        const { app: { modules } } = Store.getState();
        const pluginState = modules && modules.get(FCSConstants.PLUGIN_ID) as IFCSReduxState;
        const productItems = pluginState && pluginState.logistics && pluginState.catalog.items;
        const hasMatch = Linq2.firstOrDefault(productItems.values(), undefined, i => i.sku === sku);
        return { valid: Boolean(hasMatch), message: 'Please enter an existing SKU' };
    }

    public static validateEventCode = (eventCode: string) => {
        const required = Validators.isRequired(eventCode);
        if (!required.valid) {
            return required;
        }

        const hasMatch = Linq2.firstOrDefault(Object.values(EventCodeEnum), undefined, v => eventCode === v);
        return { valid: Boolean(hasMatch), message: 'Please enter a valid reason' };
    }

    public static validate = (stockEvent: IStockEvent): IStockEventValidation => {
        const { toStorageCode, fromStorageCode, sku, eventCode, quantity } = stockEvent;
        return {
            fromStorageCode: StockEvent.validateStorageCode(fromStorageCode),
            toStorageCode: eventCode === EventCodeEnum.MOVE ? StockEvent.validateStorageCode(toStorageCode) : { valid: true, message: '' },
            sku: StockEvent.validateSku(sku),
            eventCode: StockEvent.validateEventCode(eventCode)
        };
    }
}
