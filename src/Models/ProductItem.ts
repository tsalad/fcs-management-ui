import { Util, Validators as MCValidators } from 'o8-common';
import meta from '../Decorators/meta';
import queryBuilder from '../Decorators/queryBuilder';
import Store from '../Services/Store';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import FCSConstants from '../Utils/FCSConstants';
import Validators from '../Utils/Validators';

export interface IProductItemValidation {
    name: MC.IValidationResult;
    productCode: MC.IValidationResult;
    sku: MC.IValidationResult;
    reorderThreshold: MC.IValidationResult;
    weightOunces: MC.IValidationResult;
}

@queryBuilder({ fields: [ 'categoryCode', 'productCode', 'sku', 'name' ] })
export default class ProductItem implements IProductItem {
    public static readonly TYPE: string = 'ProductItem';
    public static readonly defaultProps: IProductItem = Object.freeze({ ...new ProductItem() });
    
    public updatedAt: string = null;
    
    @meta({displayName: 'Category'})
    public categoryCode: string =  null;

    @meta({displayName: 'Product Group'})
    public productCode: string = null;

    @meta({displayName: 'SKU'})
    public sku: string = '';

    public gtin: string = null;

    @meta({displayName: 'Reorder Level'})
    public reorderThreshold: number = 0;

    @meta({displayName: 'Image URL'})
    public imageUri: string = null;

    @meta({displayName: 'Name'})
    public name: string = '';

    @meta({displayName: 'Description'})
    public description: string = '';

    @meta({displayName: 'Item Weight'})
    public weightOunces: number = null;

    @meta({ displayName: 'Suppliers' })
    public suppliers: IProductItemSupplier[] = [];

    public static create(item: Partial<IProductItem>): ProductItem {
        if (typeof item === 'object' && Object.getPrototypeOf(item) !== ProductItem.prototype) {
            Object.setPrototypeOf(item, ProductItem.prototype);
        }
        return item as ProductItem;
    }

    public static validateName = (item: Partial<IProductItem>) => {
        const { name, sku } = item;
        const { app: { modules } } = Store.getState();
        const pluginState = modules && modules.get(FCSConstants.PLUGIN_ID) as IFCSReduxState;
        const items = pluginState && pluginState.catalog && [ ...pluginState.catalog.items.values() ];
        // Check if there is a name collision apart from the existing product item that we're editing.
        if (Util.isNonEmptyArray(items) && items.some(i => (i.sku !== sku) && (i.name !== undefined && i.name.toLowerCase() === name.toLocaleLowerCase()))) {
            return { valid: false, message: 'Name must be unique.' };
        }
        // Ensure that name has a value.
        return { valid: (name !== undefined && name !== null && name.length > 0), message: 'Name is required.' };
    }
    
    public static validateProductCode = (productCode: string) => { 
        // Ensure product code is not empty.
        return { valid: (productCode !== undefined && productCode !== null && productCode.length > 0), message: 'Product Code is required.' };
    }

    public static validateSku = (item: IProductItem) => {
        const { sku, updatedAt } = item;
        const { app: { modules } } = Store.getState();
        const pluginState = modules && modules.get(FCSConstants.PLUGIN_ID) as IFCSReduxState;
        const items = pluginState && pluginState.catalog && pluginState.catalog.items;
        // If this is an existing product item, we'll assume it's valid since it's immutable.
        if (Boolean(updatedAt)) {
            return { valid: true, message: '' };
        }
        // Ensure SKU is not empty
        if (sku === undefined || !sku.length) {
            return { valid: false, message: 'SKU is required.' };
        }
        // Ensure we don't have any collisions, sku's must be unique.
        const match = items && items.get(sku);
        const valid = match === undefined || (updatedAt === null ? false : match.updatedAt === updatedAt);
        if (!valid) {
            return { valid, message: 'SKU must be unique.' };
        }
        // Ensure that the sku passes the validation pattern.
        return Validators.isValidSKU(sku);
    }

    public static validateReorderThreshold = (reorderThreshold: number): MC.IValidationResult => {
        // Ensure reorder threshold is a positive integer.
        return MCValidators.isPositiveInteger(String(reorderThreshold));
    }

    public static validateWeightOunces = (weight: number): MC.IValidationResult => {
        return { valid: Boolean(weight) && weight > 0, message: 'Must be a positive number' };
    }

    public static validate = (item: IProductItem): IProductItemValidation => ({
        name: ProductItem.validateName(item),
        productCode: ProductItem.validateProductCode(item.productCode),
        sku: ProductItem.validateSku(item),
        reorderThreshold: ProductItem.validateReorderThreshold(item.reorderThreshold),
        weightOunces: ProductItem.validateWeightOunces(item.weightOunces)
    })
}
