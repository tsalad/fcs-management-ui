import config from '..';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import FCSConstants from '../Utils/FCSConstants';

export const types: string[] = [
    FCSConstants.FETCH_DISPATCHES_SUCCESS,
    FCSConstants.CREATE_DISPATCH_SUCCESS,
    FCSConstants.UPDATE_DISPATCH_SUCCESS,
    FCSConstants.DELETE_DISPATCH_SUCCESS,
    FCSConstants.FETCH_ORDERS_FLAGGED_SUCCESS,
    FCSConstants.FETCH_RELEASES_SUCCESS,
    FCSConstants.FETCH_ACTIVE_DISPATCH_STATUS_SUCCESS
];

export default function(state: IFCSReduxState = config, action: Redux.AnyAction): IFCSReduxState {
    const { fulfillment } = state;
    const dispatches = new Map(fulfillment.dispatches);
    switch (action.type) {
        case FCSConstants.FETCH_DISPATCHES_SUCCESS:
            return {
                ...state,
                fulfillment: {
                    ...state.fulfillment,
                    dispatches: action.payload
                }
            };
        case FCSConstants.CREATE_DISPATCH_SUCCESS:
            dispatches.set(action.payload.dispatchCode, action.payload);
            return {
                    ...state,
                    fulfillment: {
                        ...state.fulfillment,
                        dispatches
                    }
                };
        case FCSConstants.UPDATE_DISPATCH_SUCCESS:
            dispatches.set(action.payload.dispatchCode, action.payload);
            return {
                ...state,
                fulfillment: {
                    ...state.fulfillment,
                    dispatches
                }
            };
        case FCSConstants.DELETE_DISPATCH_SUCCESS:
            dispatches.delete(action.payload.dispatchCode);
            return {
                ...state,
                fulfillment: {
                    ...state.fulfillment,
                    dispatches
                }
            };
        case FCSConstants.FETCH_ORDERS_FLAGGED_SUCCESS:
            return {
                ...state,
                fulfillment: {
                    ...state.fulfillment,
                    ordersFlagged: action.payload
                }
            };
        case FCSConstants.FETCH_RELEASES_SUCCESS:
            return {
                ...state,
                fulfillment: {
                    ...state.fulfillment,
                    nonArchivedReleases: action.payload
                }
            };
        case FCSConstants.FETCH_ACTIVE_DISPATCH_STATUS_SUCCESS:
            return {
                ...state,
                fulfillment: {
                    ...state.fulfillment,
                    activeDispatchStatus: action.payload
                }
            };
    }
    return state;
}
