import { Util } from 'o8-common';
import config from '..';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import FCSConstants from '../Utils/FCSConstants';

export const types: string[] = [
    FCSConstants.SET_CLIENT_MENUS
];

export default function(state: IFCSReduxState = config, action: Redux.AnyAction): IFCSReduxState {
    const { menus } = state;
    switch (action.type) {
        case FCSConstants.SET_CLIENT_MENUS:
            if (Util.isNonEmptyArray(action.payload)) {
                const ordersMenu = menus.find(m => m.menuId === FCSConstants.MENU_ORDERS);
                ordersMenu.menus = [ ...action.payload ];
                ordersMenu.menus[0].aliases = [ FCSConstants.MENU_ORDERS ];
                return { ...state, menus: [ ...menus ] };
            }
    }
    return state;
}
