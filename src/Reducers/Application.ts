import config from '..';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import FCSConstants from '../Utils/FCSConstants';

export const types: string[] = [
    FCSConstants.SET_SNACKBAR_MESSAGE,
    FCSConstants.CLEAR_SNACKBAR_MESSAGE,
    FCSConstants.MODAL_MANIFEST_IMPORT_TOGGLE
];

export default function(state: IFCSReduxState = config, action: Redux.AnyAction): IFCSReduxState {
    const messages = new Map(state.application.messages);

    switch (action.type) {
        case FCSConstants.SET_SNACKBAR_MESSAGE:
            messages.set(action.payload.id, action.payload);
            return {
                ...state,
                application: {
                    ...state.application,
                    messages
                }
            };
        case FCSConstants.CLEAR_SNACKBAR_MESSAGE:
            messages.delete(action.payload);
            return {
                ...state,
                application: {
                    ...state.application,
                    messages
                }
            };
        case FCSConstants.MODAL_MANIFEST_IMPORT_TOGGLE:
            return {
                ...state,
                application: {
                    ...state.application,
                    showModalManifestImport: action.payload
                }
            };
        
    }
    return state;
}
