import { Constants, Util } from 'o8-common';
import config from '..';
import { fetchEnvironment } from '../Actions/Environment';
import { fetchClientList } from '../Actions/Logistics';
import NotificationsService from '../Services/NotificationsService';
import Store from '../Services/Store';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import FCSConstants from '../Utils/FCSConstants';
import ChildReducers from './ChildReducers';

export default function(state: IFCSReduxState = config, action: Redux.AnyAction): IFCSReduxState {
    switch (action.type) {
        case Constants.MODULE_LOADED:
            Store.setStore(action.store);
            break;
        case Constants.MODULE_SELECTED:
            if (action.id === config.id) {
                window.setTimeout(() => action.store.dispatch(fetchEnvironment() as any), 0);
                window.setTimeout(() => action.store.dispatch(fetchClientList() as any), 0);
                NotificationsService.start();
            } else {
                NotificationsService.stop();
            }
            break;
        case Constants.MENU_ITEM_CLICKED:
            switch (action.menuId) {
                case FCSConstants.MENU_PRODUCTS:
                case FCSConstants.MENU_MANAGE:
                case FCSConstants.MENU_CLIENTS:
                case FCSConstants.MENU_LOGISTICS:
                case FCSConstants.MENU_NOTIFICATIONS:
                case FCSConstants.MENU_SUPERVISOR_DASHBOARD:
                    return { ...state, previousMenu: action.menuId };
                case FCSConstants.MENU_ACCOUNTS:
                    const authorizationUri = Boolean(state.environment) && Boolean(state.environment.environment) && state.environment.environment[FCSConstants.authorizationUri];
                    if (Boolean(authorizationUri)) {
                        const url = new URL(authorizationUri);
                        window.open(url.origin, '_blank');
                    }
                    return { ...state, currentMenu: state.previousMenu };
                case FCSConstants.MENU_IMPORT_MANIFEST:
                    return { ...state, currentMenu: state.previousMenu, application: { ...state.application, showModalManifestImport: true } };
                case FCSConstants.MENU_ORDERS:
                default: {
                    const ordersMenu = state.menus.find(m => m.menuId === FCSConstants.MENU_ORDERS);
                    const currentMenu = Util.isNonEmptyArray(ordersMenu.menus) ? ordersMenu.menus[0].menuId : FCSConstants.MENU_ORDERS;
                    return { ...state, selectedClient: action.menuId === FCSConstants.MENU_ORDERS ? currentMenu : action.menuId, previousMenu: action.menuId };
                }
            }
    }

    if (ChildReducers.has(action.type)) {
        return ChildReducers.get(action.type).reduce((acc, reducer) => ({ ...acc, ...reducer(acc, action) }), state);
    } else {
        return state;
    }
}
