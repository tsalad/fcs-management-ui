import { Util } from 'o8-common';
import config from '..';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import FCSConstants from '../Utils/FCSConstants';

export const types: string[] = [
    FCSConstants.FETCH_PRODUCTS_REORDER_ALERT_SUCCESS,
    FCSConstants.FETCH_PROBLEM_REPORTS_SUCCESS
];

export default function(state: IFCSReduxState = config, action: Redux.AnyAction): IFCSReduxState {
    switch (action.type) {
        case FCSConstants.FETCH_PRODUCTS_REORDER_ALERT_SUCCESS:
            return {
                ...state,
                notifications: {
                    ...state.notifications,
                    reorderAlerts: action.payload
                }
            };
        case FCSConstants.FETCH_PROBLEM_REPORTS_SUCCESS:
            return {
                ...state,
                notifications: {
                    ...state.notifications,
                    problemReports: action.payload
                }
            };
    }
    return state;
}
