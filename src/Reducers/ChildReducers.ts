import { IFCSReduxState } from '../Typings/IFCSReduxState';
import Application, { types as ApplicationTypes } from './Application';
import Products, { types as ProductsTypes } from './Catalog';
import Environment, { types as EnvironmentTypes } from './Environment';
import Orders, { types as OrdersTypes } from './Fulfillment';
import Logistics, { types as LogisticsTypes } from './Logistics';
import Notifications, { types as NotificationsTypes } from './Notifications';
import Parent, { types as ParentTypes } from './Parent';

const typesMap: Map<string, Redux.Reducer<IFCSReduxState>[]> = new Map();
function addReducer(type: string, reducer: Redux.Reducer<IFCSReduxState>): void {
    if (!typesMap.has(type)) {
        typesMap.set(type, [ reducer ]);
    } else {
        typesMap.get(type).push(reducer);
    }
}

LogisticsTypes.forEach(type => addReducer(type, Logistics));
OrdersTypes.forEach(type => addReducer(type, Orders));
ParentTypes.forEach(type => addReducer(type, Parent));
ProductsTypes.forEach(type => addReducer(type, Products));
NotificationsTypes.forEach(type => addReducer(type, Notifications));
EnvironmentTypes.forEach(type => addReducer(type, Environment));
ApplicationTypes.forEach(type => addReducer(type, Application));

export default typesMap;
