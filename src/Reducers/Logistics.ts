import { Linq2, Util } from 'o8-common';
import config from '..';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import FCSConstants from '../Utils/FCSConstants';

export const types: string[] = [
    FCSConstants.FETCH_CLIENTS_SUCCESS,
    FCSConstants.FETCH_DEPOTS_SUCCESS,
    FCSConstants.FETCH_STORAGE_SUCCESS,
    FCSConstants.CREATE_DEPOT_SUCCESS,
    FCSConstants.UPDATE_DEPOT_SUCCESS,
    FCSConstants.DELETE_DEPOT_SUCCESS,
    FCSConstants.FETCH_STOCK_SUCCESS,
    FCSConstants.CREATE_STOCK_SUCCESS,
    FCSConstants.FETCH_EVENTS_SUCCESS,
    FCSConstants.FETCH_PRODUCTION_LINES_SUCCESS,
    FCSConstants.STOCK_MOVE_SUCCESS,
    FCSConstants.FETCH_SUPPLIERS_SUCCESS,
    FCSConstants.CREATE_SUPPLIER_SUCCESS,
    FCSConstants.UPDATE_SUPPLIER_SUCCESS,
    FCSConstants.DELETE_SUPPLIER_SUCCESS,
    FCSConstants.CREATE_CLIENT_SUCCESS,
    FCSConstants.UPDATE_CLIENT_SUCCESS,
    FCSConstants.FETCH_SHIPPING_METHODS_SUCCESS,
    FCSConstants.FETCH_WORKSTATION_SUCCESS,
    FCSConstants.DELETE_STORAGE_SUCCESS,
    FCSConstants.CREATE_STORAGE_SUCCESS
];

export default function(state: IFCSReduxState = config, action: Redux.AnyAction): IFCSReduxState {
    const depots = new Map(state.logistics.depots);
    const stock = new Map(state.logistics.stock);
    const events = new Map(state.logistics.events);
    const suppliers = new Map(state.logistics.suppliers);
    const clients = new Map(state.logistics.clients);
    const storage = new Map(state.logistics.storage);

    switch (action.type) {
        case FCSConstants.FETCH_CLIENTS_SUCCESS:
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    clients: action.payload
                }
            };
        case FCSConstants.FETCH_DEPOTS_SUCCESS:
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    depots: action.payload
                }
            };
        case FCSConstants.FETCH_STORAGE_SUCCESS:
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    storage: action.payload
                }
            };
        case FCSConstants.CREATE_DEPOT_SUCCESS:
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    depots: depots.set(action.payload.depotCode, action.payload)
                }
            };
        case FCSConstants.UPDATE_DEPOT_SUCCESS:
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    depots: depots.set(action.payload.depotCode, action.payload)
                }
            };
        case FCSConstants.DELETE_DEPOT_SUCCESS:
            depots.delete(action.payload);
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    depots
                }
            };
        case FCSConstants.FETCH_STOCK_SUCCESS:
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    stock: action.payload
                }
            };
        case FCSConstants.CREATE_STOCK_SUCCESS:
            const currentStock = stock.get(action.payload.sku) || [];
            const updatedStockList = Linq2.where(currentStock, c => c.storageCode !== action.payload.storageCode).append(action.payload).toArray();
            stock.set(action.payload.sku, updatedStockList);
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    stock
                }
            };
        case FCSConstants.FETCH_EVENTS_SUCCESS:
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    events: action.payload
                }
            };
        case FCSConstants.FETCH_PRODUCTION_LINES_SUCCESS:
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    productionLines: action.payload
                }
            };
        case FCSConstants.STOCK_MOVE_SUCCESS:
            const { from, to, event } = action.payload;
            const { sku } = event;
            const updatedEvents = events.has(sku) ? [ ...events.get(sku), event ] : [ event ];
            const updated = Linq2.where(stock.get(sku), s => (s.storageCode !== from.storageCode && s.storageCode !== to.storageCode))
                                 .append(from)
                                 .append(to)
                                 .toArray();
            events.set(sku, updatedEvents);
            stock.set(sku, updated);
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    stock,
                    events
                }
            };
        case FCSConstants.FETCH_SUPPLIERS_SUCCESS:
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    suppliers: action.payload
                }
            };
        case FCSConstants.CREATE_SUPPLIER_SUCCESS:
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    suppliers: suppliers.set(action.payload.supplierCode, action.payload)
                }
            };
        case FCSConstants.UPDATE_SUPPLIER_SUCCESS:
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    suppliers: suppliers.set(action.payload.supplierCode, action.payload)
                }
            };
        case FCSConstants.DELETE_SUPPLIER_SUCCESS:
            suppliers.delete(action.payload);
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    suppliers
                }
            };
        case FCSConstants.CREATE_CLIENT_SUCCESS:
            clients.set(action.payload.clientCode, action.payload);
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    clients
                }
            };
        case FCSConstants.UPDATE_CLIENT_SUCCESS:
            clients.set(action.payload.clientCode, action.payload);
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    clients
                }
            };
        case FCSConstants.FETCH_SHIPPING_METHODS_SUCCESS:
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    shippingMethods: action.payload
                }
            };
        case FCSConstants.FETCH_WORKSTATION_SUCCESS:
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    workstations: action.payload
                }
            };
        case FCSConstants.DELETE_STORAGE_SUCCESS:
            storage.delete(action.payload);
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    storage
                }
            };
        case FCSConstants.CREATE_STORAGE_SUCCESS:
            storage.set(action.payload.storageCode, action.payload);
            return {
                ...state,
                logistics: {
                    ...state.logistics,
                    storage
                }
            };

    }
    return state;
}
