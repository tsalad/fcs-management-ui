import config from '..';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import FCSConstants from '../Utils/FCSConstants';

export const types: string[] = [
    FCSConstants.CREATE_PRODUCT_SUCCESS,
    FCSConstants.CREATE_CATEGORY_SUCCESS,
    FCSConstants.CREATE_PRODUCT_ITEM_SUCCESS,
    FCSConstants.CREATE_TAG_LABEL_SUCCESS,
    FCSConstants.FETCH_ITEMS_SUCCESS,
    FCSConstants.FETCH_CATEGORIES_SUCCESS,
    FCSConstants.FETCH_PRODUCTS_SUCCESS,
    FCSConstants.FETCH_PRODUCT_TAGS_SUCCESS,
    FCSConstants.UPDATE_PRODUCT_ITEM_SUCCESS
];

export default function(state: IFCSReduxState = config, action: Redux.AnyAction): IFCSReduxState {
    const { items, products, categories } = state.catalog;
    switch (action.type) {
        case FCSConstants.FETCH_ITEMS_SUCCESS:
            return {
                ...state,
                catalog: {
                    ...state.catalog,
                    items: action.payload
                }
            };
        case FCSConstants.FETCH_CATEGORIES_SUCCESS:
            return {
                ...state,
                catalog: {
                    ...state.catalog,
                    categories: action.payload
                }
            };
        case FCSConstants.FETCH_PRODUCTS_SUCCESS:
            return {
                ...state,
                catalog: {
                    ...state.catalog,
                    products: action.payload
                }
            };
        case FCSConstants.CREATE_PRODUCT_SUCCESS:
            products.set(action.payload.productCode, action.payload);
            return {
                ...state,
                catalog: {
                    ...state.catalog,
                    products
                }
            };
        case FCSConstants.CREATE_CATEGORY_SUCCESS:
            categories.set(action.payload.categoryCode, action.payload);
            return {
                ...state,
                catalog: {
                    ...state.catalog,
                    categories
                }
            };
        case FCSConstants.CREATE_PRODUCT_ITEM_SUCCESS:
            items.set(action.payload.sku, action.payload);
            return {
                ...state,
                catalog: {
                    ...state.catalog,
                    items
                }
            };
        case FCSConstants.UPDATE_PRODUCT_ITEM_SUCCESS:
            items.set(action.payload.sku, action.payload);
            return {
                ...state,
                catalog: {
                    ...state.catalog,
                    items 
                }
            };
        case FCSConstants.FETCH_PRODUCT_TAGS_SUCCESS:
            return {
                ...state,
                catalog: {
                    ...state.catalog,
                    tags: action.payload
                }
            };
        case FCSConstants.CREATE_TAG_LABEL_SUCCESS:
            const { tags } = state.catalog;
            tags.set(action.payload.label, action.payload);
            return {
                ...state,
                catalog: {
                    ...state.catalog,
                    tags
                }
            };
    }
    return state;
}
