import config from '..';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import FCSConstants from '../Utils/FCSConstants';

export const types: string[] = [
    FCSConstants.FETCH_ENVIRONMENT_SUCCESS
];

export default function(state: IFCSReduxState = config, action: Redux.AnyAction): IFCSReduxState {
    switch (action.type) {
        case FCSConstants.FETCH_ENVIRONMENT_SUCCESS:
            return {
                ...state,
                environment: action.payload
            };
    }
    return state;
}
