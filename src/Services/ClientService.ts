import { Api } from 'o8-common';
import FCSConstants from '../Utils/FCSConstants';
import ConfigurationManager from './ConfigurationManager';

export default class ClientService {
    /**
     * @param {String} key the clientCode that we want to fetch shipping methods for
     * @returns {Promise<IClientShippingMethod[]>} a promise containing the existing shipping methods array
     */
    public static fetchClientShippingMethods = async (key: string): Promise<IClientShippingMethod[]> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/logistics/client/${key}/shipping`);
            return ((await Api.get(url)).body).methods;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {String} key the clientCode that we want to remove shipping method from
     * @param {String} serviceCode the service code of the shipping method we want to remove
     */
    public static removeClientShippingMethod = async (key: string, serviceCode: string) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/logistics/client/${key}/shipping/${serviceCode}`);
            await Api.delete(url);
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {String} key the clientcode that we want to add shipping method to
     * @param {IClientShippingMethod} shippingMethod the shipping method we want to add to the client
     * @returns {IClientShippingMethod} the newly linked shipping method
     */
    public static addClientShippingMethod = async (key: string, { method: { serviceCode }, preferencePriority, expedited }: IClientShippingMethod) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/logistics/client/${key}/shipping`);
            return (await Api.post(url, { serviceCode, preferencePriority, expedited })).body;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {String} key the clientCode we want to update shipping method for
     * @param {IClientShippingMethod} shippingMethod the shipping method we want to update
     * @returns {IClientShippingMethod} the updated shipping method
     */
    public static updateClientShippingMethod = async (key: string, { method: { serviceCode }, preferencePriority, expedited, updatedAt }: IClientShippingMethod) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/logistics/client/${key}/shipping/${serviceCode}`);
            return (await Api.put(url, { preferencePriority, expedited, updatedAt }));
        } catch (err) {
            throw new Error(err);
        }
    }
}
