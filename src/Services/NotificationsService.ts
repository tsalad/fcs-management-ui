import { Api, Linq2, Util } from 'o8-common';
import { fetchItems } from '../Actions/Catalog';
import { fetchStock } from '../Actions/Logistics';
import Store from '../Services/Store';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import { IProblemReport } from '../Typings/IProblemReport';
import FCSConstants from '../Utils/FCSConstants';
import ConfigurationManager from './ConfigurationManager';

export default class NotificationsService {
    private static timer: number;

    public static start = () => {
        NotificationsService.updateNotifications().then();
        window.clearInterval(NotificationsService.timer);
        NotificationsService.timer = window.setInterval(NotificationsService.updateNotifications, 60000);
    }

    public static stop = () => {
        window.clearInterval(NotificationsService.timer);
    }

    public static updateNotifications = async () => {
        await Store.dispatch(fetchItems() as any);
        await Store.dispatch(fetchStock() as any);

        try {
            const payload = NotificationsService.fetchProductsReorderAlert();
            Store.dispatch({ type: FCSConstants.FETCH_PRODUCTS_REORDER_ALERT_SUCCESS, payload });
        } catch (err) {
            Store.dispatch({ type: FCSConstants.FETCH_PRODUCTS_REORDER_ALERT_FAILURE });
        }

        try {
            const payload = await NotificationsService.fetchProblemReports();
            Store.dispatch({ type: FCSConstants.FETCH_PROBLEM_REPORTS_SUCCESS, payload });
        } catch (err) {
            Store.dispatch({ type: FCSConstants.FETCH_PROBLEM_REPORTS_FAILURE });
        }
    }

    /**
     * @returns {IProductItem[]} a list of product items whose reoder threshold is greater than current stock
     */
    public static fetchProductsReorderAlert = (): Map<string, IProductItem> => {
        const { app: { modules } } = Store.getState();
        const pluginState = modules && modules.get(FCSConstants.PLUGIN_ID) as IFCSReduxState;
        const products = pluginState && pluginState.catalog && pluginState.catalog.items.values();
        const stock = pluginState && pluginState.catalog && pluginState.logistics.stock;
        return Linq2.where(products, p => {
            const productStock = stock.get(p.sku);
            const total = Util.isNonEmptyArray(productStock) ? Linq2.aggregate(productStock, (acc: number = 0, curr) => acc += curr.quantity) : 0;
            return p.reorderThreshold && p.reorderThreshold > total;
        }).toDictionary(p => p.sku);
    }

    /**
     * @returns {Map<string, IProblemReport>} a list of problem reports
     */
    public static fetchProblemReports = async (): Promise<Map<string, IProblemReport>> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/logistics/problemreport');
            const problemReports: IProblemReport[] = ((await Api.get(url)).body).problemReports;
            return Linq2.toDictionary(problemReports, r => r.id);
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {String} key the id of the problem report that we want to close
     * @returns {Promise<IProblemReport>} a promise containing the problem report that was closed
     */
    public static resolveProblemReport = async (key: string): Promise<IProblemReport> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/logistics/problemreport/${key}/close`);
            return (await Api.put(url)).body;
        } catch (err) {
            throw new Error(err);
        }
    }
}
