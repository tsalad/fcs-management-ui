import { Api } from 'o8-common';
import ConfigurationManager from './ConfigurationManager';

export default class DemoService {
    /**
     * @param {File} file the .csv file that we want to upload
     * @returns {Promise<number>} a promise containing the number of orders that were successfully created
     */
    public static upload = async (file: File): Promise<number> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/fulfillment/demo/upload');
            const body = new FormData();
            body.append('file', file || new Blob([ null ], { type: 'multipart/form-data' }));
            const { createdOrders } = (await Api.post(url, body)).body;
            return createdOrders;
        } catch (err) {
            throw new Error(err);
        }
    }
}
