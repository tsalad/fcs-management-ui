import { Api } from 'o8-common';
import { ShippingProviderEnum } from '../Typings/ShippingProviderEnum';
import ConfigurationManager from './ConfigurationManager';

export default class Shipping {
    /**
     * @param {String} trackingNumber the tracking number for the shipment
     * @param {ShippingProviderEnum} provider the shipping provider that applies to this dispatch
     * @returns {File} an image file for the shipping label
     */
    public static fetchShippingLabel = async (trackingNumber: string, provider: ShippingProviderEnum = ShippingProviderEnum.UPS): Promise<string> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/shipping/${provider}/label/${trackingNumber}`);
            return (await Api.get(url)).body;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @returns {IShippingMethod[]} a list of existing shipping methods
     */
    public static fetchShippingMethods = async (): Promise<IShippingMethod[]> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/logistics/carrier/method');
            return ((await Api.get(url)).body).methods;
        } catch (err) {
            throw new Error(err);
        }
    }
}
