export default class Utilities {
    public static downloadFile(url: string): void {
        const a = document.createElement('a') as HTMLAnchorElement;
        a.href = url;
        a.style.display = 'none';
        document.body.appendChild(a);
        a.click();
        a.remove();
    }
}
