import { Api, Linq2, Util } from 'o8-common';
import { IDispatch } from '../Typings/IDispatch';
import { StateEnum } from '../Typings/StateEnum';
import ConfigurationManager from './ConfigurationManager';

export default class DispatchService {
    /**
     * @param {String} key the key for the dispatch that we want to fetch
     * @returns {IDispatch} the dispatch that matches the key supplied
     */
    public static fetchDispatch = async (key: string): Promise<IDispatch> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/dispatch/${key}`);
            return (await Api.get(url)).body;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {IDispatchInput} dispatchInput the dispatch data to be created
     * @returns {}
     */
    public static createDispatch = async (dispatchInput: IDispatchInput) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/fulfillment/dispatch/submitAsyncDispatch');
            return ((await Api.post(url, dispatchInput)).body);
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @returns {IDispatch} the dispatch that matches the key supplied
     */
    public static fetchDispatches = async (): Promise<Map<string, IDispatch>> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/dispatch?shipments=${true}&groups=${true}`);
            const { dispatches }: { dispatches: IDispatch[] } = (await Api.get(url)).body;
            return Linq2.toDictionary(dispatches, d => d.dispatchCode);
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @returns {Map<string, IDispatch>} the dispatches that are currently in FULFILLMENT state
     */
    public static fetchDispatchesInFulfillment = async (): Promise<Map<string, IDispatch>> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/dispatch?state=${StateEnum.FULFILLMENT}&shipments=${true}&groups=${true}`);
            const { dispatches }: { dispatches: IDispatch[] } = (await Api.get(url)).body;
            return Linq2.toDictionary(dispatches, d => d.dispatchCode);
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {String} startCompletedAt the date time to filter dispatches by
     * @param {String} endCompletedAt the date time to filter dispatches by
     * @returns {Map<string, IDispatch>} completed dispatches that fall within the date range
     */
    public static fetchCompletedDispatches = async (startCopmpletedAt: string, endCompletedAt: string): Promise<Map<string, IDispatch>> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/dispatch?shipments=${true}&groups=${true}&startCompletedAt=${startCopmpletedAt}&endCompletedAt=${endCompletedAt}`);
            const { dispatches }: { dispatches: IDispatch[] } = (await Api.get(url)).body;
            return Linq2.toDictionary(dispatches, d => d.dispatchCode);
        } catch (err) {
            throw new Error(err);
        }
    }
}
