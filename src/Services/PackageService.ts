import { Api } from 'o8-common';
import { IShipmentPackage } from '../Typings/IShipmentPackage';
import ConfigurationManager from './ConfigurationManager';

export default class PackageService {
    /**
     * @param {IShipmentPackage} shipmentPackage the updated shipment
     * @returns {Promise<IShipmentPackage>} a promise containing the updated package
     */
    public static updatePackage = async (shipmentPackage: IShipmentPackage): Promise<IShipmentPackage> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/package/${shipmentPackage.packageCode}`);
            return (await Api.put(url, shipmentPackage)).body;
        } catch (err) {
            throw new Error(err);
        }
    }
}
