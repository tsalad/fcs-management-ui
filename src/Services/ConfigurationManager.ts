import FCSConstants from '../Utils/FCSConstants';

export default class ConfigurationManager {
    private static porterUrl: string;
    private static printTaskUrl: string;
    private static environment: Record<string, any>;

    private static initializeEnvironmentPromise: Promise<void>;
    private static initializeSettingsPromise: Promise<void>;

    public static async initialize(): Promise<void> {
        await this.initializeEnvironment();
        await this.initializeSettings();
    }

    private static async initializeEnvironment(): Promise<void> {
        if (this.initializeEnvironmentPromise === undefined) {
            this.initializeEnvironmentPromise = new Promise(async resolve => {
                if (!Boolean(ConfigurationManager.environment)) {
                    try {
                        this.environment = await (await fetch('rest/v1.0/status/env')).json();
                    } catch (err) {
                        console.error(err);
                    }
                }
                resolve();
            });
        }
        return this.initializeEnvironmentPromise;
    }

    private static async initializeSettings(): Promise<void> {
        if (this.initializeSettingsPromise === undefined) {
            this.initializeSettingsPromise = new Promise(async resolve => {
                if (!ConfigurationManager.porterUrl || !ConfigurationManager.printTaskUrl) {
                    await this.initializeEnvironment();
                    const { environment } = this.environment;
                    const porterUri = Boolean(environment) && (environment[FCSConstants.porterUri] || environment[FCSConstants.porterUri2] || environment[FCSConstants.porterUri3]);
                    const printTaskUri = Boolean(environment) && (environment[FCSConstants.printUri] || environment[FCSConstants.printUri2] || environment[FCSConstants.porterUri3]);
                    this.porterUrl = Boolean(porterUri) ? porterUri : `http://${window.location.hostname}:9590`;
                    this.printTaskUrl = Boolean(printTaskUri) ? printTaskUri : `http://${window.location.hostname}:9592`; 
                }
                resolve();
            });
        }
        return this.initializeSettingsPromise;
    }

    public static async getPorterUrl() {
        await this.initializeSettings();
        return this.porterUrl;
    }

    public static async getPrintUrl() {
        await this.initializeSettings();
        return this.printTaskUrl;
    }

    public static async buildPorterUrl(endpoint: string) {
        const baseUrl = (await ConfigurationManager.getPorterUrl()).replace(/\/$/, '');
        return [ baseUrl, endpoint.replace(/^\//, '') ].join('/');
    }

    public static async buildPrintUrl(endpoint: string) {
        const baseUrl = (await ConfigurationManager.getPrintUrl()).replace(/\/$/, '');
        return [ baseUrl, endpoint.replace(/^\//, '') ].join('/');
    }

    public static async getEnvironment(): Promise<Record<string, any>> {
        await this.initializeEnvironment();
        return this.environment;
    }
}
