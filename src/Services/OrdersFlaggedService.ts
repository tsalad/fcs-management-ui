import { Api } from 'o8-common';
import { IOrderFlag } from '../Typings/IOrderFlag';
import ConfigurationManager from './ConfigurationManager';

export default class OrdersFlaggedService {
    /**
     * @returns {Map<string, IOrderFlag[]>} a dictionary of flagged orders by client id.
     */
    public static fetchOrdersFlagged = async (): Promise<IOrderFlag[]> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/fulfillment/orderflag');
            const { orderFlags } = (await Api.get(url)).body;
            return orderFlags;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {string} key the release item we want to remove
     */
    public static deleteFlag = async (key: string): Promise<void> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/orderflag/${key}`);
            return (await Api.delete(url)).body;
        } catch (err) {
            throw new Error(err);
        }
    }
}
