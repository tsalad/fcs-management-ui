import { Api, Linq2, Util } from 'o8-common';
import { IOrderFlag } from '../Typings/IOrderFlag';
import { IRelease } from '../Typings/IRelease';
import { StateEnum } from '../Typings/StateEnum';
import FCSConstants from '../Utils/FCSConstants';
import ConfigurationManager from './ConfigurationManager';

export default class ReleaseService {
    /**
     * @param {String} key the key for the release that we want to fetch
     * @returns {IReleaseItem} the release item that matches the key supplied
     */
    public static fetchReleaseItem = async (key: string): Promise<IReleaseItem[]> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/release/${key}/item?detail=true`);
            const { items } = (await Api.get(url)).body;
            return items;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {String} key the key for the release that we want to fetch
     * @returns {IRelease} the release item that matches the key supplied
     */
    public static fetchRelease = async (key: string): Promise<IRelease> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/release/${key}`);
            return (await Api.get(url)).body;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {String[]} state the list of states that we want to fetch releases for
     * @param {Boolean} items When true send the full release with items and assignments
     * @returns {IRelease[]} a list of releases in the specified state for the specified client
     */
    public static fetchReleases = async (state: string[], items: boolean = true): Promise<IRelease[]> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/release?state=${state}&items=${items}`);
            return ((await Api.get(url)).body).releases;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {String[]} state the list of states that we want to fetch releases for
     * @param {String} startShipBy the shipBy start date we want to filter by
     * @param {String} endShipBy the shipBy end date we want to filter by
     * @returns {IRelease[]} a list of releases in the specified state for the specified client
     */
    public static fetchReleasesByShipBy = async (state: string[], startShipBy: string, endShipBy: string): Promise<IRelease[]> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/release?state=${state}&bulk=false&startShipBy=${startShipBy}&endShipBy=${endShipBy}`);
            return ((await Api.get(url)).body).releases;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {String} key the order ID that we would like to delete
     * @returns {Promise<IRelease>} a promise containing the deleted release
     */
    public static deleteRelease = async (key: string): Promise<IRelease> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/release/${key}`);
            return (await Api.delete(url)).body;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {IRelease} release the new release to be created
     * @returns {IRelease} a promise containing the new release
     */
    public static createRelease = async (release: IRelease): Promise<IRelease> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/fulfillment/release');
            return (await Api.post(url, release)).body;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {IRelease} release the release to update
     * @returns {Promise<IRelease>} a promise containing the updated release
     */
    public static updateRelease = async (release: Partial<IRelease>): Promise<IRelease> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/release/${release.orderId}`);
            return (await Api.put(url, release)).body;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {IReleaseItem} releaseItem the release item we want to remove
     * @returns {Promise<IReleaseItem>} a promise containing the deleted release item
     */
    public static deleteReleaseItem = async (releaseItem: IReleaseItem): Promise<IReleaseItem> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/release/${releaseItem.orderId}/item/${releaseItem.sku}`);
            return (await Api.delete(url)).body;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {IReleaseItem} releaseItem the release item we want to create
     * @returns {Promise<IReleaseItem>} a promise containing the new release item
     */
    public static createReleaseItem = async (releaseItem: Partial<IReleaseItem>): Promise<IReleaseItem> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/release/${releaseItem.orderId}/item`);
            return (await Api.post(url, releaseItem)).body;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {IReleaseItem} releaseItem the release item we want to update
     * @returns {Promise<IReleaseItem>} a promise containing the updated release item
     */
    public static updateReleaseItem = async (releaseItem: Partial<IReleaseItem>): Promise<IReleaseItem> => {
        try {
            const { orderId, sku, quantity, updatedAt } = releaseItem;
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/release/${orderId}/item/${sku}`);
            return (await Api.put(url, { updatedAt, quantity })).body;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {IOrderFlag[]} orderFlags list of order flags we want to reject to DXP
     * @returns {Promise<void>} an empty promise
     */
    public static rejectOrders = async (orderFlags: IOrderFlag[]): Promise<void> => {
        if (Util.isNonEmptyArray(orderFlags)) {
            try {
                const orderReportList = Linq2.select(orderFlags, f => ({ orderId: f.orderId, description: f.status  })).toArray();
                const url = await ConfigurationManager.buildPorterUrl(`/dxp/order/reject`);
                await Api.post(url, { orderReportList });
            } catch (err) {
                throw new Error(err);
            }
        }
    }
}
