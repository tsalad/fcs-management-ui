import { Api } from 'o8-common';
import FCSConstants from '../Utils/FCSConstants';
import ConfigurationManager from './ConfigurationManager';

export default class LogisticsService {
    public static importManifest = async (sku: string, file: File): Promise<IManifest[]> => {
        try {
            const body = new FormData();
            body.append('sku', sku);
            body.append('file', file);

            const url = await ConfigurationManager.buildPorterUrl(`/logistics/manifest/upload`);
            return (await Api.post(url, body)).body.items;
        } catch (err) {
            throw new Error(err);
        }
    }
}
