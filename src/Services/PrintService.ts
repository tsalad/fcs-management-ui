import { Api, Linq2 } from 'o8-common';
import { IPrintImageInput } from '../Typings/IPrintImageInput';
import { ITemplate } from '../Typings/ITemplate';
import { PrintSideEnum } from '../Typings/PrintSideEnum';
import { TemplateTypeEnum } from '../Typings/TemplateTypeEnum';
import FCSConstants from '../Utils/FCSConstants';
import ConfigurationManager from './ConfigurationManager';

export default class PrintService {
    public static fetchTemplates = async (): Promise<Map<string, ITemplate>> => {
        try {
            const url = await ConfigurationManager.buildPrintUrl('/templates');
            const templates: ITemplate[] = (await Api.get(url)).body;
            return Linq2.toDictionary(templates, t => t.id);
        } catch (err) {
            throw new Error(err);
        }
    }

    public static fetchTrayTemplates = async (): Promise<Map<string, ITemplate>> => {
        try {
            const url = await ConfigurationManager.buildPrintUrl(`/templates?type=${TemplateTypeEnum.TRAY}`);
            const templates: ITemplate[] = (await Api.get(url)).body;
            return Linq2.toDictionary(templates, t => t.id);
        } catch (err) {
            throw new Error(err);
        }
    }

    public static fetchSlotTemplates = async (key: string): Promise<Map<string, ITemplate>> => {
        try {
            const url = await ConfigurationManager.buildPrintUrl(`/templates/${key}`);
            const templates: ITemplate[] = (await Api.get(url)).body;
            return Linq2.toDictionary(templates, t => t.id);
        } catch (err) {
            throw new Error(err);
        }
    }

    public static createImage = async (imageInput: IPrintImageInput): Promise<IPrintImage> => {
        try {
            const url = await ConfigurationManager.buildPrintUrl('/print/render');
            return (await Api.post(url, imageInput)).body;
        } catch (err) {
            throw new Error(err);
        }
    }

    private static convertAssigneeEntries = (assigneeData: string): { [key: string]: string } => {
        const transformed: {[key: string]: string} = {};
        Object.entries(JSON.parse(assigneeData)).forEach(([ key, value ]) => transformed[key] = (value as string).toUpperCase());
        return transformed;
    }

    public static createLogojetImage = async (assignments: Map<string, IReleaseAssignment[]>, side: PrintSideEnum) => {
        try {
            const templates = await PrintService.fetchTemplates();
            const tray = Boolean(templates) && Linq2.singleOrDefault(templates.values(), undefined, t => t.name === 'UV40R');

            if (Boolean(tray) && Boolean(assignments)) {
                const imageInput: IPrintImageInput = {
                    template: tray.name,
                    tray: tray.name,
                    space: FCSConstants.traySpacing,
                    side,
                    items: Linq2.select(assignments.entries(), ([orderId, assignees]) => ({
                        orderId,
                        items: assignees.map(a => ({
                            hostId: Boolean(a.assigneeId) && a.assigneeId,
                            text: Boolean(a.assigneeData) && PrintService.convertAssigneeEntries(a.assigneeData),
                            frontTemplate: a.assigneeFrontTemplate,
                            backTemplate: a.assigneeBackTemplate 
                        }))
                    })).toArray()
                };
                return await PrintService.createImage(imageInput);
            }
        } catch (err) {
            throw new Error(err);
        }
    }

    public static createBulkImage = async (templateName: string, tray: ITemplate, qty: number) => {
        try {
            if (!Boolean(templateName) || !Boolean(tray)) {
                return;
            }
    
            const { maxItems } = tray;
            const items = qty >= maxItems ? maxItems : qty;
            const imageInput: IPrintImageInput = {
                template: tray.name,
                tray: tray.name,
                space: FCSConstants.traySpacing,
                side: PrintSideEnum.front,
                items: [ ...Array(items) ].map(i => ({
                    orderId: '',
                    items: [ { hostId: '', text: {}, frontTemplate: templateName } ]
                }))
            };
            return await PrintService.createImage(imageInput);
        } catch (err) {
            throw new Error(err);
        }
    }

    public static buildPrintUrl = async (image: IPrintImage, type: 'pdf' | 'png', filename?: string): Promise<string> => {
        return ConfigurationManager.buildPrintUrl(`/print/render/${image.id}/${type}${Boolean(filename) ? `?filename=${filename}` : ''}`);
    }
}
