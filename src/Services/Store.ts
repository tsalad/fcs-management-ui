// tslint:disable:interface-name

interface Store extends Redux.Store<MC.ICombinedState> {}
class Store {
    private static reduxStore: Redux.Store<MC.ICombinedState>;

    public static dispatch<T extends Redux.AnyAction>(action: T): T {
        if (Store.reduxStore !== undefined) {
            return Store.reduxStore.dispatch(action);
        }
    }

    public static getState(): MC.ICombinedState {
        if (Store.reduxStore !== undefined) {
            return Store.reduxStore.getState();
        }
    }

    public static subscribe(listener: () => void): Redux.Unsubscribe {
        if (Store.reduxStore !== undefined) {
            return Store.reduxStore.subscribe(listener);
        }
    }

    public static setStore(store: Redux.Store<MC.ICombinedState>): void {
        Store.reduxStore = store;
    }
}

export default Store;
