import { Api } from 'o8-common';
import { IShipment } from '../Typings/IShipment';
import { IUpdateShipmentRequest } from '../Typings/IUpdateShipmentRequest';
import { StateEnum } from '../Typings/StateEnum';
import ConfigurationManager from './ConfigurationManager';

export default class ShipmentService {
    /**
     * @param {String} orderId the order ID that we want to fetch the shipment for
     * @returns {IShipment} the shipment for this order
     */
    public static fetchShipment = async (orderId: string): Promise<IShipment> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/shipment/${orderId}`);
            return (await Api.get(url)).body;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {String} start the start date / time to fetch
     * @param {String} end the end date / time to fetch
     */
    public static fetchShipments = async (start: string, end: string): Promise<IShipment[]> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/shipment?startBeganAt=${start}&endBeganAt=${end}`);
            return ((await Api.get(url)).body).shipments;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {String} startCompletedAt the start date / time to filter by
     * @param {String} endCompletedAt the end date / time to filter by
     * @returns {IShipment[]} a list of shipments
     */
    public static fetchCompletedShipments = async (startCompletedAt: string, endCompletedAt: string): Promise<IShipment[]> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/shipment?bulk=false&startCompletedAt=${startCompletedAt}&endCompletedAt=${endCompletedAt}&packages=true`);
            return ((await Api.get(url)).body).shipments;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @returns {IShipment[]} as list of shipments in fulfillment state
     */
    public static fetchShipmentsInFulfillment = async (): Promise<IShipment[]> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/shipment?bulk=false&state=${[StateEnum.FULFILLMENT]}&packages=true`);
            return ((await Api.get(url)).body).shipments;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {ICreateShipmentRequest} shipment the shipment that we want to create
     * @returns {IShipment} the shipment object that was created
     */
    public static createShipment = async (shipment: ICreateShipmentRequest): Promise<IShipment> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl('/fulfillment/shipment');
            return (await Api.post(url, shipment)).body;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {Boolean} bulk if true fetches bulk, if false fetches non bulk orders
     * @returns {Promise<IShipment[]>} a list of archived shipments from the past 30 days by default
     */
    public static fetchArchivedShipments = async (bulk: boolean = false): Promise<IShipment[]> => {
        try {
            const today = new Date();
            const startCreatedAt = new Date(today.setDate(today.getDate() - 30)).toISOString();
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/shipment?state=${StateEnum.ARCHIVED}&items=true&startCreatedAt=${startCreatedAt}&bulk=${bulk}`);
            return ((await Api.get(url)).body).shipments;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {String} orderId the order ID that we want to update shipment for
     * @param {IUpdateShipmentRequest} shipment the updated shipment
     * @returns {IShipment} the updated shipment object
     */
    public static updateShipment = async (orderId: string, shipment: IUpdateShipmentRequest): Promise<IShipment> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/fulfillment/shipment/${orderId}`);
            return (await Api.put(url, shipment)).body;
        } catch (err) {
            throw new Error(err);
        }
    }

    public static printShippingLabels = async (imageUrls: string[]): Promise<void> => {
        const container = document.createElement('div');
        imageUrls.forEach(url => {
            const image = document.createElement('img');
            image.src = url;
            container.appendChild(image);
        });
        const params = [ `height=${screen.height}`, `width=${screen.width}` ].join(',');
        const externalWindow = window.open('', '', params);
        externalWindow.document.body.innerHTML = container.innerHTML;
    }
}
