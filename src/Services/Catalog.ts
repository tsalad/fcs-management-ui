import { Api, Linq2, Util } from 'o8-common';
import FCSConstants from '../Utils/FCSConstants';
import ConfigurationManager from './ConfigurationManager';

export default class Catalog {
    /**
     * @param {String} key the unique ID for the product
     * @returns {IProductTag[]} a list of tags associated with the given product
     */
    public static fetchProductTags = async (key: string): Promise<IProductTag[]> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/catalog/product/${key}/tag`);
            return ((await Api.get(url)).body).tags;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {String} key the unique ID for the product item
     * @returns {Promise<IProductItem>} the product item with the matching sku
     */
    public static fetchProductItem = async (key: string): Promise<IProductItem> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/catalog/item/${key}?suppliers=true`);
            return ((await Api.get(url)).body);
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {String} key the SKU for the product item
     * @returns {IProductItemSupplier[]} a list of suppliers associated with this product item
     */
    public static fetchProductItemSupplier = async (key: string): Promise<IProductItemSupplier[]> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/catalog/item/${key}/supplier`);
            return ((await Api.get(url)).body).suppliers;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {String} key the unique ID for the product item
     * @returns {IProductItemTag[]} a list of tags associated with the given product item
     */
    public static fetchProductItemTags = async (key: string): Promise<IProductItemTag[]> => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/catalog/item/${key}/tag`);
            return ((await Api.get(url)).body).tags;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {IProductItemTag} tag tag that we would like to associate with the product item
     * @returns {IProductItemTag} the newly assigned tag
     */
    public static addProductItemTag = async (sku: string, tag: { label: string, value: string }): Promise<IProductItemTag> => {
        const { label, value } = tag;
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/catalog/item/${sku}/tag`);
            return (await Api.post(url, { label, value })).body;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {IProductItemTag} tag tag that we would like to update the value of
     * @returns {IProductItemTag} the updated tag
     */
    public static updateProductItemTag = async (tag: IProductItemTag): Promise<IProductItemTag> => {
        const { sku, label, value, updatedAt } = tag;
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/catalog/item/${sku}/tag/${label}`);
            return (await Api.put(url, { value, updatedAt })).body;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {IProductItemTag} tag tag that we would like to remove from the product item
     */
    public static removeProductItemTag = async (tag: IProductItemTag) => {
        const { sku, label } = tag;
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/catalog/item/${sku}/tag/${label}`);
            await Api.delete(url);
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {String} sku the sku of the product item we want to remove the supplier from
     * @param {IProductItemSupplier} productItemSupplier the product item supplier we want to remove
     */
    public static deleteProductItemSupplier = async (sku: string, productItemSupplier: IProductItemSupplier) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/catalog/item/${sku}/supplier/${productItemSupplier.supplierCode}`);
            await Api.delete(url);
        } catch (err) {
            throw new Error(err);
        }
    }
    
    /**
     * @param {String} sku the sku or the product item.
     * @param {IProductItemSupplier} productItemSupplier the product item supplier we want to remove
     */
    public static createProductItemSupplier = async (sku: string, productItemSupplier: IProductItemSupplier) => {
        try {
            const url = await ConfigurationManager.buildPorterUrl(`/catalog/item/${sku}/supplier`);
            return (await Api.post(url, productItemSupplier)).body;
        } catch (err) {
            throw new Error(err);
        }
    }

    /**
     * @param {IProductItem} productItem the product item we want to update the suppliers for
     */
    public static handleUpdateProductItemSuppliers = async (sku: string, suppliers: IProductItemSupplier[]) => {
        try {
            const existingSuppliers = await Catalog.fetchProductItemSupplier(sku);

            if (!Util.deepEqual(existingSuppliers, suppliers)) {
                await Promise.all(existingSuppliers.map(async s => Catalog.deleteProductItemSupplier(sku, s)));
                await Promise.all(suppliers.map(async s => s.supplierSku !== null && Catalog.createProductItemSupplier(sku, s)));
            }

        } catch (err) {
            throw new Error(err);
        }

        return Catalog.fetchProductItemSupplier(sku);
    }

    /**
     * @param {String} sku the sku of the product item we want to update tags for
     * @param {Partial<IProductItemTag[]>} productItemTags the new list of tags.
     * @returns {Promise<IProductItemTag[]>} returns the updated tags list for the produc item
     */
    public static rebuildProductItemTags = async (sku: string, productItemTags: Partial<IProductItemTag>[]): Promise<IProductItemTag[]> => {
        const existingTags: IProductItemTag[] = await Catalog.fetchProductItemTags(sku);
        const tagsToRemove: IProductItemTag[] = Linq2.where(existingTags, t => !productItemTags.some(tag => tag.label === t.label)).toArray();

        // First delete any attributes that currently exist and were removed
        if (Util.isNonEmptyArray(tagsToRemove)) {
            await Promise.all(tagsToRemove.map(async a => Catalog.removeProductItemTag(a)));
        }

        await Promise.all(productItemTags.map(async t => {
            // If a label was not entered, we can skip it
            if (t.label === undefined || !t.label.length) {
                return;
            }

            const existing = Util.isNonEmptyArray(existingTags) ? existingTags.find(e => e.label === t.label) : undefined;
            // If this tag label already exists for this product item and the value has changed update it.
            if (existing !== undefined && existing.value !== t.value) {
                await Catalog.updateProductItemTag({ ...existing, value: t.value });
                return;
            }

            // If there isn't a matching exising tag, create it.
            if (existing === undefined) {
                await Catalog.addProductItemTag(sku, { label: t.label, value: t.value });
            }
        }));

        return Catalog.fetchProductItemTags(sku);
    }
}
