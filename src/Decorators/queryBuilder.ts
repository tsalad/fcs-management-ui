export default function(props: Partial<IQueryBuilderDecoratorProps>): any { // PropertyDecorator|ClassDecorator
    return (target: any, key?: string | symbol) => {
        if (Array.isArray(props.fields)) {
            Reflect.defineMetadata('queryBuilderFields', props.fields, target, key);
            if (key === undefined && target.prototype !== undefined) {
                Reflect.defineMetadata('queryBuilderPrototype', target.prototype, target);
            }
        }

        if (props.foreignKeyDisplayName !== undefined) {
            Reflect.defineMetadata('queryBuilderForeignKeyDisplayName', props.foreignKeyDisplayName, target, key);
        }

        if (props.foreignKeyPrototype !== undefined) {
            Reflect.defineMetadata('queryBuilderForeignKeyPrototype', props.foreignKeyPrototype, target, key);
        }
    };
}
