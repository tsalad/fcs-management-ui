export default function(props: Partial<IMetaProps>): PropertyDecorator {
    return (target: any, key: string | symbol) => {
        if (props.displayName !== undefined) {
            Reflect.defineMetadata('displayName', props.displayName, target, key);
        }
        if (props.units !== undefined) {
            Reflect.defineMetadata('units', props.units, target, key);
        }
        if (props.min !== undefined) {
            Reflect.defineMetadata('min', props.min, target, key);
        }
        if (props.max !== undefined) {
            Reflect.defineMetadata('max', props.max, target, key);
        }
        if (props.required !== undefined) {
            Reflect.defineMetadata('required', props.required, target, key);
        }
    };
}
