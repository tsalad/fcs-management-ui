import { Dashboard as DashboardIcon, ListAlt, Search, Settings } from '@material-ui/icons';
import * as React from 'react';
import Dashboard from './Contexts/Dashboard';
import NotificationsMenuItem from './Contexts/NotificationsMenuItem';
import reducer from './Reducers';
import ConfigurationManager from './Services/ConfigurationManager';
import { IFCSReduxState } from './Typings/IFCSReduxState';
import FCSConstants from './Utils/FCSConstants';

const menuItemStyle: React.CSSProperties = {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
};

const menuIconStyle: React.CSSProperties = {
    marginRight: '1rem'
};

const config: IFCSReduxState = {
    id: 'fcs-ui',
    name: 'Fulfillment Center',
    menus: [
        {
            title: <NotificationsMenuItem />,
            menuId: FCSConstants.MENU_NOTIFICATIONS
        },
        {
            title:  <div style={menuItemStyle}><Search style={menuIconStyle} />Products</div>,
            menuId: FCSConstants.MENU_PRODUCTS
        },
        {
            title:  <div style={menuItemStyle}><ListAlt style={menuIconStyle} />Orders</div>,
            menuId: FCSConstants.MENU_ORDERS
        },
        {
            title:  <div style={menuItemStyle}><Settings style={menuIconStyle} />Manage</div>,
            menuId: FCSConstants.MENU_MANAGE,
            menus: [
                {
                    title:  'Logistics',
                    menuId: FCSConstants.MENU_LOGISTICS
                },
                {
                    title:  'Clients',
                    menuId: FCSConstants.MENU_CLIENTS
                },
                {
                    title:  'Accounts',
                    menuId: FCSConstants.MENU_ACCOUNTS
                },
                {
                    title: 'Import Manifest',
                    menuId: FCSConstants.MENU_IMPORT_MANIFEST
                }
            ]
        },
        {
            title:  <div style={menuItemStyle}><DashboardIcon style={menuIconStyle} />Dashboard</div>,
            menuId: FCSConstants.MENU_SUPERVISOR_DASHBOARD
        }
    ],
    selectedClient: undefined,
    previousMenu: undefined,
    currentMenu: FCSConstants.MENU_PRODUCTS,
    reducer,
    view: Dashboard,
    catalog: {
        items:  new Map(),
        categories: new Map(),
        products: new Map(),
        tags: new Map()
    },
    fulfillment: {
        dispatches: new Map(),
        ordersFlagged: new Map(),
        nonArchivedReleases: new Map(),
        activeDispatchStatus: new Map()
    },
    logistics: {
        clients:  new Map(),
        depots: new Map(),
        storage: new Map(),
        stock: new Map(),
        events: new Map(),
        productionLines: new Map(),
        suppliers: new Map(),
        shippingMethods: new Map(),
        workstations: new Map()
    },
    notifications: {
        problemReports: new Map(),
        reorderAlerts: new Map()
    },
    environment: undefined,
    application: {
        messages: new Map(),
        showModalManifestImport: false
    }
};

// allow tests to run without fetch
if (typeof window.fetch === 'function') {
    ConfigurationManager.initialize().then();
}

export default config;
