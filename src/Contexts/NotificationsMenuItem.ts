import { connect } from 'o8-common';
import * as React from 'react';
import NotificationsMenuItem, { INotificationsMenuItemProps } from '../Components/NotificationsMenuItem';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<INotificationsMenuItemProps> {
    return {
        reorderAlerts: state.notifications.reorderAlerts,
        problemReports: state.notifications.problemReports
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<INotificationsMenuItemProps> {
    return {};
}

export default connect<Partial<INotificationsMenuItemProps>, Partial<INotificationsMenuItemProps>, Partial<INotificationsMenuItemProps>>(
    mapStateToProps,
    mapDispatchToProps
)(NotificationsMenuItem);
