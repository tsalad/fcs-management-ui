import { connect } from 'o8-common';
import * as React from 'react';
import { fetchStock } from '../Actions/Logistics';
import ModalAddProductsToOrder, { IModalAddProductsToOrderProps } from '../Components/ModalAddProductsToOrder';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IModalAddProductsToOrderProps> {
    return { stock: state.logistics.stock };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IModalAddProductsToOrderProps> {
    return { fetchStock: () => dispatch(fetchStock() as any) };
}

export default connect<Partial<IModalAddProductsToOrderProps>, Partial<IModalAddProductsToOrderProps>, Partial<IModalAddProductsToOrderProps>>(
    mapStateToProps,
    mapDispatchToProps
)(ModalAddProductsToOrder);
