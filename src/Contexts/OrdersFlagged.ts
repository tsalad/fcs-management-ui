import { connect } from 'o8-common';
import * as React from 'react';
import OrdersFlagged, { IOrdersFlaggedProps } from '../Components/OrdersFlagged';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IOrdersFlaggedProps> {
    return {
        selectedClient: state.selectedClient,
        ordersFlagged: state.fulfillment.ordersFlagged,
        nonArchivedReleases: state.fulfillment.nonArchivedReleases
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IOrdersFlaggedProps> {
    return {};
}

export default connect<Partial<IOrdersFlaggedProps>, Partial<IOrdersFlaggedProps>, Partial<IOrdersFlaggedProps>>(
    mapStateToProps,
    mapDispatchToProps
)(OrdersFlagged);
