import { connect } from 'o8-common';
import * as React from 'react';
import { setErrorMessage, setSuccessMessage } from '../Actions/Application';
import { deleteDispatch, fetchActiveDispatchStatus, fetchDispatches, updateDispatch, updateDispatchGroup } from '../Actions/Fulfillment';
import Dispatches, { IDisptachesProps } from '../Components/Dispatches';
import { IDispatch } from '../Typings/IDispatch';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IDisptachesProps> {
    return {
        selectedClient: state.selectedClient,
        dispatches: state.fulfillment.dispatches,
        productionLines: state.logistics.productionLines,
        activeDispatchStatus: state.fulfillment.activeDispatchStatus,
        releases: state.fulfillment.nonArchivedReleases
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IDisptachesProps> {
    return {
        updateDispatch: (d: Partial<IDispatch>) => dispatch(updateDispatch(d) as any),
        deleteDispatch: (d: Partial<IDispatch>) => dispatch(deleteDispatch(d) as any),
        updateDispatchGroup: (group: IDispatchGroup) => dispatch(updateDispatchGroup(group) as any),
        setErrorMessage: (message: string) => dispatch(setErrorMessage(message) as any),
        setSuccessMessage: (message: string) => dispatch(setSuccessMessage(message) as any),
        fetchDispatches: () => dispatch(fetchDispatches() as any),
        fetchActiveDispatchStatus: () => dispatch(fetchActiveDispatchStatus() as any)
    };
}

export default connect<Partial<IDisptachesProps>, Partial<IDisptachesProps>, Partial<IDisptachesProps>>(
    mapStateToProps,
    mapDispatchToProps
)(Dispatches);
