import { connect } from 'o8-common';
import * as React from 'react';
import { setErrorMessage, setSuccessMessage } from '../Actions/Application';
import OrdersBulk, { IOrdersBulkProps } from '../Components/OrdersBulk';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IOrdersBulkProps> {
    return {
        selectedClient: state.selectedClient,
        ordersFlagged: state.fulfillment.ordersFlagged,
        nonArchivedReleases: state.fulfillment.nonArchivedReleases
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IOrdersBulkProps> {
    return {
        setErrorMessage: (message: string) => dispatch(setErrorMessage(message) as any),
        setSuccessMessage: (message: string) => dispatch(setSuccessMessage(message) as any)
    };
}

export default connect<Partial<IOrdersBulkProps>, Partial<IOrdersBulkProps>, Partial<IOrdersBulkProps>>(
    mapStateToProps,
    mapDispatchToProps
)(OrdersBulk);
