import { connect } from 'o8-common';
import * as React from 'react';
import { setErrorMessage, setSuccessMessage } from '../Actions/Application';
import { fetchItems } from '../Actions/Catalog';
import { fetchActiveDispatchStatus, fetchDispatches, fetchOrdersFlagged, fetchReleases } from '../Actions/Fulfillment';
import { fetchProductionLines, fetchStock } from '../Actions/Logistics';
import Orders, { IOrdersProps } from '../Components/Orders';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IOrdersProps> {
    return {
        clients: state.logistics.clients,
        selectedClient: state.selectedClient,
        ordersFlagged: state.fulfillment && state.fulfillment.ordersFlagged,
        nonArchivedReleases: state.fulfillment.nonArchivedReleases
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IOrdersProps> {
    return {
        fetchProductItems: () => dispatch(fetchItems() as any),
        fetchStock: () => dispatch(fetchStock() as any),
        fetchDispatches: () => dispatch(fetchDispatches() as any),
        fetchReleases: () => dispatch(fetchReleases() as any),
        fetchProductionLines: () => dispatch(fetchProductionLines() as any),
        setSuccessMessage: (message: string) => dispatch(setSuccessMessage(message) as any),
        setErrorMessage: (message: string) => dispatch(setErrorMessage(message) as any),
        fetchOrdersFlagged: () => dispatch(fetchOrdersFlagged() as any),
        fetchActiveDispatchStatus: () => dispatch(fetchActiveDispatchStatus() as any)
    };
}

export default connect<Partial<IOrdersProps>, Partial<IOrdersProps>, Partial<IOrdersProps>>(
    mapStateToProps,
    mapDispatchToProps
)(Orders);
