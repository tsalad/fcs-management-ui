import { connect } from 'o8-common';
import * as React from 'react';
import { setErrorMessage, setSuccessMessage } from '../Actions/Application';
import { createSupplier, updateSupplier } from '../Actions/Logistics';
import FormSupplier, { IFormSupplierProps } from '../Components/FormSupplier';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IFormSupplierProps> {
    return {
        productItems: state.catalog.items
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IFormSupplierProps> {
    return {
        createSupplier: (supplier: ISupplier) => dispatch(createSupplier(supplier) as any),
        updateSupplier: (supplier: ISupplier) => dispatch(updateSupplier(supplier) as any),
        setErrorMessage: (message: string) => dispatch(setErrorMessage(message) as any),
        setSuccessMessage: (message: string) => dispatch(setSuccessMessage(message) as any)
    };
}

export default connect<Partial<IFormSupplierProps>, Partial<IFormSupplierProps>, Partial<IFormSupplierProps>>(
    mapStateToProps,
    mapDispatchToProps
)(FormSupplier);
