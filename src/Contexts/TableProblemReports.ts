import { connect } from 'o8-common';
import * as React from 'react';
import { setErrorMessage, setSuccessMessage } from '../Actions/Application';
import TableProblemReports, { ITableProblemReportsProps } from '../Components/TableProblemReports';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<ITableProblemReportsProps> {
    return {
        problemReports: state.notifications.problemReports,
        productItems: state.catalog.items,
        storage: state.logistics.storage
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<ITableProblemReportsProps> {
    return {
        setErrorMessage: (message: string) => dispatch(setErrorMessage(message) as any),
        setSuccessMessage: (message: string) => dispatch(setSuccessMessage(message) as any)
    };
}

export default connect<Partial<ITableProblemReportsProps>, Partial<ITableProblemReportsProps>, Partial<ITableProblemReportsProps>>(
    mapStateToProps,
    mapDispatchToProps
)(TableProblemReports);
