import { connect } from 'o8-common';
import * as React from 'react';
import { fetchSuppliers } from '../Actions/Logistics';
import FormExternalSKU, { IFormExternalSKUProps } from '../Components/FormExternalSKU';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IFormExternalSKUProps> {
    return {
        suppliers: state.logistics.suppliers
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IFormExternalSKUProps> {
    return {
        fetchSuppliers: () => dispatch(fetchSuppliers() as any)
    };
}

export default connect<Partial<IFormExternalSKUProps>, Partial<IFormExternalSKUProps>, Partial<IFormExternalSKUProps>>(
    mapStateToProps,
    mapDispatchToProps
)(FormExternalSKU);
