import { connect } from 'o8-common';
import * as React from 'react';
import { setErrorMessage, setSuccessMessage } from '../Actions/Application';
import { createSupplier, deleteSupplier, updateSupplier } from '../Actions/Logistics';
import TableSuppliers, { ITableSuppliersProps } from '../Components/TableSuppliers';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<ITableSuppliersProps> {
    return {
        suppliers: state.logistics.suppliers,
        productItems: state.catalog.items
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<ITableSuppliersProps> {
    return {
        createSupplier: (supplier: ISupplier) => dispatch(createSupplier(supplier) as any),
        updateSupplier: (supplier: ISupplier) => dispatch(updateSupplier(supplier) as any),
        deleteSupplier: (key: string) => dispatch(deleteSupplier(key) as any),
        setErrorMessage: (message: string) => dispatch(setErrorMessage(message) as any),
        setSuccessMessage: (message: string) => dispatch(setSuccessMessage(message) as any)
    };
}

export default connect<Partial<ITableSuppliersProps>, Partial<ITableSuppliersProps>, Partial<ITableSuppliersProps>>(
    mapStateToProps,
    mapDispatchToProps
)(TableSuppliers);
