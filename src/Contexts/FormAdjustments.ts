import { connect } from 'o8-common';
import * as React from 'react';
import { fetchEvents, fetchStorage } from '../Actions/Logistics';
import FormAdjustments, { IFormAdjustmentsProps } from '../Components/FormAdjustments';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IFormAdjustmentsProps> {
    return {
        events: state.logistics.events,
        storage: state.logistics.storage,
        stock: state.logistics.stock
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IFormAdjustmentsProps> {
    return {
        fetchEvents: () => dispatch(fetchEvents() as any),
        fetchStorage: () => dispatch(fetchStorage() as any)
    };
}

export default connect<Partial<IFormAdjustmentsProps>, Partial<IFormAdjustmentsProps>, Partial<IFormAdjustmentsProps>>(
    mapStateToProps,
    mapDispatchToProps
)(FormAdjustments);
