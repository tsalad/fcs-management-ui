import { connect } from 'o8-common';
import * as React from 'react';
import { createStock, fetchStock, fetchStorage, moveStock } from '../Actions/Logistics';
import ModalStockAdjust, { IModalStockAdjustProps } from '../Components/ModalStockAdjust';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import { IStockInput } from '../Typings/IStockInput';

function mapStateToProps(state: IFCSReduxState): Partial<IModalStockAdjustProps> {
    return {
        stock: state.logistics.stock,
        storage: state.logistics.storage
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IModalStockAdjustProps> {
    return {
        fetchStock: () => dispatch(fetchStock() as any),
        fetchStorage: () => dispatch(fetchStorage() as any),
        createStock: (stockInput: IStockInput) => dispatch(createStock(stockInput) as any),
        moveStock: (stockInput: IStockMoveInput) => dispatch(moveStock(stockInput) as any)
    };
}

export default connect<Partial<IModalStockAdjustProps>, Partial<IModalStockAdjustProps>, Partial<IModalStockAdjustProps>>(
    mapStateToProps,
    mapDispatchToProps
)(ModalStockAdjust);
