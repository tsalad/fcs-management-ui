import { connect } from 'o8-common';
import * as React from 'react';
import { setErrorMessage, setSuccessMessage } from '../Actions/Application';
import { fetchItems } from '../Actions/Catalog';
import { deleteDepot, deleteStorage, fetchDepots, fetchStorage, fetchSuppliers, fetchWorkstations } from '../Actions/Logistics';
import ManageLogistics, { IManageLogisticsProps } from '../Components/ManageLogistics';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IManageLogisticsProps> {
    return {
        depots: state.logistics.depots,
        clients: state.logistics.clients,
        workstations: state.logistics.workstations,
        storage: state.logistics.storage
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IManageLogisticsProps> {
    return {
        fetchDepots: () => dispatch(fetchDepots() as any),
        deleteDepot: (key: string) => dispatch(deleteDepot(key) as any),
        fetchSuppliers: () => dispatch(fetchSuppliers() as any),
        fetchItems: () => dispatch(fetchItems() as any),
        fetchStorage: () => dispatch(fetchStorage() as any),
        fetchWorkstations: () => dispatch(fetchWorkstations() as any),
        deleteStorage: (key: string) => dispatch(deleteStorage(key) as any),
        setErrorMessage: (message: string) => dispatch(setErrorMessage(message) as any),
        setSuccessMessage: (message: string) => dispatch(setSuccessMessage(message) as any)
    };
}

export default connect<Partial<IManageLogisticsProps>, Partial<IManageLogisticsProps>, Partial<IManageLogisticsProps>>(
    mapStateToProps,
    mapDispatchToProps
)(ManageLogistics);
