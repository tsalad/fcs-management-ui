import { connect } from 'o8-common';
import * as React from 'react';
import { setErrorMessage, setSuccessMessage } from '../Actions/Application';
import { createDepot, updateDepot } from '../Actions/Logistics';
import FormDepot, { IFormDepotProps } from '../Components/FormDepot';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IFormDepotProps> {
    return {
        clients: state.logistics.clients
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IFormDepotProps> {
    return {
        updateDepot: (depot: IDepot) => dispatch(updateDepot(depot) as any),
        createDepot: (depot: IDepot) => dispatch(createDepot(depot) as any),
        setErrorMessage: (message: string) => dispatch(setErrorMessage(message) as any),
        setSuccessMessage: (message: string) => dispatch(setSuccessMessage(message) as any)
    };
}

export default connect<Partial<IFormDepotProps>, Partial<IFormDepotProps>, Partial<IFormDepotProps>>(
    mapStateToProps,
    mapDispatchToProps
)(FormDepot);
