import { connect } from 'o8-common';
import * as React from 'react';
import { setErrorMessage, setSuccessMessage } from '../Actions/Application';
import OrdersPending, { IOrdersPendingProps } from '../Components/OrdersPending';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IOrdersPendingProps> {
    return {
        selectedClient: state.selectedClient,
        productItems: state.catalog.items,
        stock: state.logistics.stock,
        productionLines: state.logistics.productionLines,
        nonArchivedReleases: state.fulfillment.nonArchivedReleases,
        activeDispatchStatus: state.fulfillment.activeDispatchStatus,
        dispatches: state.fulfillment.dispatches
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IOrdersPendingProps> {
    return {
        setErrorMessage: (message: string) => dispatch(setErrorMessage(message) as any),
        setSuccessMessage: (message: string) => dispatch(setSuccessMessage(message) as any)
    };
}

export default connect<Partial<IOrdersPendingProps>, Partial<IOrdersPendingProps>, Partial<IOrdersPendingProps>>(
    mapStateToProps,
    mapDispatchToProps
)(OrdersPending);
