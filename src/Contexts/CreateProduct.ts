import { connect } from 'o8-common';
import * as React from 'react';
import { setErrorMessage, setSuccessMessage } from '../Actions/Application';
import { createProductItem, createProductItemImageMultipart, createTag, deleteProductItemImage, fetchTags, updateProductItem } from '../Actions/Catalog';
import CreateProduct, { ICreateProductProps } from '../Components/CreateProduct';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<ICreateProductProps> {
    return {
        items: state.catalog.items,
        tags: state.catalog.tags
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<ICreateProductProps> {
    return {
        createProductItem: (item: IProductItem) => dispatch(createProductItem(item) as any),
        updateProductItem: (item: IProductItem) => dispatch(updateProductItem(item) as any),
        fetchTags: () => dispatch(fetchTags() as any),
        createTag: (label: string) => dispatch(createTag(label) as any),
        createProductItemImageMultipart: (file: File, sku: string) => dispatch(createProductItemImageMultipart(file, sku) as any),
        deleteProductItemImage: (sku: string) => dispatch(deleteProductItemImage(sku) as any),
        setErrorMessage: (message: string) => dispatch(setErrorMessage(message) as any),
        setSuccessMessage: (message: string) => dispatch(setSuccessMessage(message) as any)
    };
}

export default connect<Partial<ICreateProductProps>, Partial<ICreateProductProps>, Partial<ICreateProductProps>>(
    mapStateToProps,
    mapDispatchToProps
)(CreateProduct);
