import { connect } from 'o8-common';
import * as React from 'react';
import OrdersArchived, { IOrdersArchivedProps } from '../Components/OrdersArchived';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IOrdersArchivedProps> {
    return {
        selectedClient: state.selectedClient
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IOrdersArchivedProps> {
    return {};
}

export default connect<Partial<IOrdersArchivedProps>, Partial<IOrdersArchivedProps>, Partial<IOrdersArchivedProps>>(
    mapStateToProps,
    mapDispatchToProps
)(OrdersArchived);
