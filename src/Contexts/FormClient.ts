import { connect } from 'o8-common';
import * as React from 'react';
import { setErrorMessage, setSuccessMessage } from '../Actions/Application';
import { createClient, fetchShippingMethods, updateClient } from '../Actions/Logistics';
import FormClient, { IFormClientProps } from '../Components/FormClient';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IFormClientProps> {
    return {
        shippingMethods: state.logistics.shippingMethods
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IFormClientProps> {
    return {
        createClient: (client: IClient) => dispatch(createClient(client) as any),
        updateClient: (client: IClient) => dispatch(updateClient(client) as any),
        fetchShippingMethods: () => dispatch(fetchShippingMethods() as any),
        setErrorMessage: (message: string) => dispatch(setErrorMessage(message) as any),
        setSuccessMessage: (message: string) => dispatch(setSuccessMessage(message) as any)
    };
}

export default connect<Partial<IFormClientProps>, Partial<IFormClientProps>, Partial<IFormClientProps>>(
    mapStateToProps,
    mapDispatchToProps
)(FormClient);
