import { connect } from 'o8-common';
import * as React from 'react';
import { fetchItems } from '../Actions/Catalog';
import { fetchStock } from '../Actions/Logistics';
import ProductAvailabilityList, { IProductAvailabilityListProps } from '../Components/ProductAvailabilityList';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IProductAvailabilityListProps> {
    return {
        productItems: state.catalog.items,
        stock: state.logistics.stock
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IProductAvailabilityListProps> {
    return {
        fetchProductItems: () => dispatch(fetchItems() as any),
        fetchStock: () => dispatch(fetchStock() as any)  
    };
}

export default connect<Partial<IProductAvailabilityListProps>, Partial<IProductAvailabilityListProps>, Partial<IProductAvailabilityListProps>>(
    mapStateToProps,
    mapDispatchToProps
)(ProductAvailabilityList);
