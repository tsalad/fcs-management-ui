import { connect } from 'o8-common';
import * as React from 'react';
import { setErrorMessage, setSuccessMessage } from '../Actions/Application';
import TableOrdersFlagged, { ITableOrdersFlaggedProps } from '../Components/TableOrdersFlagged';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<ITableOrdersFlaggedProps> {
    return {
        releases: state.fulfillment.nonArchivedReleases
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<ITableOrdersFlaggedProps> {
    return {
        setErrorMessage: (message: string) => dispatch(setErrorMessage(message) as any),
        setSuccessMessage: (message: string) => dispatch(setSuccessMessage(message) as any)
    };
}

export default connect<Partial<ITableOrdersFlaggedProps>, Partial<ITableOrdersFlaggedProps>, Partial<ITableOrdersFlaggedProps>>(
    mapStateToProps,
    mapDispatchToProps
)(TableOrdersFlagged);
