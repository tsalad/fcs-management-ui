import { connect } from 'o8-common';
import * as React from 'react';
import { createStock, fetchStorage } from '../Actions/Logistics';
import FormStock, { IFormStockProps } from '../Components/FormStock';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import { IStockInput } from '../Typings/IStockInput';

function mapStateToProps(state: IFCSReduxState): Partial<IFormStockProps> {
    return {
        storage: state.logistics.storage,
        stock: state.logistics.stock
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IFormStockProps> {
    return {
        fetchStorage: () => dispatch(fetchStorage() as any),
        createStock: (stock: IStockInput) => dispatch(createStock(stock) as any)
    };
}

export default connect<Partial<IFormStockProps>, Partial<IFormStockProps>, Partial<IFormStockProps>>(
    mapStateToProps,
    mapDispatchToProps
)(FormStock);
