import { connect } from 'o8-common';
import * as React from 'react';
import { setErrorMessage, setSuccessMessage } from '../Actions/Application';
import ModalAddShipmentTracking, { IModalAddShipmentTrackingProps } from '../Components/ModalAddShipmentTracking';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IModalAddShipmentTrackingProps> {
    return {};
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IModalAddShipmentTrackingProps> {
    return {
        setErrorMessage: (message: string) => dispatch(setErrorMessage(message) as any),
        setSuccessMessage: (message: string) => dispatch(setSuccessMessage(message) as any)
    };
}

export default connect<Partial<IModalAddShipmentTrackingProps>, Partial<IModalAddShipmentTrackingProps>, Partial<IModalAddShipmentTrackingProps>>(
    mapStateToProps,
    mapDispatchToProps
)(ModalAddShipmentTracking);
