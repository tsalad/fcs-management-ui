import { connect } from 'o8-common';
import * as React from 'react';
import { fetchItems } from '../Actions/Catalog';
import { fetchStock, fetchStorage } from '../Actions/Logistics';
import Notifications, { INotificationsProps } from '../Components/Notifications';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<INotificationsProps> {
    return {
        reorderAlerts: state.notifications.reorderAlerts,
        problemReports: state.notifications.problemReports
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<INotificationsProps> {
    return {
        fetchStock: () => dispatch(fetchStock() as any),
        fetchProductItems: () => dispatch(fetchItems() as any),
        fetchStorage: () => dispatch(fetchStorage() as any)
    };
}

export default connect<Partial<INotificationsProps>, Partial<INotificationsProps>, Partial<INotificationsProps>>(
    mapStateToProps,
    mapDispatchToProps
)(Notifications);
