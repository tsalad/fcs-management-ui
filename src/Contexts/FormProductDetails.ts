import { connect } from 'o8-common';
import * as React from 'react';
import { createCategory, createProduct } from '../Actions/Catalog';
import FormProductDetails, { IFormProductDetailsProps } from '../Components/FormProductDetails';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IFormProductDetailsProps> {
    return {
        categories: state.catalog.categories,
        products: state.catalog.products
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IFormProductDetailsProps> {
    return {
        createProduct: (product: IProduct) => dispatch(createProduct(product) as any),
        createCategory: (category: ICategory) => dispatch(createCategory(category) as any)
    };
}

export default connect<Partial<IFormProductDetailsProps>, Partial<IFormProductDetailsProps>, Partial<IFormProductDetailsProps>>(
    mapStateToProps,
    mapDispatchToProps
)(FormProductDetails);
