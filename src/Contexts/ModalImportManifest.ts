import { connect } from 'o8-common';
import * as React from 'react';
import { setErrorMessage, setSuccessMessage, toggleManifestImportModal } from '../Actions/Application';
import { fetchItems } from '../Actions/Catalog';
import ModalImportManifest, { IModalImportManifestProps } from '../Components/ModalImportManifest';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IModalImportManifestProps> {
    return {
        productItems: state.catalog.items
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IModalImportManifestProps> {
    return {
        fetchItems: () => dispatch(fetchItems() as any),
        toggleModalImportManifest: (state: boolean) => dispatch(toggleManifestImportModal(state) as any),
        setErrorMessage: (message: string) => dispatch(setErrorMessage(message) as any),
        setSuccessMessage: (message: string) => dispatch(setSuccessMessage(message) as any)
    };
}

export default connect<Partial<IModalImportManifestProps>, Partial<IModalImportManifestProps>, Partial<IModalImportManifestProps>>(
    mapStateToProps,
    mapDispatchToProps
)(ModalImportManifest);
