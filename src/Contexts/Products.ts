import { connect } from 'o8-common';
import * as React from 'react';
import { fetchCategories, fetchItems, fetchProducts } from '../Actions/Catalog';
import { fetchStock } from '../Actions/Logistics';
import Products, { IProductsProps } from '../Components/Products';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IProductsProps> {
    return {
        items: state.catalog.items,
        categories: state.catalog.categories,
        products: state.catalog.products,
        stock: state.logistics.stock
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IProductsProps> {
    return {
        fetchItems: () => dispatch(fetchItems() as any),
        fetchCategories: () => dispatch(fetchCategories() as any),
        fetchProducts: () => dispatch(fetchProducts() as any),
        fetchStock: () => dispatch(fetchStock() as any)
    };
}

export default connect<Partial<IProductsProps>, Partial<IProductsProps>, Partial<IProductsProps>>(
    mapStateToProps,
    mapDispatchToProps
)(Products);
