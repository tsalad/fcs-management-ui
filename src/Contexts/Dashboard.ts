import { connect } from 'o8-common';
import * as React from 'react';
import { clearSnackbarMessage, setErrorMessage, setSuccessMessage, toggleManifestImportModal } from '../Actions/Application';
import Dashboard, { IDashboardProps } from '../Components/Dashboard';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<IDashboardProps> {
    return {
        currentMenu: state.currentMenu,
        messages: state.application.messages,
        showModalImportManifest: state.application.showModalManifestImport
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<IDashboardProps> {
    return {
        clearSnackbarMessage: (key: string) => dispatch(clearSnackbarMessage(key) as any),
        toggleModalImportManifest: (state: boolean) => dispatch(toggleManifestImportModal(state) as any),
        setErrorMessage: (message: string) => dispatch(setErrorMessage(message) as any),
        setSuccessMessage: (message: string) => dispatch(setSuccessMessage(message) as any)
    };
}

export default connect<Partial<IDashboardProps>, Partial<IDashboardProps>, Partial<IDashboardProps>>(
    mapStateToProps,
    mapDispatchToProps
)(Dashboard);
