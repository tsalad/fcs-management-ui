import { connect } from 'o8-common';
import * as React from 'react';
import ManageAccounts from '../Components/ManageAccounts';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<{}> {
    return {
        environment: state.environment
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<{}> {
    return {};
}

export default connect<Partial<{}>, Partial<{}>, Partial<{}>>(
    mapStateToProps,
    mapDispatchToProps
)(ManageAccounts);
