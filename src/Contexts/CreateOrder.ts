import { connect } from 'o8-common';
import * as React from 'react';
import { setErrorMessage, setSuccessMessage } from '../Actions/Application';
import { fetchDepots, fetchStorage } from '../Actions/Logistics';
import CreateOrder, { ICreateOrderProps } from '../Components/CreateOrder';
import { IFCSReduxState } from '../Typings/IFCSReduxState';
import { IRelease } from '../Typings/IRelease';

function mapStateToProps(state: IFCSReduxState): Partial<ICreateOrderProps> {
    return {
        depots: state.logistics.depots,
        productItems: state.catalog.items,
        clients: state.logistics.clients
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<ICreateOrderProps> {
    return {
        fetchDepots: () => dispatch(fetchDepots() as any),
        fetchStorage: () => dispatch(fetchStorage() as any),
        setErrorMessage: (message: string) => dispatch(setErrorMessage(message) as any),
        setSuccessMessage: (message: string) => dispatch(setSuccessMessage(message) as any)
    };
}

export default connect<Partial<ICreateOrderProps>, Partial<ICreateOrderProps>, Partial<ICreateOrderProps>>(
    mapStateToProps,
    mapDispatchToProps
)(CreateOrder);
