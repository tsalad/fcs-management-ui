import { connect } from 'o8-common';
import * as React from 'react';
import TableReorderAlerts, { ITableReorderAlertsProps } from '../Components/TableReorderAlerts';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<ITableReorderAlertsProps> {
    return {
        reorderAlerts: state.notifications.reorderAlerts,
        stock: state.logistics.stock
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<ITableReorderAlertsProps> {
    return {};
}

export default connect<Partial<ITableReorderAlertsProps>, Partial<ITableReorderAlertsProps>, Partial<ITableReorderAlertsProps>>(
    mapStateToProps,
    mapDispatchToProps
)(TableReorderAlerts);
