import { connect } from 'o8-common';
import * as React from 'react';
import { fetchClientList } from '../Actions/Logistics';
import ManageClients from '../Components/ManageClients';
import { IFCSReduxState } from '../Typings/IFCSReduxState';

function mapStateToProps(state: IFCSReduxState): Partial<{}> {
    return {
        clients: state.logistics.clients
    };
}

function mapDispatchToProps(dispatch: Redux.Dispatch<Redux.AnyAction>): Partial<{}> {
    return {
        fetchClients: () => dispatch(fetchClientList() as any)
    };
}

export default connect<Partial<{}>, Partial<{}>, Partial<{}>>(
    mapStateToProps,
    mapDispatchToProps
)(ManageClients);
