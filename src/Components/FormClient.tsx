import { Button, Checkbox, FormControlLabel, MenuItem } from '@material-ui/core';
import { Linq2, Util, ValidationService } from 'o8-common';
import * as React from 'react';
import Client from '../Models/Client';
import ClientService from '../Services/ClientService';
import FCSConstants from '../Utils/FCSConstants';
import EditableTextField from './EditableTextField';
import FormAddress, { FormAddressValidationEnum } from './FormAddress';
import FormSection from './FormSection';
import View from './View';

export interface IFormClientProps {
    client: IClient;
    shippingMethods: Map<string, IShippingMethod>;
    fetchShippingMethods(): void;
    cancel(): void;
    createClient(client: IClient): IClient;
    updateClient(client: IClient): IClient;
    setErrorMessage(message: string): void;
    setSuccessMessage(message: string): void;
}

interface IFormClientState {
    clientShippingMethods: IClientShippingMethod[];
    client: IClient;
    clientSnapshot: IClient;
    editMode: boolean;
    showValidationErrors: boolean;
    shippingMethodsSnapshot: IClientShippingMethod[];
}

export default class FormClient extends React.Component<IFormClientProps, IFormClientState> {
    private readonly validator: ValidationService;

    constructor(props: IFormClientProps) {
        super(props);
        this.state = {
            client: props.client,
            clientSnapshot: undefined,
            editMode: true,
            showValidationErrors: false,
            clientShippingMethods: [{ method: null, preferencePriority: 0, expedited: false }],
            shippingMethodsSnapshot: undefined
        };
        this.validator = new ValidationService(this);
        this.validator.setValidationResult = this.validator.setValidationResult.bind(this.validator);
    }

    public async componentDidMount(): Promise<void> {
        const { client, fetchShippingMethods } = this.props;
        this.validate(client);
        fetchShippingMethods();
        if (Boolean(client.updatedAt)) {
            const clientShippingMethods = await ClientService.fetchClientShippingMethods(client.clientCode) || [{ method: null, preferencePriority: 0, expedited: false }];
            this.setState({ editMode: false, clientShippingMethods });
        }
    }

    private validate = (client: IClient) => {
        const { name, addressStreet, addressLocality, addressRegion, addressPostalCode, contactEmail, contactPhone } = Client.validate(client);
        this.validator.setValidationResult('name', name);
        this.validator.setValidationResult(FormAddressValidationEnum.Street, addressStreet);
        this.validator.setValidationResult(FormAddressValidationEnum.Locality, addressLocality);
        this.validator.setValidationResult(FormAddressValidationEnum.Region, addressRegion);
        this.validator.setValidationResult(FormAddressValidationEnum.PostalCode, addressPostalCode);
        this.validator.setValidationResult('contactEmail', contactEmail);
        this.validator.setValidationResult('contactPhone', contactPhone);
    }

    private setEditMode = () => {
        this.setState({
            editMode: true,
            clientSnapshot: this.state.client,
            shippingMethodsSnapshot: this.state.clientShippingMethods
        });
    }

    private updateShippingMethods = async () => {
        const { clientShippingMethods, client } = this.state;
        const existingShippingMethods = await ClientService.fetchClientShippingMethods(client.clientCode) || [];
        const newShippingMethods = Linq2.where(clientShippingMethods, m => Boolean(m.method)).toArray();
        const itemsToDelete = Linq2.except(existingShippingMethods, newShippingMethods).toArray();
        const itemsToAdd = Linq2.except(newShippingMethods, existingShippingMethods).toArray();
        const itemsToUpdate = Linq2.intersect(newShippingMethods, existingShippingMethods, (a, b) => a.method.serviceCode.localeCompare(b.method.serviceCode))
                             .where(a => !Util.deepEqual(newShippingMethods.find(b => b.method.serviceCode === a.method.serviceCode), a))
                             .toArray();

        if (Util.isNonEmptyArray(itemsToDelete)) {
            await Promise.all(itemsToDelete.map(i => ClientService.removeClientShippingMethod(client.clientCode, i.method.serviceCode)));
        }

        if (Util.isNonEmptyArray(itemsToAdd)) {
            await Promise.all(itemsToAdd.map(i => ClientService.addClientShippingMethod(client.clientCode, i)));
        }

        if (Util.isNonEmptyArray(itemsToUpdate)) {
            await Promise.all(itemsToUpdate.map(i => ClientService.updateClientShippingMethod(client.clientCode, i)));
        }
        
        return ClientService.fetchClientShippingMethods(client.clientCode);
    }

    private cancel = async () => {
        const { client, clientSnapshot, shippingMethodsSnapshot } = this.state;
        if (client.updatedAt) {
            this.setState({ 
                client: clientSnapshot,
                editMode: false,
                clientSnapshot: undefined,
                clientShippingMethods: shippingMethodsSnapshot,
                shippingMethodsSnapshot: undefined
            });
            return;
        }
        this.props.cancel();
    }

    private save = async (): Promise<void> => {
        if (!this.validator.isValid()) {
            this.setState({ showValidationErrors: true });
            this.props.setErrorMessage('Please fix validation errors');
            return;
        }
        try {
            const client = Boolean(this.state.client.updatedAt) ? await this.props.updateClient(this.state.client) : await this.props.createClient(this.state.client);
            const clientShippingMethods = await this.updateShippingMethods();
            this.setState({
                client,
                editMode: false,
                clientSnapshot: undefined,
                showValidationErrors: false,
                clientShippingMethods
            });
            this.props.setSuccessMessage('Client Successfully Saved');
        } catch (err) {
            this.props.setErrorMessage('Client failed to save');
            console.error(err);
        }
    }

    private updateAddress = (address: IAddress) => {
        const client = { ...this.state.client, address };
        this.validate(client);
        this.setState({ client });
    }

    private update = (prop: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
        const client = { ...this.state.client, [prop]: e.target.value };
        this.validate(client);
        this.setState({ client });
    }

    private updateContact = (prop: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
        const client = { ...this.state.client, address: { ...this.state.client.address, [prop]: e.target.value } };
        this.validate(client);
        this.setState({ client });
    }

    private renderActions = () => {
        const { client, editMode, clientSnapshot, shippingMethodsSnapshot, clientShippingMethods } = this.state;
        if (editMode) {
            const disabled = client.updatedAt ? Util.deepEqual(client, clientSnapshot) && Util.deepEqual(shippingMethodsSnapshot, clientShippingMethods) : Util.deepEqual(client, this.props.client);
            return (
                <div>
                    <Button style={FCSConstants.buttonSpacing}
                            variant="contained"
                            onClick={this.cancel}>CANCEL</Button>
                    <Button style={FCSConstants.buttonSpacing}
                            variant="contained"
                            disabled={disabled}
                            color="primary"
                            onClick={this.save}>{client.updatedAt ? 'SAVE' : 'CREATE'}</Button>
                </div>
            );
        }
        return (
            <div>
                <Button style={FCSConstants.buttonSpacing}
                        variant="contained"
                        onClick={this.props.cancel}>BACK</Button>
                <Button style={FCSConstants.buttonSpacing}
                        variant="contained"
                        color="primary"
                        onClick={this.setEditMode}>EDIT</Button>
            </div>
        );
    }

    private addShippingMethod = () => {
        this.setState({ clientShippingMethods: [ ...this.state.clientShippingMethods, { method: null, preferencePriority: 0, expedited: false } ] });
    }

    private updateExpedited = (index: number) => (e: React.ChangeEvent<HTMLInputElement>) => {
        const { clientShippingMethods } = this.state;
        const updated = [ ...clientShippingMethods ];
        updated.splice(index, 1, { ...clientShippingMethods[index], expedited: e.target.checked });
        this.setState({ clientShippingMethods: updated });
    }

    private updateShippingMethod = (index: number) => (e: React.ChangeEvent<HTMLInputElement>) => {
        const { clientShippingMethods } = this.state;
        const method = this.props.shippingMethods.get(e.target.value);
        const updated = [ ...clientShippingMethods ];
        updated.splice(index, 1, { ...clientShippingMethods[index], method });
        this.setState({ clientShippingMethods: updated });
    }

    private removeShippingMethod = (index: number) => () => {
        const updated = [ ...this.state.clientShippingMethods ];
        updated.splice(index, 1);
        this.setState({ clientShippingMethods: updated });
    }

    private renderClientShippingMethod = ({ method, expedited }: IClientShippingMethod, i: number) => {
        const { clientShippingMethods, editMode } = this.state;
        return (
            <div key={i} style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                <EditableTextField select
                                   editMode={editMode}
                                   value={editMode ? Boolean(method) && method.serviceCode || '' : Boolean(method) && method.name || ''}
                                   variant="outlined"
                                   style={{ ...FCSConstants.textFieldStyle, maxWidth: '500px' }}
                                   onChange={this.updateShippingMethod(i)}>
                    {[ ...this.props.shippingMethods.values() ].map(m => (
                        <MenuItem disabled={Util.isNonEmptyArray(clientShippingMethods) && clientShippingMethods.some(s => Boolean(s.method) && s.method.serviceCode === m.serviceCode)}
                                  key={m.serviceCode}
                                  value={m.serviceCode}>{m.name}</MenuItem>)
                    )}
                </EditableTextField>
                <FormControlLabel control={<Checkbox checked={expedited}
                                                     disabled={!editMode}
                                                     onChange={this.updateExpedited(i)}
                                                     color="primary"/>}
                                  label="Expedited" />
                <Button style={FCSConstants.buttonSpacing}
                        disabled={!editMode}
                        color="primary"
                        onClick={this.removeShippingMethod(i)}>REMOVE</Button>
            </div>
        );
    }

    public render(): React.ReactNode {
        const { client, editMode, showValidationErrors, clientShippingMethods } = this.state;
        
        const nameValidator = showValidationErrors && this.validator.getResults().get('name');
        const contactPhoneValidator = showValidationErrors && this.validator.getResults().get('contactPhone');
        const contactEmailValidator = showValidationErrors && this.validator.getResults().get('contactEmail');

        const remainingMethods = Linq2.where([ ...this.props.shippingMethods.values() ], m => !clientShippingMethods.some(s => Boolean(s.method) && s.method.serviceCode === m.serviceCode)).toArray();

        return (
            <View actions={this.renderActions()} title={Boolean(client.updatedAt) ? client.name : 'Create New Client'}>
                <FormSection title="Basic Information">
                    <EditableTextField label="Client"
                                       helperText={nameValidator && !nameValidator.valid ? nameValidator.message : ''}
                                       editMode={editMode}
                                       value={client.name || ''}
                                       variant="outlined"
                                       error={nameValidator &&  !nameValidator.valid}
                                       style={FCSConstants.textFieldStyle}
                                       onChange={this.update('name')} />
                </FormSection>
                <FormSection actions={editMode && <Button color="primary"
                                                          onClick={this.addShippingMethod}
                                                          disabled={!Util.isNonEmptyArray(remainingMethods)}>ADD SHIPPING METHOD</Button>}
                             title="Shipping Method"
                             description="Add shipping methods for this client and select a preferred method.">
                    {Util.isNonEmptyArray(clientShippingMethods) && clientShippingMethods.map(this.renderClientShippingMethod)}
                </FormSection>
                <FormSection title="Contact Information">
                    <EditableTextField label="Contact Phone Number"
                                       helperText={contactPhoneValidator && !contactPhoneValidator.valid ? contactPhoneValidator.message : ''}
                                       editMode={editMode}
                                       value={client.address.phone || ''}
                                       variant="outlined"
                                       error={contactPhoneValidator &&  !contactPhoneValidator.valid}
                                       style={FCSConstants.textFieldStyle}
                                       onChange={this.updateContact('phone')} />
                    <EditableTextField label="Contact Email"
                                       helperText={contactEmailValidator && !contactEmailValidator.valid ? contactEmailValidator.message : ''}
                                       editMode={editMode}
                                       value={client.address.email || ''}
                                       variant="outlined"
                                       error={contactEmailValidator &&  !contactEmailValidator.valid}
                                       style={FCSConstants.textFieldStyle}
                                       onChange={this.updateContact('email')} />
                </FormSection>
                <FormAddress address={client.address}
                             update={this.updateAddress}
                             validator={this.validator}
                             editMode={editMode}
                             showValidationErrors={showValidationErrors} />
            </View>
        );
    }
}
