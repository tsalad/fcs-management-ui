import { Button, IconButton, MenuItem, Popover } from '@material-ui/core';
import { MoreVert } from '@material-ui/icons';
import * as React from 'react';
import FormClient from '../Contexts/FormClient';
import Client from '../Models/Client';
import { formatAddress } from '../Utils/Util';
import DataTable, { IDataTableColumn } from './DataTable';
import View from './View';

export interface IManageClientsProps {
    clients: Map<string, IClient>;
    fetchClients(): void;
}

interface IManageClientsState {
    selectedClient: IClient;
    selected: string;
    menuAnchor: HTMLElement;
}

export default class ManageClients extends React.Component<IManageClientsProps, IManageClientsState> {
    constructor(props: IManageClientsProps) {
        super(props);
        this.state = {
            selectedClient: undefined,
            selected: undefined,
            menuAnchor: undefined
        };
    }

    public componentDidMount(): void {
        this.props.fetchClients();
    }

    private showMenu = (key: string) => (e: React.MouseEvent<HTMLElement>) => {
        this.setState({ selected: key, menuAnchor: e.currentTarget });
    }

    private hideMenu = () => {
        this.setState({ selected: undefined, menuAnchor: undefined });
    }

    private toggleClientForm = (selectedClient?: IClient) => () => {
        this.setState({ selectedClient, selected: undefined, menuAnchor: undefined });
    }

    public render(): React.ReactNode {
        if (this.state.selectedClient) {
            return <FormClient client={this.state.selectedClient} cancel={this.toggleClientForm()} />;
        }

        const columns: IDataTableColumn<IClient>[] = [
            {
                title: 'Name',
                value: (item) => item.name
            },
            {
                title: 'Address',
                value: (item) => formatAddress(item.address)
            },
            {
                value: (item: IClient) => {
                    return (
                        <>
                            <IconButton onClick={this.showMenu(item.clientCode)}>
                                <MoreVert />
                            </IconButton>
                            <Popover open={this.state.selected === item.clientCode}
                                     anchorEl={this.state.menuAnchor}
                                     anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                                     transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                                     onClose={this.hideMenu}>
                                <MenuItem onClick={this.toggleClientForm(item)}>View/Edit</MenuItem>
                            </Popover>
                        </>
                    );
                }
            }
        ];

        return (
            <View title="Manage - Clients"
                  actions={<Button variant="contained" color="primary" onClick={this.toggleClientForm(new Client())}>NEW CLIENT</Button>}>
                <DataTable columns={columns}
                           data={Boolean(this.props.clients) && [ ...this.props.clients.values() ]} />
            </View>
        );
    }
}
