import { Typography } from '@material-ui/core';
import * as React from 'react';

export interface ISectionTitleProps {
    title?: string;
    subtitle?: boolean;
    titleActions?: React.ReactNode;
}

export default class SectionTitle extends React.Component<ISectionTitleProps> {
    public static defaultProps: Partial<ISectionTitleProps> = {
        subtitle: false 
    };

    public render(): React.ReactNode {
        const { title, titleActions } = this.props;
        if (!title && !titleActions) {
            return null;
        }

        return (
            <div style={{padding: '0 1rem', display: 'flex', flexDirection: 'row', alignItems: 'center', flexWrap: 'wrap', justifyContent: 'space-between'}}>
                {title !== undefined && <Typography variant={this.props.subtitle ? 'subtitle2' : 'h6'} gutterBottom>{title}</Typography>}
                {titleActions}
            </div>
        );
    }
}
