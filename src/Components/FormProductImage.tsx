import { Button, TextField } from '@material-ui/core';
import * as React from 'react';
import Catalog from '../Services/Catalog';
import FCSConstants from '../Utils/FCSConstants';
import { ICreateFormSectionProps } from './CreateProduct';
import FileUploadInput from './FileUploadInput';
import FormSection from './FormSection';

export interface IFormProductImageProps extends ICreateFormSectionProps {
    image: File;
    updateImage(image?: File): void;
}

export default class FormProductImage extends React.Component<IFormProductImageProps> {
    private removeImage = () => {
        this.props.updateItem({ ...this.props.item, imageUri: '' });
        this.props.updateImage();
    }

    private updateImage = (image?: File) => {
        this.props.updateItem({ ...this.props.item, imageUri: '' });
        this.props.updateImage(image);
    }

    private renderActions = () => {
        if (!this.props.editMode) {
            return null;
        }

        return (
            <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                <Button style={FCSConstants.buttonSpacing} onClick={this.removeImage} color="primary">REMOVE</Button>
                <FileUploadInput buttonProps={{ style: FCSConstants.buttonSpacing }} onUpdate={this.updateImage} />
            </div>
        );
    }

    public render(): React.ReactNode {
        const { image, item } = this.props;
        const imageUrl = Boolean(image) ? URL.createObjectURL(image) : item.imageUri || '';
        return (
            <FormSection title="Product Image"
                         description="Add a product image to be displayed on the mobile app."
                         actions={this.renderActions()}>
                {Boolean(imageUrl) && <img src={imageUrl} style={{maxWidth: '400px', margin: '1rem 0'}} />}
                {!Boolean(imageUrl) && <div style={{ margin: '2rem', textAlign: 'center' }}>No Image Uploaded</div>}
            </FormSection>
        ) ;
    }
}
