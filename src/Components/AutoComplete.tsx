import { MenuItem, Popover, TextField } from '@material-ui/core';
import { TextFieldProps } from '@material-ui/core/TextField';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import FCSConstants from '../Utils/FCSConstants';

export interface IAutoCompleteProps {
    suggestions: string[];
    onUpdate(value: string): void;
    maxItems?: number;
}

export interface IAutoCompleteState {
    anchorEl: HTMLElement;
    suggestions: string[];
    selected: string;
}

export default class AutoComplete extends React.Component<IAutoCompleteProps & TextFieldProps, IAutoCompleteState> {
    public static readonly defaultProps: Partial<IAutoCompleteProps> = {
        maxItems: 100
    };

    private readonly textFieldRef: React.RefObject<HTMLInputElement> = React.createRef();
    constructor(props: IAutoCompleteProps & TextFieldProps) {
        super(props);
        this.state = {
            anchorEl: undefined,
            suggestions: props.suggestions,
            selected: undefined
        };
    }

    public componentDidMount(): void {
        window.addEventListener('keydown', this.handleKeyDown);
    }
    
    public componentWillUnmount(): void {
        window.removeEventListener('keydown', this.handleKeyDown);
    }
    
    public componentWillMount(): void {
        const suggestions = this.getSuggestions(String(this.props.value));
        this.setState({ suggestions });
    }

    public componentDidUpdate(): void {
        setTimeout(() => {
            const currentMenuItem = document.querySelector(`[data-value='${this.state.selected}']`);
            if (currentMenuItem) {
                currentMenuItem.scrollIntoView();
            }
        }, 100);
    }

    private handleKeyDown = (event: any): void => {
        if (this.state.anchorEl) {
            switch (event.keyCode) {
                case 40:
                    // down
                    this.focusNext();
                    event.preventDefault();
                    event.stopPropagation();
                    break;
                case 38:
                    // up
                    this.focusPrevious();
                    event.preventDefault();
                    event.stopPropagation();
                    break;
                case 27:
                    // esc
                    this.setState({ anchorEl: undefined });
                    event.preventDefault();
                    event.stopPropagation();
                    break;
                case 9:
                    // tab
                case 13:
                    // enter
                    this.onSuggestionClicked(this.state.selected)();
                    break;
                default:
            }
        }
    }

    private focusNext(): void {
        const { suggestions } = this.state;
        if (Util.isNonEmptyArray(suggestions)) {
            const end = suggestions.length < this.props.maxItems ? suggestions.length : this.props.maxItems;
            const focusedIndex: number = (this.state.suggestions.indexOf(this.state.selected) + 1) % end;
            this.setState({ selected: this.state.suggestions[focusedIndex] });
        }
    }

    private focusPrevious(): void {
        const { maxItems } = this.props;
        const { suggestions } = this.state;
        if (Util.isNonEmptyArray(suggestions)) {
            const end = suggestions.length < maxItems ? suggestions.length : maxItems;
            let focusedIndex: number = this.state.suggestions.indexOf(this.state.selected) - 1;
            focusedIndex = focusedIndex < 0 ? end - 1 : focusedIndex;
            this.setState({ selected: this.state.suggestions[focusedIndex] });
        }
    }
    
    private onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { selected } = this.state;
        if (typeof this.props.onUpdate === 'function') {
            this.props.onUpdate(e.target.value);
        }
        const suggestions = this.getSuggestions(e.target.value);
        const selectedSuggestion = selected !== undefined && suggestions.indexOf(selected) > -1 ? selected : suggestions[0];
        this.setState({ anchorEl: e.currentTarget, suggestions, selected: selectedSuggestion });
    }

    private hideSuggestions = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ anchorEl: undefined, selected: undefined }, () => {
            if (typeof this.props.onBlur === 'function') {
                this.props.onBlur(e);
            }
        });
    }

    private onSuggestionClicked = (value: string) => (e?: React.MouseEvent<HTMLLIElement>) => {
        this.setState({ anchorEl: undefined, selected: undefined }, () => {
            this.props.onUpdate(value); 
        });
    }

    private getSuggestions = (value: string) => {
        const { suggestions } = this.props;
        return Linq2.where(suggestions, s => s.toLowerCase().includes(String(value).toLowerCase())).toArray();
    }

    public render(): React.ReactNode {
        // Pass through text field props in ...rest.
        const { suggestions, onUpdate, maxItems, ...rest } = this.props;
        const suggestionList = Util.isNonEmptyArray(this.state.suggestions) ? this.state.suggestions.slice(0, maxItems) : [];
        return (
            <>
                <TextField onBlur={this.hideSuggestions}
                           {...rest}
                           inputRef={this.textFieldRef}
                           onChange={this.onChange} />
                {Boolean(this.state.anchorEl) && Util.isNonEmptyArray(this.state.suggestions) &&
                <Popover open={true}
                         disableAutoFocus={true}
                         anchorEl={this.state.anchorEl}
                         style={{ zIndex: 10000 }}
                         PaperProps={{ style: { width: this.state.anchorEl.offsetWidth }}}
                         anchorOrigin={{
                             vertical: 'bottom',
                             horizontal: 'left'
                         }}
                         transformOrigin={{
                             vertical: 'top',
                             horizontal: 'left'
                         }}>
                    {suggestionList.map(s => <MenuItem selected={this.state.selected === s}
                                                       onMouseDown={this.onSuggestionClicked(s)}
                                                       data-value={s}
                                                       key={s}
                                                       value={s}>{s}</MenuItem>)}
                    <div style={{padding: '1rem', borderTop: `1px solid ${FCSConstants.grayLight}`}}>
                        Displaying <b>{suggestionList.length}</b> of <b>{this.state.suggestions.length}</b> items.
                    </div>
                </Popover>}
            </>
        );
    }
}
