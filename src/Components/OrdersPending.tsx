import { Button, FormGroup, IconButton, MenuItem, Popover, TextField, Tooltip } from '@material-ui/core';
import { FilterList, MoreVert } from '@material-ui/icons';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import DispatchService from '../Services/DispatchService';
import { IDispatch } from '../Typings/IDispatch';
import { IDispatchStatus } from '../Typings/IDispatchStatus';
import { IRelease } from '../Typings/IRelease';
import FCSConstants from '../Utils/FCSConstants';
import { formatDateString, getNestedValue } from '../Utils/Util';
import DataTable, { IDataTableColumn, SortOrder } from './DataTable';
import ModalCheckInventory, { IModalCheckInventoryProps } from './ModalCheckInventory';
import ModalCreateDispatch, { IModalCreateDispatchProps } from './ModalCreateDispatch';
import SectionTitle from './SectionTitle';

export interface IOrdersPendingProps {
    stock: Map<string, IStock[]>;
    productItems: Map<string, IProductItem>;
    selectedClient: string;
    productionLines: Map<string, IProductionLine>;
    fetchOrders(): void;
    setSuccessMessage(message: string): void;
    setErrorMessage(message: string): void;
    nonArchivedReleases: Map<string, IRelease>;
    activeDispatchStatus: Map<string, IDispatchStatus>;
    releases: Map<string, IRelease>;
    dispatches: Map<string, IDispatch>;
}

export interface IOrdersPendingState {
    checkInventory: IModalCheckInventoryProps;
    createDispatch: IModalCreateDispatchProps;
    sortBy: string;
    sortOrder: SortOrder;
    filterQuantity: number;
    selectedDate: string;
    selectedRange: DateRangeEnum;
    showFilters: boolean;
    selectedOrders: IRelease[];
    menuAnchor: HTMLElement;
    orderPopoverKey: string;
}

export enum DateRangeEnum {
    On = '0',
    OnOrBefore = '-1'
}

export default class OrdersPending extends React.Component<IOrdersPendingProps, IOrdersPendingState> {
    constructor(props: IOrdersPendingProps) {
        super(props);
        this.state = {
            checkInventory: undefined,
            createDispatch: undefined,
            sortBy: 'shipBy',
            sortOrder: SortOrder.Asc,
            filterQuantity: undefined,
            selectedDate: undefined,
            selectedRange: DateRangeEnum.On,
            showFilters: false,
            selectedOrders: [],
            menuAnchor: undefined,
            orderPopoverKey: undefined
        };
    }

    public componentWillReceiveProps = (nextProps: IOrdersPendingProps) => {
        if (this.props.selectedClient !== nextProps.selectedClient) {
            this.setState({ 
                checkInventory: undefined,
                createDispatch: undefined,
                sortBy: 'shipBy',
                sortOrder: SortOrder.Asc,
                filterQuantity: undefined,
                selectedDate: undefined,
                selectedRange: DateRangeEnum.On,
                showFilters: false,
                selectedOrders: []
            });
        }
    }

    private handleItemsSelect = (selectedOrders: IRelease[]) => {
        this.setState({ selectedOrders });
    }

    private showModalCheckInventory = (orders?: IRelease[]) => async (): Promise<void> => {
        const promise: Promise<void> = new Promise((confirm) => {
            this.setState({ checkInventory: { confirm, orders: Util.isNonEmptyArray(orders) ? orders : this.state.selectedOrders } });
        });
        try {
            await promise;
        } catch (e) {
            console.error(e);
        }
        this.setState({ checkInventory: undefined });
    }

    private showCreateDispatchModal = (orders: IRelease[]) => async (): Promise<void> => {
        const promise: Promise<IDispatchInput> = new Promise((confirm, cancel) => {
            const { selectedDate, selectedRange } = this.state;
            const { selectedClient, productItems, stock, productionLines, dispatches } = this.props;
            this.setState({
                createDispatch: {
                    confirm,
                    cancel,
                    orders,
                    selectedDate,
                    selectedRange,
                    selectedClient,
                    productItems,
                    stock,
                    productionLines,
                    dispatches
                }
            });
        });
        try {
            const dispatchInput: IDispatchInput = await promise;
            try {
                await DispatchService.createDispatch(dispatchInput);
                this.props.setSuccessMessage('Dispatch successfully created');
                this.props.fetchOrders();
                this.setState({ selectedOrders: [], createDispatch: undefined, menuAnchor: undefined, orderPopoverKey: undefined });
            } catch (err) {
                this.props.setErrorMessage('Failed to create dispatch');
                throw new Error(err);
            }
        } catch (e) {
            // ignore
        }

        this.setState({ createDispatch: undefined });
    }

    private showMenu = (orderPopoverKey?: string) => (e: React.MouseEvent<HTMLElement>) => {
        this.setState({ menuAnchor: e.currentTarget, orderPopoverKey });
    }

    private hideMenu = () => {
        this.setState({ menuAnchor: undefined, orderPopoverKey: undefined });
    }

    private renderTitleActions = () => {
        if (Util.isNonEmptyArray(this.state.selectedOrders)) {
            return (
                <>
                    <IconButton onClick={this.showMenu()}>
                        <MoreVert />
                    </IconButton>
                    <Popover open={Boolean(this.state.menuAnchor) && !Boolean(this.state.orderPopoverKey)}
                             anchorEl={this.state.menuAnchor}
                             anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                             transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                             onClose={this.hideMenu}>
                        <MenuItem onClick={this.showCreateDispatchModal(this.state.selectedOrders)}
                                  disabled={this.props.activeDispatchStatus.size > 0}>Create Dispatch</MenuItem>
                        <MenuItem onClick={this.showModalCheckInventory(this.state.selectedOrders)}>Check Inventory</MenuItem>
                    </Popover>
                </>
            );
        }
    }

    private handleSort = (property: string): void => {
        const { sortOrder,  sortBy } = this.state;
        const order = (property !== sortBy || sortOrder === SortOrder.Desc) ? SortOrder.Asc : SortOrder.Desc;
        this.setState({ sortOrder:  order, sortBy: property });
    }

    private getFilteredSortedData = (): IRelease[] => {
        const { releases } = this.props;
        const { sortOrder,  sortBy, filterQuantity, selectedDate, selectedRange } = this.state;
        let modified: IRelease[] = [ ...releases.values() ];

        // First find releases that fall within the specified date range.
        if (selectedDate !== undefined) {
            if  (selectedRange === DateRangeEnum.On) {
                modified = modified.filter(d => d.shipBy && (d.shipBy.localeCompare(selectedDate) === 0)); 
            }
            if (selectedRange === DateRangeEnum.OnOrBefore) {
                modified = modified.filter(d => d.shipBy && (d.shipBy.localeCompare(selectedDate) <= 0)); 
            }
        }

        // Next sort the releases
        if (Util.isNonEmptyArray(modified)) {
            modified.sort((a: IRelease, b: IRelease) => (
            String(getNestedValue(sortBy, a)).localeCompare(String(getNestedValue(sortBy, b))) * (sortOrder === SortOrder.Asc ? 1 : -1)));
        }

        // Finally filter releases by quantity
        if (filterQuantity !== undefined && filterQuantity >= 0) {
            modified = modified.slice(0, filterQuantity); 
        }

        return modified;
    }

    private setFilterQuantity = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const { value } = e.target;
        if (value !== '+' && value !== 'e') {
            this.setState({ filterQuantity: Number(value), selectedOrders: [] }); 
        }
    }

    private clearFilterQuantity = (): void => {
        this.setState({ filterQuantity: undefined }); 
    }

    private setFilterDate = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ selectedDate: e.target.value, selectedOrders: [] }); 
    }

    private clearSelectedDate = (): void => {
        this.setState({ selectedDate: undefined, selectedRange: DateRangeEnum.On, selectedOrders: [] }); 
    }

    private setSelectedRange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ selectedRange: e.target.value as DateRangeEnum }); 
    }

    private toggleShowFilters = () => {
        this.setState({ showFilters: !this.state.showFilters }); 
    }

    private renderFilters = (releases: IRelease[]): React.ReactNode => {
        const { filterQuantity,  selectedDate, selectedRange } = this.state;
        const dateList: string[] = Linq2.select(releases, d => d.shipBy).distinct().toArray();
        const flexCenter: React.CSSProperties = { display:  'flex', flexDirection: 'row', alignItems: 'center' };

        return (
            <>
                <FormGroup style={flexCenter}>
                    <p style={{fontWeight: 'bold', width: '170px'}}>Ship By Date: </p>
                    <TextField select
                               style={{ ...FCSConstants.textFieldStyle, maxWidth: '150px' }}
                               variant="outlined"
                               value={selectedRange || ''}
                               onChange={this.setSelectedRange}>
                        <MenuItem value={DateRangeEnum.On}>On</MenuItem>
                        <MenuItem value={DateRangeEnum.OnOrBefore}>On or before</MenuItem>
                    </TextField>
                    <TextField select
                               style={{ ...FCSConstants.textFieldStyle, maxWidth: '200px' }}
                               disabled={!Util.isNonEmptyArray(dateList)}
                               variant="outlined"
                               label="Select Date"
                               value={selectedDate || ''}
                               onChange={this.setFilterDate}>
                        {Util.isNonEmptyArray(dateList) &&
                        dateList.map(d => <MenuItem key={d} value={d}>{d}</MenuItem>)}
                    </TextField>
                    <Button color="primary" onClick={this.clearSelectedDate}>Clear</Button>
                </FormGroup>
                <FormGroup style={flexCenter}>
                    <p style={{fontWeight: 'bold', width: '170px'}}>First N Orders: </p>
                    <TextField type="number"
                               style={{ ...FCSConstants.textFieldStyle, maxWidth: '150px' }}
                               variant="outlined"
                               placeholder="Enter Quantity"
                               InputProps={{ inputProps: { min: '0' } }}
                               value={filterQuantity !== undefined ? Number(filterQuantity) : ''}
                               onChange={this.setFilterQuantity} />
                    <Button color="primary" onClick={this.clearFilterQuantity}>Clear</Button>
                </FormGroup>
            </>
        );
    }

    private renderSubtitle = (filtered: IRelease[] = []) => {
        const { releases } = this.props;

        return (
            <div style={{display: 'flex', flexDirection: 'column'}}>
                <SectionTitle title={`Filters (SHOWING ${(filtered || []).length} OF ${(releases || new Map()).size})`}
                              subtitle
                              titleActions={
                              <Tooltip title={this.state.showFilters ? 'Hide Filters' : 'Show Filters'}>
                                    <IconButton onClick={this.toggleShowFilters}>
                                        <FilterList />
                                    </IconButton>
                              </Tooltip>} />
                {this.state.showFilters && <div style={{ padding: '0 1rem' }}>{this.renderFilters([ ...releases.values() ])}</div>}
            </div>
        );
    }

    public render(): React.ReactNode {
        const { checkInventory, createDispatch, selectedOrders } = this.state;
        const columns: IDataTableColumn<IRelease>[] = [
            {
                title: 'Order ID',
                value: (item: IRelease) => item.orderId,
                headerClicked: () => this.handleSort('orderId'),
                sortOrder: this.state.sortBy === 'orderId' ? this.state.sortOrder : undefined
            },
            {
                title: 'Client',
                value: (item: IRelease) => item.clientName,
                headerClicked: () => this.handleSort('clientName'),
                sortOrder: this.state.sortBy === 'clientName' ? this.state.sortOrder : undefined
            },
            {
                title: 'Guest Name',
                value: (item: IRelease) => item.address && item.address.name || '',
                headerClicked: () => this.handleSort('address.name'),
                sortOrder: this.state.sortBy === 'address.name' ? this.state.sortOrder : undefined
            },
            {
                title: 'Ship By',
                value: (item: IRelease) => formatDateString(item.shipBy),
                headerClicked: () => this.handleSort('shipBy'),
                sortOrder: this.state.sortBy === 'shipBy' ? this.state.sortOrder : undefined
            }
        ];

        const data = this.getFilteredSortedData();

        return (
            <>
                {Boolean(checkInventory) && <ModalCheckInventory {...checkInventory} />}
                {Boolean(createDispatch) && <ModalCreateDispatch {...createDispatch} />}
                <DataTable title="Orders Pending Dispatch"
                           subtitle={this.renderSubtitle(data)}
                           titleActions={this.renderTitleActions()}
                           paginate
                           data={data}
                           selectedItems={selectedOrders}
                           handleItemsSelect={this.handleItemsSelect}
                           columns={columns} />
            </>
        );
    }
}
