import { FlatButton, MenuItem, Paper, RaisedButton, SelectField } from 'material-ui';
import { AutoBindingComponent, Col, Grid, Row, Util } from 'o8-common';
import * as React from 'react';
import DelayedInput from './DelayedInput';

import { QueryBuilderComparison } from '../Typings/QueryBuilderComparison';
import { QueryBuilderOperation } from '../Typings/QueryBuilderOperation';

import AddIcon from 'material-ui/svg-icons/content/add';
import AddBoxIcon from 'material-ui/svg-icons/content/add-box';
import ClearIcon from 'material-ui/svg-icons/content/clear';
import RemoveCircleIcon from 'material-ui/svg-icons/content/remove-circle';
import CloudDownload from 'material-ui/svg-icons/file/cloud-download';
import CheckboxIcon from 'material-ui/svg-icons/toggle/check-box';
import { IQueryBuilderGroup } from '../Typings/IQueryBuilderGroup';
import { IQueryBuilderProps } from '../Typings/IQueryBuilderProps';
import { IQueryBuilderRule } from '../Typings/IQueryBuilderRule';

const isNonEmptyArray = Util.isNonEmptyArray;

export default class QueryBuilder extends AutoBindingComponent<IQueryBuilderProps> {
    public static defaultProps: Partial<IQueryBuilderProps> = {
        showRemoveButton: false,
        showResetButton: false
    };

    public static isRule(rule: IQueryBuilderGroup | IQueryBuilderRule): boolean {
        return (rule as IQueryBuilderGroup).type === undefined;
    }

    public static isGroup(rule: IQueryBuilderGroup | IQueryBuilderRule): boolean {
        return !QueryBuilder.isRule(rule);
    }

    public static countActiveRules(filters: IQueryBuilderGroup[]): number {
        let count = 0;
        if (filters !== undefined) {
            for (const filter of filters) {
                if (filter !== undefined) {
                    const groups: IQueryBuilderGroup[] = [];
                    filter.rules.forEach(x => {
                        if (QueryBuilder.isRule(x)) {
                            if ((x as IQueryBuilderRule).field !== undefined && (x as IQueryBuilderRule).value !== undefined) {
                                count++;
                            }
                        } else {
                            groups.push(x as IQueryBuilderGroup);
                        }
                    });
                    count += groups.length > 0 ? QueryBuilder.countActiveRules(groups) : 0;
                }
            }
        }
        return count;
    }

    public static applyFilter(items: any[] | Iterable<any>, filters: IQueryBuilderGroup[]): any[] {
        if (!Array.isArray(filters) || filters.length === 0) {
            return Array.isArray(items) ? items : [ ...items ];
        }

        const filtered: Set<any> = new Set(items);
        for (const filter of filters) {
            if (Array.isArray(filter.rules)) {
                const rules: IQueryBuilderRule[] = filter.rules.filter(QueryBuilder.isRule) as IQueryBuilderRule[];
                if (rules.length > 0) {
                    for (const item of filtered) {
                        const result: boolean = filter.type === QueryBuilderOperation.And
                            ? rules.every((rule: IQueryBuilderRule) => QueryBuilder.doFilter(item, rule))
                            : rules.some((rule: IQueryBuilderRule) => QueryBuilder.doFilter(item, rule));
                        if (!result) {
                            filtered.delete(item);
                        }
                    }
                }

                const groups: IQueryBuilderGroup[] = filter.rules.filter(QueryBuilder.isGroup) as IQueryBuilderGroup[];
                if (groups.length > 0) {
                    const results: any[] = QueryBuilder.applyFilter(filter.type === QueryBuilderOperation.And ? filtered : items, groups);
                    if (filter.type === QueryBuilderOperation.And) {
                        filtered.clear();
                    }
                    for (const item of results) {
                        filtered.add(item);
                    }
                }
            }
        }
        return [ ...filtered ];
    }

    private static doFilter(item: any, rule: IQueryBuilderRule): boolean {
        if (rule.field === undefined || rule.value === undefined) {
            return true;
        }

        const fields: string[] = rule.field.split('.');
        const value: any = fields.reduce((acc, field) => acc !== undefined ? acc[field] : undefined, item);
        let returnValue: boolean = false;

        switch (rule.op) {
            case QueryBuilderComparison.eq:
                returnValue = String(value) === rule.value;
                break;
            case QueryBuilderComparison.gt:
                returnValue = typeof value === 'number' ? Number(value) > Number(rule.value) : String(value).localeCompare(rule.value) > 0;
                break;
            case QueryBuilderComparison.lt:
                returnValue = typeof value === 'number' ? Number(value) < Number(rule.value) : String(value).localeCompare(rule.value) < 0;
                break;
            case QueryBuilderComparison.gte:
                returnValue = typeof value === 'number' ? Number(value) >= Number(rule.value) : String(value).localeCompare(rule.value) >= 0;
                break;
            case QueryBuilderComparison.lte:
                returnValue = typeof value === 'number' ? Number(value) <= Number(rule.value) : String(value).localeCompare(rule.value) <= 0;
                break;
            case QueryBuilderComparison.contains:
                returnValue = !rule.caseSensitive ? String(value).toLowerCase().includes(rule.value.toLowerCase())
                    : String(value).includes(rule.value);
                break;
            case QueryBuilderComparison.regex:
                const regexParts = rule.value.match(/^\/(.*?)\/([gimuy]*)$/);
                try {
                    const regex: RegExp = regexParts !== null ? new RegExp(regexParts[1], regexParts[2]) : new RegExp(rule.value);
                    returnValue = regex.test(value);
                } catch (e) {
                    returnValue = false;
                }
                break;
        }

        return rule.not ? !returnValue : returnValue;
    }

    private static getComparisonDisplayName(op: QueryBuilderComparison): string {
        switch (op) {
            case QueryBuilderComparison.eq: return 'Equal To';
            case QueryBuilderComparison.gt: return 'Greater Than';
            case QueryBuilderComparison.gte: return 'Greater Than or Equal To';
            case QueryBuilderComparison.lt: return 'Less Than';
            case QueryBuilderComparison.lte: return 'Less Than or Equal To';
            case QueryBuilderComparison.contains: return 'Inclusive of';
            case QueryBuilderComparison.regex: return 'Matching Expression';
        }
    }

    private onRuleChange(original: IQueryBuilderRule | IQueryBuilderGroup,
                         partial: Partial<IQueryBuilderRule | IQueryBuilderGroup>): void {
        const rules = this.props.group.rules.map(rule => {
            if (rule === original) {
                return { ...original, ...partial };
            } else {
                return rule;
            }
        });

        this.props.onChange({ ...this.props.group, rules });
    }

    private onChange(partial: Partial<IQueryBuilderGroup>): void {
        this.props.onChange({ ...this.props.group, ...partial });
    }

    private removeRule(original: IQueryBuilderRule | IQueryBuilderGroup) {
        const rules = this.props.group.rules.filter(rule => rule !== original);
        this.props.onChange({ ...this.props.group, rules });
    }

    private addRule(): void {
        const current: IQueryBuilderGroup = this.props.group;
        const newRule: IQueryBuilderRule = {
            not: false,
            op: QueryBuilderComparison.contains,
            field: this.props.fields[0].value,
            value: ''
        };
        if (!Array.isArray(current.rules)) {
            this.props.onChange({ ...current, rules: [ newRule ] });
        } else {
            this.props.onChange({ ...current, rules: [ ...current.rules, newRule ] });
        }
    }

    private addGroup(): void {
        const current: IQueryBuilderGroup = this.props.group;
        const newGroup: IQueryBuilderGroup = {
            type: QueryBuilderOperation.And,
            rules: [
                {
                    not: false,
                    op: QueryBuilderComparison.contains,
                    field: this.props.fields[0].value,
                    value: ''
                }
            ]
        };
        if (!Array.isArray(current.rules)) {
            this.props.onChange({ ...current, rules: [ newGroup ] });
        } else {
            this.props.onChange({ ...current, rules: [ ...current.rules, newGroup ] });
        }
    }

    private updateValue = (rule: IQueryBuilderRule, e: React.ChangeEvent<HTMLInputElement>): void => {
        console.log('eeeeeeeee:::::::::::', e);
        this.onRuleChange(rule, { value: e.target && e.target.value });
    }

    public render(): React.ReactNode {
        const { group } = this.props;
        const rules: IQueryBuilderRule[] = group.rules.filter(QueryBuilder.isRule) as IQueryBuilderRule[];
        const groups: IQueryBuilderGroup[] = group.rules.filter(QueryBuilder.isGroup) as IQueryBuilderGroup[];
        const style: React.CSSProperties = { margin: '10px', paddingBottom: '10px', ...this.props.style };
        return (
            <Paper style={style}>
                <Grid fluid>
                    <Row style={{ paddingTop: '10px' }}>
                        <Col xs={12} sm={4}>
                            <RaisedButton label="And" secondary={group.type === QueryBuilderOperation.And} style={{ width: '70px' }}
                                          icon={group.type === QueryBuilderOperation.And ? <CheckboxIcon/> : undefined}
                                          onClick={() => this.onChange({ type: QueryBuilderOperation.And })} />
                            <RaisedButton label="Or" secondary={group.type === QueryBuilderOperation.Or} style={{ width: '70px' }}
                                          icon={group.type === QueryBuilderOperation.Or ? <CheckboxIcon/> : undefined}
                                          onClick={() => this.onChange({ type: QueryBuilderOperation.Or })} />
                        </Col>
                        <Col xs={12} sm={8} style={{ textAlign: 'right' }}>
                            <FlatButton onClick={this.addRule} label="Rule" labelPosition="after" icon={<AddIcon/>} secondary />
                            <FlatButton onClick={this.addGroup} label="Group" labelPosition="after" icon={<AddBoxIcon/>} secondary />
                            {this.props.showRemoveButton && typeof this.props.onRemoveClicked === 'function' &&
                            <FlatButton onClick={this.props.onRemoveClicked} label="Group" labelPosition="after"
                                        icon={<RemoveCircleIcon/>} secondary />}
                            {this.props.showResetButton && typeof this.props.onResetClicked === 'function' &&
                            <FlatButton onClick={this.props.onResetClicked} label="Reset" labelPosition="after"
                                        icon={<ClearIcon/>} secondary />}
                            {this.props.showExportButton && typeof this.props.onExportClicked === 'function' &&
                            <FlatButton onClick={this.props.onExportClicked} label="Export" labelPosition="after"
                                        icon={<CloudDownload/>} secondary />}
                        </Col>
                    </Row>
                    {rules.map((rule: IQueryBuilderRule, index: number) =>
                    <Row key={index}>
                        <Col sm={3} xs={12}>
                            <SelectField floatingLabelText="Field" value={rule.field} fullWidth autoWidth
                                         onChange={(e, k, field) => this.onRuleChange(rule, { field })}>
                                {this.props.fields.sort((a, b) =>
                                    a.displayName.localeCompare(b.displayName)).map((f: MC.ISimpleDropdownValue, key: number) =>
                                    <MenuItem key={key} value={f.value} primaryText={f.displayName} />)}
                            </SelectField>
                        </Col>
                        <Col sm={2} xs={12}>
                            <SelectField floatingLabelText="Type" value={rule.not} fullWidth autoWidth
                                         onChange={(e, k, not) => this.onRuleChange(rule, { not })}>
                                <MenuItem value={false} primaryText="Is" />
                                <MenuItem value={true} primaryText="Is Not" />
                            </SelectField>
                        </Col>
                        <Col sm={2} xs={12}>
                            <SelectField floatingLabelText="Comparison" value={rule.op} fullWidth autoWidth
                                         onChange={(e, k, op) => this.onRuleChange(rule, { op })}>
                                {Object.keys(QueryBuilderComparison).map(key =>
                                    <MenuItem key={key} value={key}
                                              primaryText={QueryBuilder.getComparisonDisplayName(key as QueryBuilderComparison)} />)}
                            </SelectField>
                        </Col>
                        <Col sm={3} xs={9}>
                            <DelayedInput label="Value" value={rule.value}
                                          onChange={console.log} />
                        </Col>
                        <Col sm={2} xs={3} style={{ textAlign: 'right', paddingTop: '30px' }}>
                            {rules.length > 1 &&
                            <FlatButton onClick={() => this.removeRule(rule)} label="" labelPosition="after"
                                        icon={<RemoveCircleIcon/>} secondary />}
                        </Col>
                    </Row>)}
                </Grid>
                {isNonEmptyArray(groups) && groups.map((g: IQueryBuilderGroup, index: number) =>
                <QueryBuilder key={index} group={g} fields={this.props.fields} showRemoveButton
                              onRemoveClicked={() => this.removeRule(g)}
                              onChange={(changed: IQueryBuilderGroup) => this.onRuleChange(g, changed)} />)}
            </Paper>
        );
    }
}
