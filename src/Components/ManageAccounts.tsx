import * as React from 'react';
import FCSConstants from '../Utils/FCSConstants';
import View from './View';

export interface IManageLogisticsProps {
    environment: Record<string, any>;
}

export default class ManageLogistics extends React.Component<IManageLogisticsProps> {
    public render(): React.ReactNode {
        const { environment } = this.props;
        const url = Boolean(environment) && Boolean(environment.environment) && new URL(environment.environment[FCSConstants.authorizationUri]);
        return (
            <View title="Manage - Accounts">
                {Boolean(url) ? <a href={url.origin} target="_blank">View Account Management</a> : <div>URL not found.</div>}
            </View>
        );
    }
}
