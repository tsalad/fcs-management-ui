import { Badge } from '@material-ui/core';
import { NotificationImportant } from '@material-ui/icons';
import { Linq2 } from 'o8-common';
import * as React from 'react';
import { IProblemReport } from '../Typings/IProblemReport';

export interface INotificationsMenuItemProps {
    problemReports: Map<string, IProblemReport>;
    reorderAlerts: Map<string, IProductItem>;
}

export default class NotificationsMenuItem extends React.Component<INotificationsMenuItemProps> {
    public render(): React.ReactNode {
        const menuIconStyle: React.CSSProperties = {
            marginRight: '1rem'
        };

        const menuItemStyle: React.CSSProperties = {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center'
        };

        const problemReports = Linq2.where([ ...this.props.problemReports.values() ], r => !Boolean(r.closedAt)).toDictionary();

        return (
            <div style={menuItemStyle}>
                <Badge color="error"
                       max={99}
                       variant="dot"
                       invisible={!problemReports.size && !this.props.reorderAlerts.size}
                       style={menuIconStyle}>
                    <NotificationImportant />
                </Badge>
                Notifications
            </div>
        );
    }
}
