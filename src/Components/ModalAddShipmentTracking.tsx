import { MenuItem } from '@material-ui/core';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import OrdersFlaggedService from '../Services/OrdersFlaggedService';
import PackageService from '../Services/PackageService';
import ShipmentService from '../Services/ShipmentService';
import Shipping from '../Services/Shipping';
import { IOrderFlag } from '../Typings/IOrderFlag';
import { IShipment } from '../Typings/IShipment';
import { IShipmentPackage } from '../Typings/IShipmentPackage';
import FCSConstants from '../Utils/FCSConstants';
import EditableTextField from './EditableTextField';
import SimpleModal from './SimpleModal';

export interface IModalAddShipmentTrackingProps {
    orderId: string;
    orderFlag: IOrderFlag;
    confirm(): void;
    cancel(): void;
    setErrorMessage(message: string): void;
    setSuccessMessage(message: string): void;
}

interface IModalAddShipmentTrackingState {
    shipment: IShipment;
    shipmentPackages: Map<string, IShipmentPackage>;
    shippingCarrier: string;
    carriers: Map<string, IShippingMethod>;
    loading: boolean;
}

export default class ModalAddShipmentTracking extends React.Component<IModalAddShipmentTrackingProps, IModalAddShipmentTrackingState> {
    constructor(props: IModalAddShipmentTrackingProps) {
        super(props);
        this.state = {
            shipment: undefined,
            shipmentPackages: undefined,
            shippingCarrier: undefined,
            carriers: undefined,
            loading: false
        };
    }

    public componentWillMount = async () => {
        const { orderId } = this.props;
        if (Boolean(orderId)) {
            const carriers: Map<string, IShippingMethod> = Linq2.where(await Shipping.fetchShippingMethods() || [], m => m.serviceCode.includes('-EXT')).toDictionary(c => c.carrierCode);
            const shipment = await ShipmentService.fetchShipment(orderId);
            const shipmentPackages: Map<string, IShipmentPackage> = Boolean(shipment) && Linq2.toDictionary(shipment.packages || [], p => p.packageCode);
            this.setState({ shipment, shipmentPackages, carriers });
        }
    }

    private confirm = async () => {
        const { shippingCarrier, carriers, shipmentPackages, shipment } = this.state;
        this.setState({ loading: true });

        try {
            // First update shipment with the selected carrier
            try {
                const { orderId, priority, updatedAt, dispatchCode, state } = shipment;
                const { serviceCode } = carriers.get(shippingCarrier);
                await ShipmentService.updateShipment(orderId, { priority, updatedAt, serviceCode, dispatchCode, state });
            } catch (err) {
                this.props.setErrorMessage('Failed to update shipment');
            }
    
            // Next update all packages with the entered tracking #'s
            try {
                if (Boolean(shipmentPackages)) {
                    await Promise.all([ ...shipmentPackages.values() ].map(async p => PackageService.updatePackage(p)));
                }
            } catch (err) {
                this.props.setErrorMessage('Failed to update package');
            }
    
            // Finally delete the order flag
            try {
                await OrdersFlaggedService.deleteFlag(this.props.orderFlag.id);
            } catch (err) {
                this.props.setErrorMessage('Failed to delete order flag');
            }
            this.props.setSuccessMessage('Successfully added tracking numbers');
        } catch (err) {
            this.props.setErrorMessage('Failed to add tracking numbers');
        }

        this.setState({ loading: false });
        this.props.confirm();
    }

    private updateTackingNumber = (key: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
        const shipmentPackages = new Map(this.state.shipmentPackages);
        const updated = { ...shipmentPackages.get(key), trackingNumber: e.target.value };
        shipmentPackages.set(key, updated);
        this.setState({ shipmentPackages });
    }

    private renderPackage = (shipmentPackage: IShipmentPackage, i: number) => {
        return (
            <div style={{ ...FCSConstants.textFieldStyle, display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <div>{`Package #${i + 1}`}</div>
                <EditableTextField value={shipmentPackage.trackingNumber}
                                   style={{ width: '100%', maxWidth: '300px' }}
                                   onChange={this.updateTackingNumber(shipmentPackage.packageCode)}
                                   placeholder="Enter Tracking" />
            </div>
        );
    }

    private updateShippingCarrier = (e: React.ChangeEvent<HTMLSelectElement>) => {
        this.setState({ shippingCarrier: e.target.value });
    }

    public render(): React.ReactNode {
        const { shipment, shipmentPackages, carriers } = this.state;
        const packageList = Boolean(shipmentPackages) ? [ ...shipmentPackages.values() ] : [];

        const isValid = Boolean(this.state.shippingCarrier) && Linq2.all(Boolean(shipmentPackages) ? shipmentPackages.values() : [], p => Boolean(p.trackingNumber));

        return (
            <SimpleModal title="Add Tracking Numbers"
                         disableConfirmButton={!isValid || this.state.loading}
                         confirm={this.confirm}
                         cancel={this.props.cancel}>
                <EditableTextField editMode={false}
                                   label="Guest Name"
                                   style={FCSConstants.textFieldStyle}
                                   value={Boolean(shipment) && Boolean(shipment.toAddress) ? shipment.toAddress.name : ''} />
                <EditableTextField editMode={false}
                                   label="Order ID"
                                   style={FCSConstants.textFieldStyle}
                                   value={Boolean(shipment) ? shipment.orderId : ''} />
                <EditableTextField select
                                   label="Shipping Carrier"
                                   style={FCSConstants.textFieldStyle}
                                   value={this.state.shippingCarrier || ''}
                                   onChange={this.updateShippingCarrier}>
                    {Boolean(carriers) && [ ...carriers.values()].map(c => <MenuItem value={c.carrierCode} key={c.carrierCode}>{c.carrierCode}</MenuItem>)}
                </EditableTextField>
                {Util.isNonEmptyArray(packageList) ? packageList.map((p, i) => this.renderPackage(p, i)) : 'No packages found.'}
            </SimpleModal>
        );
    }
}
