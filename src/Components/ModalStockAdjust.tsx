import { Divider, MenuItem, TextField } from '@material-ui/core';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import NotificationsService from '../Services/NotificationsService';
import { EventCodeEnum } from '../Typings/EventCodeEnum';
import { IStockInput } from '../Typings/IStockInput';
import FCSConstants from '../Utils/FCSConstants';
import EditableTextField from './EditableTextField';
import FormMoveStock from './FormMoveStock';
import FormRecountStock from './FormRecountStock';
import SimpleModal from './SimpleModal';

export interface IModalStockAdjustProps {
    storage: Map<string, IStorage>;
    stock: Map<string, IStock[]>;
    fetchStorage(): void;
    fetchStock(): void;
    createStock(stockInput: IStockInput): Promise<void>;
    moveStock(stockInput: IStockMoveInput): Promise<void>;
    productItem: IProductItem;
    confirm(): void;
    cancel(): void;
}

export interface IModalStockAdjustState {
    adjustments: IStockInput[];
    move: IStockMoveInput;
    reason: EventCodeEnum;
}

const defaultAdjustment: IStockInput = {
    storageCode: '',
    sku: '',
    eventCode: EventCodeEnum.RECOUNT,
    adjustment: 0
};

const defaultMove: IStockMoveInput = {
    fromStorageCode: '',
    fromUpdatedAt: '',
    toStorageCode: '',
    toUpdatedAt: '',
    sku: '',
    quantity: 0
};

export default class ModalStockAdjust extends React.Component<IModalStockAdjustProps, IModalStockAdjustState> {
    constructor(props: IModalStockAdjustProps) {
        super(props);
        this.state = {
            reason: undefined,
            adjustments: [ { ...defaultAdjustment } ],
            move: { ...defaultMove }
        };
    }

    public componentDidMount() {
        this.props.fetchStock();
        this.props.fetchStorage();
    }

    private getUpdatedQuantity = (a: IStockInput): number => {
        const { sku, adjustment } = a;
        const currentStock = Linq2.firstOrDefault(this.props.stock.get(sku), undefined, s => s.storageCode === a.storageCode);
        const total = Boolean(currentStock) && Boolean(currentStock.quantity) ? currentStock.quantity : 0;
        return (Math.abs(total - adjustment) * (total > adjustment ? -1 : 1));
    }

    private getTotalStock = (sku: string) => {
        const currentStock = this.props.stock.get(sku);
        const storageStock = Util.isNonEmptyArray(currentStock) && Linq2.firstOrDefault(currentStock, undefined, s => s.storageCode === this.state.move.fromStorageCode);
        return Boolean(storageStock) ? storageStock.quantity : 0;
    }

    private confirm = async () => {
        try {
            const { reason, adjustments, move } = this.state;
            if (reason === EventCodeEnum.RECOUNT) {
                await Promise.all(adjustments.map(async a => this.props.createStock({ ...a, adjustment: this.getUpdatedQuantity(a) })));
            } else {
                this.props.moveStock(move);
            }
        } catch (err) {
            console.error(err);
        }
        NotificationsService.updateNotifications();
        this.props.confirm();
    }

    private updateAdjustments = (adjustments: IStockInput[]) => {
        this.setState({ adjustments });
    }

    private addAdjustment = () => {
        this.setState({ adjustments: [ ...this.state.adjustments, { ...defaultAdjustment, sku: this.props.productItem.sku } ] });
    }

    private renderRecount = () => {
        return <FormRecountStock productItem={this.props.productItem}
                                 storage={this.props.storage}
                                 stock={this.props.stock}
                                 adjustments={this.state.adjustments}
                                 addAdjustment={this.addAdjustment}
                                 update={this.updateAdjustments} />;
    }

    private updateMove = (move: IStockMoveInput) => {
        this.setState({ move });
    }

    private renderMove = () => {
        return <FormMoveStock productItem={this.props.productItem}
                              storage={this.props.storage}
                              stock={this.props.stock}
                              move={this.state.move}
                              update={this.updateMove} />;
    }

    private updateReason = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({
            adjustments: [ { ...defaultAdjustment, sku: this.props.productItem.sku } ],
            move: { ...defaultMove, sku: this.props.productItem.sku },
            reason: e.target.value as EventCodeEnum
        });
    }

    private isValid = () => {
        const { reason, adjustments, move } = this.state;
        if (!Boolean(reason)) {
            return false;
        }
        if (reason === EventCodeEnum.RECOUNT && (!Util.isNonEmptyArray(adjustments) || !adjustments.every(a => a.adjustment >= 0 && Boolean(a.storageCode)))) {
            return false;
        }
        if (reason === EventCodeEnum.MOVE && move.quantity <= 0 || this.getTotalStock(move.sku) < move.quantity) {
            return false;
        }
        return true;
    }

    public render(): React.ReactNode {
        const { stock, productItem, storage } = this.props;
        const { reason } = this.state;
        const currentStock = Boolean(stock) && Boolean(productItem) && stock.get(productItem.sku);

        return (
            <SimpleModal title="Adjust Stock" disableConfirmButton={!this.isValid()} confirm={this.confirm} cancel={this.props.cancel}>
                <p>Current stock information</p>
                {Util.isNonEmptyArray(currentStock) && currentStock.map((s, i) => (
                    <div key={`${s.storageCode}-${i}`} style={{ display: 'flex', flexDirection: 'row' }}>
                        <EditableTextField label={i === 0 ? 'Location' : ''}
                                           editMode={false}
                                           style={{padding: 0, margin: 0}}
                                           value={storage.get(s.storageCode).name} />
                        <EditableTextField label={i === 0 ? 'Quantity' : ''}
                                           editMode={false}
                                           style={{padding: 0, margin: 0}}
                                           value={s.quantity} />
                    </div>
                ))}
                <Divider />
                <p>To make a stock adjustment, select a reason first:</p>
                <TextField select
                           variant="outlined"
                           value={reason || ''}
                           onChange={this.updateReason}
                           style={{ ...FCSConstants.textFieldStyle, maxWidth: '250px' }}>
                    <MenuItem key={EventCodeEnum.RECOUNT} value={EventCodeEnum.RECOUNT}>Inventory Recount</MenuItem>
                    <MenuItem key={EventCodeEnum.MOVE} value={EventCodeEnum.MOVE}>Stock Move</MenuItem>
                </TextField>
                {Boolean(reason) && reason === EventCodeEnum.RECOUNT && this.renderRecount()}
                {Boolean(reason) && reason === EventCodeEnum.MOVE && this.renderMove()}
            </SimpleModal>
        );
    }
}
