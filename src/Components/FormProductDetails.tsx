import { Button, ListSubheader, MenuItem, TextField } from '@material-ui/core';
import { Edit } from '@material-ui/icons';
import { Util } from 'o8-common';
import * as React from 'react';
import { ProductItem } from '../Models';
import Category from '../Models/Category';
import Product from '../Models/Product';
import FCSConstants from '../Utils/FCSConstants';
import { ICreateFormSectionProps } from './CreateProduct';
import EditableTextField from './EditableTextField';
import FormSection from './FormSection';
import ModalCreateCategory, { IModalCreateCategoryProps } from './ModalCreateCategory';
import ModalCreateProduct, { IModalCreateProductProps } from './ModalCreateProduct';

export interface IFormProductDetailsProps extends ICreateFormSectionProps {
    products: Map<string, IProduct>;
    categories: Map<string, ICategory>;
    createProduct(product: IProduct): Promise<IProduct>;
    createCategory(category: ICategory): Promise<ICategory>;
}

interface IFormProductDetailsState {
    createCategoryModalProps: IModalCreateCategoryProps;
    createProductModalProps: IModalCreateProductProps;
}

export default class FormProductDetails extends React.Component<IFormProductDetailsProps, IFormProductDetailsState> {
    constructor(props: IFormProductDetailsProps) {
        super(props);
        this.state = {
            createCategoryModalProps: undefined,
            createProductModalProps: undefined
        };
    }

    private createCategory = async (): Promise<void> => {
        const promise: Promise<ICategory> = new Promise((confirm, cancel) => {
            this.setState({
                createCategoryModalProps: {
                    confirm,
                    cancel,
                    categories: this.props.categories,
                    category: new Category() } });
        });

        try {
            const category: ICategory = await promise;
            const newCategory: ICategory = await this.props.createCategory(category);
            this.props.updateItem({ ...this.props.item, categoryCode: newCategory.categoryCode, productCode: undefined });
        } catch (err) {
            // ignore
        } finally {
            this.setState({ createCategoryModalProps: undefined });
        }
    }

    private createProduct = async (): Promise<void> => {
        const promise: Promise<IProduct> = new Promise((confirm, cancel) => {
            this.setState({
                createProductModalProps: {
                    confirm,
                    cancel,
                    product: { ...new Product(), categoryCode: this.props.item.categoryCode},
                    products: this.props.products
                }
            });
        });

        try {
            const product: IProduct = await promise;
            const newProduct = await this.props.createProduct(product);
            this.props.updateItem({ ...this.props.item, productCode: newProduct.productCode });
        } catch (err) {
            // ignore
        } finally {
            this.setState({ createProductModalProps: undefined });
        }
    }

    private updateCategory = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.props.updateItem({ ...this.props.item, categoryCode: e.target.value, productCode: undefined });
    }

    private updateProduct = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.props.updateItem({ ...this.props.item, productCode: e.target.value });
    }

    private updateItem = (property: string) => (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.props.updateItem({ ...this.props.item, [property]: e.target.value });
    }

    private updateWeight = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.props.updateItem({ ...this.props.item, weightOunces: Number(e.target.value) });
    }

    public render(): React.ReactNode {
        const { categories, products, item, showValidationErrors, validator, editMode } = this.props;
        const { name, productCode, categoryCode, weightOunces } = item;

        const categoryList = [ ...categories.values() ];
        const productList = item.categoryCode ? [ ...products.values() ].filter(p => p.categoryCode === item.categoryCode) : [];
        const categoryValue = editMode ? categoryCode : this.props.categories.get(categoryCode).name;
        const productValue = editMode ? productCode : this.props.products.get(productCode).name;

        const nameValidator = showValidationErrors && validator.getResults().get('name');
        const productCodeValidator = showValidationErrors && validator.getResults().get('productCode');
        const weightOuncesValidator = showValidationErrors && validator.getResults().get('weightOunces');

        return (
            <>
                {this.state.createCategoryModalProps !== undefined && <ModalCreateCategory {...this.state.createCategoryModalProps} />}
                {this.state.createProductModalProps !== undefined && <ModalCreateProduct {...this.state.createProductModalProps} />}
                <FormSection title="Product Details"
                             description="Add basic product details.  Check to see if the product already exists.">
                    <EditableTextField variant="outlined"
                                       label={Reflect.getMetadata('displayName', ProductItem.prototype, 'name')}
                                       editMode={editMode}
                                       required
                                       helperText={nameValidator && !nameValidator.valid ? nameValidator.message : 'Name of the product'}
                                       margin="normal"
                                       style={{...FCSConstants.textFieldStyle}}
                                       value={name}
                                       error={nameValidator && !nameValidator.valid}
                                       onChange={this.updateItem('name')} />
                    <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                        <EditableTextField select
                                           SelectProps={{ MenuProps: { MenuListProps: { style: { paddingBottom: '0' } } } }}
                                           editMode={editMode}
                                           variant="outlined"
                                           style={{...FCSConstants.textFieldStyle, maxWidth: '300px'}}
                                           label={Reflect.getMetadata('displayName', ProductItem.prototype, 'categoryCode')}
                                           required
                                           helperText="Select from list or add new"
                                           value={categoryValue || ''}
                                           onChange={this.updateCategory}>
                            {Util.isNonEmptyArray(categoryList) && categoryList.map(c => <MenuItem key={c.categoryCode} value={c.categoryCode}>{c.name}</MenuItem>)}
                            <ListSubheader style={FCSConstants.listFooterStyle}>
                                <Button onClick={this.createCategory} style={FCSConstants.listFooterButtonStyle}>
                                    Add New
                                    <Edit />
                                </Button>
                            </ListSubheader>
                        </EditableTextField>
                    </div>
                    <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                        <EditableTextField select
                                           SelectProps={{ MenuProps: { MenuListProps: { style: { paddingBottom: '0' } } } }}
                                           editMode={editMode}
                                           disabled={item.categoryCode === undefined}
                                           variant="outlined"
                                           style={{...FCSConstants.textFieldStyle, maxWidth: '300px'}}
                                           label={Reflect.getMetadata('displayName', ProductItem.prototype, 'productCode')}
                                           required
                                           helperText={productCodeValidator && !productCodeValidator.valid ? productCodeValidator.message : 'Select from list or add new'}
                                           value={productValue || ''}
                                           error={productCodeValidator && !productCodeValidator.valid}
                                           onChange={this.updateProduct}>
                            {Util.isNonEmptyArray(productList) && productList.map(p => <MenuItem key={p.productCode} value={p.productCode}>{p.name}</MenuItem>)}
                            <ListSubheader style={FCSConstants.listFooterStyle}>
                                <Button onClick={this.createProduct} style={FCSConstants.listFooterButtonStyle}>
                                    Add New
                                    <Edit />
                                </Button>
                            </ListSubheader>
                        </EditableTextField>
                    </div>
                    <EditableTextField variant="outlined"
                                       label={Reflect.getMetadata('displayName', ProductItem.prototype, 'weightOunces')}
                                       editMode={editMode}
                                       required
                                       type="number"
                                       helperText={weightOuncesValidator && !weightOuncesValidator.valid ? weightOuncesValidator.message : '(e.g. 3.25 oz)'}
                                       InputProps={{ endAdornment:  <span>oz</span>, inputProps: { min: 0 } }}
                                       margin="normal"
                                       style={{...FCSConstants.textFieldStyle, maxWidth: '300px'}}
                                       value={weightOunces !== null && weightOunces !== undefined ? editMode ? weightOunces : weightOunces.toFixed(2) : ''}
                                       error={weightOuncesValidator && !weightOuncesValidator.valid}
                                       onChange={this.updateWeight} />
                </FormSection>
            </>
        );
    }
}
