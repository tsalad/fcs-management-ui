import { getMuiTheme } from 'o8-common';
import * as React from 'react';

const legacyTheme = getMuiTheme();

export interface ITileProps {
    title: React.ReactNode;
    style: React.CSSProperties;
}

export default class Tile extends React.Component<ITileProps> {
    public static readonly defaultProps: Partial<ITileProps> = { title: '' };

    public render(): React.ReactNode {
        const style = {
            border: `3px solid ${legacyTheme.palette.primary1Color}`,
            display: 'flex',
            padding: '1rem',
            flexFlow: 'column nowrap'
        };

        return (
            <div style={{ ...style, ...this.props.style }}>
                {this.props.title}
                {this.props.children}
            </div>
        );
    }
}
