import { Divider } from '@material-ui/core';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import ProductAvailabilityList from '../Contexts/ProductAvailabilityList';
import { IDispatch } from '../Typings/IDispatch';
import SimpleModal from './SimpleModal';

export interface IModalFulfillDispatchProps {
    confirm(): void;
    cancel(): void;
    dispatch: IDispatch;
    productionLines: Map<string, IProductionLine>;
}

interface IModalFulfillDispatchState {
    isSaving: boolean;
}

export default class ModalFulfillDispatch extends React.Component<IModalFulfillDispatchProps, IModalFulfillDispatchState> {
    constructor(props: IModalFulfillDispatchProps) {
        super(props);
        this.state = {
            isSaving: false
        };
    }

    private confirm = () => {
        this.setState({ isSaving: true });
        this.props.confirm();
    }

    public render(): React.ReactNode {
        const { dispatch, productionLines } = this.props;
        const packages = Linq2.selectMany(dispatch.shipments, s => s.packages).toArray();

        return (
            <SimpleModal confirm={this.confirm}
                         disableConfirmButton={this.state.isSaving}
                         disableCancelButton={this.state.isSaving}
                         confirmButtonText="FULFILL"
                         cancelButtonText="CANCEL"
                         cancel={this.props.cancel}
                         title="Fulfill Dispatch">
                <p>Dispatch Name: <strong>{dispatch.dispatchCode}</strong></p>
                <p>Total # of orders: <strong>{Util.isNonEmptyArray(packages) && packages.length}</strong></p>
                <p>Production Line: <strong>{productionLines.has(dispatch.productionCode) && productionLines.get(dispatch.productionCode).name}</strong></p>
                <Divider />
                    <ProductAvailabilityList shipments={packages} />
                <Divider />
            </SimpleModal>
        );
    }
}
