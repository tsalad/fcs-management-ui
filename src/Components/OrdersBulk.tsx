import { Button, IconButton, MenuItem, Popover, Typography } from '@material-ui/core';
import { MoreVert } from '@material-ui/icons';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import ReleaseService from '../Services/ReleaseService';
import ShipmentService from '../Services/ShipmentService';
import { IOrderFlag } from '../Typings/IOrderFlag';
import { IRelease } from '../Typings/IRelease';
import { IShipment } from '../Typings/IShipment';
import { StateEnum } from '../Typings/StateEnum';
import FCSConstants from '../Utils/FCSConstants';
import { formatDateString, getNestedValue } from '../Utils/Util';
import DataTable, { IDataTableColumn, SortOrder } from './DataTable';
import SimpleModal from './SimpleModal';

export interface IOrdersBulkProps {
    nonArchivedReleases: Map<string, IRelease>;
    selectedClient: string;
    ordersFlagged: Map<string, IOrderFlag>;
    editOrder(release: IRelease): () => void;
    fetchOrders(): void;
    setSuccessMessage(message: string): void;
    setErrorMessage(message: string): void;
}

interface IFulfillBulkOrderModalProps {
    confirm(): void;
    cancel(): void;
    order: IRelease;
}

interface ICancelOrderModalProps {
    confirm(): void;
    cancel(): void;
    order: IRelease;
}

interface IOrdersBulkState {
    fulfillBulkOrderModalProps: IFulfillBulkOrderModalProps;
    orderAnchor: string;
    menuAnchor: HTMLElement;
    cancelOrderModalProps: ICancelOrderModalProps;
    sortBy: string;
    sortOrder: SortOrder;
}

export default class OrdersBulk extends React.Component<IOrdersBulkProps, IOrdersBulkState> {
    constructor(props: IOrdersBulkProps) {
        super(props);
        this.state = {
            fulfillBulkOrderModalProps: undefined,
            orderAnchor: undefined,
            menuAnchor: undefined,
            cancelOrderModalProps: undefined,
            sortBy: 'shipBy',
            sortOrder: SortOrder.Asc
        };
    }

    private showFulfillBulkOrderModal = (order: IRelease) => {
        return async () => {
            const promise: Promise<void> = new Promise((confirm, cancel) => {
                this.setState({ fulfillBulkOrderModalProps: { confirm, cancel, order } });
            });
            try {
                await promise;
                try {
                    await ShipmentService.createShipment({
                        serviceCode: null,
                        dispatchCode: null,
                        orderId: order.orderId,
                        priority: 0,
                        fulfillmentDepotCode: FCSConstants.depotCode,
                        createPackages: false
                    });
                    this.props.fetchOrders();
                    this.props.setSuccessMessage('Bulk Order successfully updated');
                } catch (e) {
                    this.props.setErrorMessage('Bulk Order failed to update');
                    throw new Error(e);
                }
            } catch (e) {
                // ignore
            }
            this.setState({ fulfillBulkOrderModalProps: undefined });
        };
    }

    private showCancelOrderModal = (order: IRelease) => {
        return async () => {
            const promise: Promise<void> = new Promise((confirm, cancel) => {
                this.setState({ cancelOrderModalProps: { confirm, cancel, order } });
            });
            try {
                await promise;
                try {
                    await ReleaseService.deleteRelease(order.orderId);
                    this.props.fetchOrders();
                    this.props.setSuccessMessage('Bulk Order successfully cancelled');
                } catch (e) {
                    this.props.setErrorMessage('Failed to cancel Bulk Order');
                    throw new Error(e);
                }
            } catch (e) {
                // ignore
            }
            this.setState({ cancelOrderModalProps: undefined });
        };
    }

    private renderCancelOrderModal = () => {
        const { confirm, cancel, order } = this.state.cancelOrderModalProps;
        return (
            <SimpleModal title={`Cancel Bulk Order: ${order.orderId}`} cancel={cancel} confirm={confirm}>
                <p>Are you sure you want to cancel this bulk order? You would have to manually move inventory back to storage location using the mobile app.</p>
            </SimpleModal>
        );
    }

    private renderFulfillBulkOrderModal = () => {
        const { confirm, cancel, order } = this.state.fulfillBulkOrderModalProps;
        return (
            <SimpleModal title={`Fulfill Bulk Order: ${order.orderId}`} cancel={cancel} confirm={confirm}>
                <p>This order will be sent to the mobile app. You can return to the page later to view the status or complete the order.</p>
            </SimpleModal>
        );
    }

    private showMenu = (key: string) => (e: React.MouseEvent<HTMLElement>) => {
        this.setState({ orderAnchor: key, menuAnchor: e.currentTarget });
    }

    private hideMenu = () => {
        this.setState({ orderAnchor: undefined, menuAnchor: undefined });
    }

    private getStatusText = (status: StateEnum): React.ReactNode => {
        switch (status) {
            case StateEnum.PENDING:
                return 'New';
            case StateEnum.READY:
            case StateEnum.FULFILLMENT:
            case StateEnum.SHIPPABLE:
                return <Typography color="primary"><strong>In Progress</strong></Typography>;
            case StateEnum.COMPLETED:
                return 'Completed';
            case StateEnum.ARCHIVED:
                return 'Archived';
        }
    }

    private archiveOrder = (item: IRelease) => async () => {
        try {
            const shipment: IShipment = await ShipmentService.fetchShipment(item.orderId);
            const { orderId, priority, updatedAt, serviceCode, dispatchCode } = shipment;
            await ShipmentService.updateShipment(orderId, { priority, updatedAt, serviceCode, dispatchCode, state: StateEnum.ARCHIVED });
            this.props.fetchOrders();
            this.props.setSuccessMessage('Bulk Order successfully updated');
        } catch (err) {
            this.props.setErrorMessage('Bulk Order failed to update');
            console.error(err);
        }
    }

    private getActionButton = (item: IRelease) => {
        switch (item.state) {
            case StateEnum.PENDING:
            case StateEnum.SHIPPABLE:
            case StateEnum.READY:
            case StateEnum.FULFILLMENT:
                return (
                    <Button onClick={this.showFulfillBulkOrderModal(item)}
                            color="primary"
                            variant="contained"
                            disabled={item.state !== StateEnum.PENDING}>FULFILL</Button>
                );
            case StateEnum.COMPLETED:
                return <Button onClick={this.archiveOrder(item)}
                               color="primary"
                               variant="outlined">ARCHIVE</Button>;
        }
    }

    private handleSort = (property: string): void => {
        const { sortOrder, sortBy } = this.state;
        const order = (property !== sortBy || sortOrder === SortOrder.Desc) ? SortOrder.Asc : SortOrder.Desc;
        this.setState({ sortOrder: order, sortBy: property });
    }

    private getSortedOrders = (data: IRelease[]): IRelease[] => {
        const { sortBy, sortOrder } = this.state;
        if (Util.isNonEmptyArray(data)) {
            data.sort((a: IRelease, b: IRelease) => (
                String(getNestedValue(sortBy, a)).localeCompare(String(getNestedValue(sortBy, b))) * (sortOrder === SortOrder.Asc ? 1 : -1))
            );
        }
        return data;
    }

    public render(): React.ReactNode {
        const { nonArchivedReleases, selectedClient, ordersFlagged } = this.props;

        const columns: IDataTableColumn<IRelease>[] = [
            {
                title: 'Bulk Order ID',
                value: (item) => item.orderId,
                headerClicked: () => this.handleSort('orderId'),
                sortOrder: this.state.sortBy === 'orderId' ? this.state.sortOrder : undefined
            },
            {
                title: 'Shipping Location',
                value: (item) => item.depotName,
                headerClicked: () => this.handleSort('depotName'),
                sortOrder: this.state.sortBy === 'depotName' ? this.state.sortOrder : undefined
            },
            {
                title: 'Last Updated',
                value: (item) => item.updatedAt && formatDateString(item.updatedAt, true),
                headerClicked: () => this.handleSort('updatedAt'),
                sortOrder: this.state.sortBy === 'updatedAt' ? this.state.sortOrder : undefined
            },
            {
                title: 'Ship By Date',
                value: (item) => item.shipBy && formatDateString(item.shipBy),
                headerClicked: () => this.handleSort('shipBy'),
                sortOrder: this.state.sortBy === 'shipBy' ? this.state.sortOrder : undefined
            },
            {
                title: 'Status',
                value: (item) => this.getStatusText(item.state),
                headerClicked: () => this.handleSort('state'),
                sortOrder: this.state.sortBy === 'state' ? this.state.sortOrder : undefined
            },
            {
                value: (item: IRelease) => (
                    <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                        {this.getActionButton(item)}
                        <IconButton onClick={this.showMenu(item.orderId)}>
                            <MoreVert />
                        </IconButton>
                        <Popover open={this.state.orderAnchor === item.orderId}
                                 anchorEl={this.state.menuAnchor}
                                 anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                                 transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                                 onClose={this.hideMenu}>
                            <MenuItem onClick={this.props.editOrder(item)}>{item.state !== StateEnum.COMPLETED ? 'View/Edit' : 'View'}</MenuItem>
                            {item.state === StateEnum.PENDING && <MenuItem onClick={this.showCancelOrderModal(item)}>Cancel Order</MenuItem>}
                        </Popover>
                    </div>
                )
            }
        ];

        const releases = Linq2.where((nonArchivedReleases || new Map()).values(), r => r.clientCode === selectedClient)
                              .except((ordersFlagged as unknown as Map<string, IRelease>).values(), (a, b) => a.orderId.localeCompare(b.orderId))
                              .where(r => Boolean(r.depotCode))
                              .toArray();
        const readyOrders = Util.isNonEmptyArray(releases) && Linq2.where(releases, o => o.state !== StateEnum.COMPLETED && o.state !== StateEnum.ARCHIVED).toArray();
        const completed = Util.isNonEmptyArray(releases) && Linq2.where(releases, o => o.state === StateEnum.COMPLETED).toArray();

        return (
            <>
                {this.state.fulfillBulkOrderModalProps !== undefined && this.renderFulfillBulkOrderModal()}
                {this.state.cancelOrderModalProps !== undefined && this.renderCancelOrderModal()} 
                <DataTable placeholderText="No Bulk Orders ready for fulfillment"
                           title="Bulk Orders Ready for Fulfillment"
                           data={this.getSortedOrders(readyOrders)}
                           columns={columns} />
                <DataTable placeholderText="No completed Bulk Orders to show"
                           title="Bulk Orders - Completed"
                           data={this.getSortedOrders(completed)}
                           columns={columns} />
            </>
        );
    }
}
