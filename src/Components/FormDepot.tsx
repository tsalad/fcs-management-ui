import { Button, MenuItem } from '@material-ui/core';
import { Util, ValidationService } from 'o8-common';
import * as React from 'react';
import Depot from '../Models/Depot';
import FCSConstants from '../Utils/FCSConstants';
import EditableTextField from './EditableTextField';
import FormSection from './FormSection';
import View from './View';

export interface IFormDepotProps {
    depot: IDepot;
    clients: Map<string, IClient>;
    cancel(): void;
    createDepot(depot: IDepot): IDepot;
    updateDepot(depot: IDepot): IDepot;
    setErrorMessage(message: string): void;
    setSuccessMessage(message: string): void;
}

interface IFormDepotState {
    depot: IDepot;
    depotSnapshot: IDepot;
    editMode: boolean;
    showValidationErrors: boolean;
}

export default class FormDepot extends React.Component<IFormDepotProps, IFormDepotState> {
    private readonly validator: ValidationService;

    constructor(props: IFormDepotProps) {
        super(props);
        this.state = {
            depot: props.depot,
            depotSnapshot: undefined,
            editMode: true,
            showValidationErrors: false
        };
        this.validator = new ValidationService(this);
        this.validator.setValidationResult = this.validator.setValidationResult.bind(this.validator);
    }

    public componentDidMount = () => {
        this.validate(this.props.depot);
        if (Boolean(this.props.depot.updatedAt)) {
            this.setState({ editMode: false });
        }
    }

    private validate = (depot: IDepot) => {
        const { name, clientCode, addressStreet, addressLocality, addressRegion, addressPostalCode, contactPhone, contactEmail } = Depot.validate(depot);
        this.validator.setValidationResult('name', name);
        this.validator.setValidationResult('clientCode', clientCode);
        this.validator.setValidationResult('addressStreet', addressStreet);
        this.validator.setValidationResult('addressLocality', addressLocality);
        this.validator.setValidationResult('addressRegion', addressRegion);
        this.validator.setValidationResult('addressPostalCode', addressPostalCode);
        this.validator.setValidationResult('contactPhone', contactPhone);
        this.validator.setValidationResult('contactEmail', contactEmail);
    }

    private cancel = () => {
        const { depot, depotSnapshot } = this.state;
        if (depot.updatedAt) {
            this.setState({ depot: depotSnapshot, editMode: false, depotSnapshot: undefined });
            return;
        }
        this.props.cancel();
    }

    private save = async () => {
        if (!this.validator.isValid()) {
            this.setState({ showValidationErrors: true });
            this.props.setErrorMessage('Please fix validation errors');
            return;
        }
        try {
            if (Boolean(this.state.depot.updatedAt)) {
                const updated = await this.props.updateDepot(this.state.depot);
                this.setState({ depotSnapshot: undefined, editMode: false, depot: updated, showValidationErrors: false });
                this.props.setSuccessMessage('Depot Successfully Saved');
            } else {
                await this.props.createDepot(this.state.depot);
                this.props.cancel();
            }
        } catch (err) {
            this.props.setErrorMessage('Depot failed to save');
            console.error(err);
        }
    }

    private setEditMode = () => {
        this.setState({ editMode: true, depotSnapshot: this.state.depot });
    }

    private renderActions = () => {
        if (this.state.editMode) {
            const { depot, depotSnapshot } = this.state;
            const disabled = depot.updatedAt ? Util.deepEqual(depot, depotSnapshot) : Util.deepEqual(depot, this.props.depot);
            return (
                <div>
                    <Button style={FCSConstants.buttonSpacing}
                            variant="contained"
                            onClick={this.cancel}>CANCEL</Button>
                    <Button style={FCSConstants.buttonSpacing}
                            variant="contained"
                            color="primary"
                            disabled={disabled}
                            onClick={this.save}>{this.state.depot.updatedAt ? 'SAVE' : 'CREATE'}</Button>    
                </div>
            );
        }
        return (
            <div>
                <Button style={FCSConstants.buttonSpacing}
                        variant="contained"
                        onClick={this.props.cancel}>BACK</Button>
                <Button style={FCSConstants.buttonSpacing}
                        variant="contained"
                        color="primary"
                        onClick={this.setEditMode}>EDIT</Button>
            </div>
        );
    }

    private updateDepotDetails = (prop: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
        this.updateDepot({ ...this.state.depot, [prop]: e.target.value });
    }

    private updateDepot = (depot: IDepot) => {
        this.validate(depot);
        this.setState({ depot });
    }

    private updateAddress = (prop: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
        this.updateDepot({ ...this.state.depot, address: { ...this.state.depot.address, [prop]: e.target.value } });
    }

    private getClientFieldValue = () => {
        const selectedClient = this.props.clients.get(this.state.depot.clientCode);
        if (!Boolean(selectedClient)) {
            return '';
        }
        return this.state.editMode ? selectedClient.clientCode : selectedClient.name;
    }

    public render(): React.ReactNode {
        const flexRow: React.CSSProperties = { display: 'flex', flexDirection: 'row', alignItems: 'center' };

        const nameValidator = this.state.showValidationErrors && this.validator && this.validator.getResults().get('name');
        const clientCodeValidator = this.state.showValidationErrors && this.validator && this.validator.getResults().get('clientCode');
        const addressStreetValidator = this.state.showValidationErrors && this.validator && this.validator.getResults().get('addressStreet');
        const addressLocalityValidator = this.state.showValidationErrors && this.validator && this.validator.getResults().get('addressLocality');
        const addressRegionValidator = this.state.showValidationErrors && this.validator && this.validator.getResults().get('addressRegion');
        const postalCodeValidator = this.state.showValidationErrors && this.validator && this.validator.getResults().get('addressPostalCode');
        const contactPhoneValidator = this.state.showValidationErrors && this.validator && this.validator.getResults().get('contactPhone');
        const contactEmailValidator = this.state.showValidationErrors && this.validator && this.validator.getResults().get('contactEmail');

        return (
            <View title="Create New Shipping Location"
                  actions={this.renderActions()}>
                <FormSection title="Basic Information"
                             description="*All fields are REQUIRED unless marked optional.">
                    <EditableTextField label="Name"
                                       helperText={nameValidator && !nameValidator.valid ? nameValidator.message : 'Shipping location name'}
                                       value={this.state.depot.name}
                                       variant="outlined"
                                       style={FCSConstants.textFieldStyle}
                                       editMode={this.state.editMode}
                                       error={nameValidator && !nameValidator.valid}
                                       onChange={this.updateDepotDetails('name')} />
                    <EditableTextField select
                                       value={this.getClientFieldValue()}
                                       helperText={clientCodeValidator && !clientCodeValidator.valid ? clientCodeValidator.message : 'Select client'}
                                       label="Client"
                                       variant="outlined"
                                       error={clientCodeValidator && !clientCodeValidator.valid}
                                       style={{ ...FCSConstants.textFieldStyle, maxWidth: '300px' }}
                                       editMode={this.state.editMode}
                                       disabled={Boolean(this.state.depot.depotCode)}
                                       onChange={this.updateDepotDetails('clientCode')}>
                        {[ ...this.props.clients.values() ].map(c => <MenuItem key={c.clientCode} value={c.clientCode}>{c.name}</MenuItem>)}
                    </EditableTextField>
                </FormSection>
                <FormSection title="Shipping Address"
                             description="*All fields are REQUIRED unless marked optional.">
                    <EditableTextField label="Company or Name"
                                       helperText="Optional"
                                       editMode={this.state.editMode}
                                       value={this.state.depot.address.name || ''}
                                       variant="outlined"
                                       style={FCSConstants.textFieldStyle}
                                       onChange={this.updateAddress('name')} />
                    <EditableTextField label="Address"
                                       editMode={this.state.editMode}
                                       helperText={addressStreetValidator && !addressStreetValidator.valid ? addressStreetValidator.message : ''}
                                       value={this.state.depot.address.street || ''}
                                       error={addressStreetValidator && !addressStreetValidator.valid}
                                       variant="outlined"
                                       style={FCSConstants.textFieldStyle}
                                       onChange={this.updateAddress('street')} />
                    <EditableTextField label="Unit"
                                       helperText="Optional"
                                       editMode={this.state.editMode}
                                       value={this.state.depot.address.unit || ''}
                                       variant="outlined"
                                       style={FCSConstants.textFieldStyle}
                                       onChange={this.updateAddress('unit')} />
                    <div style={flexRow}>
                        <EditableTextField label="City"
                                           editMode={this.state.editMode}
                                           value={this.state.depot.address.locality || ''}
                                           helperText={addressLocalityValidator && !addressLocalityValidator.valid ? addressLocalityValidator.message : ''}
                                           error={addressLocalityValidator && !addressLocalityValidator.valid}
                                           variant="outlined"
                                           style={FCSConstants.textFieldStyle}
                                           onChange={this.updateAddress('locality')} />
                        <EditableTextField label="State"
                                           select
                                           editMode={this.state.editMode}
                                           style={FCSConstants.textFieldStyle}
                                           value={this.state.depot.address.region || ''}
                                           helperText={addressRegionValidator && !addressRegionValidator.valid ? addressRegionValidator.message : ''}
                                           error={addressRegionValidator && !addressRegionValidator.valid}
                                           variant="outlined"
                                           onChange={this.updateAddress('region')}>
                            {FCSConstants.stateList.map(s => <MenuItem key={s} value={s}>{s}</MenuItem>)}
                        </EditableTextField>
                    </div>
                    <EditableTextField label="Postal Code"
                                       editMode={this.state.editMode}
                                       value={this.state.depot.address.postalCode || ''}
                                       variant="outlined"
                                       helperText={(postalCodeValidator && !postalCodeValidator.valid) ? postalCodeValidator.message : ''}
                                       error={postalCodeValidator && !postalCodeValidator.valid}
                                       style={{ ...FCSConstants.textFieldStyle, maxWidth: '300px' }}
                                       onChange={this.updateAddress('postalCode')} />
                </FormSection>
                <FormSection title="Contact Information"
                             description="*All fields are OPTIONAL.">
                    <EditableTextField label="Phone"
                                       helperText={contactPhoneValidator && !contactPhoneValidator.valid ? contactPhoneValidator.message : 'Contact Phone Number (optional)'}
                                       error={contactPhoneValidator && !contactPhoneValidator.valid}
                                       editMode={this.state.editMode}
                                       value={this.state.depot.address.phone || ''}
                                       variant="outlined"
                                       style={FCSConstants.textFieldStyle}
                                       onChange={this.updateAddress('phone')} />
                    <EditableTextField label="Email"
                                       helperText={contactEmailValidator && !contactEmailValidator.valid ? contactEmailValidator.message : 'Contact Email (optional)'}
                                       editMode={this.state.editMode}
                                       error={contactEmailValidator && !contactEmailValidator.valid}
                                       value={this.state.depot.address.email || ''}
                                       variant="outlined"
                                       style={FCSConstants.textFieldStyle}
                                       onChange={this.updateAddress('email')} />
                </FormSection>
            </View>
        );
    }
}
