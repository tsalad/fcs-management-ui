import { Button, Divider, IconButton, MenuItem, Popover, Tab, Tabs } from '@material-ui/core';
import { MoreVert } from '@material-ui/icons';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import FormDepot from '../Contexts/FormDepot';
import FormSupplier from '../Contexts/FormSupplier';
import TableSuppliers from '../Contexts/TableSuppliers';
import Depot from '../Models/Depot';
import Supplier from '../Models/Supplier';
import { formatAddress, getNestedValue } from '../Utils/Util';
import DataTable, { IDataTableColumn, SortOrder } from './DataTable';
import View from './View';

export interface IManageLogisticsProps {
    fetchDepots(): void;
    fetchStorage(): void;
    fetchSuppliers(): void;
    fetchStock(): void;
    deleteDepot(key: string): Promise<void>;
    deleteStorage(key: string): Promise<void>;
    fetchItems(): void;
    fetchWorkstations(): Promise<void>;
    depots: Map<string, IDepot>;
    clients: Map<string, IClient>;
    workstations: Map<string, IWorkstation>;
    storage: Map<string, IStorage>;
    setErrorMessage(message: string): void;
    setSuccessMessage(message: string): void;
}

export interface IManageLogisticsState {
    currentTab: number;
    sortBy: string;
    sortOrder: SortOrder;
    locationAnchor: string;
    menuAnchor: HTMLElement;
    selectedDepot: IDepot;
    selectedSupplier: ISupplier;
}

export default class Depots extends React.Component<IManageLogisticsProps, IManageLogisticsState> {
    constructor(props: IManageLogisticsProps) {
        super(props);
        this.state = {
            currentTab: 0,
            sortBy: '',
            sortOrder: SortOrder.Asc,
            locationAnchor: undefined,
            menuAnchor: undefined,
            selectedDepot: undefined,
            selectedSupplier: undefined
        };
    }

    public componentDidMount(): void {
        this.props.fetchDepots();
        this.props.fetchSuppliers();
        this.props.fetchItems();
        this.props.fetchWorkstations();
        this.props.fetchStorage();
    }

    private updateTab = (e: React.MouseEvent<HTMLButtonElement>, currentTab: number): void => {
        this.setState({ currentTab });
    }

    private handleSort = (property: string): void => {
        const { sortOrder, sortBy } = this.state;
        const order = (property !== sortBy || sortOrder === SortOrder.Desc) ? SortOrder.Asc : SortOrder.Desc;
        this.setState({ sortOrder: order, sortBy: property });
    }

    private getSortedLocations = (): IDepot[] => {
        const { sortBy, sortOrder } = this.state;
        const locations = [ ...this.props.depots.values() ];
        if (Util.isNonEmptyArray(locations)) {
            locations.sort((a: IDepot, b: IDepot) => (
                String(getNestedValue(sortBy, a)).localeCompare(String(getNestedValue(sortBy, b))) * (sortOrder === SortOrder.Asc ? 1 : -1))
            );
        }
        return locations;
    }

    private showMenu = (key: string) => (e: React.MouseEvent<HTMLElement>) => {
        this.setState({ locationAnchor: key, menuAnchor: e.currentTarget });
    }

    private hideMenu = () => {
        this.setState({ locationAnchor: undefined, menuAnchor: undefined });
    }

    private deleteDepot = (key: string) => async () => {
        try {
            const storage = Linq2.where(this.props.storage.values(), s => s.depotCode === key).toArray();
            if (Util.isNonEmptyArray(storage)) {
                await Promise.all(storage.map(async s => this.props.deleteStorage(s.storageCode)));
            }
            await this.props.deleteDepot(key);
            this.props.setSuccessMessage('Shipping Location successfully deleted');
        } catch (err) {
            this.props.setErrorMessage('Failed to delete Shipping Location');
        }
    }

    private renderShippingLocations = () => {
        const columns: IDataTableColumn<IDepot>[] = [
            {
                title: 'Client',
                value: (item: IDepot) => {
                    const client = this.props.clients.get(item.clientCode);
                    return client ? client.name : '';
                },
                headerClicked: () => this.handleSort('clientCode'),
                sortOrder: this.state.sortBy === 'clientCode' ? this.state.sortOrder : undefined
            },
            {
                title: 'Name',
                value: (item: IDepot) => item.name,
                headerClicked: () => this.handleSort('name'),
                sortOrder: this.state.sortBy === 'name' ? this.state.sortOrder : undefined
            },
            {
                title: 'Address',
                value: (item: IDepot) => formatAddress(item.address),
                headerClicked: () => this.handleSort('address.street'),
                sortOrder: this.state.sortBy === 'address.street' ? this.state.sortOrder : undefined
            },
            { value: (item: IDepot) => {
                // Find any storage for this depot that has stock and / or work stations.
                const itemStorage = Linq2.where(this.props.storage.values(),
                                                s => (s.depotCode === item.depotCode) &&
                                                Util.isNonEmptyArray(s.stock) ||
                                                Util.isNonEmptyArray(Linq2.where(this.props.workstations.values(), w => w.storageCode === s.storageCode)))
                                         .toArray();
                return (
                    <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                        <IconButton onClick={this.showMenu(item.depotCode)}>
                            <MoreVert />
                        </IconButton>
                        <Popover open={this.state.locationAnchor === item.depotCode}
                                 anchorEl={this.state.menuAnchor}
                                 anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                                 transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                                 onClose={this.hideMenu}>
                            <MenuItem onClick={this.setSelectedDepot(item)}>View/Edit</MenuItem>
                            <MenuItem onClick={this.deleteDepot(item.depotCode)}
                                      disabled={Util.isNonEmptyArray(itemStorage)}>Delete</MenuItem>
                        </Popover>
                    </div>
                );
            }}
        ];

        return (
            <DataTable placeholderText="No shipping locations found."
                       data={this.getSortedLocations()}
                       columns={columns} />
        );
    }

    private setSelectedDepot = (selectedDepot?: IDepot) => () => {
        this.setState({ selectedDepot, menuAnchor: undefined, locationAnchor: undefined });
    }

    private setSelectedSupplier = (selectedSupplier?: ISupplier) => () => {
        this.setState({ selectedSupplier, menuAnchor: undefined, locationAnchor: undefined });
    }

    private renderActions = () => {
        switch (this.state.currentTab) {
            case 0:
                return <Button variant="contained" color="primary" onClick={this.setSelectedDepot(new Depot())}>NEW SHIPPING LOCATION</Button>;
            case 1:
                return <Button variant="contained" color="primary" onClick={this.setSelectedSupplier(new Supplier())}>NEW SUPPLIER</Button>;
            default:
                return null;
        }
    }

    private renderSuppliers = () => {
        return <TableSuppliers editSupplier={this.setSelectedSupplier} />;
    }

    private renderContent = () => {
        switch (this.state.currentTab) {
            case 0:
                return this.renderShippingLocations();
            case 1:
                return this.renderSuppliers();
        }
    }

    public render(): React.ReactNode {
        if (this.state.selectedDepot !== undefined) {
            return <FormDepot depot={this.state.selectedDepot} cancel={this.setSelectedDepot()} />;
        }

        if (this.state.selectedSupplier !== undefined) {
            return <FormSupplier supplier={this.state.selectedSupplier}
                                 cancel={this.setSelectedSupplier()} />;
        }

        return (
            <View title="Manage - Logistics"
                  actions={this.renderActions()}>
                <Tabs value={this.state.currentTab}
                      onChange={this.updateTab}
                      indicatorColor="primary"
                      textColor="primary">
                    <Tab label="SHIPPING LOCATIONS" />
                    <Tab label="SUPPLIERS" />
                </Tabs>
                <Divider />
                {this.renderContent()}
            </View>
        );
    }
}
