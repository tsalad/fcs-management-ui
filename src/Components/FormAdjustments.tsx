import { Button } from '@material-ui/core';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import ModalStockAdjust from '../Contexts/ModalStockAdjust';
import { IStockEvent } from '../Typings/IStockEvent';
import { formatDateString, getNestedValue } from '../Utils/Util';
import { ICreateFormSectionProps } from './CreateProduct';
import DataTable, { IDataTableColumn, SortOrder } from './DataTable';
import FormSection from './FormSection';
import { IModalStockAdjustProps } from './ModalStockAdjust';

export interface IFormAdjustmentsProps extends ICreateFormSectionProps {
    fetchEvents(): void;
    fetchStorage(): void;
    events: Map<string, IStockEvent[]>;
    storage: Map<string, IStorage>;
    stock: Map<string, IStock[]>;
}

interface IFormAdjustmentsState {
    sortBy: string;
    sortOrder: SortOrder;
    stockAdjustModalPromise: Partial<IModalStockAdjustProps>;
}

export default class FormAdjustments extends React.Component<IFormAdjustmentsProps, IFormAdjustmentsState> {
    constructor(props: IFormAdjustmentsProps) {
        super(props);
        this.state = {
            sortBy: 'createdAt',
            sortOrder: SortOrder.Desc,
            stockAdjustModalPromise: undefined
        };
    }

    public componentDidMount = () => {
        this.props.fetchEvents();
        this.props.fetchStorage();
    }

    private handleSort = (property: string): void => {
        const { sortOrder, sortBy } = this.state;
        const order = (property !== sortBy || sortOrder === SortOrder.Desc) ? SortOrder.Asc : SortOrder.Desc;
        this.setState({ sortOrder: order, sortBy: property });
    }

    private getSortedEvents = (): IStockEvent[] => {
        const { sortBy, sortOrder } = this.state;
        const events = this.props.events.get(this.props.item.sku);
        if (Util.isNonEmptyArray(events)) {
            events.sort((a: IStockEvent, b: IStockEvent) => (
                String(getNestedValue(sortBy, a)).localeCompare(String(getNestedValue(sortBy, b))) * (sortOrder === SortOrder.Asc ? 1 : -1))
            );
        }
        return events;
    }

    private showStockAdjustModal = async () => {
        const promise: Promise<void> = new Promise((confirm, cancel) => {
            this.setState({ stockAdjustModalPromise: { confirm, cancel, productItem: this.props.item } });
        });
        try {
            await promise;
        } catch (e) {
            // ignore
        }
        this.setState({ stockAdjustModalPromise: undefined });
    }

    public render(): React.ReactNode {
        const { storage, item } = this.props;
        const currentStock = this.props.stock ? this.props.stock.get(item.sku) : [];
        const total = Util.isNonEmptyArray(currentStock) ? Linq2.aggregate(currentStock, (acc: number = 0, curr) => acc += curr.quantity) : 0;

        const columns: IDataTableColumn<IStockEvent>[] = [
            {
                title: 'Move Date',
                value: (s) => formatDateString(s.createdAt, true),
                headerClicked: () => this.handleSort('createdAt'),
                sortOrder: this.state.sortBy === 'createdAt' ? this.state.sortOrder : undefined
            },
            {
                title: 'Move From',
                value: (s) => storage.has(s.fromStorageCode) ? storage.get(s.fromStorageCode).name : '',
                headerClicked: () => this.handleSort('fromStorageCode'),
                sortOrder: this.state.sortBy === 'fromStorageCode' ? this.state.sortOrder : undefined
            },
            {
                title: 'Move To',
                value: (s) => storage.has(s.toStorageCode) ? storage.get(s.toStorageCode).name : '',
                headerClicked: () => this.handleSort('toStorageCode'),
                sortOrder: this.state.sortBy === 'toStorageCode' ? this.state.sortOrder : undefined
            },
            {
                title: 'Reason',
                value: (s) => s.eventCode,
                headerClicked: () => this.handleSort('eventCode'),
                sortOrder: this.state.sortBy === 'eventCode' ? this.state.sortOrder : undefined
            },
            {
                title: 'Quantity',
                value: (s) => s.quantity,
                headerClicked: () => this.handleSort('quantity'),
                sortOrder: this.state.sortBy === 'quantity' ? this.state.sortOrder : undefined
            }
        ];

        return (
            <FormSection title="Adjustments"
                         actions={<Button onClick={this.showStockAdjustModal}
                                          disabled={!Boolean(item.updatedAt) || !Boolean(total)}
                                          color="primary">ADJUST STOCK</Button>}>
                {Boolean(this.state.stockAdjustModalPromise) && <ModalStockAdjust {...this.state.stockAdjustModalPromise} />}
                <DataTable placeholderText="No stock adjustments"
                           paginate
                           data={this.getSortedEvents()}
                           columns={columns} />
            </FormSection>
        );
    }
}
