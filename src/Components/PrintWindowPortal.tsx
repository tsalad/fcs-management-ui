import * as React from 'react';
import * as ReactDOM from 'react-dom';

export interface IPrintWindowPortal {
    onBeforeUnload(): void;
}

export default class PrintWindowPortal extends React.PureComponent<IPrintWindowPortal> {
    private externalWindow: Window;

    public render() {
        return this.props.children;
    }

    public componentDidMount() {
        const params = [ `height=${screen.height}`, `width=${screen.width}` ].join(',');
        this.externalWindow = window.open('', '', params);
        this.externalWindow.addEventListener('beforeunload', this.cleanUpComponent);
        this.externalWindow.document.body.innerHTML = (ReactDOM.findDOMNode(this) as HTMLDivElement).innerHTML;
    }

    private cleanUpComponent = () => {
        if (typeof this.props.onBeforeUnload === 'function') {
            this.props.onBeforeUnload();
        }

        this.externalWindow.removeEventListener('beforeunload', this.cleanUpComponent);
        this.externalWindow.close();
    }
}
