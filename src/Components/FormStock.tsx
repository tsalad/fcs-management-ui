import { Button, TextField } from '@material-ui/core';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import { ProductItem } from '../Models';
import { IStockInput } from '../Typings/IStockInput';
import FCSConstants from '../Utils/FCSConstants';
import { getNestedValue } from '../Utils/Util';
import { ICreateFormSectionProps } from './CreateProduct';
import DataTable, { SortOrder } from './DataTable';
import FormSection from './FormSection';
import ModalAddProductStock, { IModalAddProductStockProps } from './ModalAddProductStock';
import SimpleModal from './SimpleModal';

interface IFormStockState {
    setReorderThresholdModalProps: {confirm(): void, cancel(): void};
    addProductStockProps: IModalAddProductStockProps;
    reorderThreshold: number;
    sortBy: string;
    sortOrder: SortOrder;
}

export interface IFormStockProps extends ICreateFormSectionProps {
    storage: Map<string, IStorage>;
    fetchStorage(): void;
    stock: Map<string, IStock[]>;
    createStock(stock: IStockInput): void;
}

export default class FormStock extends React.Component<IFormStockProps, IFormStockState> {
    constructor(props: IFormStockProps) {
        super(props);
        this.state = {
            setReorderThresholdModalProps: undefined,
            reorderThreshold: props.item.reorderThreshold || 0,
            addProductStockProps: undefined,
            sortBy: 'storageCode',
            sortOrder: SortOrder.Asc
        };
    }

    public componentDidMount = async (): Promise<void> => {
        this.props.fetchStorage();
    }

    private handleSort = (property: string): void => {
        const { sortOrder, sortBy } = this.state;
        const order = (property !== sortBy || sortOrder === SortOrder.Desc) ? SortOrder.Asc : SortOrder.Desc;
        this.setState({ sortOrder: order, sortBy: property });
    }

    private getSortedStock = (): IStock[] => {
        const { sortBy, sortOrder } = this.state;
        const stock = this.props.stock.get(this.props.item.sku);
        if (Util.isNonEmptyArray(stock)) {
            stock.sort((a: IStock, b: IStock) => (
                String(getNestedValue(sortBy, a)).localeCompare(String(getNestedValue(sortBy, b))) * (sortOrder === SortOrder.Asc ? 1 : -1))
            );
        }
        return stock;
    }

    private setReorderLevel = async (): Promise<void> => {
        const promise: Promise<number> = new Promise((confirm, cancel) => {
            this.setState({ setReorderThresholdModalProps: { confirm, cancel }, reorderThreshold: this.props.item.reorderThreshold });
        });

        try {
            await promise;
            this.props.updateItem({ ...this.props.item, reorderThreshold: this.state.reorderThreshold });
        } catch (err) {
            // ignore
        } finally {
            this.setState({ setReorderThresholdModalProps: undefined });
        }
    }

    private updateReorderThreshold = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ reorderThreshold: Number(e.target.value) });
    }

    private renderReorderLevelModal = () => {
        const error = typeof this.state.reorderThreshold !== 'number' || this.state.reorderThreshold <= 0;
        return (
            <SimpleModal title="Set Reorder Level"
                         confirmButtonText="Set"
                         disableConfirmButton={this.state.reorderThreshold <= 0}
                         { ...this.state.setReorderThresholdModalProps }>
                <p>Reorder alert will be triggered when the Avaialble Stock quantity falls below the reorder level.</p>
                <TextField type="number"
                           value={this.state.reorderThreshold}
                           variant="outlined"
                           style={FCSConstants.textFieldStyle}
                           error={error}
                           InputProps={{ inputProps: { min:  '0' } }}
                           helperText={error ? 'Please enter an interger greater than 0' : 'When to trigger reorder alert'}
                           onChange={this.updateReorderThreshold} />
            </SimpleModal>
        );
    }

    private showAddStockModal = async (): Promise<void> => {
        const { item, storage, stock } = this.props;
        const currentStock = item && Boolean(item.sku) && stock ? this.props.stock.get(item.sku) : [];
        const promise: Promise<IStockInput> = new Promise((confirm, cancel) => {
            this.setState({ addProductStockProps: { confirm, cancel, productItem: item, currentStock, storage } });
        });

        try {
            const stockInput = await promise;
            await this.props.createStock(stockInput);
        } catch (err) {
            // ignore
        } finally {
            this.setState({ addProductStockProps: undefined });
        }
    }

    private renderFooter = () => {
        const currentStock = this.props.stock ? this.props.stock.get(this.props.item.sku) : [];
        return (
            <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', marginTop: '1rem'}}>
                <span style={{display: 'flex', flexDirection: 'row', marginRight: '1rem'}}>
                    <label style={{fontWeight: 'bold', marginRight: '1rem'}}>
                        Total Stock:
                    </label>
                    {Util.isNonEmptyArray(currentStock) ? Linq2.aggregate(currentStock, (acc: number = 0, curr) => acc += curr.quantity) : 0}
                </span>
                <span style={{display: 'flex', flexDirection: 'row', marginRight: '1rem'}}>
                    <label style={{fontWeight: 'bold', marginRight: '1rem'}}>
                        {`${Reflect.getMetadata('displayName', ProductItem.prototype, 'reorderThreshold')}: `}
                    </label>
                    {this.props.item.reorderThreshold || 0}
                </span>
            </div>
        );
    }

    public render(): React.ReactNode {
        return (
            <>
                {this.state.setReorderThresholdModalProps !== undefined && this.renderReorderLevelModal()}
                {this.state.addProductStockProps !== undefined && <ModalAddProductStock {...this.state.addProductStockProps} />}
                <FormSection title="Stock"
                             actions={
                                 <span>
                                     <Button disabled={!Boolean(this.props.item.updatedAt)} onClick={this.showAddStockModal} color="primary">ADD STOCK</Button>
                                     <Button disabled={!this.props.editMode} color="primary" onClick={this.setReorderLevel}>SET REORDER LEVEL</Button>
                                 </span>}>
                    <DataTable data={this.getSortedStock()}
                               placeholderText={!this.props.item.updatedAt ? 'Finish creating this product before adding stock' : 'No stock'}
                               columns={[
                                    {
                                        title: 'Location',
                                        value: (item: IStock) => {
                                            const storage = this.props.storage.get(item.storageCode);
                                            return storage && Boolean(storage.name) ? storage.name : '';
                                        },
                                        headerClicked: () => this.handleSort('storageCode'),
                                        sortOrder: this.state.sortBy === 'storageCode' ? this.state.sortOrder : undefined
                                    },
                                    {
                                        title: 'Quantity',
                                        value: (item: IStock) => item.quantity,
                                        headerClicked: () => this.handleSort('quantity'),
                                        sortOrder: this.state.sortBy === 'quantity' ? this.state.sortOrder : undefined
                                    }
                                ]} />
                    {this.renderFooter()}
                </FormSection>
            </>
        );
    }
}
