import TextField, { TextFieldProps } from '@material-ui/core/TextField';
import { AutoBindingComponent } from 'o8-common';
import * as React from 'react';

export interface IDelayedInputProps {
    delay: number;
}

export default class DelayedInput extends AutoBindingComponent<IDelayedInputProps & TextFieldProps> {
    public static defaultProps: Partial<IDelayedInputProps> = {
        delay: 250
    };

    private timer: number;

    private onChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        e.persist();
        window.clearTimeout(this.timer);
        this.timer = window.setTimeout(() => {
            this.props.onChange(e);
        }, this.props.delay);
    }

    public render(): React.ReactNode {
        return (
            <TextField {...this.props} onChange={this.onChange} />
        );
    }
}
