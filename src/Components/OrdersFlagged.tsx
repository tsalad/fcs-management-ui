import { Badge } from '@material-ui/core';
import { Linq2 } from 'o8-common';
import * as React from 'react';
import TableOrdersFlagged from '../Contexts/TableOrdersFlagged';
import { IOrderFlag } from '../Typings/IOrderFlag';
import { IRelease } from '../Typings/IRelease';
import { OrderFlagReasonEnum } from '../Typings/OrderFlagReasonEnum';
import Accordion from './Accordion';

export interface IOrdersFlaggedProps {
    selectedClient: string;
    ordersFlagged: Map<string, IOrderFlag>;
    nonArchivedReleases: Map<string, IRelease>;
    fetchOrders(): void;
}

interface IOrdersFlaggedState {
    expanded: ReasonGroupEnum;
}

export enum ReasonGroupEnum {
    ShippingDefect = 'ShippingDefect',
    ShippingLabelDefect = 'ShippingLabelDefect',
    TrackableDefect = 'TrackableDefect',
    Other = 'Other'
}

const reasonGroupVanity = new Map([
    [ReasonGroupEnum.ShippingDefect, 'Shipping Defect'],
    [ReasonGroupEnum.ShippingLabelDefect, 'Shipping Label Defect'],
    [ReasonGroupEnum.TrackableDefect, 'Trackable Defect'],
    [ReasonGroupEnum.Other, 'Other']
]);

export default class OrdersFlagged extends React.Component<IOrdersFlaggedProps, IOrdersFlaggedState> {
    constructor(props: IOrdersFlaggedProps) {
        super(props);
        this.state = {
            expanded: undefined
        };
    }

    private toggleAccordion = (reason: ReasonGroupEnum) => () => {
        this.setState({ expanded: this.state.expanded === reason ? undefined : reason });
    }

    private renderAccordion = (reason: ReasonGroupEnum) => {
        const { fetchOrders } = this.props;
        const flags = this.getFilteredData(reason);

        return (
            <Accordion title={<Badge badgeContent={(flags || new Map()).size}
                                     max={99}
                                     color="error"
                                     style={{paddingRight: '1.5rem'}}>{reasonGroupVanity.get(reason)}</Badge>}
                       open={this.state.expanded === reason}
                       onChange={this.toggleAccordion(reason)}
                       ExpansionPanelDetailsProps={{ style: { padding: 0, display: 'flex', flexDirection: 'column' } }}
                       ExpansionPanelSummaryProps={{ style: { padding: '0 1rem' } }}
                       key={reason}>
                <TableOrdersFlagged orderFlagReason={reason}
                                    ordersFlagged={flags}
                                    fetchOrders={fetchOrders} />
            </Accordion>
        );
    }

    private getFilteredData = (reason: ReasonGroupEnum): Map<string, IOrderFlag> => {
        const { ordersFlagged, nonArchivedReleases, selectedClient } = this.props;
        const data = Linq2.where(ordersFlagged.values(), f => Boolean(nonArchivedReleases) && nonArchivedReleases.has(f.orderId))
                          .where(f => f.clientCode === selectedClient)
                          .toArray();
        switch (reason) {
            case ReasonGroupEnum.ShippingDefect:
                return Linq2.where(data, f => f.reason === OrderFlagReasonEnum.InvalidAddress ||
                                              f.reason === OrderFlagReasonEnum.Unshippable)
                            .toDictionary(f => f.id);
            case ReasonGroupEnum.ShippingLabelDefect:
                return Linq2.where(data, f => f.reason === OrderFlagReasonEnum.ShippingLabelDefect)
                            .toDictionary(f => f.id);
            case ReasonGroupEnum.TrackableDefect:
                return Linq2.where(data, f => f.reason === OrderFlagReasonEnum.PackageItemHardwareDefect ||
                                              f.reason === OrderFlagReasonEnum.PackageItemMismatch ||
                                              f.reason === OrderFlagReasonEnum.PackageItemPrintDefect ||
                                              f.reason === OrderFlagReasonEnum.PackageItemBadManifest)
                            .toDictionary(f => f.id);
            case ReasonGroupEnum.Other:
                return Linq2.where(data, f => f.reason === OrderFlagReasonEnum.Other)
                            .toDictionary(f => f.id);
        }
    }

    public render(): React.ReactNode {
        return Object.values(ReasonGroupEnum).map(r => this.renderAccordion(r));
    }
}
