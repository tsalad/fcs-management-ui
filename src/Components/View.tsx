import { IconButton, Toolbar, Tooltip,  Typography } from '@material-ui/core';
import ArrowBack from '@material-ui/icons/ArrowBack';
import * as React from 'react';

export interface IViewProps {
    title?: React.ReactNode;
    actions?: React.ReactNode;
    onBackPressed?(): void;
    noGutter?: boolean;
    bodyStyle?: React.CSSProperties;
}

export default class View extends React.Component<IViewProps> {
    public static defaultProps: Partial<IViewProps> = {
        noGutter: false,
        title: undefined,
        actions: undefined
    };

    public render(): React.ReactNode {
        const bodyStyle: React.CSSProperties = {
            width: '100%',
            padding: this.props.noGutter ? undefined : '1rem',
            flex: '1',
            ...this.props.bodyStyle
        };

        const toolbarStyle: React.CSSProperties = {
            width: '100%',
            minHeight: '55px',
            backgroundColor: '#fff',
            display: 'flex',
            justifyContent: 'space-between',
            flexWrap: 'wrap',
            padding: '0 1rem'
        };

        const actionsStyle: React.CSSProperties = {
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'flex-end',
            alignItems: 'center'
        };

        const titleWrapperStyle: React.CSSProperties = {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center'
        };

        const contentWrapperStyle: React.CSSProperties = {
            width: 'inherit',
            height: 'inherit',
            display: 'flex',
            flexDirection: 'column'
        };

        return (
            <div style={contentWrapperStyle}>
                {this.props.title !== undefined &&
                <Toolbar disableGutters style={toolbarStyle}>
                    <div style={titleWrapperStyle}>
                        {typeof this.props.onBackPressed === 'function' &&
                        <Tooltip title="Back">
                            <IconButton onClick={this.props.onBackPressed}>
                                <ArrowBack />
                            </IconButton>
                        </Tooltip>}
                        <Typography variant="h5" style={{ margin: '1rem 0', whiteSpace: 'nowrap' }} color="inherit">
                            {this.props.title}
                        </Typography>
                    </div>
                    <div style={actionsStyle}>{this.props.actions}</div>
                </Toolbar>}
                <div style={bodyStyle}>
                    {this.props.children}
                </div>
            </div>
        );
    }
}
