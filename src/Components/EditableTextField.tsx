import { TextField } from '@material-ui/core';
import { TextFieldProps } from '@material-ui/core/TextField';
import * as React from 'react';
import FCSConstants from '../Utils/FCSConstants';

export interface IEditableTextFieldProps { editMode?: boolean; }

export default class EditableTextField extends React.Component<IEditableTextFieldProps & TextFieldProps> {
    public static readonly defaultProps: Partial<IEditableTextFieldProps & TextFieldProps> = {
        editMode: true,
        variant: 'outlined'
    };

    public render(): React.ReactNode {
        const { editMode, ...rest } = this.props;

        if (!editMode) {
            const { label, value } = rest;
            return (
                <div style={{...FCSConstants.textFieldStyle, display: 'flex', flexDirection: 'column', flex: 'auto', ...rest.style}}>
                    {label !== undefined && <label style={{color: '#000'}}>{label}</label>}
                    {value !== undefined && <p style={{margin: '.5rem 0'}}>{value}</p>}
                </div>
            );
        }

        return (
            <TextField { ...rest }>
                {this.props.children}
            </TextField>
        );
    }
}
