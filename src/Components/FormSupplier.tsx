import { Button } from '@material-ui/core';
import { Linq2, Util, ValidationService } from 'o8-common';
import * as React from 'react';
import CreateProduct from '../Contexts/CreateProduct';
import Supplier from '../Models/Supplier';
import FCSConstants from '../Utils/FCSConstants';
import DataTable, { IDataTableColumn } from './DataTable';
import EditableTextField from './EditableTextField';
import FormSection from './FormSection';
import View from './View';

export interface IFormSupplierProps {
    supplier: ISupplier;
    cancel(): void;
    createSupplier(supplier: ISupplier): Promise<ISupplier>;
    updateSupplier(supplier: ISupplier): Promise<ISupplier>;
    productItems: Map<string, IProductItem>;
    setErrorMessage(message: string): void;
    setSuccessMessage(message: string): void;
}

interface IFormSupplierState {
    editMode: boolean;
    supplierSnapshot: ISupplier;
    supplier: ISupplier;
    showValidationErrors: boolean;
    selectedProductItem: IProductItem;
}

export default class FormSupplier extends React.Component<IFormSupplierProps, IFormSupplierState> {
    private readonly validator: ValidationService;

    constructor(props: IFormSupplierProps) {
        super(props);
        this.state = {
            editMode: true,
            supplierSnapshot: undefined,
            supplier: props.supplier || new Supplier(),
            showValidationErrors: false,
            selectedProductItem: undefined
        };
        this.validator = new ValidationService(this);
        this.validator.setValidationResult = this.validator.setValidationResult.bind(this.validator);
    }

    public componentDidMount = () => {
        this.validate(this.props.supplier);
        if (Boolean(this.props.supplier.updatedAt)) {
            this.setState({ editMode: false });
        }
    }

    private validate = (supplier: ISupplier) => {
        const { name, supplierCode, contactPhone, contactEmail } = Supplier.validate(supplier);
        this.validator.setValidationResult('name', name);
        this.validator.setValidationResult('supplierCode', supplierCode);
        this.validator.setValidationResult('contactPhone', contactPhone);
        this.validator.setValidationResult('contactEmail', contactEmail);
    }

    private update = (supplier: ISupplier) => {
        this.validate(supplier);
        this.setState({ supplier });
    }

    private updateSupplier = (property: string) => (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.update({ ...this.state.supplier, [property]: e.target.value });
    }

    private updateContact = (property: string) => (e: React.ChangeEvent<HTMLInputElement>): void => {
        const { address } = this.state.supplier;
        this.update({ ...this.state.supplier, address: { ...address, [property]: e.target.value } });
    }

    private updateCode = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.update({ ...this.state.supplier, supplierCode: e.target.value.toUpperCase().trim() });
    }

    private save = async () => {
        if (!this.validator.isValid()) {
            this.setState({ showValidationErrors: true });
            this.props.setErrorMessage('Please fix validation errors');
            return;
        }
        try {
            if (Boolean(this.state.supplier.updatedAt)) {
                const updated = await this.props.updateSupplier(this.state.supplier);
                this.setState({ supplierSnapshot: undefined, editMode: false, supplier: updated });
                this.props.setSuccessMessage('Supplier successfully saved');
            } else {
                await this.props.createSupplier(this.state.supplier);
                this.props.setSuccessMessage('Supplier successfully created');
                this.props.cancel();
            }
        } catch (err) {
            this.props.setErrorMessage('Supplier failed to save');
        }
    }

    private setEditMode = () => {
        this.setState({ editMode: true, supplierSnapshot: this.state.supplier });
    }

    private renderActions = () => {
        if (this.state.editMode) {
            const { supplier, supplierSnapshot } = this.state;
            const disabled = supplier.updatedAt ? Util.deepEqual(supplier, supplierSnapshot) : Util.deepEqual(supplier, this.props.supplier);
            return (
                <div>
                    <Button style={FCSConstants.buttonSpacing}
                            variant="contained"
                            onClick={this.props.cancel}>CANCEL</Button>
                    <Button style={FCSConstants.buttonSpacing}
                            variant="contained"
                            color="primary"
                            disabled={disabled}
                            onClick={this.save}>{this.state.supplier.updatedAt ? 'SAVE' : 'CREATE'}</Button>    
                </div>
            );
        }
        return (
            <div>
                <Button style={FCSConstants.buttonSpacing}
                        variant="contained"
                        onClick={this.props.cancel}>BACK</Button>
                <Button style={FCSConstants.buttonSpacing}
                        variant="contained"
                        color="primary"
                        onClick={this.setEditMode}>EDIT</Button>
            </div>
        );
    }

    private selectProductItem = (productItemSupplier: { sku: string, supplierSku: string, supplierCode: string }) => () => {
        const selectedProductItem = this.props.productItems.get(productItemSupplier.sku);
        this.setState({ selectedProductItem });
    }

    private clearSelectedProductItem = () => {
        this.setState({ selectedProductItem: undefined });
    }

    public render(): React.ReactNode {
        const { supplier, selectedProductItem } = this.state;

        if (Boolean(selectedProductItem)) {
            return <CreateProduct item={selectedProductItem} cancel={this.clearSelectedProductItem} />;
        }

        if (!Boolean(supplier)) {
            return null;
        }

        const { address } = supplier;

        const nameValidator = this.state.showValidationErrors && this.validator && this.validator.getResults().get('name');
        const supplierCodeValidator = this.state.showValidationErrors && this.validator && this.validator.getResults().get('supplierCode');
        const contactPhoneValidator = this.state.showValidationErrors && this.validator && this.validator.getResults().get('contactPhone');
        const contactEmailValidator = this.state.showValidationErrors && this.validator && this.validator.getResults().get('contactEmail');

        const data: { sku: string, supplierSku: string, supplierCode: string }[] = [ ...this.props.productItems.values() ].reduce((acc, curr) => {
            const suppliers = curr.suppliers.map(s => ({ sku: curr.sku, ...s }));
            return [ ...acc, ...suppliers ];
        }, []);
        const productItemSuppliers = Linq2.where(data, s => s.supplierCode === supplier.supplierCode)
                                          .toArray();

        const columns: IDataTableColumn<{ sku: string, supplierSku: string, supplierCode: string }>[] = [
            {
                title: 'External SKU',
                value: (item) => item.supplierSku
            },
            {
                title: 'Internal SKU',
                value: (item) => item.sku
            },
            {
                title: 'Product Name',
                value: (item) => this.props.productItems.get(item.sku).name
            },
            {
                value: (item) => <Button onClick={this.selectProductItem(item)}
                                         color="primary">VIEW PRODUCT</Button>
            }
        ];

        return (
            <View title={Boolean(supplier.updatedAt) ? 'Update Shipping Suppler' : 'Create New Shipping Supplier'}
                  actions={this.renderActions()}>
                <FormSection title="Basic Information" description="* All fields are REQUIRED unless marked optional.">
                    <EditableTextField label="Name"
                                       helperText={nameValidator && !nameValidator.valid ? nameValidator.message : 'Supplier Name'}
                                       error={nameValidator && !nameValidator.valid}
                                       value={supplier.name || ''}
                                       variant="outlined"
                                       style={FCSConstants.textFieldStyle}
                                       editMode={this.state.editMode}
                                       onChange={this.updateSupplier('name')} />
                    <EditableTextField label="Supplier Code"
                                       helperText={supplierCodeValidator && !supplierCodeValidator.valid ? supplierCodeValidator.message : 'Unique Supplier Code'}
                                       error={supplierCodeValidator && !supplierCodeValidator.valid}
                                       value={supplier.supplierCode || ''}
                                       variant="outlined"
                                       style={FCSConstants.textFieldStyle}
                                       editMode={this.state.editMode}
                                       onChange={this.updateCode}
                                       disabled={Boolean(supplier.updatedAt)} />
                </FormSection>
                <FormSection title="Contact Information" description="* All fields are OPTIONAL.">
                    <EditableTextField label="Name"
                                       helperText="Contact Name (Optional)"
                                       value={address.name || ''}
                                       variant="outlined"
                                       style={FCSConstants.textFieldStyle}
                                       editMode={this.state.editMode}
                                       onChange={this.updateContact('name')} />
                    <EditableTextField label="Phone"
                                       helperText={contactPhoneValidator && !contactPhoneValidator.valid ? contactPhoneValidator.message : 'Contact Phone Number (Optional)'}
                                       error={contactPhoneValidator && !contactPhoneValidator.valid}
                                       value={address.phone || ''}
                                       variant="outlined"
                                       style={FCSConstants.textFieldStyle}
                                       editMode={this.state.editMode}
                                       onChange={this.updateContact('phone')} />
                    <EditableTextField label="Email"
                                       value={address.email || ''}
                                       helperText={contactEmailValidator && !contactEmailValidator.valid ? contactEmailValidator.message : 'Contact Email (Optional)'}
                                       error={contactEmailValidator && !contactEmailValidator.valid}
                                       variant="outlined"
                                       style={FCSConstants.textFieldStyle}
                                       editMode={this.state.editMode}
                                       onChange={this.updateContact('email')} />
                </FormSection>
                {Boolean(supplier.updatedAt) &&
                <FormSection title="External SKU" description="Add external SKUs from the product page.">
                    <DataTable data={productItemSuppliers} columns={columns} />
                </FormSection>}
            </View>
        );
    }
}
