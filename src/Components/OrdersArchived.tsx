import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import ShipmentService from '../Services/ShipmentService';
import { IShipment } from '../Typings/IShipment';
import { formatDateString, getNestedValue } from '../Utils/Util';
import DataTable, { IDataTableColumn, SortOrder } from './DataTable';

export interface IOrdersArchivedProps {
    selectedClient: string;
}

interface IOrdersArchivedState {
    bulkShipments: Map<string, IShipment[]>;
    nonBulkShipments: Map<string, IShipment[]>;
    bulkSortBy: string;
    nonBulkSortBy: string;
    nonBulkSortOrder: SortOrder;
    bulkSortOrder: SortOrder;
}

export default class OrdersArchived extends React.Component<IOrdersArchivedProps, IOrdersArchivedState> {
    constructor(props: IOrdersArchivedProps) {
        super(props);
        this.state = {
            bulkShipments: new Map(),
            nonBulkShipments: new Map(),
            bulkSortBy: 'completedAt',
            bulkSortOrder: SortOrder.Desc,
            nonBulkSortBy: 'completedAt',
            nonBulkSortOrder: SortOrder.Desc
        };
    }

    private fetchShipments = async () => {
        const bulk = await ShipmentService.fetchArchivedShipments(true);
        const nonBulk = await ShipmentService.fetchArchivedShipments(false);
        const bulkShipments: Map<string, IShipment[]> = Util.isNonEmptyArray(bulk) ? Linq2.groupBy(bulk, o => o.clientCode).toDictionary() : new Map();
        const nonBulkShipments: Map<string, IShipment[]> = Util.isNonEmptyArray(nonBulk) ? Linq2.groupBy(nonBulk, o => o.clientCode).toDictionary() : new Map();
        this.setState({ bulkShipments, nonBulkShipments });
    }

    public componentDidMount() {
        this.fetchShipments();
    }

    private getSortedOrders = (data: IShipment[], sortBy: string, sortOrder: SortOrder): IShipment[] => {
        if (Util.isNonEmptyArray(data)) {
            data.sort((a: IShipment, b: IShipment) => (
                String(getNestedValue(sortBy, a)).localeCompare(String(getNestedValue(sortBy, b))) * (sortOrder === SortOrder.Asc ? 1 : -1))
            );
        }
        return data;
    }

    private handleNonBulkSort = (property: string): void => {
        const { nonBulkSortOrder, nonBulkSortBy } = this.state;
        const order = (property !== nonBulkSortBy || nonBulkSortOrder === SortOrder.Desc) ? SortOrder.Asc : SortOrder.Desc;
        this.setState({ nonBulkSortOrder: order, nonBulkSortBy: property });
    }

    private handleBulkSort = (property: string): void => {
        const { bulkSortOrder, bulkSortBy } = this.state;
        const order = (property !== bulkSortBy || bulkSortOrder === SortOrder.Desc) ? SortOrder.Asc : SortOrder.Desc;
        this.setState({ bulkSortOrder: order, bulkSortBy: property });
    }   

    public render(): React.ReactNode {
        const { selectedClient } = this.props;
        const { nonBulkSortBy, nonBulkSortOrder, bulkSortBy, bulkSortOrder, bulkShipments, nonBulkShipments } = this.state;
        const nonBulkColumns: IDataTableColumn<IShipment>[] = [
            {
                title: 'Order ID',
                value: (item) => item.orderId,
                headerClicked: () => this.handleNonBulkSort('orderId'),
                sortOrder: nonBulkSortBy === 'orderId' ? nonBulkSortOrder : undefined
            },
            {
                title: 'Guest Name',
                value: (item) => Boolean(item) && Boolean(item.toAddress) && item.toAddress.name || '',
                headerClicked: () => this.handleNonBulkSort('toAddress.name'),
                sortOrder: nonBulkSortBy === 'toAddress.name' ? nonBulkSortOrder : undefined
            },
            {
                title: 'Completed',
                value: (item) => formatDateString(item.completedAt),
                headerClicked: () => this.handleNonBulkSort('completedAt'),
                sortOrder: nonBulkSortBy === 'completedAt' ? nonBulkSortOrder : undefined
            }
        ];
        
        const bulkColumns: IDataTableColumn<IShipment>[] = [
            {
                title: 'Bulk Order ID',
                value: (item) => item.orderId,
                headerClicked: () => this.handleBulkSort('orderId'),
                sortOrder: bulkSortBy === 'orderId' ? bulkSortOrder : undefined
            },
            {
                title:  'Shipping Location',
                value: (item) => Boolean(item) && Boolean(item.shipperAddress) && item.shipperAddress.name || '',
                headerClicked: () => this.handleBulkSort('shipperAddress.name'),
                sortOrder: bulkSortBy === 'shipperAddress.name' ? bulkSortOrder : undefined
            },
            {
                title: 'Completed',
                value: (item) => formatDateString(item.completedAt),
                headerClicked: () => this.handleBulkSort('completedAt'),
                sortOrder: bulkSortBy === 'completedAt' ? bulkSortOrder : undefined
            }
        ];

        const bulkData = Boolean(selectedClient) && this.getSortedOrders(bulkShipments.get(selectedClient), bulkSortBy, bulkSortOrder);
        const nonBulkData = Boolean(selectedClient) && this.getSortedOrders(nonBulkShipments.get(selectedClient), nonBulkSortBy, nonBulkSortOrder);

        return (
            <>
                <DataTable placeholderText="No Orders Found."
                           title="Orders (Last 30 Days)"
                           data={nonBulkData}
                           paginate
                           columns={nonBulkColumns} />
                <DataTable placeholderText="No Bulk Orders Found."
                           title="Bulk Orders (Last 30 Days)"
                           data={bulkData}
                           paginate
                           columns={bulkColumns} />
            </>
        );
    }
}
