import { Button, IconButton, MenuItem, Popover, Typography } from '@material-ui/core';
import { MoreVert } from '@material-ui/icons';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import NotificationsService from '../Services/NotificationsService';
import { IProblemReport } from '../Typings/IProblemReport';
import { IssueTypeEnum } from '../Typings/IssueTypeEnum';
import { ReasonEnum } from '../Typings/ReasonEnum';
import { formatDateString, getNestedValue } from '../Utils/Util';
import DataTable, { IDataTableColumn, SortOrder } from './DataTable';
import SimpleModal from './SimpleModal';

export interface ITableProblemReportsProps {
    productItems: Map<string, IProductItem>;
    storage: Map<string, IStorage>;
    problemReports: Map<string, IProblemReport>;
    setSuccessMessage(message: string): void;
    setErrorMessage(message: string): void;
}

interface ITableProblemReportsState {
    viewComment: IProblemReport;
    menuAnchor: HTMLElement;
    selectedId: string;
    sortBy: string;
    sortOrder: SortOrder;
}

export default class TableProblemReports extends React.Component<ITableProblemReportsProps, ITableProblemReportsState> {
    constructor(props: ITableProblemReportsProps) {
        super(props);
        this.state = {
            sortBy: 'createdAt',
            sortOrder: SortOrder.Asc,
            viewComment: undefined,
            menuAnchor: undefined,
            selectedId: undefined
        };
    }

    private getIssueTypeVanityText = (issueType: IssueTypeEnum): string => {
        switch (issueType) {
            case IssueTypeEnum.EXISTING_STOCK:
                return 'Existing Stock';
            case IssueTypeEnum.LOCATION:
                return 'Location';
            case IssueTypeEnum.RECEIVED_STOCK:
                return 'Received Stock';
        }
    }

    private getReasonVanityText = (reason: ReasonEnum): React.ReactNode => {
        switch (reason) {
            case ReasonEnum.BARCODE_LABEL_DAMAGED:
                return 'Barcode/Label is damaged.';
            case ReasonEnum.OTHER:
                return 'Other';
            case ReasonEnum.STOCK_LOCATION_ISSUE:
                return 'Stock Location Issue';
            case ReasonEnum.STOCK_QUANTITY_ISSUE:
                return 'Stock Quantity Issue';
            case ReasonEnum.UNIDENTIFIED_PRODUCT_BARCODE:
                return 'Unidentified product/barcode.';
        }
    }

    private showCommentModal = (item?: IProblemReport) => () => {
        this.setState({ viewComment: item });
    }

    private renderViewCommentModal = () => {
        return (
            <SimpleModal showCancelButton={false}
                         confirmButtonText="Close"
                         confirm={this.showCommentModal()}
                         title="View Comment">
                <p>{this.state.viewComment.comment}</p>
            </SimpleModal>
        );
    }

    private showMenu = (selectedId: string) => (e: React.MouseEvent<HTMLElement>) => {
        this.setState({ menuAnchor: e.currentTarget, selectedId });
    }

    private hideMenu = () => {
        this.setState({ menuAnchor: undefined, selectedId: undefined });
    }
    
    private resolveIssue = (item: IProblemReport) => async () => {
        try {
            await NotificationsService.resolveProblemReport(item.id);
            await NotificationsService.updateNotifications();
            this.props.setSuccessMessage('Issue resolved');
        } catch (err) {
            this.props.setErrorMessage('Failed to resolve issue');
            console.error(err);
        }
        this.hideMenu();
    }

    private getSortedData = (data: IProblemReport[]): IProblemReport[] => {
        const { sortBy, sortOrder } = this.state;
        if (Util.isNonEmptyArray(data)) {
            data.sort((a: IProblemReport, b: IProblemReport) => (
                String(getNestedValue(sortBy, a)).localeCompare(String(getNestedValue(sortBy, b))) * (sortOrder === SortOrder.Asc ? 1 : -1))
            );
        }
        return data;
    }

    private handleSort = (property: string): void => {
        const { sortOrder, sortBy } = this.state;
        const order = (property !== sortBy || sortOrder === SortOrder.Desc) ? SortOrder.Asc : SortOrder.Desc;
        this.setState({ sortOrder: order, sortBy: property });
    }

    public render(): React.ReactNode {
        const columns: IDataTableColumn<IProblemReport>[] = [
            {
                title: 'Time Stamp',
                value: (item) => formatDateString(item.createdAt, true),
                headerClicked: () => this.handleSort('createdAt'),
                sortOrder: this.state.sortBy === 'createdAt' ? this.state.sortOrder : undefined
            },
            {
                title: 'Product',
                value: (item) => {
                    const productItem = this.props.productItems.get(item.productItemSku);
                    if (Boolean(productItem)) {
                        return (
                            <div style={{display: 'flex', flexDirection: 'column'}}>
                                <div>{productItem.name}</div>
                                <Typography variant="caption">{productItem.sku}</Typography>
                            </div>
                        );
                    } else {
                        return <p>Product not found.</p>;
                    }
                }
            },
            {
                title: 'Location',
                value: (item) => {
                    const storage = this.props.storage.get(item.storageCode);
                    if (Boolean(storage)) {
                        return storage.name;
                    } else {
                        return <p>Location not found.</p>;
                    }
                }
            },
            {
                title: 'Issue Type',
                value: (item) => this.getIssueTypeVanityText(item.issueType),
                headerClicked: () => this.handleSort('issueType'),
                sortOrder: this.state.sortBy === 'issueType' ? this.state.sortOrder : undefined
            },
            {
                title: 'Reason',
                value: (item) => (
                    Boolean(item.comment) ?
                    <Button onClick={this.showCommentModal(item)} style={{padding: 0}} color="primary">VIEW COMMENT</Button> :
                    this.getReasonVanityText(item.reason)
                ),
                headerClicked: () => this.handleSort('reason'),
                sortOrder: this.state.sortBy === 'reason' ? this.state.sortOrder : undefined
            },
            {
                value: (item: IProblemReport) => {
                    return (
                        <>
                            <IconButton onClick={this.showMenu(item.id)}>
                                <MoreVert />
                            </IconButton>
                            <Popover open={this.state.selectedId === item.id}
                                     anchorEl={this.state.menuAnchor}
                                     anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                                     transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                                     onClose={this.hideMenu}>
                                <MenuItem onClick={this.resolveIssue(item)}>Resolve Issue</MenuItem>
                            </Popover>
                        </>
                    );
                }
            }
        ];

        const data = Linq2.where([ ...this.props.problemReports.values() ], r => !Boolean(r.closedAt)).toArray();

        return (
            <>
                {Boolean(this.state.viewComment) && this.renderViewCommentModal()}
                <DataTable columns={columns}
                           data={this.getSortedData(data)}
                           paginate />
            </>
        );
    }
}
