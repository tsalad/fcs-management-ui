import { MenuItem, TextField } from '@material-ui/core';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import FCSConstants from '../Utils/FCSConstants';

export interface IFormMoveStockProps {
    storage: Map<string, IStorage>;
    stock: Map<string, IStock[]>;
    productItem: IProductItem;
    move: IStockMoveInput;
    update(move: IStockMoveInput): void;
}

export default class FormMoveStock extends React.Component<IFormMoveStockProps> {
    private updateToLocation = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { move } = this.props;
        const storage = this.props.storage.get(e.target.value);
        if (Boolean(storage)) {
            const currentStock = Linq2.firstOrDefault(this.props.stock.get(this.props.productItem.sku), undefined, s => s.storageCode === storage.storageCode);
            const updated: IStockMoveInput = { ...move, toStorageCode: storage.storageCode, toUpdatedAt: Boolean(currentStock) ? currentStock.updatedAt : null };
            this.props.update(updated);
        }
    }

    private updateFromLocation = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { move } = this.props;
        const storage = this.props.storage.get(e.target.value);
        if (Boolean(storage)) {
            const currentStock = Linq2.firstOrDefault(this.props.stock.get(this.props.productItem.sku), undefined, s => s.storageCode === storage.storageCode);
            const updated: IStockMoveInput = { ...move, fromStorageCode: storage.storageCode, fromUpdatedAt: Boolean(currentStock) && currentStock.updatedAt };
            this.props.update(updated);
        }
    }

    private updateQuantity = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.update({ ...this.props.move, quantity: Number(e.target.value) });
    }

    private getCurrentQuantity = (storageCode: string) => {
        const { stock, productItem } = this.props;
        if (storageCode) {
            const c = Linq2.firstOrDefault(stock.get(productItem.sku), undefined, s => s.storageCode === storageCode);
            return Boolean(c) ? c.quantity : 0;
        }
        return '';
    }

    private getTotalStock = (sku: string) => {
        const currentStock = this.props.stock.get(sku);
        const storageStock = Util.isNonEmptyArray(currentStock) && Linq2.firstOrDefault(currentStock, undefined, s => s.storageCode === this.props.move.fromStorageCode);
        return Boolean(storageStock) ? storageStock.quantity : 0;
    }

    public render(): React.ReactNode {
        const { move, stock, productItem, storage } = this.props;
        const locations = Linq2.select(stock.get(productItem.sku), s => storage.get(s.storageCode)).toArray();
        const fromLocations = Linq2.where(locations, s => move.toStorageCode !== s.storageCode).toArray();
        const toLocations = Linq2.where(storage.values(), s => move.fromStorageCode !== s.storageCode).toArray();

        return (
            <div>
                <p>Enter new stock information:</p>
                <div style={{ display: 'flex', flexDirection: 'row' }}>
                    <TextField select
                               label="Current Location"
                               style={{ ...FCSConstants.textFieldStyle, flex: 'auto' }}
                               value={move.fromStorageCode}
                               variant="outlined"
                               helperText="List of Current Locations"
                               onChange={this.updateFromLocation}>
                            {Util.isNonEmptyArray(fromLocations) &&
                            fromLocations.map(l => <MenuItem key={l.storageCode}
                                                             value={l.storageCode}>{l.name}</MenuItem>)}
                    </TextField>
                    <TextField style={{ ...FCSConstants.textFieldStyle, flex: 'auto' }}
                               variant="outlined"
                               helperText="Current stock quantity"
                               value={this.getCurrentQuantity(move.fromStorageCode)}
                               disabled/>
                </div>
                <div style={{ display: 'flex', flexDirection: 'row' }}>
                    <TextField select
                               label="Move To"
                               style={{ ...FCSConstants.textFieldStyle, flex: 'auto' }}
                               value={move.toStorageCode}
                               variant="outlined"
                               onChange={this.updateToLocation}>
                            {Util.isNonEmptyArray(toLocations) &&
                            toLocations.map(l => <MenuItem key={l.storageCode}
                                                           value={l.storageCode}>{l.name}</MenuItem>)}
                    </TextField>
                    <TextField type="number"
                               style={{ ...FCSConstants.textFieldStyle, flex: 'auto' }}
                               value={move.quantity}
                               variant="outlined"
                               onChange={this.updateQuantity}
                               label="Move Quantity"
                               error={this.getTotalStock(this.props.productItem.sku) < move.quantity}
                               helperText={this.getTotalStock(this.props.productItem.sku) < move.quantity ? 'Move quantity is greater than available stock' : 'Quantity to move'}
                               InputProps={{ inputProps: { min: 0 } }} />
                </div>
            </div>
        );
    }
}
