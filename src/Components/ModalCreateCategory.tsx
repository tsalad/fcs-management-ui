import { TextField } from '@material-ui/core';
import { ValidationService } from 'o8-common';
import * as React from 'react';
import Category from '../Models/Category';
import FCSConstants from '../Utils/FCSConstants';
import SimpleModal from './SimpleModal';

export interface IModalCreateCategoryProps {
    categories: Map<string, ICategory>;
    category: ICategory;
    confirm(category: ICategory): void;
    cancel(): void;
}

interface IModalCreateCategoryState {
    category: ICategory;
    showErrors: boolean;
}

export default class ModalCreateCategory extends React.Component<IModalCreateCategoryProps, IModalCreateCategoryState> {
    private readonly validator: ValidationService;

    constructor(props: IModalCreateCategoryProps) {
        super(props);
        this.state = {
            category: props.category,
            showErrors: false
        };
        this.validator = new ValidationService(this);
        this.validator.setValidationResult = this.validator.setValidationResult.bind(this.validator);
    }

    public componentDidMount(): void {
        this.validate(this.props.category);
    }

    private validate = (item: ICategory) => {
        const { name, categoryCode } = Category.validate(item);
        this.validator.setValidationResult('name', name);
        this.validator.setValidationResult('categoryCode', categoryCode);
    }

    private confirm = () => {
        if (!this.validator.isValid()) {
            this.setState({ showErrors: true });
            return;
        }
        this.props.confirm(this.state.category);
    }

    private updateName = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.updateCategory({ ...this.state.category, name: e.target.value });
    }

    private updateCategoryCode = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.updateCategory({ ...this.state.category, categoryCode: e.target.value.toUpperCase().trim() });
    }

    private updateCategory = (category: ICategory): void => {
        this.validate(category);
        this.setState({ category });
    }

    public render(): React.ReactNode {
        const { category, showErrors } = this.state;
        const nameValidator = showErrors && this.validator && this.validator.getResults().get('name');
        const categoryCodeValidator = showErrors && this.validator && this.validator.getResults().get('categoryCode');

        return (
            <SimpleModal title="Create New Category"
                         cancel={this.props.cancel}
                         confirmButtonText="Add"
                         confirm={this.confirm}>
                <p>Add a new Category and create a unique Category Code.</p>
                <div style={{display: 'flex', flexDirection: 'row'}}>
                    <TextField variant="outlined"
                               label="Category"
                               helperText={nameValidator && !nameValidator.valid ? nameValidator.message : 'New Category'}
                               error={nameValidator && !nameValidator.valid}
                               margin="normal"
                               style={{...FCSConstants.textFieldStyle}}
                               value={category.name}
                               onChange={this.updateName}
                               required />
                    <TextField variant="outlined"
                               label="Category Code"
                               helperText={categoryCodeValidator && !categoryCodeValidator.valid ? categoryCodeValidator.message : '3 or 4 characters'}
                               margin="normal"
                               style={{...FCSConstants.textFieldStyle}}
                               value={category.categoryCode}
                               error={categoryCodeValidator && !categoryCodeValidator.valid}
                               onChange={this.updateCategoryCode}
                               required />
                </div>
            </SimpleModal>
        );
    }
}
