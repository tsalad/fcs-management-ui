import { Button, Dialog, DialogActions, DialogContent, FormGroup, IconButton, MenuItem, Popover, TextField, Tooltip } from '@material-ui/core';
import { FilterList, Info, MoreVert } from '@material-ui/icons';
import * as moment from 'moment';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import ModalAddShipmentTracking from '../Contexts/ModalAddShipmentTracking';
import ConfigurationManager from '../Services/ConfigurationManager';
import OrdersFlaggedService from '../Services/OrdersFlaggedService';
import PrintService from '../Services/PrintService';
import ReleaseService from '../Services/ReleaseService';
import ShipmentService from '../Services/ShipmentService';
import Utilities from '../Services/Utilities';
import { IOrderFlag } from '../Typings/IOrderFlag';
import { IRelease } from '../Typings/IRelease';
import { OrderFlagReasonEnum } from '../Typings/OrderFlagReasonEnum';
import { PrintSideEnum } from '../Typings/PrintSideEnum';
import FCSConstants from '../Utils/FCSConstants';
import { formatDateString, getNestedValue } from '../Utils/Util';
import DataTable, { IDataTableColumn, SortOrder } from './DataTable';
import { IModalAddShipmentTrackingProps } from './ModalAddShipmentTracking';
import { ReasonGroupEnum } from './OrdersFlagged';
import { DateRangeEnum } from './OrdersPending';
import SectionTitle from './SectionTitle';

export interface ITableOrdersFlaggedProps {
    ordersFlagged: Map<string, IOrderFlag>;
    orderFlagReason: ReasonGroupEnum;
    fetchOrders(): void;
    setSuccessMessage(message: string): void;
    setErrorMessage(message: string): void;
    releases: Map<string, IRelease>;
}

interface ITableOrdersFlaggedState {
    menuAnchor: HTMLElement;
    orderPopoverKey: string;
    sortBy: string;
    sortOrder: SortOrder;
    filterQuantity: number;
    selectedDate: string;
    selectedRange: DateRangeEnum;
    showFilters: boolean;
    selectedFlags: IOrderFlag[];
    modalAddShipmentTrackingProps: Partial<IModalAddShipmentTrackingProps>;
    removeOrderFlagConfirmation: { confirm(): void, cancel(): void };
    flagsToRemove: IOrderFlag[];
}

const orderFlagReasonVanity = new Map([
    [OrderFlagReasonEnum.InvalidAddress, 'Invalid Address'],
    [OrderFlagReasonEnum.Other, 'Other'],
    [OrderFlagReasonEnum.Unshippable, 'Unshippable'],
    [OrderFlagReasonEnum.PackageItemHardwareDefect, 'Trackable is damaged or defective'],
    [OrderFlagReasonEnum.PackageItemPrintDefect, 'Trackable needs to be reprinted'],
    [OrderFlagReasonEnum.PackageItemMismatch, 'Trackable could not be matched'],
    [OrderFlagReasonEnum.PackageItemBadManifest, 'Trackable manifest entry not found'],
    [OrderFlagReasonEnum.ShippingLabelDefect, 'Shipping label needs to be reprinted']
]);

export default class TableOrdersFlagged extends React.Component<ITableOrdersFlaggedProps, ITableOrdersFlaggedState> {
    constructor(props: ITableOrdersFlaggedProps) {
        super(props);
        this.state = {
            menuAnchor: undefined,
            orderPopoverKey: undefined,
            sortBy: 'shipBy',
            sortOrder: SortOrder.Asc,
            filterQuantity: undefined,
            selectedDate: undefined,
            selectedRange: DateRangeEnum.On,
            showFilters: false,
            selectedFlags: [],
            modalAddShipmentTrackingProps: undefined,
            removeOrderFlagConfirmation: undefined,
            flagsToRemove: []
        };
    }

    private showMenu = (orderPopoverKey?: string) => (e: React.MouseEvent<HTMLElement>) => {
        this.setState({ menuAnchor: e.currentTarget, orderPopoverKey });
    }

    private hideMenu = () => {
        this.setState({ menuAnchor: undefined, orderPopoverKey: undefined });
    }

    private handleSort = (property: string): void => {
        const { sortOrder,  sortBy } = this.state;
        const order = (property !== sortBy || sortOrder === SortOrder.Desc) ? SortOrder.Asc : SortOrder.Desc;
        this.setState({ sortOrder:  order, sortBy: property });
    }

    private getFilteredSortedData = (): IOrderFlag[] => {
        const { ordersFlagged } = this.props;
        const { sortOrder,  sortBy, filterQuantity, selectedDate, selectedRange } = this.state;
        let modified: IOrderFlag[] = [ ...ordersFlagged.values() ];

        // First find flags that fall within the specified date range.
        if (selectedDate !== undefined) {
            if  (selectedRange === DateRangeEnum.On) {
                modified = modified.filter(d => d.shipBy && (d.shipBy.localeCompare(selectedDate) === 0)); 
            }
            if (selectedRange === DateRangeEnum.OnOrBefore) {
                modified = modified.filter(d => d.shipBy && (d.shipBy.localeCompare(selectedDate) <= 0)); 
            }
        }

        // Next sort the flags
        if (Util.isNonEmptyArray(modified)) {
            modified.sort((a: IOrderFlag, b: IOrderFlag) => (
            String(getNestedValue(sortBy, a)).localeCompare(String(getNestedValue(sortBy, b))) * (sortOrder === SortOrder.Asc ? 1 : -1)));
        }

        // Finally filter flags by quantity
        if (filterQuantity !== undefined && filterQuantity >= 0) {
            modified = modified.slice(0, filterQuantity); 
        }

        return modified;
    }

    private toggleShowFilters = () => {
        this.setState({ showFilters: !this.state.showFilters }); 
    }

    private clearFilterQuantity = (): void => {
        this.setState({ filterQuantity: undefined }); 
    }

    private setFilterDate = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ selectedDate: e.target.value, selectedFlags: [] }); 
    }

    private clearSelectedDate = (): void => {
        this.setState({ selectedDate: undefined, selectedRange: DateRangeEnum.On, selectedFlags: [] }); 
    }

    private setSelectedRange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ selectedRange: e.target.value as DateRangeEnum }); 
    }

    private setFilterQuantity = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const { value } = e.target;
        if (value !== '+' && value !== 'e') {
            this.setState({ filterQuantity: Number(value), selectedFlags: [] }); 
        }
    }

    private renderSubtitle = (filtered: IOrderFlag[] = []) => {
        const { ordersFlagged } = this.props;

        return (
            <div style={{display: 'flex', flexDirection: 'column'}}>
                <SectionTitle title={`Filters (SHOWING ${(filtered || []).length} OF ${(ordersFlagged || new Map()).size})`}
                              subtitle
                              titleActions={
                              <Tooltip title={this.state.showFilters ? 'Hide Filters' : 'Show Filters'}>
                                    <IconButton onClick={this.toggleShowFilters}>
                                        <FilterList />
                                    </IconButton>
                              </Tooltip>} />
                {this.state.showFilters && <div style={{ padding: '0 1rem' }}>{this.renderFilters([ ...ordersFlagged.values() ])}</div>}
            </div>
        );
    }

    private renderFilters = (flags: IOrderFlag[]): React.ReactNode => {
        const { filterQuantity,  selectedDate, selectedRange } = this.state;
        const dateList: string[] = Linq2.select(flags, d => d.shipBy).distinct().toArray();
        const flexCenter: React.CSSProperties = { display:  'flex', flexDirection: 'row', alignItems: 'center' };

        return (
            <>
                <FormGroup style={flexCenter}>
                    <p style={{fontWeight: 'bold', width: '170px'}}>Ship By Date: </p>
                    <TextField select
                               style={{ ...FCSConstants.textFieldStyle, maxWidth: '150px' }}
                               variant="outlined"
                               value={selectedRange || ''}
                               onChange={this.setSelectedRange}>
                        <MenuItem value={DateRangeEnum.On}>On</MenuItem>
                        <MenuItem value={DateRangeEnum.OnOrBefore}>On or before</MenuItem>
                    </TextField>
                    <TextField select
                               style={{ ...FCSConstants.textFieldStyle, maxWidth: '200px' }}
                               disabled={!Util.isNonEmptyArray(dateList)}
                               variant="outlined"
                               label="Select Date"
                               value={selectedDate || ''}
                               onChange={this.setFilterDate}>
                        {Util.isNonEmptyArray(dateList) &&
                        dateList.map(d => <MenuItem key={d} value={d}>{d}</MenuItem>)}
                    </TextField>
                    <Button color="primary" onClick={this.clearSelectedDate}>Clear</Button>
                </FormGroup>
                <FormGroup style={flexCenter}>
                    <p style={{fontWeight: 'bold', width: '170px'}}>First N Orders: </p>
                    <TextField type="number"
                               style={{ ...FCSConstants.textFieldStyle, maxWidth: '150px' }}
                               variant="outlined"
                               placeholder="Enter Quantity"
                               InputProps={{ inputProps: { min: '0' } }}
                               value={filterQuantity !== undefined ? Number(filterQuantity) : ''}
                               onChange={this.setFilterQuantity} />
                    <Button color="primary" onClick={this.clearFilterQuantity}>Clear</Button>
                </FormGroup>
            </>
        );
    }

    private handleItemsSelect = (selectedFlags: IOrderFlag[]) => {
        this.setState({ selectedFlags });
    }

    private renderTitleActions = () => {
        if (Util.isNonEmptyArray(this.state.selectedFlags)) {
            return (
                <>
                    <IconButton onClick={this.showMenu()}>
                        <MoreVert />
                    </IconButton>
                    <Popover open={Boolean(this.state.menuAnchor) && !Boolean(this.state.orderPopoverKey)}
                             anchorEl={this.state.menuAnchor}
                             anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                             transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                             onClose={this.hideMenu}>
                        {this.renderOptions(this.state.selectedFlags)}
                    </Popover>
                </>
            );
        }
    }

    private rejectOrders = (flags: IOrderFlag[]) => async () => {
        try {
            await ReleaseService.rejectOrders(flags);
            this.props.fetchOrders();
            this.setState({ selectedFlags: [], menuAnchor: undefined, orderPopoverKey: undefined });
            this.props.setSuccessMessage(`Successfully reject order${flags.length > 1 ? 's' : ''} to DXP`);
        } catch (err) {
            this.props.setErrorMessage(`Failed to reject order${flags.length > 1 ? 's' : ''} to DXP`);
            throw new Error(err);
        }
    }

    private showModalAddShipmentTracking = (orderFlag: IOrderFlag) => async (): Promise<void> => {
        const promise: Promise<void> = new Promise((confirm, cancel) => {
            this.setState({ modalAddShipmentTrackingProps: { confirm, cancel, orderId: orderFlag.orderId, orderFlag } });
        });
        try {
            await promise;
        } catch (e) {
            // ignore
        }
        this.props.fetchOrders();
        this.setState({ modalAddShipmentTrackingProps: undefined, selectedFlags: [], menuAnchor: undefined, orderPopoverKey: undefined });
    }

    private deleteOrderFlags = (flags: IOrderFlag[]) => async () => {
        try {
            if (Util.isNonEmptyArray(flags)) {
                await Promise.all(flags.map(async f => OrdersFlaggedService.deleteFlag(f.id)));
            }
            this.props.fetchOrders();
            this.setState({ selectedFlags: [], menuAnchor: undefined, orderPopoverKey: undefined });
            this.props.setSuccessMessage('Successfully deleted flags');
        } catch (err) {
            this.props.setErrorMessage('Failed to delete flags');
        }

        if (Util.isNonEmptyArray(this.state.flagsToRemove)) {
            this.cancelDeleteOrderFlags();
        }
    }

    private cancelDeleteOrderFlags = () => {
        this.setState({ flagsToRemove: undefined });
    }

    private showModalPrintShippingLabels = (flags?: IOrderFlag[]) => async (): Promise<void> => {
        try {
            const shipments = await Promise.all(flags.map(async f => ShipmentService.fetchShipment(f.orderId)));
            const imageUrls = Linq2.selectMany(shipments, s => s.packages)
                                   .where(p => Boolean(p.labelUrl))
                                   .select(p => p.labelUrl)
                                   .toArray();
            const promise: Promise<void> = new Promise((confirm, cancel) => {
                this.setState({ removeOrderFlagConfirmation: { confirm, cancel } });
            });
            let remove = false;
            try {
                await promise;
                remove = true;
            } catch (e) {
                // ignore
            }
            await ShipmentService.printShippingLabels(imageUrls);
            if (remove) {
                await Promise.all(flags.map(async f => OrdersFlaggedService.deleteFlag(f.id)));        
            }
        } catch (e) {
            this.props.setErrorMessage('Failed to print shipping labels');
        }

        this.props.fetchOrders();
        this.setState({ removeOrderFlagConfirmation: undefined, selectedFlags: [], menuAnchor: undefined, orderPopoverKey: undefined });
    }

    private showRemoveOrderFlagCofirmation = (flagsToRemove: IOrderFlag[]) => () => {
        this.setState({ flagsToRemove });
    }

    private printLogojetImage = (flags: IOrderFlag[], side: PrintSideEnum) => async (): Promise<void> => {
        try {
            const { releases } = this.props;
            const orders: IRelease[] = Linq2.select(flags, f => releases.get(f.orderId)).toArray();
            const filtered: Map<string, IReleaseAssignment[]> = Linq2.selectMany(orders, o => o.items)
                                                                     .where(r => Util.isNonEmptyArray(r.assignments))
                                                                     .selectMany(r => r.assignments)
                                                                     .where(a => flags.some(f => Boolean(f.assigneeId) && f.assigneeId === a.assigneeId))
                                                                     .groupBy(a => a.orderId)
                                                                     .toDictionary();
            const promise: Promise<void> = new Promise((confirm, cancel) => {
                this.setState({ removeOrderFlagConfirmation: { confirm, cancel } });
            });
            let remove = false;

            try {
                await promise;
                remove = true;
            } catch (e) {
                // ignore
            }

            const image = await PrintService.createLogojetImage(filtered, side);
            const url = await PrintService.buildPrintUrl(image, 'pdf', `FLAGGED_${moment().format('MM-DD-YYYY')}_${side}.pdf`);
            Utilities.downloadFile(url);
            if (remove) {
                await Promise.all(flags.map(async f => OrdersFlaggedService.deleteFlag(f.id)));
            }
        } catch (e) {
            this.props.setErrorMessage('Failed to create image');
        }

        this.setState({ removeOrderFlagConfirmation: undefined });
    }
    
    private renderOptions = (flags: IOrderFlag[] = []) => {
        const rejectOrders = <MenuItem onClick={this.rejectOrders(flags)}>Send to DXP</MenuItem>;
        const addTracking = flags.length === 1 && <MenuItem onClick={this.showModalAddShipmentTracking(flags[0])}>Enter Tracking #s</MenuItem>;
        const printLabels = <MenuItem onClick={this.showModalPrintShippingLabels(flags)}>{`Print Shipping Label${flags.length > 1 ? 's' : ''}`}</MenuItem>;
        const deleteOrderFlags = <MenuItem onClick={this.showRemoveOrderFlagCofirmation(flags)}>Delete Flags</MenuItem>;
        const printLogojetImageFront = <MenuItem onClick={this.printLogojetImage(flags, PrintSideEnum.front)}>Print Logojet Front</MenuItem>;
        const printLogojetImageBack = <MenuItem onClick={this.printLogojetImage(flags, PrintSideEnum.back)}>Print Logojet Back</MenuItem>;

        switch (this.props.orderFlagReason) {
            case ReasonGroupEnum.ShippingDefect:
                return (
                    <>
                        {rejectOrders}
                        {addTracking}
                        {deleteOrderFlags}
                    </>
                );
            case ReasonGroupEnum.ShippingLabelDefect:
                return (
                    <>
                        {printLabels}
                        {deleteOrderFlags}
                    </>
                );
            case ReasonGroupEnum.TrackableDefect:
                return (
                    <>
                        {printLogojetImageFront}
                        {printLogojetImageBack}
                        {deleteOrderFlags}
                    </>
                );
            case ReasonGroupEnum.Other:
                return (
                    <>
                        {rejectOrders}
                        {addTracking}
                        {deleteOrderFlags}
                    </>
                );
        }
    }

    private renderRemoveOrderFlagModal = () => {
        const { removeOrderFlagConfirmation } = this.state;
        return (
            <Dialog open={true}>
                <DialogContent style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '2rem' }}>
                    <p>Would you like to remove the flags for the selected orders?</p>
                </DialogContent>
                <DialogActions>
                    <Button color="primary" variant="outlined" onClick={removeOrderFlagConfirmation.cancel}>No</Button>
                    <Button color="primary" variant="contained" onClick={removeOrderFlagConfirmation.confirm}>Yes</Button>
                </DialogActions>
            </Dialog>
        );
    }

    private renderDeleteOrderFlagConfirmationModal = () => {
        const { flagsToRemove } = this.state;
        return (
            <Dialog open={true}>
                <DialogContent style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '2rem' }}>
                    <p>Are you sure you would like to remove the flags for the selected orders?</p>
                </DialogContent>
                <DialogActions>
                    <Button color="primary" variant="outlined" onClick={this.cancelDeleteOrderFlags}>Cancel</Button>
                    <Button color="primary" variant="contained" onClick={this.deleteOrderFlags(flagsToRemove)}>Confirm</Button>
                </DialogActions>
            </Dialog>
        );
    }

    public render(): React.ReactNode {
        const { modalAddShipmentTrackingProps, removeOrderFlagConfirmation, flagsToRemove } = this.state;
        const columns: IDataTableColumn<IOrderFlag>[] = [
            {
                title: 'Guest Name',
                value: (item) => {
                    if (Boolean(item.assigneeId)) {
                        const release = this.props.releases.get(item.orderId);
                        const assignee = Linq2.firstOrDefault(Linq2.selectMany(release.items, i => i.assignments), undefined, a => a.assigneeId === item.assigneeId);
                        const assigneeData = JSON.parse(assignee.assigneeData);
                        const first = assigneeData.caption1 || '';
                        const last = assigneeData.caption3 ? ` ${assigneeData.caption3}` : '';
                        return `${first}${last}`;
                    }
                    return item.addressName;
                },
                headerClicked: () => this.handleSort('addressName'),
                sortOrder: this.state.sortBy === 'addressName' ? this.state.sortOrder : undefined
            },
            {
                title: 'Ship By',
                value: (item) => formatDateString(item.shipBy),
                headerClicked: () => this.handleSort('shipBy'),
                sortOrder: this.state.sortBy === 'shipBy' ? this.state.sortOrder : undefined
            },
            {
                title: 'Flag Reason',
                value: (item) => orderFlagReasonVanity.get(item.reason),
                headerClicked: () => this.handleSort('reason'),
                sortOrder: this.state.sortBy === 'reason' ? this.state.sortOrder : undefined
            },
            {
                title: 'Status',
                value: (item) => <Tooltip disableFocusListener={!item.status}
                                          disableHoverListener={!item.status}
                                          title={item.status}><Info color="primary" /></Tooltip>
            },
            {
                title: 'Actions',
                value: (item) => (
                    <>
                        <IconButton onClick={this.showMenu(item.id)}>
                            <MoreVert />
                        </IconButton>
                        <Popover open={Boolean(this.state.menuAnchor) && this.state.orderPopoverKey === item.id}
                                 anchorEl={this.state.menuAnchor}
                                 anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                                 transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                                 onClose={this.hideMenu}>
                            {this.renderOptions([ item ])}
                        </Popover>
                    </>
                )
            }
        ];

        const data = this.getFilteredSortedData();

        return (
            <>
                {Boolean(modalAddShipmentTrackingProps) && <ModalAddShipmentTracking {...modalAddShipmentTrackingProps} />}
                {Boolean(removeOrderFlagConfirmation) && this.renderRemoveOrderFlagModal()}
                {Util.isNonEmptyArray(flagsToRemove) && this.renderDeleteOrderFlagConfirmationModal()}
                <DataTable data={data}
                           handleItemsSelect={this.handleItemsSelect}
                           titleActions={this.renderTitleActions()}
                           paginate
                           subtitle={this.renderSubtitle(data)}
                           selectedItems={this.state.selectedFlags}
                           columns={columns} />
            </>
        );
    }
}
