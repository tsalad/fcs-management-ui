import { Button, FormGroup, IconButton, MenuItem, Popover, TextField, Tooltip, Typography } from '@material-ui/core';
import { FilterList, MoreVert, Warning } from '@material-ui/icons';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import CreateProduct from '../Contexts/CreateProduct';
import { ProductItem } from '../Models';
import FCSConstants from '../Utils/FCSConstants';
import DataTable, { IDataTableColumn, SortOrder } from './DataTable';
import SectionTitle from './SectionTitle';
import View from './View';

export interface IProductsProps {
    items: Map<string, IProductItem>;
    categories: Map<string, ICategory>;
    products: Map<string, IProduct>;
    fetchItems(): void;
    fetchCategories(): void;
    fetchProducts(): void;
    fetchStock(): void;
    stock: Map<string, IStock[]>;
}

export interface IProductsState {
    item: IProductItem;
    menuAnchor: HTMLElement;
    sortBy: string;
    sortOrder: SortOrder;
    selectedSku: string;
    showFilters: boolean;
    categoryFilter: string;
    productFilter: string;
}

export default class Products extends React.Component<IProductsProps, IProductsState> {
    constructor(props: IProductsProps) {
        super(props);
        this.state = {
            item: undefined,
            menuAnchor: undefined,
            sortBy: 'name',
            sortOrder: SortOrder.Asc,
            selectedSku: undefined,
            showFilters: false,
            categoryFilter: undefined,
            productFilter: undefined
        };
    }

    public async componentDidMount(): Promise<void> {
        try {
            this.props.fetchItems();
            this.props.fetchCategories();
            this.props.fetchProducts();
            this.props.fetchStock();
        } catch (err) {
            console.error(err);
        }
    }

    private toggleProductForm = (item?: IProductItem) => {
        return () => {
            this.setState({ item, menuAnchor: undefined, selectedSku: undefined });
        };
    }

    private getCategoryName = (categoryCode: string): string => {
        if (this.props.categories.has(categoryCode)) {
            return this.props.categories.get(categoryCode).name;
        }
        return '';
    }

    private getGroupName = (productCode: string): string => {
        if (this.props.products.has(productCode)) {
            return this.props.products.get(productCode).name;
        }
        return '';
    }

    private showMenu = (selectedSku: string) => (e: React.MouseEvent<HTMLElement>) => {
        this.setState({ menuAnchor: e.currentTarget, selectedSku });
    }

    private hideMenu = () => {
        this.setState({ menuAnchor: undefined, selectedSku: undefined });
    }

    private handleSort = (property: string): void => {
        const { sortOrder, sortBy } = this.state;
        const order = (property !== sortBy || sortOrder === SortOrder.Desc) ? SortOrder.Asc : SortOrder.Desc;
        this.setState({ sortOrder: order, sortBy: property });
    }
    
    private getFilteredSortedData = (): IProductItem[] => {
        const { items } = this.props;
        const { sortOrder, sortBy, categoryFilter, productFilter } = this.state;
        const itemArr = Linq2.where(items.values(), i => (categoryFilter === undefined || i.categoryCode === categoryFilter) &&
                                                         (productFilter === undefined || i.productCode === productFilter)).toArray();

        if (Util.isNonEmptyArray(itemArr)) {
            return itemArr.sort((a: IProductItem, b: IProductItem) => (
                String(a[sortBy]).localeCompare(String(b[sortBy])) * (sortOrder === SortOrder.Asc ? 1 : -1))
            );
        }
        return itemArr;
    }

    private toggleShowFilters = () => {
        this.setState({ showFilters: !this.state.showFilters });
    }

    private setCategoryFilter = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ categoryFilter: e.target.value });
    }

    private clearCategoryFilter = () => {
        this.setState({ categoryFilter: undefined });
    }

    private setProductFilter = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ productFilter: e.target.value });
    }

    private clearProductFilter = () => {
        this.setState({ productFilter: undefined });
    }

    private renderFilters = () => {
        const { categoryFilter, productFilter } = this.state;
        const categories = [ ...this.props.categories.values() ].map(c => <MenuItem value={c.categoryCode} key={c.categoryCode}>{c.name}</MenuItem>);
        const groups = [ ...this.props.products.values() ].map(g => <MenuItem value={g.productCode} key={g.productCode}>{g.name}</MenuItem>);
        const flexCenter: React.CSSProperties = { display: 'flex', flexDirection: 'row', alignItems: 'center' };
        return (
            <>
                <FormGroup style={flexCenter}>
                    <p style={{fontWeight: 'bold', width: '170px'}}>Category: </p>
                    <TextField style={{...FCSConstants.textFieldStyle, maxWidth: '300px'}}
                               select
                               variant="outlined"
                               value={categoryFilter !== undefined ? categoryFilter : ''}
                               onChange={this.setCategoryFilter}>
                        {categories}
                    </TextField>
                    <Button color="primary" disabled={categoryFilter === undefined} onClick={this.clearCategoryFilter}>Clear</Button>
                </FormGroup>
                <FormGroup style={flexCenter}>
                    <p style={{fontWeight: 'bold', width: '170px'}}>Product Group: </p>
                    <TextField style={{...FCSConstants.textFieldStyle, maxWidth: '300px'}}
                               select
                               variant="outlined"
                               value={productFilter !== undefined ? productFilter : ''}
                               onChange={this.setProductFilter}>
                        {groups}
                    </TextField>
                    <Button color="primary" disabled={productFilter === undefined} onClick={this.clearProductFilter}>Clear</Button>
                </FormGroup>
            </>
        );
    }

    private getTotalStock = (item: IProductItem) => {
        const currentStock = this.props.stock ? this.props.stock.get(item.sku) : [];
        const total = Util.isNonEmptyArray(currentStock) ? Linq2.aggregate(currentStock, (acc: number = 0, curr) => acc += curr.quantity) : 0;
        return (
            <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                {total}
                {Boolean(item.reorderThreshold) && total < item.reorderThreshold &&
                <Tooltip title="Current Stock is less than reorder threshold!">
                    <Warning style={{color: 'red', marginLeft: '1rem'}} />
                </Tooltip>}
            </div>
        );
    }

    private renderSubtitle = (filtered: IProductItem[] = []) => {
        return (
            <div style={{display: 'flex', flexDirection: 'column'}}>
                <SectionTitle title={`Filters (SHOWING ${(filtered || []).length} OF ${this.props.items.size || 0})`}
                              subtitle
                              titleActions={
                              <Tooltip title={this.state.showFilters ? 'Hide Filters' : 'Show Filters'}>
                                    <IconButton onClick={this.toggleShowFilters}>
                                        <FilterList />
                                    </IconButton>
                              </Tooltip>} />
                {this.state.showFilters && <div style={{ padding: '0 1rem' }}>{this.renderFilters()}</div>}
            </div>
        );
    }

    public render(): React.ReactNode {
        if (this.state.item !== undefined) {
            return  <CreateProduct cancel={this.toggleProductForm()} item={this.state.item} />;
        }

        const columns: IDataTableColumn<IProductItem>[] = [
            {
                title: 'Product Name',
                value: (item: IProductItem) => item.name,
                headerClicked: () => this.handleSort('name'),
                sortOrder: this.state.sortBy === 'name' ? this.state.sortOrder : undefined
            },
            {
                title: 'SKU',
                value: (item: IProductItem) => item.sku,
                headerClicked: () => this.handleSort('sku'),
                sortOrder: this.state.sortBy === 'sku' ? this.state.sortOrder : undefined
            },
            {
                title: 'Category',
                value: (item: IProductItem) => this.getCategoryName(item.categoryCode),
                headerClicked: () => this.handleSort('categoryCode'),
                sortOrder: this.state.sortBy === 'categoryCode' ? this.state.sortOrder : undefined
            },
            {
                title: 'Product Group',
                value: (item: IProductItem) => this.getGroupName(item.productCode),
                headerClicked: () => this.handleSort('productCode'),
                sortOrder: this.state.sortBy === 'productCode' ? this.state.sortOrder : undefined
            },
            {
                title: 'Stock',
                value: (item: IProductItem) => this.getTotalStock(item)
            },
            {
                value: (item: IProductItem) => {
                    return (
                        <>
                            <IconButton onClick={this.showMenu(item.sku)}>
                                <MoreVert />
                            </IconButton>
                            <Popover open={this.state.selectedSku === item.sku}
                                     anchorEl={this.state.menuAnchor}
                                     anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                                     transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                                     onClose={this.hideMenu}>
                                <MenuItem onClick={this.toggleProductForm(item)}>View/Edit</MenuItem>
                            </Popover>
                        </>
                    );
                }
            }
        ];

        const data = this.getFilteredSortedData();

        return (
            <View title="Products"
                  actions={<Button onClick={this.toggleProductForm(new ProductItem())}
                                   variant="contained"
                                   color="primary">New Product</Button>}>
                <DataTable paginate
                           subtitle={this.renderSubtitle(data)}
                           data={data}
                           columns={columns} />
            </View>
        );
    }
}
