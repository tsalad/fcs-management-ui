import { Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import { IRelease } from '../Typings/IRelease';
import { IShipmentPackage } from '../Typings/IShipmentPackage';

export interface IProductAvailabilityListProps {
    orders?: IRelease[];
    shipments?: IShipmentPackage[];
    productItems: Map<string, IProductItem>;
    stock: Map<string, IStock[]>;
    fetchStock(): void;
    fetchProductItems(): void;
}

export default class ProductAvailabilityList extends React.Component<IProductAvailabilityListProps> {
    public componentDidMount() {
        this.props.fetchStock();
        this.props.fetchProductItems();
    }

    private renderItem = (sku: string, items: IReleaseItem[] | IShipmentPackageItem[], index: number) => {
        const { productItems, stock } = this.props;
        const total = Util.isNonEmptyArray(items) ? Linq2.aggregate(items, (acc: number = 0, curr) => acc += curr.quantity) : 0;
        const totalStock = Boolean(stock) && stock.has(sku) ? Linq2.aggregate(stock.get(sku), (acc: number = 0, curr) => acc += curr.quantity) : 0;
        return (
            <TableRow key={`table-row-${index}`}>
                <TableCell style={{ border: 'none' }}>{productItems.has(sku) && productItems.get(sku).name}</TableCell>
                <TableCell style={{ border: 'none' }}>{sku}</TableCell>
                <TableCell style={{ border: 'none', textAlign: 'right' }}>{totalStock}</TableCell>
                <TableCell style={{ border: 'none', textAlign: 'right' }}>{total}</TableCell>
            </TableRow>
        );
    }

    public render(): React.ReactNode {
        const { orders, shipments } = this.props;
        if (!Util.isNonEmptyArray(orders) && !Util.isNonEmptyArray(shipments)) {
            return null;
        }

        const orderItems = Util.isNonEmptyArray(orders) ? Linq2.selectMany(orders, order => order.items).groupBy(item => item.sku).toArray() : [];
        const shipmentItems = Util.isNonEmptyArray(shipments) ? Linq2.selectMany(shipments, shipment => shipment.items).groupBy(item => item.sku).toArray() : [];
        const data = [ ...orderItems, ...shipmentItems ];

        return (
            <div style={{padding: '1rem'}}>
                <Table padding="none" style={{maxHeight: '200px', overflow: 'auto'}}>
                    <TableHead>
                        <TableRow>
                            <TableCell style={{ border: 'none' }}>Product Name</TableCell>
                            <TableCell style={{ border: 'none' }}>SKU</TableCell>
                            <TableCell style={{ border: 'none', textAlign: 'right' }}>Available</TableCell>
                            <TableCell style={{ border: 'none', textAlign: 'right' }}>Needed</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {Util.isNonEmptyArray(data) && data.sort((a, b) => a[0].localeCompare(b[0]))
                                                        .map(([ sku, items ], i) => this.renderItem(sku, items, i))}
                    </TableBody>
                </Table>
            </div>
        );
    }
}
