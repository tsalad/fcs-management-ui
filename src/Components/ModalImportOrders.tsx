import * as React from 'react';
import DemoService from '../Services/DemoService';
import FileUploadInput from './FileUploadInput';
import SimpleModal from './SimpleModal';

export interface IModalImportOrdersProps {
    close(): void;
    setErrorMessage(message: string): void;
    setSuccessMessage(message: string): void;
    fetchOrders(): void;
}

interface IModalImportOrdersState {
    file: File;
}

export default class ModalImportOrders extends React.Component<IModalImportOrdersProps, IModalImportOrdersState> {
    constructor(props: IModalImportOrdersProps) {
        super(props);
        this.state = { file: undefined };
    }

    private updateFile = (file: File): void => {
        this.setState({ file });
    }

    private confirm = async () => {
        try {
            const { file } = this.state;
            const items = await DemoService.upload(file);
            this.props.setSuccessMessage(`Successfully created ${items || 0} order${Boolean(items) ? 's' : ''}`);
            this.props.fetchOrders();
        } catch (err) {
            this.props.setErrorMessage('Failed to import orders');
        }
        this.props.close();
    }

    public render(): React.ReactNode {
        return (
            <SimpleModal title="Import Orders"
                         disableConfirmButton={!Boolean(this.state.file)}
                         confirm={this.confirm}
                         cancel={this.props.close}>
                <p>Choose a .csv file to import</p>
                <FileUploadInput onUpdate={this.updateFile} title="BROWSE" acceptType=".csv" />
                {Boolean(this.state.file) && <p style={{ margin: '1rem 0 0 .5rem' }}>Selected file: {this.state.file.name}</p>}
            </SimpleModal>
        );
    }
}
