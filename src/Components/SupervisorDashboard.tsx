import * as moment from 'moment';
import { getMuiTheme, Linq2, Util } from 'o8-common';
import * as React from 'react';
import { Area, AreaChart, Bar, BarChart, LabelList, ResponsiveContainer, Tooltip, XAxis } from 'recharts';
import DispatchService from '../Services/DispatchService';
import ReleaseService from '../Services/ReleaseService';
import ShipmentService from '../Services/ShipmentService';
import { IDispatch } from '../Typings/IDispatch';
import { IRelease } from '../Typings/IRelease';
import { IShipment } from '../Typings/IShipment';
import { IShipmentPackage } from '../Typings/IShipmentPackage';
import { StateEnum } from '../Typings/StateEnum';
import Tile from './Tile';

const legacyTheme = getMuiTheme();

interface ISupervisorDashboardState {
    dispatches: Map<string, IDispatch>;
    shipByReleases: Map<string, IRelease[]>;
    shipments: IShipment[];
}

export default class SupervisorDashboard extends React.Component<undefined, ISupervisorDashboardState> {
    public timer: number;

    constructor(props: undefined) {
        super(props);
        this.state = {
            dispatches: new Map(),
            shipByReleases: new Map(),
            shipments: []
        };
    }

    public componentWillMount = async () => {
        this.fetchData();
        this.timer = window.setInterval(this.fetchData, 30000);
    }

    public componentWillUnmount = () => {
        window.clearInterval(this.timer);
    }

    private fetchData = async () => {
        const format = 'YYYY-MM-DD';

        const shipments = [
            ...await ShipmentService.fetchCompletedShipments(
                moment().subtract(1, 'day').startOf('day').toISOString(),
                moment().endOf('day').toISOString()),
            ...await ShipmentService.fetchShipmentsInFulfillment()
        ];

        const dispatchCodes = Linq2.where(shipments, s => Boolean(s.dispatchCode))
                                   .select(s => s.dispatchCode)
                                   .distinct()
                                   .toArray() || [];

        const dispatches: Map<string, IDispatch> = Linq2.toDictionary(await Promise.all(dispatchCodes.map(async c => DispatchService.fetchDispatch(c))), d => d.dispatchCode);

        const releases = await ReleaseService.fetchReleasesByShipBy(
            [StateEnum.READY, StateEnum.PENDING, StateEnum.SHIPPABLE],
            moment().format(format),
            moment().add(7, 'days').format(format)
        );

        this.setState({ dispatches, shipments, shipByReleases: Linq2.groupBy(releases || [], r => r.shipBy).toDictionary() });
    }

    private renderTotalOrdersCompleted = () => {
        const groupedOrders = this.groupOrdersStatus();
        const completed = groupedOrders.get('COMPLETED');
        const total = Linq2.selectMany(groupedOrders.values(), o => o).toArray();
        return (
            <Tile style={{marginBottom: '1rem'}}
                  title={<h3 style={{color: legacyTheme.palette.primary1Color}}>TOTAL <strong>ORDERS COMPLETED</strong></h3>}>
                <p style={{color: legacyTheme.palette.primary1Color, margin: '1rem 0 0 0', fontSize: '45px', textAlign: 'right'}}>
                    <strong>{`${Util.isNonEmptyArray(completed) ? completed.length : 0} / `}</strong>
                    {Util.isNonEmptyArray(total) ? total.length : 0}
                </p>
            </Tile>
        );
    }

    private renderTotalOrdersPending = () => {
        const groupedOrders = this.groupOrdersStatus();
        const pending = groupedOrders.get('PENDING');
        const total = Linq2.selectMany(groupedOrders.values(), o => o).toArray();
        return (
            <Tile style={{marginBottom: '1rem'}}
                  title={<h3 style={{color: legacyTheme.palette.primary1Color}}>TOTAL <strong>ORDERS PENDING</strong></h3>}>
                <p style={{color: legacyTheme.palette.primary1Color, margin: '1rem 0 0 0', fontSize: '45px', textAlign: 'right'}}>
                    <strong>{`${Util.isNonEmptyArray(pending) ? pending.length : 0} / `}</strong>
                    {Util.isNonEmptyArray(total) ? total.length : 0}
                </p>
            </Tile>
        );
    }

    private renderTodaysOrderStatusTick = (props: any) => {
        const { x, y, payload } = props;

        return (
            <g transform={`translate(${x},${y})`}>
                <text x={0} y={0} dy={16} style={{ fontSize: '20px', fontWeight: 'bold' }} textAnchor="middle" fill="#fff">{payload.value}</text>
                <text x={0} y={0} dy={32} style={{ fontSize: '14px' }} textAnchor="middle" fill="#fff">ORDERS</text>
            </g>
        );
    }

    private groupOrdersStatus = (): Map<'PENDING' | 'PRINTING' | 'ASSIGNING' | 'COMPLETED', IShipmentPackage[]> => {
        const { dispatches, shipments } = this.state;

        const packages = Linq2.selectMany(shipments, s => s.packages)
                              .where(p => !Boolean(p.completedAt) || moment(p.completedAt).isSame(moment(), 'day'))
                              .toArray();

        const dispatchGroups = Linq2.selectMany(dispatches.values(), d => d.groups)
                                    .toArray();

        const completed = Linq2.where(packages, p => Boolean(p.completedAt)).toArray();
        const allPending = Linq2.except(packages, completed, (a, b) => a.packageCode.localeCompare(b.packageCode)).toArray();
        const assigned = Linq2.where(allPending, p => Util.isNonEmptyArray(p.assignments))
                                .where(p => p.assignments.every(a => Boolean(a.uid)))
                                .toArray();
        const printed = Linq2.except(allPending, assigned, (a, b) => a.packageCode.localeCompare(b.packageCode))
                                .where(p => {
                                    const group = dispatchGroups.find(g => g.orderIds.includes(p.orderId));
                                    return Boolean(group) && Boolean(group.imageUri);
                                }).toArray();
        const pending = Linq2.except(allPending, printed, (a, b) => a.packageCode.localeCompare(b.packageCode))
                                .except(assigned, (a, b) => a.packageCode.localeCompare(b.packageCode))
                                .toArray();
        return new Map([
            ['PENDING', pending],
            ['PRINTING', printed],
            ['ASSIGNING', assigned],
            ['COMPLETED', completed]
        ]);
    }

    private renderTodaysOrderStatusLabels = (props: any) => {
        const { x, y, width, value } = props;
        const radius = 25;

        return (
            <g>
                <circle cx={x + width / 2}
                        cy={(y - radius * 1.5)}
                        r={radius}
                        stroke="#fff"
                        strokeWidth={2}
                        fill="#fff"
                        fillOpacity={.9} />
                <text x={x + width / 2}
                      y={(y - radius * 1.5)}
                      style={{fontWeight: 'bold'}}
                      fill={legacyTheme.palette.primary1Color}
                      textAnchor="middle"
                      dominantBaseline="middle">{value}</text>
            </g>
        );
    }

    private renderTodaysOrdersStatus = () => {
        const data = Linq2.select((this.groupOrdersStatus() || new Map()).entries(), ([ label, value ]) => ({ label, value: Util.isNonEmptyArray(value) ? value.length : 0 }))
                          .toArray();

        return (
            <Tile style={{marginBottom: '1rem', background: legacyTheme.palette.primary1Color, flex: '2', flexFlow: 'none', alignItems: 'flex-end'}}>
                <ResponsiveContainer width="100%" height="100%">
                    <BarChart data={data} margin={{ top: 90, right: 40, bottom: 0, left: 40 }}>
                        <Bar barSize={75} stroke="#fff" strokeWidth={3} fill="#fff" fillOpacity={.5} dataKey="value">
                            <LabelList dataKey="value" content={this.renderTodaysOrderStatusLabels} />
                        </Bar>
                        <XAxis dataKey="label"
                               height={60}
                               tickLine={false}
                               axisLine={{ fill: '#fff', stroke: '#fff', strokeWidth: 3.5 }}
                               tick={this.renderTodaysOrderStatusTick} />
                    </BarChart>
                </ResponsiveContainer>
            </Tile>
        );
    }

    private formatYesterdaysData = (hour: number): number => {
        const { dispatches } = this.state;
        const totalDispatchPackages = Linq2.where(dispatches.values(), d => moment(d.beganAt).startOf('day').isBefore(moment().startOf('day')))
                                           .selectMany(d => d.shipments)
                                           .selectMany(s => s.packages)
                                           .where(p => !Boolean(p.completedAt) || moment(p.completedAt).isAfter(moment().subtract(1, 'day').startOf('day')))
                                           .toArray();
        const totalCompleted = Linq2.where(totalDispatchPackages, p => Boolean(p.completedAt) && moment(p.completedAt).isBefore(moment().subtract(1, 'day').hour(hour).startOf('hour'))).toArray() || [];
        return Util.isNonEmptyArray(totalCompleted) ? Math.round(((totalDispatchPackages.length - totalCompleted.length) / totalDispatchPackages.length) * 100) : 100;
    }

    private formatOrdersRemaningData = (): { time: number, totalToday: number, totalYesterday: number }[] => {
        const data = [];
        const { shipments } = this.state;
        for (let i = 6; i <= 17; i++) {
            const packages = Linq2.selectMany(shipments, d => d.packages)
                                  .toArray();
            const packagesToday = Linq2.where(packages, p => !Boolean(p.completedAt) || moment(p.completedAt).startOf('day').isSame(moment().startOf('day'), 'day'))
                                       .toArray();
            const completedToday = Linq2.where(packagesToday, p => Boolean(p.completedAt))
                                        .where(p => moment(p.completedAt).isBefore(moment().hour(i).startOf('hour')))
                                        .toArray();

            data.push({
                time: i,
                totalToday: moment().hours() >= i ? (Util.isNonEmptyArray(completedToday) ? Math.round(((packagesToday.length - completedToday.length) / packagesToday.length) * 100) : 100) : null,
                totalYesterday: this.formatYesterdaysData(i)
            });
        }
        return data;
    }

    private renderOrdersRemaningComparisonLabel = (data: { time: number, totalToday: number, totalYesterday: number }[]) => (props: any) => {
        const { x, y, index } = props;
        const item = data[index];
        const currentTime = moment().hour();
        if ((currentTime >= 17 && index === data.length - 1) || (currentTime === item.time)) {
            const radius = 15;
            return (
                <g>
                    <circle cx={x}
                            cy={(y - radius) - 10}
                            r={radius}
                            stroke="#fff"
                            strokeWidth={2}
                            fill="#fff"
                            fillOpacity={.9} />
                    <text x={x}
                          y={(y - radius) - 10}
                          style={{fontWeight: 'bold', fontSize: '10px'}}
                          fill={legacyTheme.palette.primary1Color}
                          textAnchor="middle"
                          dominantBaseline="middle">{`${item.totalToday}%`}</text>
                </g>
            );
        }
    }

    private renderOrdersRemainingComparison = () => {
        const data = this.formatOrdersRemaningData();
        return (
            <Tile style={{background: legacyTheme.palette.primary1Color, flex: '1'}}
                  title={<h3 style={{color: '#fff'}}>PERCENTAGE OF <strong>ORDERS REMAINING</strong> (<strong style={{color: '#00386E'}}>YESTERDAY</strong> vs <strong>TODAY</strong>)</h3>}>
                <ResponsiveContainer width="100%" height="100%">
                    <AreaChart data={data} margin={{ top: 60, right: 40, bottom: 0, left: 40 }}>
                        <XAxis dataKey="time"
                               tickLine={false}
                               axisLine={{ fill: '#fff', stroke: '#fff', strokeWidth: 3.5 }}
                               tick={this.renderOrdersRemaningXAxisTick} />
                        <Area dot={{ stroke: '#00386E', fill: '#00386E', fillOpacity: 1, r: 4 }}
                              fillOpacity={.5}
                              dataKey="totalYesterday"
                              fill="#00386E" />
                        <Area dot={{ stroke: '#fff', fill: '#fff', fillOpacity: 1, r: 4 }}
                              fillOpacity={.5}
                              dataKey="totalToday"
                              fill="#fff">
                            <LabelList dataKey="totalToday" content={this.renderOrdersRemaningComparisonLabel(data)} />
                        </Area>
                        <Tooltip formatter={(value, name) => [`${value}%`, name === 'totalYesterday' ? 'Yesterday' : 'Today' ]}
                                 labelFormatter={(label) => moment().hour(Number(label)).startOf('hour').format('h A')} />
                    </AreaChart>
                </ResponsiveContainer>
            </Tile>
        );
    }

    private renderOrdersRemaningXAxisTick = (props: any) => {
        const { x, y, payload } = props;
        const time = moment().hour(payload.value).startOf('hour').format('h A');
        const style: React.CSSProperties = { fontSize: '14px' };

        if (moment().hours() === payload.value) {
            style.fontWeight = 'bold';
            style.textDecoration = 'underline';
        }

        return (
            <g transform={`translate(${x},${y})`}>
                <text x={0} y={0} dy={16} style={style} textAnchor="middle" fill="#fff">{time}</text>
            </g>
        );
    }

    private renderForecastXAxisTick = (props: any) => {
        const { x, y, payload } = props;

        const time = moment(payload.value).format('M/D');
        const style: React.CSSProperties = { fontSize: '12px' };
        if (moment().startOf('day').toISOString() === moment(payload.value).startOf('day').toISOString()) {
            style.fontWeight = 'bold';
            style.textDecoration = 'underline';
        }

        return <text x={x} y={y} dy={16} style={style} textAnchor="middle" fill={legacyTheme.palette.primary1Color}>{time}</text>;
    }

    private renderCustomizedLabel = (props: any) => {
        const { x, y, width, value } = props;
        if (Number(value) > 0) {
            return <text transform={`translate(${x + width / 2},${y + 5}) rotate(270)`}
                         textAnchor="end"
                         dominantBaseline="central"
                         style={{fontSize: '12px', fontWeight: 'bold'}}
                         fill="#fff">{value}</text>;
        }
    }

    private renderForecast = () => {
        const { shipByReleases } = this.state;
        
        const data: { date: string, total: number }[] = [];
        for (const m = moment().startOf('day'); m.isBefore(moment().startOf('day').add(1, 'week')); m.add(1, 'days')) {
            const date = m.format('YYYY-MM-DD');
            const releases: IRelease[] = shipByReleases.has(m.format(date)) ? shipByReleases.get(m.format(date)) : [];
            data.push({ date, total: releases.length });
        }

        return (
            <Tile style={{height: '250px'}}
                  title={<h3 style={{color: legacyTheme.palette.primary1Color}}>7 DAY <strong>ORDERS FORECAST</strong></h3>}>
                <ResponsiveContainer width="100%" height="100%">
                    <BarChart data={data}>
                        <Bar fill={legacyTheme.palette.primary1Color}
                             stroke={legacyTheme.palette.primary1Color}
                             barSize={25}
                             dataKey="total">
                            <LabelList dataKey="total" position="top" content={this.renderCustomizedLabel} />
                        </Bar>
                        <XAxis dataKey="date"
                               tickLine={false}
                               interval={0}
                               tick={this.renderForecastXAxisTick}
                               axisLine={{ fill: legacyTheme.palette.primary1Color, stroke: legacyTheme.palette.primary1Color, strokeWidth: 2 }} />
                    </BarChart>
                </ResponsiveContainer>
            </Tile>
        );
    }

    public render(): React.ReactNode {
        return (
            <div style={{height: 'inherit', display: 'flex', flexFlow: 'row nowrap', padding: '.5rem'}}>
                <div style={{display: 'flex', flexDirection: 'column', flex: '1', padding: '.5rem'}}>
                    {this.renderTotalOrdersPending()}
                    {this.renderTotalOrdersCompleted()}
                    {this.renderForecast()}
                </div>
                <div style={{display: 'flex', flexDirection: 'column', flex: '3', padding: '.5rem'}}>
                    {this.renderTodaysOrdersStatus()}
                    {this.renderOrdersRemainingComparison()}
                </div>
            </div>
        );
    }
}
