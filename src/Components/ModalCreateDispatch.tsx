import { Divider, MenuItem, TextField } from '@material-ui/core';
import { Linq2, Util, ValidationService } from 'o8-common';
import * as React from 'react';
import { v4 } from 'uuid';
import ProductAvailabilityList from '../Contexts/ProductAvailabilityList';
import Dispatch from '../Models/Dispatch';
import PrintService from '../Services/PrintService';
import { IDispatch } from '../Typings/IDispatch';
import { IRelease } from '../Typings/IRelease';
import { ITemplate } from '../Typings/ITemplate';
import { TemplateTypeEnum } from '../Typings/TemplateTypeEnum';
import FCSConstants from '../Utils/FCSConstants';
import { formatDateString } from '../Utils/Util';
import { DateRangeEnum } from './OrdersPending';
import SimpleModal from './SimpleModal';

export interface IModalCreateDispatchProps {
    confirm(dispatch: IDispatchInput): void;
    cancel(): void;
    orders: IRelease[];
    selectedDate: string;
    selectedRange: DateRangeEnum;
    selectedClient: string;
    productItems: Map<string, IProductItem>;
    stock: Map<string, IStock[]>;
    productionLines: Map<string, IProductionLine>;
    dispatches: Map<string, IDispatch>;
}

interface IModalCreateDispatchState {
    dispatch: Partial<IDispatch>;
    showErrors: boolean;
    isCreating: boolean;
}

export default class ModalCreateDispatch extends React.Component<IModalCreateDispatchProps, IModalCreateDispatchState> {
    private readonly validator: ValidationService;

    constructor(props: IModalCreateDispatchProps) {
        super(props);
        this.state = {
            dispatch: {
                dispatchCode: '',
                productionCode: undefined
            },
            isCreating: false,
            showErrors: false
        };
        this.validator = new ValidationService(this);
        this.validator.setValidationResult = this.validator.setValidationResult.bind(this.validator);
    }

    public componentWillMount = (): void => {
        const { dispatches } = this.props;
        if (Boolean(dispatches)) {
            const dateString = (new Date()).toLocaleDateString().split('/').join('-');
            const max = Linq2.where(dispatches.values(), d => Boolean(d.dispatchCode))
                             .where(d => d.dispatchCode.includes(dateString))
                             .where(d => d.dispatchCode.includes('_'))
                             .select(d => d.dispatchCode)
                             .toArray()
                             .sort((a, b) => a.split('_')[1].localeCompare(b.split('_')[1]))
                             .pop();

            const dispatchCode = `${dateString}_${Boolean(max) ? Number(max.split('_')[1]) + 1 : 1}`;
            this.setState({ dispatch: { ...this.state.dispatch, dispatchCode } });
        }
    }

    public componentDidMount(): void {
        this.validate(this.state.dispatch);
    }

    private validate = (item: Partial<IDispatch>) => {
        const { dispatchCode, productionCode } = Dispatch.validate(item);
        this.validator.setValidationResult('dispatchCode', dispatchCode);
        this.validator.setValidationResult('productionCode', productionCode);
    }

    private updateDispatch = (dispatch: Partial<IDispatch>) => {
        this.validate(dispatch);
        this.setState({ dispatch });
    }

    private updateProductionLine = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.updateDispatch({ ...this.state.dispatch, productionCode: e.target.value });
    }

    private confirm = async () => {
        if (!this.validator.isValid()) {
            this.setState({ showErrors: true });
            return;
        }

        this.setState({ isCreating: true });

        const { dispatchCode, productionCode } = this.state.dispatch;

        let tray: ITemplate;
        try {
            const templates = await PrintService.fetchTemplates();
            tray = Linq2.firstOrDefault(templates.values(), undefined, t => t.type === TemplateTypeEnum.TRAY);
        } catch (err) {
            // ignore
        }

        const dispatchInput: IDispatchInput = {
            clientCode: this.props.selectedClient,
            dispatchCode,
            priority: 50,
            productionCode,
            orderIds: this.props.orders.map(o => o.orderId),
            autoGroupRequest: {
                space: FCSConstants.traySpacing,
                slots: Boolean(tray) ? tray.maxItems : 170
            }
        };
        this.props.confirm(dispatchInput);
    }

    public render(): React.ReactNode {
        const { selectedRange, selectedDate, orders, productionLines } = this.props;
        const rangeText = selectedRange === DateRangeEnum.On ? 'On' : 'On or before';
        const lines = [ ...productionLines.values() ];
        const nameValidator = this.state.showErrors && this.validator && this.validator.getResults().get('dispatchCode');
        const productionCodeValidator = this.state.showErrors && this.validator && this.validator.getResults().get('productionCode');
        return (
            <SimpleModal title="Create Dispatch"
                         disableCancelButton={this.state.isCreating}
                         disableConfirmButton={this.state.isCreating}
                         confirm={this.confirm}
                         cancel={this.props.cancel}>
                {Boolean(selectedDate) && <p>Ship By Date: <strong>{`${rangeText} ${formatDateString(selectedDate)}`}</strong></p>}
                <p>Number of orders: <strong>{orders.length}</strong></p>
                <Divider />
                    <ProductAvailabilityList orders={orders} />
                <Divider />
                <p>Create a new dispatch.</p>
                <div style={{ ...FCSConstants.textFieldStyle, display: 'flex', flexDirection: 'row'}}>
                    <TextField value={this.state.dispatch.dispatchCode || ''}
                               disabled={true}
                               label="Dispatch Name"
                               helperText={Boolean(nameValidator) && !nameValidator.valid ? nameValidator.message : ''}
                               error={Boolean(nameValidator) && !nameValidator.valid}
                               variant="outlined"
                               style={{ ...FCSConstants.textFieldStyle, maxWidth: '300px'}} />
                </div>
                <TextField value={this.state.dispatch.productionCode || ''}
                           select
                           label="Production Line"
                           onChange={this.updateProductionLine}
                           helperText={Boolean(productionCodeValidator) && !productionCodeValidator.valid ? productionCodeValidator.message : ''}
                           error={Boolean(productionCodeValidator) && !productionCodeValidator.valid}
                           variant="outlined"
                           style={{ ...FCSConstants.textFieldStyle, maxWidth: '300px'}}>
                    {Util.isNonEmptyArray(lines) && lines.map(l => <MenuItem key={l.productionCode}
                                                                             value={l.productionCode}>{l.name}</MenuItem>)}
                </TextField>
            </SimpleModal>
        );
    }
}
