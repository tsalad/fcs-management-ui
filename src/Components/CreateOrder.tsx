import { Button } from '@material-ui/core';
import { Linq2, Util, ValidationService } from 'o8-common';
import * as React from 'react';
import ModalAddProductsToOrder from '../Contexts/ModalAddProductsToOrder';
import { Release } from '../Models';
import ReleaseService from '../Services/ReleaseService';
import ShipmentService from '../Services/ShipmentService';
import { IRelease } from '../Typings/IRelease';
import { IShipment } from '../Typings/IShipment';
import { StateEnum } from '../Typings/StateEnum';
import FCSConstants from '../Utils/FCSConstants';
import DataTable, { IDataTableColumn } from './DataTable';
import Error from './Error';
import FormOrderDetails from './FormOrderDetails';
import FormSection from './FormSection';
import FormShippingLocation from './FormShippingLocation';
import { IModalAddProductsToOrderProps, IModalAddProductsToOrderReturnValue } from './ModalAddProductsToOrder';
import View from './View';

export interface ICreateOrderFormSectionProps {
    order: IRelease;
    updateOrder(order: IRelease): void;
    validator: ValidationService;
    showValidationErrors: boolean;
    save(): Promise<void>;
    editMode: boolean;
}

export interface ICreateOrderProps {
    order: IRelease;
    depots: Map<string, IDepot>;
    clients: Map<string, IClient>;
    productItems: Map<string, IProductItem>;
    fetchDepots(): void;
    fetchStorage(): void;
    cancel(): void;
    fetchOrders(): void;
    setErrorMessage(message: string): void;
    setSuccessMessage(message: string): void;
}

export interface ICreateOrderState {
    order: IRelease;
    modalAddProductsToOrderProps: Partial<IModalAddProductsToOrderProps>;
    showValidationErrors: boolean;
    editMode: boolean;
    orderSnapshot: IRelease;
    shipment: IShipment;
}

export default class CreateOrder extends React.Component<ICreateOrderProps, ICreateOrderState> {
    private readonly validator: ValidationService;

    constructor(props: ICreateOrderProps) {
        super(props);
        this.state = {
            order: props.order,
            modalAddProductsToOrderProps: undefined,
            showValidationErrors: false,
            editMode: true,
            orderSnapshot: undefined,
            shipment: undefined
        };
        this.validator = new ValidationService(this);
        this.validator.setValidationResult = this.validator.setValidationResult.bind(this.validator);
    }

    public async componentDidMount(): Promise<void> {
        const { order } = this.props;
        this.props.fetchDepots();
        this.props.fetchStorage();
        if (Boolean(order.updatedAt)) {
            const shipment: IShipment = Boolean(order) && order.state !== StateEnum.PENDING && await ShipmentService.fetchShipment(order.orderId);
            this.setState({ editMode: false, shipment });
        }
    }

    private updateOrder = (order: IRelease) => {
        this.setState({ order });
    }

    private validate = async () => {
        const { order } = this.state;
        const { shipBy, depotCode, items, orderId } = await Release.validate(order);
        this.validator.setValidationResult('shipBy', shipBy);
        this.validator.setValidationResult('depotCode', depotCode);
        this.validator.setValidationResult('items', items);
        this.validator.setValidationResult('orderId', orderId);
    }

    private updateOrderItems = async () => {
        const { order, orderSnapshot } = this.state;
        const existingItems: Map<string, IReleaseItem> = orderSnapshot && Linq2.toDictionary(orderSnapshot.items, i => i.sku);
        const newItems: Map<string, IReleaseItem> = order && Linq2.toDictionary(order.items, i => i.sku);

        // Delete any removed items
        const itemsToDelete = Linq2.where(existingItems.values(), i => !newItems.has(i.sku)).toArray();
        if (Util.isNonEmptyArray(itemsToDelete)) {
            await Promise.all(itemsToDelete.map(async i => ReleaseService.deleteReleaseItem(i)));
        }

        // Create any new items
        const itemsToCreate = Linq2.where(newItems.values(), i => !existingItems.has(i.sku)).toArray();
        if (Util.isNonEmptyArray(itemsToCreate)) {
            await Promise.all(itemsToCreate.map(async i => ReleaseService.createReleaseItem(i)));
        }

        // Update any existing items
        const itemsToUpdate = Linq2.where(existingItems.values(), i => newItems.has(i.sku)).select(i => ({ ...i, quantity: newItems.get(i.sku).quantity })).toArray();
        if (Util.isNonEmptyArray(itemsToUpdate)) {
            await Promise.all(itemsToUpdate.map(async i => ReleaseService.updateReleaseItem(i)));
        }
    }

    private save = async (): Promise<void> => {
        await this.validate();

        if (!this.validator.isValid()) {
            this.setState({ showValidationErrors: true });
            this.props.setErrorMessage('Please fix validation errors');
            return;
        }

        try {
            if (!Boolean(this.state.order.updatedAt)) {
                await ReleaseService.createRelease(Release.create(this.state.order));
                this.props.setSuccessMessage('Order successfully created');
                this.props.cancel();
            } else {
                await this.updateOrderItems();
                const order = await ReleaseService.updateRelease(Release.create(this.state.order));
                this.setState({ order, editMode: false, orderSnapshot: order, showValidationErrors: false });
                this.props.setSuccessMessage('Order successfully saved');
            }
        } catch (err) {
            this.props.setErrorMessage('Order failed to save');
            console.error(err);
        }

        this.props.fetchOrders();
    }

    private showAddProductModal = async (): Promise<void> => {
        const promise: Promise<IModalAddProductsToOrderReturnValue> = new Promise((confirm, cancel) => {
            this.setState({ modalAddProductsToOrderProps: { confirm, cancel, products: this.props.productItems } });
        });

        try {
            const { product, quantity } = await promise;
            await this.handleAddItem(product, quantity);
        } catch (err) {
            // ignore
        } finally {
            this.setState({ modalAddProductsToOrderProps: undefined });
        }
    }

    private handleAddItem = async (product: IProductItem, quantity: number): Promise<void> => {
        const { order } = this.state;
        const { items } = order;
        const existingIndex = items.findIndex(i => i.sku === product.sku);
        let newItem: IReleaseItem = { sku: product.sku, quantity, orderId: order.orderId };
        if (existingIndex > -1) {
            newItem = { ...items[existingIndex], quantity };
        }
        this.updateOrder({ ...order, items: Linq2.where(items, i => i.sku !== product.sku).append(newItem).toArray() });
    }

    private handleRemoveItem = (item: IReleaseItem): () => void => {
        return () => {
            const { order } = this.state;

            if (!Util.isNonEmptyArray(order.items)) {
                return;
            }

            const filtered = order.items.filter(i => i.sku !== item.sku);
            this.updateOrder({ ...order, items: filtered });
        };
    }

    private cancel = () => {
        const { order, orderSnapshot } = this.state;
        if (order.updatedAt) {
            this.setState({ order: orderSnapshot, editMode: false, orderSnapshot: undefined });
            return;
        }
        this.props.cancel();
    }

    private setEditMode = () => {
        this.setState({ editMode: true, orderSnapshot: this.state.order });
    }

    private renderActions = () => {
        const { order, editMode, orderSnapshot } = this.state;
        if (editMode) {
            const disabled = order.updatedAt ? Util.deepEqual(order, orderSnapshot) : Util.deepEqual(order, this.props.order);
            return (
                <div>
                    <Button style={FCSConstants.buttonSpacing}
                            variant="contained"
                            onClick={this.cancel}>CANCEL</Button>
                    <Button style={FCSConstants.buttonSpacing}
                            variant="contained"
                            disabled={disabled}
                            color="primary"
                            onClick={this.save}>{order.updatedAt ? 'SAVE' : 'CREATE'}</Button>
                </div>
            );
        }
        return (
            <div>
                <Button style={FCSConstants.buttonSpacing}
                        variant="contained"
                        onClick={this.props.cancel}>BACK</Button>
                {order.state !== StateEnum.COMPLETED && order.state !== StateEnum.ARCHIVED &&
                <Button style={FCSConstants.buttonSpacing}
                        variant="contained"
                        color="primary"
                        onClick={this.setEditMode}>EDIT</Button>}
            </div>
        );
    }

    public render(): React.ReactNode {
        const { order, modalAddProductsToOrderProps, showValidationErrors, shipment } = this.state;

        const depotList: IDepot[] = Linq2.where(this.props.depots.values(), d => d.clientCode === this.props.order.clientCode).toArray();

        const columns: IDataTableColumn<IReleaseItem>[] = [
            {
                title: 'Product Name',
                value: (item: IReleaseItem) => this.props.productItems.get(item.sku).name
            },
            {
                title: 'SKU',
                value: (item: IReleaseItem) => item.sku
            },
            {
                title: 'Quantity',
                value: (item: IReleaseItem) => item.quantity
            },
            {
                value: (item: IReleaseItem) => this.state.editMode && <Button onClick={this.handleRemoveItem(item)} color="primary">REMOVE</Button>
            }
        ];

        const itemsValidator = showValidationErrors && this.validator.getResults().get('items');
        const trackingNumbers = Boolean(shipment) && Linq2.where(shipment.packages, s => Boolean(s.trackingNumber))
                                                          .select(s => s.trackingNumber)
                                                          .toArray();

        return (
            <>
                {modalAddProductsToOrderProps !== undefined && <ModalAddProductsToOrder { ...modalAddProductsToOrderProps } />}
                <View actions={this.renderActions()} title={order.updatedAt ? 'Update Bulk Order' : 'Create New Bulk Order'}>
                    <FormOrderDetails order={order}
                                      editMode={this.state.editMode}
                                      showValidationErrors={this.state.showValidationErrors}
                                      updateOrder={this.updateOrder}
                                      validator={this.validator}
                                      clients={this.props.clients}
                                      save={this.save} />

                    <FormShippingLocation order={order}
                                          editMode={this.state.editMode}
                                          showValidationErrors={this.state.showValidationErrors}
                                          updateOrder={this.updateOrder}
                                          validator={this.validator}
                                          depots={depotList}
                                          save={this.save} />
                    <FormSection title="Tracking Numbers"
                                 description="Tracking numbers are displayed here when the pick list is completed and shipping labels are scanned.">
                        <div style={{margin: '2rem 0'}}>
                            <strong>Scanned Tracking Numbers</strong>
                            {Util.isNonEmptyArray(trackingNumbers) ? trackingNumbers.map(t => <div key={t}>{t}</div>) : <div>N/A</div>}
                        </div>
                    </FormSection>
                    <FormSection title="Products"
                                 description="Add product and enter quantity to ship."
                                 actions={this.state.editMode && <Button onClick={this.showAddProductModal} color="primary">ADD PRODUCT</Button>}>
                        {Util.isNonEmptyArray(order.items) ?
                        <DataTable data={order.items}
                                   columns={columns} />  : <div style={{ margin: '2rem', textAlign: 'center' }}>Add a product to ship</div>}
                        {itemsValidator && !itemsValidator.valid && <Error text={itemsValidator.message} />}
                    </FormSection>
                </View>
            </>
        );
    }
}
