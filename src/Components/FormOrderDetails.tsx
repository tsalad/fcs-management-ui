import { Button, TextField } from '@material-ui/core';
import * as React from 'react';
import { Release } from '../Models';
import FCSConstants from '../Utils/FCSConstants';
import { formatDateString } from '../Utils/Util';
import { ICreateOrderFormSectionProps } from './CreateOrder';
import EditableTextField from './EditableTextField';
import FormSection from './FormSection';

export interface IFormOrderDetailsProps extends ICreateOrderFormSectionProps {
    clients: Map<string, IClient>;
}

export default class FormOrderDetails extends React.Component<IFormOrderDetailsProps> {
    private updateShipBy = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const shipBy = e.target.value ? new Date(e.target.value).toISOString() : null;
        this.props.updateOrder({ ...this.props.order, shipBy });
    }
    
    private updateOrder = (prop: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.updateOrder({ ...this.props.order, [prop]: e.target.value });
    }

    public render(): React.ReactNode {
        const { order, validator, showValidationErrors, editMode } = this.props;
        const client = order.clientCode && this.props.clients.get(order.clientCode);
        
        // Need to format value for the date picker input otherwise we need to have proper format for display purpose
        const shipBy = order.shipBy && editMode ? new Date(order.shipBy).toISOString().split('T')[0] : formatDateString(order.shipBy);
        const shipByValidator = showValidationErrors && validator.getResults().get('shipBy');

        const orderIdValidator = showValidationErrors && validator.getResults().get('orderId');

        return (
            <FormSection title="Bulk Order Information"
                         description="Basic information about the bulk order.">
                <div style={{display: 'flex', flexDirection: 'column', alignItems: 'flex-start'}}>

                    <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between', width: '100%'}}>
                        {client &&
                        <EditableTextField variant="outlined"
                                           editMode={false}
                                           style={{ ...FCSConstants.textFieldStyle, maxWidth: '300px' }}
                                           label={Reflect.getMetadata('displayName', Release.prototype, 'clientName')}
                                           value={client.name} />}
                        {order.updatedAt &&
                        <EditableTextField variant="outlined"
                                           editMode={false}
                                           style={{ ...FCSConstants.textFieldStyle, maxWidth: '300px' }}
                                           label={Reflect.getMetadata('displayName', Release.prototype, 'updatedAt')}
                                           value={order.updatedAt ? formatDateString(order.updatedAt, true) : ''} />}
                    </div>

                    <EditableTextField onChange={this.updateOrder('orderId')}
                                       value={order.orderId}
                                       editMode={!order.updatedAt ? true : false}
                                       helperText={orderIdValidator && !orderIdValidator.valid ? orderIdValidator.message : 'Unique order ID'}
                                       variant="outlined"
                                       placeholder="Enter"
                                       error={orderIdValidator && !orderIdValidator.valid}
                                       style={{ ...FCSConstants.textFieldStyle, maxWidth: '300px' }}
                                       InputLabelProps={{ shrink: true }}
                                       label={Reflect.getMetadata('displayName', Release.prototype, 'orderId')} />

                    <EditableTextField onChange={this.updateShipBy}
                                       editMode={editMode}
                                       type="date"
                                       variant="outlined"
                                       value={shipBy}
                                       InputLabelProps={{ shrink: true }}
                                       helperText={shipByValidator && !shipByValidator.valid ? shipByValidator.message : 'Ship by date'}
                                       error={shipByValidator && !shipByValidator.valid}
                                       style={{ ...FCSConstants.textFieldStyle, maxWidth: '300px' }}
                                       label={Reflect.getMetadata('displayName', Release.prototype, 'shipBy')} />
                </div>
            </FormSection>
        );
    }
}
