import { TextField } from '@material-ui/core';
import { ValidationService } from 'o8-common';
import * as React from 'react';
import Product from '../Models/Product';
import FCSConstants from '../Utils/FCSConstants';
import SimpleModal from './SimpleModal';

export interface IModalCreateProductProps {
    confirm(product: IProduct): void;
    cancel(): void;
    product: IProduct;
    products: Map<string, IProduct>;
}

export interface IModalCreateProductState {
    product: IProduct;
    showErrors: boolean;
}

export default class ModalCreateProduct extends React.Component<IModalCreateProductProps, IModalCreateProductState> {
    private readonly validator: ValidationService;

    constructor(props: IModalCreateProductProps) {
        super(props);
        this.state = {
            product: props.product,
            showErrors: false
        };
        this.validator = new ValidationService(this);
        this.validator.setValidationResult = this.validator.setValidationResult.bind(this.validator);
    }

    public componentDidMount(): void {
        this.validate(this.props.product);
    }

    private confirm = () => {
        if (!this.validator.isValid()) {
            this.setState({ showErrors: true });
            return;
        }
        this.props.confirm(this.state.product);
    }

    private updateName = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.updateProduct({ ...this.state.product, name: e.target.value });
    }

    private updateProductCode = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.updateProduct({ ...this.state.product, productCode: e.target.value.toUpperCase().trim() });
    }

    private updateProduct = (product: IProduct): void => {
        this.validate(product);
        this.setState({ product });
    }

    private validate = (item: IProduct) => {
        const { name, productCode } = Product.validate(item);
        this.validator.setValidationResult('name', name);
        this.validator.setValidationResult('productCode', productCode);
    }

    public render(): React.ReactNode {
        const { showErrors } = this.state;
        const nameValidator = showErrors && this.validator && this.validator.getResults().get('name');
        const productCodeValidator = showErrors && this.validator && this.validator.getResults().get('productCode');

        return (
            <SimpleModal title="Create Product Group"
                         confirm={this.confirm}
                         cancel={this.props.cancel}>
                <p>Add a new Group and create a unique Group Code.</p>
                <div style={{display: 'flex', flexDirection: 'row'}}>
                    <TextField variant="outlined"
                               label="Product Group"
                               helperText={nameValidator && !nameValidator.valid ? nameValidator.message : 'New Product Group'}
                               margin="normal"
                               style={{...FCSConstants.textFieldStyle}}
                               error={nameValidator && !nameValidator.valid}
                               value={this.state.product.name}
                               required
                               onChange={this.updateName} />
                    <TextField variant="outlined"
                               label="Product Code"
                               helperText={productCodeValidator && !productCodeValidator.valid ? productCodeValidator.message : '3 or 4 characters'}
                               margin="normal"
                               error={productCodeValidator && !productCodeValidator.valid}
                               required
                               style={{...FCSConstants.textFieldStyle}}
                               value={this.state.product.productCode}
                               onChange={this.updateProductCode} />
                </div>
            </SimpleModal>
        );
    }
}
