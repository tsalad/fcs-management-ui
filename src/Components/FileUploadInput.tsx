import { Button, createStyles, Theme, withStyles, WithStyles } from '@material-ui/core';
import { ButtonProps } from '@material-ui/core/Button';
import { Decorators } from 'o8-common';
import * as React from 'react';
import { v4 } from 'uuid';

export interface IFileUploadInputProps {
    onUpdate(file: File): void;
    acceptType?: string;
    buttonProps?: ButtonProps;
    title?: string;
}

interface IFileUploadInputState {
    file: File;
}

const styles = (theme: Theme) => createStyles({
    rightIcon: {
        marginLeft: theme.spacing.unit
    },
    input: {
        display: 'none !important'
    }
});

const { autoBindingComponent } = Decorators;

@autoBindingComponent
class FileUploadInput extends React.Component<IFileUploadInputProps & WithStyles<typeof styles>, IFileUploadInputState> {
    private readonly id: string = v4();
    public static readonly defaultProps: Partial<IFileUploadInputProps> = {
        title: 'UPLOAD'
    };

    public state: IFileUploadInputState = {
        file: undefined
    };

    private fileSelected(e: React.ChangeEvent<HTMLInputElement>): void {
        const file: File = e.target.files[0];
        e.target.value = null;
        this.setState({ file });
        this.props.onUpdate(file);
    }

    public render(): React.ReactNode {
        const { acceptType, classes, buttonProps } = this.props;

        return (
            <>
                <input onChange={this.fileSelected}
                       className={classes.input}
                       type="file"
                       id={this.id}
                       accept={acceptType} />
                <label htmlFor={this.id}>
                    <Button color="primary" component="span" {...buttonProps}>
                        {this.props.title}
                    </Button>
                </label>
            </>
        );
    }
}

export default withStyles(styles)(FileUploadInput) as React.ComponentClass<IFileUploadInputProps>;
