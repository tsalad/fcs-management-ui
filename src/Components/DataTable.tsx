import { Checkbox, Table, TableBody, TableCell, TableFooter, TableHead, TablePagination, TableRow, TableSortLabel, Toolbar, Typography } from '@material-ui/core';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import FCSConstants from '../Utils/FCSConstants';
import SectionTitle from './SectionTitle';

export enum SortOrder {
    Asc = 'asc',
    Desc = 'desc'
}

export interface IDataTableColumn<T> {
    title?: string;
    value(item: T): React.ReactNode;
    headerClicked?(): void;
    sortOrder?: SortOrder;
}

export interface IDataTableProps<T> {
    data: T[];
    columns: IDataTableColumn<T>[];
    paginate?: boolean;
    headerChildren?: React.ReactNode;
    title?: string;
    subtitle?: React.ReactNode;
    titleActions?: React.ReactNode;
    placeholderText?: string;
    handleItemsSelect?(items: T[]): void;
    selectedItems?: T[];
    headerStyle?: React.CSSProperties;
    renderRowChildren?(item: T): React.ReactNode;
}

export interface IDataTableState {
    currentPage: number;
    itemsPerPage: number;
}

const perPageOptions = [ 10, 25, 50, 100 ];

export default class DataTable<T> extends React.Component<IDataTableProps<T>, IDataTableState> {
    public static readonly defaultProps: Partial<IDataTableProps<any>> = {
        headerChildren: undefined,
        renderRowChildren: undefined,
        paginate: false,
        title: undefined,
        titleActions: undefined,
        placeholderText: 'No data available.'
    };

    constructor(props: IDataTableProps<T>) {
        super(props);
        this.state = {
            currentPage: 0,
            itemsPerPage: perPageOptions[0]
        };
    }

    private renderLabel = (col: IDataTableColumn<T>): React.ReactNode => {
        if (typeof col.headerClicked === 'function') {
            return (
                <TableSortLabel active={col.sortOrder !== undefined}
                                direction={col.sortOrder}
                                onClick={col.headerClicked}>
                    {col.title}
                </TableSortLabel>
            );
        }
        return col.title;
    }

    private selectAll = (e: React.ChangeEvent<HTMLInputElement>, checked: boolean) => {
        const data = checked ? this.props.data : [];
        this.props.handleItemsSelect(data);
    }
    
    private renderTableHeader = (): React.ReactNode => {
        const { columns, data, selectedItems } = this.props;
        const isSelected = Util.isNonEmptyArray(data) && Util.isNonEmptyArray(selectedItems) && data.length === selectedItems.length;
        return (
            <TableHead>
                <TableRow>
                    {typeof this.props.handleItemsSelect === 'function' &&
                    <TableCell style={{width: '50px'}} key="table-header-checkbox">
                        <Checkbox color="primary" disabled={!Util.isNonEmptyArray(data)} checked={isSelected} onChange={this.selectAll} />
                    </TableCell>}
                    {Util.isNonEmptyArray(columns) && columns.map((c, i) => (
                        <TableCell key={`table-header-cell-${i}`}>
                            {this.renderLabel(c)}
                        </TableCell>
                    ))}
                </TableRow>
                {this.props.headerChildren}
            </TableHead>
        );
    }

    private handleItemSelect = (item: T) => (e: React.ChangeEvent<HTMLInputElement>, checked: boolean) => {
        const selectedData = [ ...Linq2.where(this.props.selectedItems, d => !Util.deepEqual(item, d)) ];
        if (checked) {
            selectedData.push(item);
        }
        this.props.handleItemsSelect(selectedData);
    }

    private renderTableBodyRow = (item: T, i: number): React.ReactNode => {
        const { columns, renderRowChildren, selectedItems } = this.props;
        const isSelected = Util.isNonEmptyArray(selectedItems) ? selectedItems.some(t => Util.deepEqual(t, item)) : false;
        const rowChild = Boolean(renderRowChildren) && renderRowChildren(item);
        return (
            <React.Fragment key={`table-body-row-${i}`}>
                <TableRow style={{ background: Boolean(rowChild) ? FCSConstants.grayLighter : 'initial' }} hover>
                    {typeof this.props.handleItemsSelect === 'function' &&
                    <TableCell style={{width: '50px'}} key={`table-body-checkbox-${i}`}>
                        <Checkbox color="primary" checked={isSelected} onChange={this.handleItemSelect(item)} />
                    </TableCell>}
                    {columns.map((c, index) => (
                        <TableCell style={{fontWeight: Boolean(rowChild) ? 'bold' : 'initial'}}
                                   key={`table-body-cell-${index}`}>{c.value(item)}</TableCell>
                    ))}
                </TableRow>
                {Boolean(rowChild) &&
                <TableRow>
                    <TableCell style={{padding: 0}} colSpan={columns.length + 1}>{rowChild}</TableCell>
                </TableRow>}
            </React.Fragment>
        );
    }

    private getFilteredData = (): T[] => {
        const { data, paginate } = this.props;
        const { currentPage, itemsPerPage } = this.state;

        if (!Util.isNonEmptyArray(data)) {
            return [];
        }

        if (!paginate) {
            return data;
        }

        const start = currentPage * itemsPerPage;
        const end = (currentPage * itemsPerPage) + itemsPerPage;
        return data.slice(start, end);
    }

    private renderPlaceholder = (): React.ReactNode => {
        return (
            <TableRow>
                <TableCell style={{border: 'none'}}>
                    <Typography variant="caption">{this.props.placeholderText}</Typography>
                </TableCell>
            </TableRow>
        );
    }

    private renderTableBody = (): React.ReactNode => {
        const data = this.getFilteredData();
        return (
            <TableBody>
                {Util.isNonEmptyArray(data) ? data.map(this.renderTableBodyRow) : this.renderPlaceholder()}
            </TableBody>
        );
    }

    private updatePage = (e: React.MouseEvent<HTMLButtonElement>, currentPage: number): void => {
        this.setState({ currentPage });
    }

    private updateItemsPerPage = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ itemsPerPage: Number(e.target.value) });
    }

    private renderFooter = () => {
        const { paginate, data, columns } = this.props;
        const { currentPage, itemsPerPage } = this.state;

        if (!Util.isNonEmptyArray(data)) {
            return null;
        }

        return paginate &&
            <TableFooter>
                <TableRow>
                    <TablePagination rowsPerPageOptions={perPageOptions}
                                     colSpan={columns.length}
                                     count={data.length}
                                     rowsPerPage={itemsPerPage}
                                     page={currentPage}
                                     onChangePage={this.updatePage}
                                     onChangeRowsPerPage={this.updateItemsPerPage} />
                </TableRow>
            </TableFooter>;
    }

    private renderHeader = () => {
        const { title, selectedItems, titleActions } = this.props;
        if (Util.isNonEmptyArray(selectedItems)) {
            return (
                <Toolbar style={{backgroundColor: '#D2E3F5', justifyContent: 'space-between'}}>
                    <Typography variant="h6" color="primary">{`${selectedItems.length} Selected`}</Typography>
                    {titleActions}
                </Toolbar>
            );
        }
        return  <SectionTitle title={title} titleActions={titleActions} />;
    }

    public render(): React.ReactNode {
        return (
            <>
                {this.renderHeader()}
                {Boolean(this.props.subtitle) && this.props.subtitle}
                <Table style={{padding: '.5rem 0'}}>
                    {this.renderTableHeader()}
                    {this.renderTableBody()}
                    {this.renderFooter()}
                </Table>
            </>
        );
    }
}
