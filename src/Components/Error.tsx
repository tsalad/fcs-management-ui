import * as React from 'react';

export default class Error extends React.Component<{ text: string, style?: React.CSSProperties }> {
    private static readonly style: React.CSSProperties = {
        fontSize: '12px',
        margin: '1rem',
        lineHeight: '12px',
        color: '#F44336'
    };

    public render(): React.ReactNode {
        return <div style={{ ...this.props.style, ...Error.style }}>{this.props.text || ''}</div>;
    }
}
