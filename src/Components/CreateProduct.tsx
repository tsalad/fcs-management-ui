import { Button } from '@material-ui/core';
import { Linq2, Util, ValidationService } from 'o8-common';
import * as React from 'react';
import FormAdjustments from '../Contexts/FormAdjustments';
import FormExternalSKU from '../Contexts/FormExternalSKU';
import FormProductDetails from '../Contexts/FormProductDetails';
import FormStock from '../Contexts/FormStock';
import { ProductItem } from '../Models';
import Catalog from '../Services/Catalog';
import FCSConstants from '../Utils/FCSConstants';
import FormAttributes from './FormAttributes';
import FormInternalSKU from './FormInternalSKU';
import FormProductImage from './FormProductImage';
import View from './View';

export interface ICreateFormSectionProps {
    item: IProductItem;
    updateItem?(item: IProductItem): void;
    validator?: ValidationService;
    showValidationErrors?: boolean;
    editMode: boolean;
}

export interface ICreateProductProps {
    cancel(): void;
    createProductItem(item: IProductItem): IProductItem;
    createTag(label: string): ITagLabel;
    fetchTags(): void;
    item: IProductItem;
    items: Map<string, IProductItem>;
    tags: Map<string, ITagLabel>;
    updateProductItem(item: IProductItem): IProductItem;
    createProductItemImageMultipart(file: File, sku: string): Promise<IProductItemImage>;
    deleteProductItemImage(sku: string): Promise<void>;
    setSuccessMessage(message: string): void;
    setErrorMessage(message: string): void;
}

interface ICreateProductState {
    item: IProductItem;
    showValidationErrors: boolean;
    productItemTags: Partial<IProductItemTag>[];
    productItemSuppliers: IProductItemSupplier[];
    editMode: boolean;
    productSnapshot: IProductItem;
    tagsSnapshot: Partial<IProductItemTag>[];
    suppliersSnapshot: IProductItemSupplier[];
    imageFile: File;
}

export default class CreateProduct extends React.Component<ICreateProductProps, ICreateProductState> {
    private readonly validator: ValidationService;

    constructor(props: ICreateProductProps) {
        super(props);
        this.state = {
            item: ProductItem.create(props.item),
            showValidationErrors: false,
            productItemTags: [ { label: '', value: '' } ],
            productItemSuppliers: [],
            editMode: true,
            productSnapshot: undefined,
            tagsSnapshot: undefined,
            suppliersSnapshot: undefined,
            imageFile: undefined
        };
        this.validator = new ValidationService(this);
        this.validator.setValidationResult = this.validator.setValidationResult.bind(this.validator);
    }

    public async componentDidMount(): Promise<void> {
        this.props.fetchTags();
        
        const { item } = this.props;

        this.validate(this.props.item);

        if (this.props.item.updatedAt) {
            const productItemTags: Partial<IProductItemTag>[] = await Catalog.fetchProductItemTags(item.sku);
            const productItemSuppliers: IProductItemSupplier[] = await Catalog.fetchProductItemSupplier(item.sku);
            this.setState({ productItemTags, productItemSuppliers, editMode: false });
        }
    }

    private updateItem = (item: IProductItem): void => {
        this.validate(item);
        this.setState({ item });
    }

    private save = async () => {
        const { item, productItemTags, imageFile } = this.state;
        if (!this.validator.isValid()) {
            this.setState({ showValidationErrors: true });
            this.props.setErrorMessage('Please fix validation errors');
            return;
        }
        try {
            if (Boolean(item.updatedAt)) {
                await this.props.updateProductItem(item);
            } else {
                await this.props.createProductItem(item);
            }
            
            const updatedSuppliers = await Catalog.handleUpdateProductItemSuppliers(item.sku, this.state.productItemSuppliers);
            const updatedTags = await Catalog.rebuildProductItemTags(item.sku, productItemTags);
            let updatedItem = await Catalog.fetchProductItem(item.sku);

            if (Boolean(imageFile)) {
                const imageData = await this.props.createProductItemImageMultipart(imageFile, item.sku);
                updatedItem = await this.props.updateProductItem({ ...updatedItem, imageUri: imageData.url });
            }

            this.setState({
                editMode: false,
                productSnapshot: undefined,
                tagsSnapshot: undefined,
                suppliersSnapshot: undefined,
                item: updatedItem,
                productItemTags: updatedTags,
                productItemSuppliers: updatedSuppliers,
                imageFile: undefined
            });
            this.props.setSuccessMessage('Product Successfully Saved');
        } catch (err) {
            this.props.setErrorMessage('Product failed to save');
            console.error(err);
        }
    }

    private validate = (item: IProductItem) => {
        const { name, productCode, sku, reorderThreshold, weightOunces } = ProductItem.validate(item);
        this.validator.setValidationResult('name', name);
        this.validator.setValidationResult('productCode', productCode);
        this.validator.setValidationResult('sku', sku);
        this.validator.setValidationResult('reorderThreshold', reorderThreshold);
        this.validator.setValidationResult('weightOunces', weightOunces);
    }

    private addAttribute = (tag: Partial<IProductItemTag>) => {
        const productItemTags = Util.isNonEmptyArray(this.state.productItemTags) ? [ ...this.state.productItemTags, tag ] : [ tag ];
        this.setState({ productItemTags });
    }

    private updateAttributes = (productItemTags: Partial<IProductItemTag>[]) => {
        this.setState({ productItemTags });
    }

    private updateProductItemSuppliers = (productItemSuppliers: IProductItemSupplier[]) => {
        this.setState({ productItemSuppliers });
    }

    private setEditMode = () => {
        const { editMode, item, productItemTags, productItemSuppliers } = this.state;
        this.setState({
            editMode: !editMode,
            productSnapshot: item,
            tagsSnapshot: [ ...productItemTags ],
            suppliersSnapshot: [ ...productItemSuppliers]
        });
    }

    private cancel = async () => {
        const { item, productSnapshot, tagsSnapshot, suppliersSnapshot } = this.state;
        if (item.updatedAt) {
            this.setState({
                item: productSnapshot,
                productItemTags: tagsSnapshot,
                productItemSuppliers: suppliersSnapshot,
                editMode: false,
                productSnapshot: undefined,
                tagsSnapshot: undefined,
                suppliersSnapshot: undefined,
                imageFile: undefined
            });
            return;
        }
        this.props.cancel();
    }

    private renderActions = () => {
        const { item, editMode, productSnapshot, tagsSnapshot, productItemTags, productItemSuppliers, suppliersSnapshot, imageFile } = this.state;
        if (editMode) {
            const disabled = item.updatedAt ? Util.deepEqual(item, productSnapshot) &&
                                              Util.deepEqual(productItemTags, tagsSnapshot) &&
                                              Util.deepEqual(productItemSuppliers, suppliersSnapshot) &&
                                              !imageFile : Util.deepEqual(item, this.props.item);
            return (
                <div>
                    <Button style={FCSConstants.buttonSpacing}
                            variant="contained"
                            onClick={this.cancel}>CANCEL</Button>
                    <Button style={FCSConstants.buttonSpacing}
                            variant="contained"
                            disabled={disabled}
                            color="primary"
                            onClick={this.save}>{item.updatedAt ? 'SAVE' : 'CREATE'}</Button>
                </div>
            );
        }
        return (
            <div>
                <Button style={FCSConstants.buttonSpacing}
                        variant="contained"
                        onClick={this.props.cancel}>BACK</Button>
                <Button style={FCSConstants.buttonSpacing}
                        variant="contained"
                        color="primary"
                        onClick={this.setEditMode}>EDIT</Button>
            </div>
        );
    }

    private updateImage = (imageFile?: File): void => {
        this.setState({ imageFile });
    }

    public render(): React.ReactNode {
        const { item } = this.state;

        return (
            <View title={Boolean(item.updatedAt) ? 'Update Product' : 'Create New Product'} actions={this.renderActions()}>
                <FormProductDetails validator={this.validator}
                                    showValidationErrors={this.state.showValidationErrors}
                                    item={this.state.item}
                                    updateItem={this.updateItem}
                                    editMode={this.state.editMode} />
                <FormAttributes validator={this.validator}
                                attributes={this.state.productItemTags}
                                tags={this.props.tags}
                                addAttribute={this.addAttribute}
                                updateAttributes={this.updateAttributes}
                                showValidationErrors={this.state.showValidationErrors}
                                item={this.state.item}
                                createTag={this.props.createTag}
                                updateItem={this.updateItem}
                                editMode={this.state.editMode} />
                <FormProductImage item={this.state.item}
                                  image={this.state.imageFile}
                                  updateItem={this.updateItem}
                                  updateImage={this.updateImage}
                                  editMode={this.state.editMode} />
                <FormInternalSKU validator={this.validator}
                                 showValidationErrors={this.state.showValidationErrors}
                                 item={this.state.item}
                                 updateItem={this.updateItem}
                                 editMode={this.state.editMode} />
                <FormExternalSKU validator={this.validator}
                                 productItemSuppliers={this.state.productItemSuppliers}
                                 updateProductItemSuppliers={this.updateProductItemSuppliers}
                                 showValidationErrors={this.state.showValidationErrors}
                                 item={this.state.item}
                                 editMode={this.state.editMode} />
                <FormStock validator={this.validator}
                           showValidationErrors={this.state.showValidationErrors}
                           item={this.state.item}
                           updateItem={this.updateItem}
                           editMode={this.state.editMode} />
                <FormAdjustments validator={this.validator}
                                 showValidationErrors={this.state.showValidationErrors}
                                 item={this.state.item}
                                 updateItem={this.updateItem}
                                 editMode={this.state.editMode} />
            </View>
        );
    }
}
