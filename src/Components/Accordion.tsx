import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, Typography } from '@material-ui/core';
import { ExpansionPanelProps } from '@material-ui/core/ExpansionPanel';
import { ExpansionPanelDetailsProps } from '@material-ui/core/ExpansionPanelDetails';
import { ExpansionPanelSummaryProps } from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { AutoBindingComponent } from 'o8-common';
import * as React from 'react';

interface IAccordionProps {
    title: React.ReactNode;
    open: boolean;
    onChange(): void;
    ExpansionPanelProps?: ExpansionPanelProps;
    ExpansionPanelSummaryProps?: ExpansionPanelSummaryProps;
    ExpansionPanelDetailsProps?: ExpansionPanelDetailsProps;
}

export default class Accordion extends AutoBindingComponent<IAccordionProps> {
    public render(): React.ReactNode {
        return (
            <ExpansionPanel {...this.props.ExpansionPanelProps} onChange={this.props.onChange} expanded={this.props.open}>
                <ExpansionPanelSummary {...this.props.ExpansionPanelSummaryProps} expandIcon={<ExpandMoreIcon />}>
                    <Typography variant="h6">
                        {this.props.title}
                    </Typography>
                </ExpansionPanelSummary>
                {this.props.open &&
                <ExpansionPanelDetails {...this.props.ExpansionPanelDetailsProps}>
                    {this.props.children}
                </ExpansionPanelDetails>}
            </ExpansionPanel>
        );
    }
}
