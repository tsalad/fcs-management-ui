import { TextField } from '@material-ui/core';
import * as React from 'react';
import FCSConstants from '../Utils/FCSConstants';
import Validators from '../Utils/Validators';
import SimpleModal from './SimpleModal';

export interface IModalCreateTagLabelProps {
    tags: Map<string, ITagLabel>;
    confirm(label: string): void;
    cancel(): void;
}

interface IModalCreateTagLabelState {
    label: string;
}

export default class ModalCreateTagLabel extends React.Component<IModalCreateTagLabelProps, IModalCreateTagLabelState> {
    constructor(props: IModalCreateTagLabelProps) {
        super(props);
        this.state = { label: '' };
    }

    private confirm = () => {
        this.props.confirm(this.state.label);
    }

    private updateLabel = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ label: e.target.value.toUpperCase() });
    }

    private validateCode = (): MC.IValidationResult => {
        // Check for collisions, tag labels must be unique
        const hasMatch = [ ...this.props.tags.keys() ].some(t => t.toLowerCase() === this.state.label.toLowerCase());
        if (hasMatch) {
            return { valid: false, message: 'Tag label already exists.' };
        }
        // Ensure it passes the regex validation required by the back end
        return Validators.isValidTagLabel(this.state.label);
    }

    public render(): React.ReactNode {
        const { valid, message } = this.validateCode();
        return (
            <SimpleModal title="Create New Attribute"
                         cancel={this.props.cancel}
                         confirmButtonText="Create"
                         disableConfirmButton={!valid || !this.state.label.length}
                         confirm={this.confirm}>
                <div style={{display: 'flex', flexDirection: 'row'}}>
                    <TextField variant="outlined"
                               label="Attribute"
                               helperText={this.state.label.length > 0 && !valid ? message : 'Enter a new attribute'}
                               error={this.state.label.length > 0 && !valid}
                               margin="normal"
                               style={{...FCSConstants.textFieldStyle}}
                               value={this.state.label}
                               onChange={this.updateLabel}
                               required />
                </div>
            </SimpleModal>
        );
    }
}
