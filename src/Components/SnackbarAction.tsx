import { IconButton, Snackbar, SnackbarContent } from '@material-ui/core';
import { SvgIconProps } from '@material-ui/core/SvgIcon';
import { CheckCircle, Close, Error as ErrorIcon, Warning } from '@material-ui/icons';
import { getMuiTheme } from 'o8-common';
import * as React from 'react';

const legacyTheme = getMuiTheme();

export enum SnackbarActionStateEnum {
    SUCCESS = 'SUCCESS',
    ERROR = 'ERROR',
    WARNING = 'WARNING'
}

export interface ISnackbarActionProps {
    variant: SnackbarActionStateEnum;
    message: string;
    open: boolean;
    onClose(): void;
}

export default class SnackbarAction extends React.Component<ISnackbarActionProps> {
    public static readonly variantIcon = new Map <SnackbarActionStateEnum, React.ComponentType<SvgIconProps>>([
        [SnackbarActionStateEnum.SUCCESS, CheckCircle],
        [SnackbarActionStateEnum.ERROR, ErrorIcon],
        [SnackbarActionStateEnum.WARNING, Warning]
    ]);

    public static readonly variantColor = new Map <SnackbarActionStateEnum, string>([
        [SnackbarActionStateEnum.SUCCESS, legacyTheme.palette.statusOkay],
        [SnackbarActionStateEnum.WARNING, legacyTheme.palette.statusWarning],
        [SnackbarActionStateEnum.ERROR, legacyTheme.palette.statusDanger]
    ]);

    private handleClose = (event: any, reason: string) => {
        if (reason === 'clickaway') {
          return;
        }
        this.props.onClose();
      }

    public render(): React.ReactNode {
        const { variant, message, open, onClose } = this.props;
        const Icon = SnackbarAction.variantIcon.get(variant);
        return (
            <Snackbar anchorOrigin={{ vertical: 'top', horizontal: 'center' }} open={open} onClose={this.handleClose} autoHideDuration={5000}>
                <SnackbarContent style={{backgroundColor: SnackbarAction.variantColor.get(variant)}}
                                 message={<span style={{display: 'flex', alignItems: 'center'}}>
                                             <Icon style={{backgroundColor: SnackbarAction.variantColor.get(variant), marginRight: '.5rem' }} />
                                             {message}
                                         </span>}
                                 action={[
                                     <IconButton key="close" aria-label="Close" color="inherit" onClick={onClose}>
                                         <Close style={{fontSize: '20'}} />
                                     </IconButton>
                                 ]} />
            </Snackbar>
        );
    }
}
