import { Button, MenuItem, TextField } from '@material-ui/core';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import { IStockInput } from '../Typings/IStockInput';
import FCSConstants from '../Utils/FCSConstants';

export interface IFormRecountStockProps {
    storage: Map<string, IStorage>;
    stock: Map<string, IStock[]>;
    productItem: IProductItem;
    adjustments: IStockInput[];
    update(adjustments: IStockInput[]): void;
    addAdjustment(): void;
}

export default class FormRecountStock extends React.Component<IFormRecountStockProps> {
    private updateLocation = (i: number) => (e: React.ChangeEvent<HTMLInputElement>) => {
        const { adjustments } = this.props;
        const storage = this.props.storage.get(e.target.value);
        const currentStock = Linq2.firstOrDefault(this.props.stock.get(this.props.productItem.sku), undefined, s => s.storageCode === storage.storageCode);
        const updated = { ...adjustments[i], storageCode: storage.storageCode, updatedAt: Boolean(currentStock) ? currentStock.updatedAt : null };
        adjustments.splice(i, 1, updated);
        this.props.update(adjustments);
    }

    private updateQuantity = (i: number) => (e: React.ChangeEvent<HTMLInputElement>) => {
        const { adjustments } = this.props;
        const updated = { ...adjustments[i], adjustment: Number(e.target.value) };
        adjustments.splice(i, 1, updated);
        this.props.update(adjustments);
    }

    private getCurrentQuantity = (a: IStockInput) => {
        const { stock, productItem } = this.props;
        if (a.storageCode) {
            const c = stock.has(productItem.sku) && Linq2.firstOrDefault(stock.get(productItem.sku), undefined, s => s.storageCode === a.storageCode);
            return Boolean(c) ? c.quantity : 0;
        }
        return '';
    }

    public render(): React.ReactNode {
        const { addAdjustment, adjustments, stock, productItem, storage } = this.props;
        const currentLocations = stock.has(productItem.sku) && Linq2.select(stock.get(productItem.sku), s => storage.get(s.storageCode)).toArray();
        const filtered = Boolean(currentLocations) && Linq2.where(currentLocations, l => !adjustments.some(a => a.storageCode === l.storageCode)).toArray();

        return (
            <div>
                <p>Enter new stock information:</p>
                {Util.isNonEmptyArray(adjustments) && adjustments.map((a, i) =>
                <div key={`${a.sku}-${i}-${a.storageCode}`} style={{ display: 'flex', flexDirection: 'row' }}> 
                    <TextField select
                               label="Current Locations"
                               helperText="List of Current Locations"
                               style={{ ...FCSConstants.textFieldStyle, flex: 'auto' }}
                               value={a.storageCode}
                               variant="outlined"
                               onChange={this.updateLocation(i)}>
                        {Util.isNonEmptyArray(currentLocations) &&
                        currentLocations.map(l => <MenuItem key={l.storageCode}
                                                            disabled={adjustments.some(adjustment => adjustment.storageCode === l.storageCode)}
                                                            value={l.storageCode}>{l.name}</MenuItem>)}
                    </TextField>
                    <TextField disabled
                               label="Current Quantity"
                               helperText="Current stock quantity"
                               style={{ ...FCSConstants.textFieldStyle, flex: 'auto' }}
                               variant="outlined"
                               value={this.getCurrentQuantity(a)} />
                    <TextField label="New Quantity"
                               helperText="Enter stock quantity"
                               style={{ ...FCSConstants.textFieldStyle, flex: 'auto' }}
                               variant="outlined"
                               type="number"
                               InputProps={{ inputProps: { min: 0 } }}
                               onChange={this.updateQuantity(i)}
                               value={a.adjustment !== null && a.adjustment !== undefined ? a.adjustment : ''} />
                </div>)}
                <Button disabled={!Util.isNonEmptyArray(filtered)}
                        color="primary"
                        onClick={addAdjustment}>ADD ROW</Button>
            </div>
        );
    }
}
