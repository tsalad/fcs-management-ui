import { Badge, Divider, Tab, Tabs } from '@material-ui/core';
import { Linq2 } from 'o8-common';
import * as React from 'react';
import CreateProduct from '../Contexts/CreateProduct';
import TableProblemReports from '../Contexts/TableProblemReports';
import TableReorderAlerts from '../Contexts/TableReorderAlerts';
import NotificationsService from '../Services/NotificationsService';
import { IProblemReport } from '../Typings/IProblemReport';
import View from './View';

interface INotificationsState {
    currentTab: number;
    selectedProductItem: IProductItem;
}

export interface INotificationsProps {
    reorderAlerts: Map<string, IProductItem>;
    problemReports: Map<string, IProblemReport>;
    fetchStock(): Promise<void>;
    fetchProductItems(): Promise<void>;
    fetchStorage(): Promise<void>;
}

export default class Notifications extends React.Component<INotificationsProps, INotificationsState> {
    constructor(props: INotificationsProps) {
        super(props);
        this.state = {
            currentTab: 0,
            selectedProductItem: undefined
        };
    }

    public componentDidMount() {
        this.props.fetchStock();
        this.props.fetchProductItems();
        this.props.fetchStorage();
    }

    private updateTab = (e: React.MouseEvent<HTMLButtonElement>, currentTab: number): void => {
        this.setState({ currentTab });
    }

    private showProductDetails = (selectedProductItem?: IProductItem) => {
        this.setState({ selectedProductItem });
    }

    private hideProductDetails = () => {
        NotificationsService.updateNotifications();
        this.setState({ selectedProductItem: undefined });
    }

    public render(): React.ReactNode {
        const { currentTab, selectedProductItem } = this.state;

        if (selectedProductItem !== undefined) {
            return  <CreateProduct cancel={this.hideProductDetails} item={selectedProductItem} />;
        }

        const problemReports = Linq2.where([ ...this.props.problemReports.values() ], r => !Boolean(r.closedAt)).toDictionary(r => r.id);

        return (
            <View title="Notifications">
                <Tabs value={currentTab}
                      onChange={this.updateTab}
                      indicatorColor="primary"
                      textColor="primary">
                    <Tab label={<Badge badgeContent={Boolean(this.props.reorderAlerts) ? this.props.reorderAlerts.size : 0}
                                       max={99}
                                       color="error"
                                       style={{padding: '0 1.5rem'}}>REORDER ALERTS</Badge>}
                         style={{paddingTop: '.5rem'}} />
                    <Tab label={<Badge badgeContent={Boolean(problemReports) ? problemReports.size : 0}
                                       max={99}
                                       color="error"
                                       style={{padding: '0 1.5rem'}}>REPORTED ISSUES</Badge>}
                         style={{paddingTop: '.5rem'}} />
                </Tabs>
                <Divider />
                <div style={{ margin: '1rem 0' }}>
                    {currentTab === 0 && <TableReorderAlerts toggleProductForm={this.showProductDetails} />}
                    {currentTab === 1 && <TableProblemReports />}
                </div>
            </View>
        );
    }
}
