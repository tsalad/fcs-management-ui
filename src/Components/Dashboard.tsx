import { createMuiTheme, MuiThemeProvider } from '@material-ui/core';
import { getMuiTheme, Util } from 'o8-common';
import * as React from 'react';
import ManageClients from '../Contexts/ManageClients';
import ManageLogistics from '../Contexts/ManageLogistics';
import ModalImportManifest from '../Contexts/ModalImportManifest';
import Notifications from '../Contexts/Notifications';
import Orders from '../Contexts/Orders';
import Products from '../Contexts/Products';
import { IApplicationMessage } from '../Typings/IApplicationMessage';
import FCSConstants from '../Utils/FCSConstants';
import SnackbarAction from './SnackbarAction';
import SupervisorDashboard from './SupervisorDashboard';

// tslint:disable:object-literal-key-quotes

const legacyTheme = getMuiTheme();
const theme = createMuiTheme({
    palette: {
        primary: { main: legacyTheme.palette.primary1Color },
        secondary: { main: legacyTheme.palette.accent1Color },
        background: {
            'default': legacyTheme.toolbar.backgroundColor
        }
    }
});

export interface IDashboardProps {
    currentMenu: string;
    clearSnackbarMessage(key: string): void;
    messages: Map<string, IApplicationMessage>;
    showModalImportManifest: boolean;
    toggleModalImportManifest(state: boolean): void;
    setErrorMessage(message: string): void;
    setSuccessMessage(message: string): void;
}

export default class Dashboard extends React.Component<IDashboardProps> {
    private static readonly menuViewMap = new Map <string, React.ComponentClass>([
        [FCSConstants.MENU_CLIENTS, ManageClients],
        [FCSConstants.MENU_LOGISTICS, ManageLogistics],
        [FCSConstants.MENU_MANAGE, ManageLogistics],
        [FCSConstants.MENU_NOTIFICATIONS, Notifications],
        [FCSConstants.MENU_ORDERS, Orders],
        [FCSConstants.MENU_PRODUCTS, Products],
        [FCSConstants.MENU_SUPERVISOR_DASHBOARD, SupervisorDashboard]
    ]);

    private renderCurrentView = () => {
        const { currentMenu } = this.props;
        if (Dashboard.menuViewMap.has(currentMenu)) {
            return React.createElement(Dashboard.menuViewMap.get(currentMenu));
        } else {
            return <Orders />;
        }
    }

    private clearSnackbarMessage = (key: string) => () => {
        this.props.clearSnackbarMessage(key);
    }

    private renderMessage = (message: IApplicationMessage) => {
        return <SnackbarAction key={message.id} variant={message.type} message={message.message} open={true} onClose={this.clearSnackbarMessage(message.id)} />;
    }

    private renderMessages = () => {
        const messages = [ ...this.props.messages.values() ];
        if (Util.isNonEmptyArray(messages)) {
            return messages.map(this.renderMessage);
        }
        return null;
    }

    private renderImportManifestModal = () => {
        if (!this.props.showModalImportManifest) {
            return null;
        }

        return (
            <ModalImportManifest />
        );
    }

    public render(): React.ReactNode {
        return (
            <MuiThemeProvider theme={theme}>
                {this.renderCurrentView()}
                {this.renderImportManifestModal()}
                {this.renderMessages()}
            </MuiThemeProvider>
        );
    }
}
