import { MenuItem } from '@material-ui/core';
import { ValidationService } from 'o8-common';
import * as React from 'react';
import FCSConstants from '../Utils/FCSConstants';
import EditableTextField from './EditableTextField';
import FormSection from './FormSection';

export enum FormAddressValidationEnum {
    Street = 'street',
    Locality = 'locality',
    Region = 'region',
    PostalCode = 'postalCode'
}

export interface IFormAddressProps {
    address: IAddress;
    update(address: IAddress): void;
    showValidationErrors: boolean;
    validator: ValidationService;
    editMode: boolean;
}

export default class FormAddress extends React.Component<IFormAddressProps> {
    private update = (prop: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.update({ ...this.props.address, [prop]: e.target.value });
    }

    public render(): React.ReactNode {
        const { editMode, address, showValidationErrors, validator } = this.props;

        const addressStreetValidator = showValidationErrors && validator.getResults().get(FormAddressValidationEnum.Street);
        const addressLocalityValidator = showValidationErrors && validator.getResults().get(FormAddressValidationEnum.Locality);
        const addressRegionValidator = showValidationErrors && validator.getResults().get(FormAddressValidationEnum.Region);
        const postalCodeValidator = showValidationErrors && validator.getResults().get(FormAddressValidationEnum.PostalCode);

        return (
            <FormSection title="Address"
                         description="*All fields are REQUIRED unless marked optional.">
                <EditableTextField label="Name/Company"
                                   helperText="Optional"
                                   editMode={editMode}
                                   value={address.name || ''}
                                   variant="outlined"
                                   style={FCSConstants.textFieldStyle}
                                   onChange={this.update('name')} />
                <EditableTextField label="Address"
                                   editMode={editMode}
                                   helperText={addressStreetValidator && !addressStreetValidator.valid ? addressStreetValidator.message : ''}
                                   value={address.street || ''}
                                   error={addressStreetValidator && !addressStreetValidator.valid}
                                   variant="outlined"
                                   style={FCSConstants.textFieldStyle}
                                   onChange={this.update('street')} />
                <EditableTextField label="Suite/Room/Floor"
                                   helperText="Optional"
                                   editMode={editMode}
                                   value={address.unit || ''}
                                   variant="outlined"
                                   style={FCSConstants.textFieldStyle}
                                   onChange={this.update('unit')} />
                <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                    <EditableTextField label="City"
                                       editMode={editMode}
                                       value={address.locality || ''}
                                       helperText={addressLocalityValidator && !addressLocalityValidator.valid ? addressLocalityValidator.message : ''}
                                       error={addressLocalityValidator && !addressLocalityValidator.valid}
                                       variant="outlined"
                                       style={FCSConstants.textFieldStyle}
                                       onChange={this.update('locality')} />
                    <EditableTextField label="State"
                                       select
                                       editMode={editMode}
                                       style={FCSConstants.textFieldStyle}
                                       value={address.region || ''}
                                       helperText={addressRegionValidator && !addressRegionValidator.valid ? addressRegionValidator.message : ''}
                                       error={addressRegionValidator && !addressRegionValidator.valid}
                                       variant="outlined"
                                       onChange={this.update('region')}>
                        {FCSConstants.stateList.map(s => <MenuItem key={s} value={s}>{s}</MenuItem>)}
                    </EditableTextField>
                </div>
                <EditableTextField label="Postal Code"
                                   editMode={editMode}
                                   value={address.postalCode || ''}
                                   variant="outlined"
                                   helperText={(postalCodeValidator && !postalCodeValidator.valid) ? postalCodeValidator.message : ''}
                                   error={postalCodeValidator && !postalCodeValidator.valid}
                                   style={{ ...FCSConstants.textFieldStyle, maxWidth: '300px' }}
                                   onChange={this.update('postalCode')} />
            </FormSection>
        );
    }
}
