import { Button, IconButton, ListSubheader, MenuItem, TextField, Tooltip } from '@material-ui/core';
import { Delete, Edit } from '@material-ui/icons';
import { Util } from 'o8-common';
import * as React from 'react';
import FCSConstants from '../Utils/FCSConstants';
import { ICreateFormSectionProps } from './CreateProduct';
import EditableTextField from './EditableTextField';
import FormSection from './FormSection';
import ModalCreateTagLabel, { IModalCreateTagLabelProps } from './ModalCreateTagLabel';

export interface IFormAttributesProps extends ICreateFormSectionProps {
    attributes: Partial<IProductItemTag>[];
    addAttribute(attribute: Partial<IProductItemTag>): void;
    updateAttributes(attributes: Partial<IProductItemTag>[]): void;
    createTag(label: string): ITagLabel;
    tags: Map<string, ITagLabel>;
}

interface IFormAttributesState {
    modalCreateTagLabelProps: IModalCreateTagLabelProps;
}

const defaultAttribute: Partial<IProductItemTag> = {
    label: '',
    value: ''
};

export default class FormAttributes extends React.Component<IFormAttributesProps, IFormAttributesState> {
    constructor(props: IFormAttributesProps) {
        super(props);
        this.state = { modalCreateTagLabelProps: undefined };
    }

    private addAttribute = () => {
        this.props.addAttribute({ ...defaultAttribute });
    }

    private updateAttribute = (attribute: Partial<IProductItemTag>, index: number) => {
        const { attributes } = this.props;
        attributes.splice(index, 1, attribute);
        this.props.updateAttributes(attributes);
    }

    private removeAttribute = (index: number) => {
        return () => {
            const { attributes } = this.props;
            attributes.splice(index, 1);
            this.props.updateAttributes(attributes);
        };
    }

    private createTag = (index: number) => async (e: React.MouseEvent<HTMLSelectElement>) => {
        const promise: Promise<string> = new Promise((confirm, cancel) => {
            this.setState({ modalCreateTagLabelProps: { confirm, cancel, tags: this.props.tags } });
        });

        try {
            const label = await promise;
            const newTag: ITagLabel = await this.props.createTag(label);
            const existing = this.props.attributes[index];
            this.updateAttribute({ ...existing, label: newTag.label }, index);
        } catch (err) {
            // ignore
        } finally {
            this.setState({ modalCreateTagLabelProps: undefined });
        }
    }

    private updateLabel = (attribute: Partial<IProductItemTag>, index: number) => (e: React.ChangeEvent<HTMLInputElement>) => {
        this.updateAttribute({ ...attribute, label: e.target.value }, index);
    }

    private updateValue = (attribute: Partial<IProductItemTag>, index: number) => (e: React.ChangeEvent<HTMLInputElement>) => {
        this.updateAttribute({ ...attribute, value: e.target.value }, index);
    }

    private renderAttribute = (attribute: Partial<IProductItemTag>, index: number) => {
        const { tags, editMode } = this.props;
        const tagList = [ ...tags.values() ];
        const existingLabels = this.props.attributes.map(a => a.label);
        const label = attribute.label || '';
        return (
            <div key={`Attribute__Form-${index}`} style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                <EditableTextField variant="outlined"
                                   label={(editMode || index === 0) ? 'Attribute' : ''}
                                   editMode={editMode}
                                   select
                                   helperText="Select or add new attribute"
                                   margin="normal"
                                   style={{...FCSConstants.textFieldStyle, maxWidth: '300px'}}
                                   SelectProps={{ MenuProps: { MenuListProps: { style: { paddingBottom: '0' } } } }}
                                   value={label}
                                   onChange={this.updateLabel(attribute, index)}>
                    {Util.isNonEmptyArray(tagList) && tagList.map(t => (
                    <MenuItem disabled={Util.isNonEmptyArray(existingLabels) && existingLabels.indexOf(t.label) > -1}
                              key={t.label}
                              value={t.label}>{t.label}</MenuItem>))}
                    <ListSubheader style={FCSConstants.listFooterStyle}>
                        <Button onClick={this.createTag(index)} style={FCSConstants.listFooterButtonStyle}>
                            Add New
                            <Edit />
                        </Button>
                    </ListSubheader>
                </EditableTextField>
                <EditableTextField variant="outlined"
                                   label={(editMode || index === 0) ? 'Value' : ''}
                                   editMode={editMode}
                                   helperText="Add value for the attribute"
                                   margin="normal"
                                   disabled={!label.length}
                                   style={{...FCSConstants.textFieldStyle, maxWidth: '300px'}}
                                   value={attribute.value || ''}
                                   onChange={this.updateValue(attribute, index)} />
                {editMode && <Button onClick={this.removeAttribute(index)} color="primary">Remove</Button>}
            </div>
        );
    }

    public render(): React.ReactNode {
        const { attributes, editMode } = this.props;
        const { modalCreateTagLabelProps } = this.state;

        return (
            <>
                {modalCreateTagLabelProps !== undefined && <ModalCreateTagLabel {...modalCreateTagLabelProps} />}
                <FormSection title="Attributes"
                             description="Add product attributes such as client, BLE, NFX, or color."
                             actions={editMode && <Button onClick={this.addAttribute} color="primary">ADD ROW</Button>}>
                    {Util.isNonEmptyArray(attributes) && attributes.map((a, i) => this.renderAttribute(a, i))}
                </FormSection>
            </>
        );
    }
}
