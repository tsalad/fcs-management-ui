import { Button, Dialog, DialogActions, DialogContent, DialogTitle, MenuItem, TextField } from '@material-ui/core';
import * as moment from 'moment';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import ConfigurationManager from '../Services/ConfigurationManager';
import PrintService from '../Services/PrintService';
import Utilities from '../Services/Utilities';
import { ITemplate } from '../Typings/ITemplate';
import FCSConstants from '../Utils/FCSConstants';

export interface IModalBulkPrintProps {
    onClose(): void;
    setErrorMessage(message: string): void;
}

interface IModalBulkPrintState {
    trays: Map<string, ITemplate>;
    tray: ITemplate;
    template: string;
    quantity: number;
    loading: boolean;
}

export default class ModalBulkPrint extends React.Component<IModalBulkPrintProps, IModalBulkPrintState> {
    constructor(props: IModalBulkPrintProps) {
        super(props);
        this.state = {
            trays: new Map(),
            tray: undefined,
            template: undefined,
            quantity: undefined,
            loading: false
        };
    }

    public componentWillMount = async () => {
        const trays = await PrintService.fetchTrayTemplates();
        this.setState({ trays });
    }

    private updateTemplate = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ template: e.target.value });
    }

    private updateTray = async (e: React.ChangeEvent<HTMLInputElement>) => {
        const { trays } = this.state;
        const tray = trays.get(e.target.value);
        this.setState({ tray, quantity: tray.maxItems, template: '' });
    }

    private updateQuantity = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ quantity: Number(e.target.value) });
    }

    private handleCreateImage = async () => {
        this.setState({ loading: true });
        try {
            const { tray, template, quantity } = this.state;
            const image: IPrintImage = await PrintService.createBulkImage(template, tray, quantity);
            const url = await PrintService.buildPrintUrl(image, 'pdf', `${tray.name}_${template}_${quantity}.pdf`);
            Utilities.downloadFile(url);
        } catch (err) {
            this.props.setErrorMessage('Failed create image');
        }
        this.setState({ loading: false });
    }

    public render(): React.ReactNode {
        const { template, tray, quantity, trays } = this.state;

        return (
            <Dialog open={true}>
                <DialogTitle>
                    Create Bulk Image
                </DialogTitle>
                <DialogContent>
                    <p>Select a tray and template and choose number of trackables to print.</p>
                    <TextField select
                               variant="outlined"
                               label="Tray"
                               style={FCSConstants.textFieldStyle}
                               value={Boolean(tray) ? tray.id : ''}
                               onChange={this.updateTray}>
                        {Boolean(trays) && Linq2.select(trays.values(), t => <MenuItem key={t.id} value={t.id}>{t.name}</MenuItem>).toArray()}
                    </TextField>
                    <TextField select
                               label="Template"
                               variant="outlined"
                               disabled={!Boolean(tray)}
                               style={FCSConstants.textFieldStyle}
                               value={template || ''}
                               onChange={this.updateTemplate}>
                        {Boolean(tray) && Util.isNonEmptyArray(tray.supportedSlotTypes) &&
                        tray.supportedSlotTypes.map(t => <MenuItem key={t} value={t}>{t}</MenuItem>)}
                    </TextField>
                    {Boolean(template) && Boolean(tray) &&
                    <TextField type="number"
                               variant="outlined"
                               style={FCSConstants.textFieldStyle}
                               error={quantity > tray.maxItems}
                               label="Number of Trackables"
                               InputProps={{ inputProps: { min: 1, max: tray.maxItems } }}
                               helperText={quantity > tray.maxItems ? `Please enter a quantity that is less than ${tray.maxItems}` : `Enter quantity between 1 - ${tray.maxItems}`}
                               value={quantity === undefined ? '' : String(quantity)}
                               onChange={this.updateQuantity} />}
                    <div style={{ ...FCSConstants.textFieldStyle, textAlign: 'center' }}>
                        <Button color="primary"
                                variant="contained"
                                disabled={this.state.loading || (Boolean(tray) && quantity > tray.maxItems) || (!Boolean(template) || !Boolean(tray) || !Boolean(quantity))}
                                onClick={this.handleCreateImage}>Create Image</Button>
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button color="primary" onClick={this.props.onClose}>Close</Button>
                </DialogActions>
            </Dialog>
        );
    }
}
