import { Badge, Button, Divider, Tab, Tabs } from '@material-ui/core';
import { Linq2 } from 'o8-common';
import * as React from 'react';
import CreateOrder from '../Contexts/CreateOrder';
import Dispatches from '../Contexts/Dispatches';
import OrdersArchived from '../Contexts/OrdersArchived';
import OrdersBulk from '../Contexts/OrdersBulk';
import OrdersFlagged from '../Contexts/OrdersFlagged';
import OrdersPending from '../Contexts/OrdersPending';
import { Release } from '../Models';
import { IOrderFlag } from '../Typings/IOrderFlag';
import { IRelease } from '../Typings/IRelease';
import { StateEnum } from '../Typings/StateEnum';
import FCSConstants from '../Utils/FCSConstants';
import ModalBulkPrint, { IModalBulkPrintProps } from './ModalBulkPrint';
import ModalImportOrders from './ModalImportOrders';
import View from './View';

export interface IOrdersProps {
    fetchProductItems(): void;
    fetchStock(): void;
    fetchProductionLines(): void;
    clients: Map<string, IClient>;
    selectedClient: string;
    setSuccessMessage(message: string): void;
    setErrorMessage(message: string): void;
    ordersFlagged: Map<string, IOrderFlag>;
    fetchOrdersFlagged(): Promise<void>;
    fetchDispatches(): Promise<void>;
    fetchReleases(): Promise<void>;
    fetchActiveDispatchStatus(): Promise<void>;
    nonArchivedReleases: Map<string, IRelease>;
}

export interface IOrdersState {
    currentTab: number;
    bulkOrder: IRelease;
    createBulkImageProps: IModalBulkPrintProps;
    showImportOrdersModal: boolean;
}

export default class Orders extends React.Component<IOrdersProps, IOrdersState> {
    public timer: number;

    constructor(props: IOrdersProps) {
        super(props);
        this.state = {
            currentTab: 0,
            bulkOrder: undefined,
            createBulkImageProps: undefined,
            showImportOrdersModal: false
        };
    }

    private fetchOrderData = async (): Promise<void> => {
        this.props.fetchOrdersFlagged();
        this.props.fetchDispatches();
        this.props.fetchReleases();
        this.props.fetchActiveDispatchStatus();
    }

    public componentWillMount = (): void => {
        this.props.fetchProductItems();
        this.props.fetchStock();
        this.props.fetchProductionLines();
        this.fetchOrderData();
        this.timer = window.setInterval(this.fetchOrderData, 30000);
    }

    public componentWillUnmount = (): void => {
        window.clearInterval(this.timer);
    }

    private updateTab = (e: React.MouseEvent<HTMLButtonElement>, currentTab: number): void => {
        this.setState({ currentTab });
    }

    private createBulkOrder = (bulkOrder?: IRelease): () => void => {
        return () => {
            this.setState({ bulkOrder });
        };
    }

    private createBulkImage = async () => {
        const promise: Promise<IDispatchInput> = new Promise((onClose) => {
            this.setState({ createBulkImageProps: { onClose, setErrorMessage: this.props.setErrorMessage }});
        });
        await promise;
        this.setState({ createBulkImageProps: undefined });
    }

    private toggleImportOrdersModal = (state: boolean) => () => {
        this.setState({ showImportOrdersModal: state });
    }

    private renderActions = (): React.ReactNode => {
        switch (this.state.currentTab) {
            case 0:
            case 1:
            case 3:
            case 4:
                return (
                    <>
                        {!Boolean(this.state.currentTab) &&
                        <Button onClick={this.toggleImportOrdersModal(true)}
                                variant="outlined"
                                color="primary"
                                style={{ marginRight: '1rem' }}>IMPORT ORDERS</Button>}
                        <Button onClick={this.fetchOrderData}
                                variant="contained"
                                color="primary">REQUEST ORDERS</Button>
                    </>
                );
            case 2:
                const { clientCode } = this.props.clients.get(this.props.selectedClient);
                return (
                    <>
                        <Button onClick={this.createBulkImage}
                                color="primary"
                                variant="outlined"
                                style={{ marginRight: '1rem' }}>Create Bulk Image</Button>
                        <Button onClick={this.createBulkOrder({ ...new Release(), clientCode })}
                                variant="contained"
                                color="primary">NEW BULK ORDER</Button>
                    </>
                );
        }
    }

    public render(): React.ReactNode {
        const { bulkOrder, currentTab } = this.state;
        const { clients, selectedClient, ordersFlagged, nonArchivedReleases } = this.props;

        if (!selectedClient || selectedClient === FCSConstants.MENU_ORDERS) {
            return <div>No clients were found...</div>;
        }

        if (bulkOrder !== undefined) {
            return <CreateOrder cancel={this.createBulkOrder()}
                                order={bulkOrder}
                                fetchOrders={this.fetchOrderData} />;
        }

        const client = clients !== undefined && clients.get(selectedClient);
        const title = `Orders ${client !== undefined ? ` - ${client.name}` : ''}`;
        const filteredOrdersFlagged = Linq2.where((ordersFlagged || new Map()).values(), f => f.clientCode === selectedClient)
                                           .where(f => (nonArchivedReleases || new Map()).has(f.orderId))
                                           .toDictionary(f => f.id);

        const pendingOrders: Map<string, IRelease> = Linq2.where(nonArchivedReleases.values(), r => r.clientCode === this.props.selectedClient)
                                                          .where(r => !Boolean(r.dispatchCode))
                                                          .where(r => !Boolean(r.depotCode))
                                                          .except((filteredOrdersFlagged as unknown as Map<string, IRelease>).values(), (a, b) => a.orderId.localeCompare(b.orderId))
                                                          .where(r => r.state === StateEnum.READY || r.state === StateEnum.PENDING || r.state === StateEnum.SHIPPABLE)
                                                          .toDictionary(r => r.orderId);

        return (
            <View title={title}
                  actions={this.renderActions()}>
                {Boolean(this.state.createBulkImageProps) && <ModalBulkPrint {...this.state.createBulkImageProps} />}
                {Boolean(this.state.showImportOrdersModal) && <ModalImportOrders close={this.toggleImportOrdersModal(false)}
                                                                                 fetchOrders={this.fetchOrderData} 
                                                                                 setSuccessMessage={this.props.setSuccessMessage}
                                                                                 setErrorMessage={this.props.setErrorMessage} />}
                <Tabs value={currentTab}
                      onChange={this.updateTab}
                      indicatorColor="primary"
                      textColor="primary">
                    <Tab label="Pending Orders" style={{paddingTop: '.5rem'}} />
                    <Tab label="Dispatches" style={{paddingTop: '.5rem'}} />
                    <Tab label="Bulk Orders" style={{paddingTop: '.5rem'}} />
                    <Tab label={<Badge badgeContent={(filteredOrdersFlagged || new Map()).size}
                                       max={99}
                                       color="error"
                                       style={{padding: '0 1.5rem'}}>Flagged Orders</Badge>}
                         style={{paddingTop: '.5rem'}} />
                    <Tab label="Archived Orders" style={{paddingTop: '.5rem'}} />
                </Tabs>
                <Divider />
                <div style={{ margin: '1rem 0' }}>
                    {currentTab === 0 && <OrdersPending fetchOrders={this.fetchOrderData} releases={pendingOrders} />}
                    {currentTab === 1 && <Dispatches fetchOrders={this.fetchOrderData} />}
                    {currentTab === 2 && <OrdersBulk editOrder={this.createBulkOrder} fetchOrders={this.fetchOrderData} />}
                    {currentTab === 3 && <OrdersFlagged fetchOrders={this.fetchOrderData} />}
                    {currentTab === 4 && <OrdersArchived />}
                </div>
            </View>
        );
    }
}
