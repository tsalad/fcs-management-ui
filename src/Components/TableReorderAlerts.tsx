import { IconButton, MenuItem, Popover } from '@material-ui/core';
import { MoreVert } from '@material-ui/icons';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import { getNestedValue } from '../Utils/Util';
import DataTable, { IDataTableColumn, SortOrder } from './DataTable';

export interface ITableReorderAlertsProps {
    reorderAlerts: Map<string, IProductItem>;
    stock: Map<string, IStock[]>;
    toggleProductForm(item: IProductItem): void;
}

interface ITableReorderAlertsState {
    sortBy: string;
    sortOrder: SortOrder;
    menuAnchor: HTMLElement;
    selectedSku: string;
}

export default class TableReorderAlerts extends React.Component<ITableReorderAlertsProps, ITableReorderAlertsState> {
    constructor(props: ITableReorderAlertsProps) {
        super(props);
        this.state = {
            sortBy: 'name',
            sortOrder: SortOrder.Asc,
            menuAnchor: undefined,
            selectedSku: undefined
        };
    }

    private getProductStock = (item: IProductItem): number => {
        const productStock = this.props.stock.get(item.sku);
        return Util.isNonEmptyArray(productStock) ? Linq2.aggregate(productStock, (acc: number = 0, curr) => acc += curr.quantity) : 0;
    }

    private getSortedData = (data: IProductItem[]): IProductItem[] => {
        const { sortBy, sortOrder } = this.state;
        if (Util.isNonEmptyArray(data)) {
            data.sort((a: IProductItem, b: IProductItem) => (
                String(getNestedValue(sortBy, a)).localeCompare(String(getNestedValue(sortBy, b))) * (sortOrder === SortOrder.Asc ? 1 : -1))
            );
        }
        return data;
    }

    private handleSort = (property: string): void => {
        const { sortOrder, sortBy } = this.state;
        const order = (property !== sortBy || sortOrder === SortOrder.Desc) ? SortOrder.Asc : SortOrder.Desc;
        this.setState({ sortOrder: order, sortBy: property });
    }

    private showMenu = (selectedSku: string) => (e: React.MouseEvent<HTMLElement>) => {
        this.setState({ menuAnchor: e.currentTarget, selectedSku });
    }

    private hideMenu = () => {
        this.setState({ menuAnchor: undefined, selectedSku: undefined });
    }

    private toggleProductForm = (item: IProductItem) => () => {
        this.hideMenu();
        this.props.toggleProductForm(item);
    }

    public render(): React.ReactNode {
        const columns: IDataTableColumn<IProductItem>[] = [
            {
                title: 'Product',
                value: (item) => item.name,
                headerClicked: () => this.handleSort('name'),
                sortOrder: this.state.sortBy === 'name' ? this.state.sortOrder : undefined
            },
            {
                title: 'SKU',
                value: (item) => item.sku,
                headerClicked: () => this.handleSort('sku'),
                sortOrder: this.state.sortBy === 'sku' ? this.state.sortOrder : undefined
            },
            {
                title: 'Reorder Level',
                value: (item) => item.reorderThreshold
            },
            {
                title: 'Stock',
                value: (item) => this.getProductStock(item)
            },
            {
                value: (item: IProductItem) => {
                    return (
                        <>
                            <IconButton onClick={this.showMenu(item.sku)}>
                                <MoreVert />
                            </IconButton>
                            <Popover open={this.state.selectedSku === item.sku}
                                     anchorEl={this.state.menuAnchor}
                                     anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                                     transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                                     onClose={this.hideMenu}>
                                <MenuItem onClick={this.toggleProductForm(item)}>View/Edit</MenuItem>
                            </Popover>
                        </>
                    );
                }
            }
        ];

        return <DataTable columns={columns}
                          paginate
                          data={this.getSortedData([ ...this.props.reorderAlerts.values() ])} />;
    }
}
