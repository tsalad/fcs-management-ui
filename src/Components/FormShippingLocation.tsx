import { Button, MenuItem } from '@material-ui/core';
import { Util } from 'o8-common';
import * as React from 'react';
import FCSConstants from '../Utils/FCSConstants';
import { formatAddress } from '../Utils/Util';
import { ICreateOrderFormSectionProps } from './CreateOrder';
import EditableTextField from './EditableTextField';
import FormSection from './FormSection';

export interface IFormShippingLocationProps extends ICreateOrderFormSectionProps {
    depots: IDepot[];
}

export default class FormShippingLocation extends React.Component<IFormShippingLocationProps> {
    private updateShippingLocation = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.props.updateOrder({ ...this.props.order, depotCode: e.target.value });
    }

    public render(): React.ReactNode {
        const { showValidationErrors, validator, order, depots, editMode} = this.props;
        const depotCodeValidator = showValidationErrors && validator.getResults().get('depotCode');
        const selectedDepot = this.props.depots.find(d => d.depotCode === order.depotCode);
        const depotValue = editMode ? order.depotCode : order.depotName;
        return (
            <FormSection title="Shipping Location"
                         description="Select the shipping location for the client's bulk order.">
                <EditableTextField select
                                   editMode={editMode}
                                   onChange={this.updateShippingLocation}
                                   variant="outlined"
                                   helperText={depotCodeValidator && !depotCodeValidator.valid ? depotCodeValidator.message : 'Clients shipping location'}
                                   value={depotValue || ''}
                                   error={depotCodeValidator && !depotCodeValidator.valid}
                                   style={{ ...FCSConstants.textFieldStyle, maxWidth: '300px' }}
                                   label="Shipping Location">
                    {Util.isNonEmptyArray(depots) && depots.map(d => <MenuItem key={d.depotCode} value={d.depotCode}>{d.name}</MenuItem>)}
                </EditableTextField>
                {selectedDepot !== undefined &&
                <>
                    <div style={{display: 'flex', flexDirection: 'row', width: '100%', justifyContent: 'space-between'}}>
                        <EditableTextField label="Name" value={selectedDepot.name} editMode={false} />
                        <EditableTextField label="Contact" value={selectedDepot.address.name} editMode={false} />
                    </div>
                    <EditableTextField label="Address" value={formatAddress(selectedDepot && selectedDepot.address)} editMode={false} />
                </>}
            </FormSection>
        );
    }
}
