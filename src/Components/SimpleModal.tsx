import { Button, Dialog, DialogActions, DialogContent, DialogTitle, withMobileDialog } from '@material-ui/core';
import * as React from 'react';

export interface ISimpleModalProps extends MC.Omit<__MaterialUI.DialogProps, 'open'> {
    fullScreen?: boolean;
    fullWidth?: boolean;
    disableBackdropClick?: boolean;
    show?: boolean;
    cancel?(e: React.MouseEvent<HTMLButtonElement>): void;
    confirm?(e: React.MouseEvent<HTMLButtonElement>): void;
    showCancelButton?: boolean;
    showConfirmButton?: boolean;
    disableCancelButton?: boolean;
    disableConfirmButton?: boolean;
    cancelButtonText?: any;
    confirmButtonText?: any;
}

class SimpleModal extends React.Component<ISimpleModalProps> {
    public static defaultProps: Partial<ISimpleModalProps> = {
        fullScreen: false,
        fullWidth: true,
        title: '',
        show: true,
        cancel: () => undefined,
        confirm: () => undefined,
        showCancelButton: true,
        showConfirmButton: true,
        disableCancelButton: false,
        disableConfirmButton: false,
        cancelButtonText: 'Cancel',
        confirmButtonText: 'Confirm',
        disableBackdropClick: false
    };

    private static focusInputElement(element: HTMLElement): void {
        if (element && typeof element.focus === 'function') {
            element.focus();
        }
    }

    private getDialogActions(): React.ReactNode {
        const { showCancelButton, disableCancelButton, cancelButtonText, confirm, cancel,
                showConfirmButton, disableConfirmButton, confirmButtonText } = this.props;
        return (
            <>
                {showCancelButton &&
                <Button onClick={cancel} color="default" disabled={disableCancelButton}>
                    {cancelButtonText}
                </Button>
                }
                {showConfirmButton &&
                <Button onClick={confirm} color="primary" disabled={disableConfirmButton}>
                    {confirmButtonText}
                </Button>
                }
            </>
        );
    }

    public render(): React.ReactNode {
        const { cancel, confirm, showCancelButton, showConfirmButton, disableCancelButton, disableConfirmButton, cancelButtonText,
                confirmButtonText, title, show, children, titleStyle, contentStyle, actionsContainerStyle, ...rest } = this.props;
        const actions = this.getDialogActions();
        return (
            <Dialog {...rest}
                    onClose={cancel}
                    aria-labelledby={typeof title === 'string' ? title : 'mobile-responsive-dialog'}
                    open={show}>
                {title && <DialogTitle style={titleStyle}>{title}</DialogTitle>}
                {children && <DialogContent style={contentStyle}>{children}</DialogContent>}
                {actions && <DialogActions style={actionsContainerStyle}>{actions}</DialogActions>}
            </Dialog>
        );
    }
}

export default withMobileDialog<ISimpleModalProps>()(SimpleModal);
