import { Divider, TextField } from '@material-ui/core';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import FCSConstants from '../Utils/FCSConstants';
import AutoComplete from './AutoComplete';
import EditableTextField from './EditableTextField';
import SimpleModal from './SimpleModal';

export interface IModalAddProductsToOrderReturnValue {
    product: IProductItem;
    quantity: number;
}

export interface IModalAddProductsToOrderProps {
    products: Map<string, IProductItem>;
    stock: Map<string, IStock[]>;
    fetchStock(): void;
    confirm({ product, quantity }: IModalAddProductsToOrderReturnValue): void;
    cancel(): void;
}

interface IModalAddProductsToOrderState {
    product: IProductItem;
    productSearch: string;
    quantity: number;
}

export default class ModalAddProductsToOrder extends React.Component<IModalAddProductsToOrderProps, IModalAddProductsToOrderState> {
    constructor(props: IModalAddProductsToOrderProps) {
        super(props);
        this.state = {
            product: undefined,
            productSearch: '',
            quantity: undefined
        };
    }

    public componentDidMount = () => {
        this.props.fetchStock();
    }

    private onChange = (productSearch: string, product: IProductItem) => {
        this.setState({ productSearch, product });
    }

    private updateQuantity = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ quantity: Number(e.target.value) });
    }

    private confirm = () => {
        const { product, quantity } = this.state;
        this.props.confirm({ product, quantity });
    }

    public render(): React.ReactNode {
        const { product, productSearch } = this.state;
        const suggestions: Map<string, IProductItem> = Linq2.toDictionary(this.props.products.values(), p => `${p.name} (${p.sku})`);
        const currentStock = product && Boolean(product.sku) && this.props.stock.get(this.state.product.sku);
        return (
            <SimpleModal title="Add Product"
                         disableConfirmButton={this.state.product === undefined || !this.state.quantity}
                         confirmButtonText="Add"
                         confirm={this.confirm}
                         cancel={this.props.cancel}>
                <p>Search for a product but product name or internal SKU:</p>
                <AutoComplete suggestions={[ ...suggestions.keys() ]}
                              onUpdate={(value) => this.onChange(value, suggestions.get(value))}
                              variant="outlined"
                              placeholder="Search"
                              style={{ ...FCSConstants.textFieldStyle, marginBottom: '1rem'}}
                              value={productSearch} />
                {product !== undefined &&
                <div style={{ display: 'flex', flexDirection: 'row', margin: '1rem 0' }}>
                    <EditableTextField label="Product Name"
                                       value={product.name}
                                       editMode={false} />
                    <EditableTextField label="SKU"
                                       value={product.sku}
                                       editMode={false} />
                    <EditableTextField label="Available Stock"
                                       value={Util.isNonEmptyArray(currentStock) ? Linq2.aggregate(currentStock, (acc: number = 0, curr) => acc += curr.quantity) : 0}
                                       editMode={false} />
                </div>}
                <Divider />
                <p>Search for a product but product name or internal SKU:</p>
                <TextField value={this.state.quantity}
                           type="number"
                           helperText="Quantity to ship"
                           placeholder="Quantity"
                           InputProps={{ inputProps: { min: 0 } }}
                           variant="outlined"
                           onChange={this.updateQuantity} />
            </SimpleModal>
        );
    }
}
