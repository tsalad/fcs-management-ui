import { Divider, TextField } from '@material-ui/core';
import { Search } from '@material-ui/icons';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import { EventCodeEnum } from '../Typings/EventCodeEnum';
import { IStockInput } from '../Typings/IStockInput';
import FCSConstants from '../Utils/FCSConstants';
import AutoComplete from './AutoComplete';
import EditableTextField from './EditableTextField';
import SimpleModal from './SimpleModal';

export interface IModalAddProductStockProps {
    cancel(): void;
    confirm(stock: IStockInput): void;
    productItem: IProductItem;
    storage: Map<string, IStorage>;
    currentStock: IStock[];
}

interface IModalAddProductStockState {
    stock: IStockInput;
    storageQuery: string;
    showErrors: boolean;
}

export default class ModalAddProductStock extends React.Component<IModalAddProductStockProps, IModalAddProductStockState> {
    constructor(props: IModalAddProductStockProps) {
        super(props);
        this.state = {
            stock: {
                storageCode: '',
                sku: props.productItem.sku,
                eventCode: EventCodeEnum.RECEIVING,
                adjustment: 0
            },
            storageQuery: '',
            showErrors: false
        };
    }

    private confirm = () => {
        const { stock } = this.state;
        const { currentStock } = this.props;

        if (!Boolean(stock.storageCode) || !Boolean(stock.adjustment)) {
            this.setState({ showErrors: true });
            return;
        }
        
        // If there is existing stock at the selcted storage, we need to pass in the current updatedAt.
        const match = Util.isNonEmptyArray(currentStock) && Linq2.firstOrDefault(currentStock, undefined, p => p.storageCode === stock.storageCode);

        if (Boolean(match)) {
            stock.updatedAt = match.updatedAt;
        }

        this.props.confirm(stock);
    }

    private updateStorage = (storageQuery: string) => {
        const { storage } = this.props;
        const match = Linq2.firstOrDefault(storage.values(), undefined, s => s.name.toLowerCase() === storageQuery.toLowerCase());
        this.setState({ storageQuery, stock: { ...this.state.stock, storageCode: match && match.storageCode } });
    }

    private updateAdjustment = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ stock: { ...this.state.stock, adjustment: Number(e.target.value) } });
    }

    public render(): React.ReactNode {
        const { currentStock, cancel, storage } = this.props;
        const { stock, storageQuery, showErrors } = this.state;

        return (
            <SimpleModal title="Add new stock" confirmButtonText="Add" confirm={this.confirm} cancel={cancel}>
                <p>Current stock information</p>
                {Util.isNonEmptyArray(currentStock) && currentStock.map((s, i) => (
                    <div key={`${s.storageCode}-${i}`} style={{ display: 'flex', flexDirection: 'row' }}>
                        <EditableTextField label={i === 0 ? 'Location' : ''}
                                           editMode={false}
                                           style={{padding: 0, margin: 0}}
                                           value={this.props.storage.get(s.storageCode).name} />
                        <EditableTextField label={i === 0 ? 'Quantity' : ''}
                                           editMode={false}
                                           style={{padding: 0, margin: 0}}
                                           value={s.quantity} />
                    </div>
                ))}
                <Divider />
                <div style={{ display: 'flex', flexDirection: 'row', margin: '1rem 0' }}>
                    <AutoComplete onUpdate={this.updateStorage}
                                  value={storageQuery}
                                  suggestions={[ ...storage.values() ].map(s => s.name)}
                                  error={showErrors && !Boolean(stock.storageCode)}
                                  variant="outlined"
                                  InputLabelProps={{ shrink: true }}
                                  helperText={showErrors && !Boolean(stock.storageCode) ? 'Enter an existing location' : 'Search and select a location'}
                                  label="Enter new stock information"
                                  placeholder="Search Location"
                                  InputProps={{ endAdornment: <Search color="disabled" /> }}
                                  style={{ ...FCSConstants.textFieldStyle, flex: 'auto' }} />
                    <TextField style={{ ...FCSConstants.textFieldStyle, flex: 'auto' }}
                               value={stock.adjustment}
                               type="number"
                               InputProps={{ inputProps: { min: 0 } }}
                               label="Quantity Received"
                               InputLabelProps={{ shrink: true }}
                               placeholder="Quantity"
                               helperText={showErrors && !Boolean(stock.adjustment) ? 'Enter a quantity.' : ''}
                               error={showErrors && !Boolean(stock.adjustment)}
                               disabled={!Boolean(stock.storageCode)}
                               variant="outlined"
                               onChange={this.updateAdjustment} />
                </div>
            </SimpleModal>
        );
    }
}
