import { Button, MenuItem } from '@material-ui/core';
import { Linq2, Util } from 'o8-common';
import * as React from 'react';
import FCSConstants from '../Utils/FCSConstants';
import { ICreateFormSectionProps } from './CreateProduct';
import EditableTextField from './EditableTextField';
import FormSection from './FormSection';

export interface IFormExternalSKUProps extends ICreateFormSectionProps {
    productItemSuppliers: IProductItemSupplier[];
    updateProductItemSuppliers(productItemSuppliers: IProductItemSupplier[]): void;
    fetchSuppliers(): void;
    suppliers: Map<string, ISupplier>;
}

interface IFormExternalSKUState {
    initialItem: IProductItem;
}

export default class FormExternalSKU extends React.Component<IFormExternalSKUProps, IFormExternalSKUState> {
    constructor(props: IFormExternalSKUProps) {
        super(props);
        this.state = {
            initialItem: props.item
        };
    }

    public componentDidMount() {
        this.props.fetchSuppliers();
    }

    private updateSuppliers = (supplier: IProductItemSupplier, index: number) => {
        const updated = [ ...this.props.productItemSuppliers ];
        updated.splice(index, 1, supplier);
        this.props.updateProductItemSuppliers(updated);
    }

    private removeSupplier = (index: number) => {
        return () => {
            const updated = [ ...this.props.productItemSuppliers ];
            updated.splice(index, 1);
            this.props.updateProductItemSuppliers(updated);
        };
    }

    private addNewSupplier = () => {
        const{ productItemSuppliers, updateProductItemSuppliers, item } = this.props;
        updateProductItemSuppliers([ ...productItemSuppliers, { supplierSku: null, supplierCode: null } ]);
    }

    private updateSupplierCode = (supplier: IProductItemSupplier, i: number) => (e: React.ChangeEvent<HTMLInputElement>) => {
        this.updateSuppliers({ ...supplier, supplierCode: e.target.value }, i);
    }

    private updateSupplierSku = (supplier: IProductItemSupplier, i: number) => (e: React.ChangeEvent<HTMLInputElement>) => {
        this.updateSuppliers({ ...supplier, supplierSku: e.target.value.trim().toUpperCase() }, i);
    }

    private renderSupplier = (productItemSupplier: IProductItemSupplier, i: number) => {
        const { suppliers, editMode, productItemSuppliers } = this.props;
        const { supplierCode, supplierSku } = productItemSupplier;
        const supplierList = [ ...suppliers.values() ];
        const existingSuppliers =  productItemSuppliers.map(s => s.supplierCode);
        return (
            <div key={`Attribute__Form-${i}`} style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                <EditableTextField select
                                   label={(editMode || i === 0) ? 'Supplier' : ''}
                                   helperText="Choose supplier"
                                   style={{...FCSConstants.textFieldStyle, maxWidth: '300px'}}
                                   value={supplierCode || ''}
                                   editMode={editMode}
                                   onChange={this.updateSupplierCode(productItemSupplier, i)}>
                    {Util.isNonEmptyArray(supplierList) &&
                    supplierList.map(s => <MenuItem key={s.supplierCode}
                                                    value={s.supplierCode}
                                                    disabled={Util.isNonEmptyArray(existingSuppliers) && existingSuppliers.indexOf(s.supplierCode) > -1}>
                                                {s.name}
                                          </MenuItem>)}
                </EditableTextField>
                <EditableTextField placeholder="Enter External SKU"
                                   helperText="Enter a new External SKU"
                                   label={(editMode || i === 0) ? 'External SKU' : ''}
                                   style={{...FCSConstants.textFieldStyle, maxWidth: '300px'}}
                                   value={supplierSku || ''}
                                   disabled={!Boolean(supplierCode)}
                                   editMode={editMode}
                                   onChange={this.updateSupplierSku(productItemSupplier, i)} />
                {editMode && <Button color="primary" onClick={this.removeSupplier(i)}>REMOVE</Button>}
            </div>
        );
    }

    public render(): React.ReactNode {
        const { productItemSuppliers, suppliers } = this.props;
        const existingSuppliers = [ ...suppliers.values() ].map(s => s.supplierCode);
        const remainingSuppliers = Linq2.where(existingSuppliers, s => !productItemSuppliers.some(p => p.supplierCode === s)).toArray();
        return (
            <FormSection title="External SKU"
                         description="Add external supplier / external SKU for this product."
                         actions={this.props.editMode && <Button disabled={!Util.isNonEmptyArray(remainingSuppliers)}
                                                                 onClick={this.addNewSupplier} color="primary">ADD ROW</Button>}>
                {Util.isNonEmptyArray(productItemSuppliers) && productItemSuppliers.map((s, i) => this.renderSupplier(s, i))}
            </FormSection>
        );
    }
}
