import { Typography } from '@material-ui/core';
import * as React from 'react';
import FCSConstants from '../Utils/FCSConstants';

export interface IFormSectionProps {
    title?: React.ReactNode;
    description?: string;
    actions?: React.ReactNode;
    footer?: React.ReactNode;
    fullWidth?: boolean;
}

export default class FormSection extends React.Component<IFormSectionProps> {
    public static defaultProps: Partial<IFormSectionProps> = {
        fullWidth: false
    };

    public render(): React.ReactNode {
        const wrapperStyle: React.CSSProperties = {
            maxWidth: this.props.fullWidth ? undefined : '800px',
            width: '100%',
            display: 'flex',
            flexDirection: 'column',
            padding: '1rem 0'
        };

        const bodyStyle: React.CSSProperties = {
            display: 'flex',
            flexDirection: 'column',
            border: `1px solid ${FCSConstants.grayLight}`,
            marginTop: '1rem'
        };

        const footerStyle: React.CSSProperties = {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-end',
            borderTop: `1px solid ${FCSConstants.grayLight}`
        };

        const sectionStyle: React.CSSProperties = {
            padding: '.5rem 1rem'
        };

        return (
            <div style={wrapperStyle}>
                {this.props.title !== undefined && <Typography variant="h6">{this.props.title}</Typography>}
                <div style={bodyStyle}>
                    <div style={sectionStyle}>
                        {this.props.description !== undefined && <Typography variant="body1">{this.props.description}</Typography>}
                        {this.props.children}
                    </div>
                    {this.props.actions &&
                    <div style={{...sectionStyle, ...footerStyle}}>{this.props.actions}</div>}
                </div>
            </div>
        );
    }
}
