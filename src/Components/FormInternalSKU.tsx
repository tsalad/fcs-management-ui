import { Button, TextField } from '@material-ui/core';
import * as React from 'react';
import { ProductItem } from '../Models';
import FCSConstants from '../Utils/FCSConstants';
import { ICreateFormSectionProps } from './CreateProduct';
import Error from './Error';
import FormSection from './FormSection';

export interface IFormInternalSKUProps extends ICreateFormSectionProps {
    validator: MC.ValidationService;
}

export default class FormInternalSKU extends React.Component<IFormInternalSKUProps> {
    private updateSku = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.updateItem({ ...this.props.item, sku: e.target.value.toUpperCase().trim() });
    }

    public render(): React.ReactNode {
        const { item, validator, showValidationErrors } = this.props;
        const skuValidator = showValidationErrors && validator.getResults().get('sku');
        return (
            <FormSection title="Internal SKU"
                         description="Create an internal SKU to mask any external supplier SKUs.  It cannot be modified once created.">
                <TextField variant="outlined"
                           placeholder="Enter"
                           helperText="(eg: TRK-BND-BLACK02)"
                           error={skuValidator && !skuValidator.valid}
                           value={item.sku || ''}
                           disabled={Boolean(item.updatedAt)}
                           onChange={this.updateSku}
                           style={FCSConstants.textFieldStyle} />
                {skuValidator && !skuValidator.valid && <Error text={skuValidator.message} />}
            </FormSection>
        );
    }
}
