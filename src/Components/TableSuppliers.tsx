import { IconButton, MenuItem, Popover } from '@material-ui/core';
import { MoreVert } from '@material-ui/icons';
import { Linq2 } from 'o8-common';
import * as React from 'react';
import FormSupplier from '../Contexts/FormSupplier';
import DataTable, { IDataTableColumn } from './DataTable';

export interface ITableSuppliersProps {
    suppliers: Map<string, ISupplier>;
    productItems: Map<string, IProductItem>;
    createSupplier(supplier: ISupplier): Promise<ISupplier>;
    updateSupplier(supplier: ISupplier): Promise<ISupplier>;
    deleteSupplier(key: string): Promise<void>;
    editSupplier: (supplier?: ISupplier) => () => void;
    selectedSupplier: ISupplier;
    setSuccessMessage(message: string): void;
    setErrorMessage(message: string): void;
}

interface ITableSuppliersState {
    productItemSuppliers: Map<string, IProductItemSupplier[]>;
    selected: string;
    menuAnchor: HTMLElement;
}

export default class TableSuppliers extends React.Component<ITableSuppliersProps, ITableSuppliersState> {
    constructor(props: ITableSuppliersProps) {
        super(props);
        this.state = {
            productItemSuppliers: new Map(),
            selected: undefined,
            menuAnchor: undefined
        };
    }

    public async componentDidMount() {
        const { productItems } = this.props;
        const productItemSuppliers: Map<string, IProductItemSupplier[]> = Linq2.selectMany(productItems.values(), i => i.suppliers)
                                                                               .groupBy(s => s.supplierCode)
                                                                               .toDictionary();
        this.setState({ productItemSuppliers });
    }
    
    private showMenu = (key: string) => (e: React.MouseEvent<HTMLElement>) => {
        this.setState({ selected: key, menuAnchor: e.currentTarget });
    }

    private hideMenu = () => {
        this.setState({ selected: undefined, menuAnchor: undefined });
    }

    private toggleSupplierForm = (item?: ISupplier) => () => {
        this.props.editSupplier(item)();
        this.setState({ menuAnchor: undefined, selected: undefined });
    }

    private deleteItem = (supplier: ISupplier) => async () => {
        try {
            await this.props.deleteSupplier(supplier.supplierCode);
            this.props.setSuccessMessage('Supplier successfully deleted');
        } catch (err) {
            this.props.setErrorMessage('Failed to delete Supplier');
        }
    }

    public render(): React.ReactNode {
        if (this.props.selectedSupplier !== undefined) {
            return <FormSupplier supplier={this.props.selectedSupplier}
                                 cancel={this.toggleSupplierForm()} />;
        }

        const { productItemSuppliers } = this.state;
        const columns: IDataTableColumn<ISupplier>[] = [
            {
                title: 'Supplier',
                value: (item) => item.name
            },
            {
                title: 'Number of SKUs',
                value: (item) => productItemSuppliers.has(item.supplierCode) ? productItemSuppliers.get(item.supplierCode).length : 0
            },
            {
                value: (item: ISupplier) => {
                    const numSkus = productItemSuppliers.has(item.supplierCode) ? productItemSuppliers.get(item.supplierCode).length : 0;
                    return (
                        <>
                            <IconButton onClick={this.showMenu(item.supplierCode)}>
                                <MoreVert />
                            </IconButton>
                            <Popover open={this.state.selected === item.supplierCode}
                                     anchorEl={this.state.menuAnchor}
                                     anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                                     transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                                     onClose={this.hideMenu}>
                                <MenuItem onClick={this.toggleSupplierForm(item)}>View/Edit</MenuItem>
                                <MenuItem disabled={numSkus > 0} onClick={this.deleteItem(item)}>Delete</MenuItem>
                            </Popover>
                        </>
                    );
                }
            }
        ];

        return <DataTable data={[ ...this.props.suppliers.values() ]} columns={columns} />;
    }
}
