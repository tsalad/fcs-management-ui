import { MenuItem, TextField } from '@material-ui/core';
import { Util } from 'o8-common';
import * as React from 'react';
import LogisticsService from '../Services/LogisticsService';
import FCSConstants from '../Utils/FCSConstants';
import FileUploadInput from './FileUploadInput';
import SimpleModal from './SimpleModal';

export interface IModalImportManifestProps {
    productItems: Map<string, IProductItem>;
    fetchItems(): void;
    toggleModalImportManifest(state: boolean): void;
    setErrorMessage(message: string): void;
    setSuccessMessage(message: string): void;
}

interface IModalImportManifestState {
    sku: string;
    file: File;
}

export default class ModalImportManifest extends React.Component<IModalImportManifestProps, IModalImportManifestState> {
    constructor(props: IModalImportManifestProps) {
        super(props);
        this.state = {
            sku: undefined,
            file: undefined
        };
    }

    public componentDidMount(): void {
        this.props.fetchItems();
    }

    private updateSku = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ sku: e.target.value });
    }

    private updateFile = (file: File): void => {
        this.setState({ file });
    }

    private confirm = async () => {
        try {
            const { sku, file } = this.state;
            const items = await LogisticsService.importManifest(sku, file);
            this.props.setSuccessMessage(`Successfully created ${items.length || 0} entries`);
            this.closeModal();
        } catch (err) {
            this.props.setErrorMessage('Failed to import device manifest');
        }
    }

    private closeModal = () => {
        this.props.toggleModalImportManifest(false);
    }

    public render(): React.ReactNode {
        const { productItems } = this.props;
        const items = Boolean(productItems) ? [ ...productItems.values() ] : [];
        return (
            <SimpleModal title="Import Device Manifest"
                         disableConfirmButton={!Boolean(this.state.sku) || !Boolean(this.state.file)}
                         confirm={this.confirm}
                         cancel={this.closeModal}>
                <p>Choose a Product SKU and import manifest .csv file</p>
                <TextField select
                           label="Product SKU"
                           style={{ ...FCSConstants.textFieldStyle, flex: 'auto' }}
                           value={this.state.sku || ''}
                           variant="outlined"
                           helperText="List of current product items"
                           onChange={this.updateSku}>
                    {Util.isNonEmptyArray(items) && items.map(i => <MenuItem key={i.sku} value={i.sku}>{i.sku}</MenuItem>)}
                </TextField>
                <FileUploadInput onUpdate={this.updateFile} title="BROWSE" acceptType=".csv" />
                {Boolean(this.state.file) && <p style={{ margin: '1rem 0 0 .5rem' }}>Selected file: {this.state.file.name}</p>}
            </SimpleModal>
        );
    }
}
