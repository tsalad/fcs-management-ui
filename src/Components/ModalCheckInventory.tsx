import { Divider } from '@material-ui/core';
import { Util } from 'o8-common';
import * as React from 'react';
import ProductAvailabilityList from '../Contexts/ProductAvailabilityList';
import { IRelease } from '../Typings/IRelease';
import SimpleModal from './SimpleModal';

export interface IModalCheckInventoryProps {
    orders: IRelease[];
    confirm(): void;
}

export default class ModalCheckInventory extends React.Component<IModalCheckInventoryProps> {
    public render(): React.ReactNode {
        const { orders } = this.props;
        return (
            <SimpleModal title="Check Inventory"
                         confirm={this.props.confirm}
                         showCancelButton={false}>
                <p>Total # of orders: <strong>{Util.isNonEmptyArray(orders) ? orders.length : 0}</strong></p>
                {Util.isNonEmptyArray(orders) && <><Divider /><ProductAvailabilityList orders={orders} /><Divider /></>}
            </SimpleModal>
        );
    }
}
