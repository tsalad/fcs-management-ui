import { Button, Dialog, DialogActions, DialogContent, DialogTitle, IconButton, MenuItem, Popover, Typography } from '@material-ui/core';
import { ExpandLess, ExpandMore, MoreVert } from '@material-ui/icons';
import { Linq2, SimpleModal, Util } from 'o8-common';
import * as React from 'react';
import Dispatch from '../Models/Dispatch';
import ConfigurationManager from '../Services/ConfigurationManager';
import PrintService from '../Services/PrintService';
import ShipmentService from '../Services/ShipmentService';
import Utilities from '../Services/Utilities';
import { IDispatch } from '../Typings/IDispatch';
import { IDispatchStatus } from '../Typings/IDispatchStatus';
import { IRelease } from '../Typings/IRelease';
import { IShipment } from '../Typings/IShipment';
import { JobStateEnum } from '../Typings/JobStateEnum';
import { PrintSideEnum } from '../Typings/PrintSideEnum';
import { StateEnum } from '../Typings/StateEnum';
import FCSConstants from '../Utils/FCSConstants';
import { formatDateString } from '../Utils/Util';
import DataTable, { IDataTableColumn } from './DataTable';
import ModalCheckInventory from './ModalCheckInventory';
import ModalFulfillDispatch, { IModalFulfillDispatchProps } from './ModalFulfillDispatch';
import PrintWindowPortal from './PrintWindowPortal';

export interface IDisptachesProps {
    releases: Map<string, IRelease>;
    dispatches: Map<string, IDispatch>;
    selectedClient: string;
    updateDispatch(d: Partial<IDispatch>): IDispatch;
    updateDispatchGroup(g: IDispatchGroup): IDispatch;
    deleteDispatch(d: Partial<IDispatch>): void;
    productionLines: Map<string, IProductionLine>;
    shipDispatch(d: Partial<IDispatch>): Promise<IDispatch>;
    fetchOrders(): void;
    setErrorMessage(message: string): void;
    setSuccessMessage(message: string): void;
    fetchDispatches(): void;
    fetchActiveDispatchStatus(): void;
    activeDispatchStatus: Map<string, IDispatchStatus>;
}

export interface IDisptachesState {
    menuAnchor: HTMLElement;
    cancelDispatchItem: IDispatch;
    showInventoryReleases: IRelease[];
    fulfillDispatchModalProps: IModalFulfillDispatchProps;
    archiveDispatch: IDispatch;
    imageUrls: string[];
    expandedDispatch: IDispatch;
    creatingImage: boolean;
    printLogojetModalOptions: { group: IDispatchGroup, side: PrintSideEnum };
    groupOptionsKey: string;
}

export default class Dispatches extends React.Component<IDisptachesProps, IDisptachesState> {
    public timer: number;
    private mounted: boolean = false;

    constructor(props: undefined) {
        super(props);
        this.state = {
            cancelDispatchItem: undefined,
            showInventoryReleases: undefined,
            fulfillDispatchModalProps: undefined,
            archiveDispatch: undefined,
            imageUrls: undefined,
            expandedDispatch: undefined,
            creatingImage: false,
            menuAnchor: undefined,
            printLogojetModalOptions: undefined,
            groupOptionsKey: undefined
        };
    }

    public componentWillMount() {
        this.props.fetchDispatches();
        this.props.fetchActiveDispatchStatus();
    }

    public componentDidMount() {
        this.mounted = true;
    }

    public componentWillUnmount() {
        this.mounted = false;
    }

    private setCancelDispatch = (cancelDispatchItem?: IDispatch) => {
        return () => {
            this.setState({ cancelDispatchItem });
        };
    }

    private setShowInventory = (showInventoryItem?: IDispatch) => {
        return async () => {
            if (!Boolean(showInventoryItem)) {
                this.setState({ showInventoryReleases: undefined });
                return;
            }

            const showInventoryReleases: IRelease[] = Linq2.select(showInventoryItem.shipments, s => this.props.releases.get(s.orderId)).toArray();
            this.setState({ showInventoryReleases });
        };
    }

    private showModalFulfillDispatch = (dispatch: IDispatch) => {
        return async () => {
            const promise: Promise<IDispatchInput> = new Promise((confirm, cancel) => {
                this.setState({ fulfillDispatchModalProps: { confirm, cancel, productionLines: this.props.productionLines, dispatch }});
            });
            try {
                await promise;
                try {
                    await this.props.updateDispatch({ ...dispatch, state: StateEnum.FULFILLMENT });
                    this.props.setSuccessMessage('Dispatch Successfully updated');
                } catch (err) {
                    this.props.setErrorMessage('Dispatch failed to update');
                    throw new Error(err);
                }
            } catch (e) {
                // ignore
            }
            this.setState({ fulfillDispatchModalProps: undefined });
        };
    }

    private completeDispatch = (dispatch: IDispatch) => async () => {
        try {
            await this.props.updateDispatch({ ...dispatch, state: StateEnum.COMPLETED });
            this.props.setSuccessMessage('Dispatch Successfully updated');
        } catch (err) {
            this.props.setErrorMessage('Dispatch failed to update');
            console.error(err);
        }
    }

    private deleteDispatch = async () => {
        try {
            await this.props.deleteDispatch(this.state.cancelDispatchItem);
            this.props.fetchOrders();
            this.props.setSuccessMessage('Dispatch Successfully deleted');
        } catch (err) {
            this.props.setErrorMessage('Failed to delete Dispatch');
            console.error(err);
        }
        this.setState({ cancelDispatchItem: undefined });
    }

    private renderCancelConfirmationModal(): React.ReactNode {
        return (
            <SimpleModal title="Cancel Dispatch"
                         confirm={this.deleteDispatch}
                         cancel={this.setCancelDispatch()}>
                <p>Are you sure you'd like to cancel Dispatch and move all its orders back to Pending Dispatch?</p>
            </SimpleModal>
        );
    }

    private setPrintDispatch = (dispatchGroup: IDispatchGroup) => async () => {
        this.clearPrintDispatch();
        const dispatch: IDispatch = this.props.dispatches.get(dispatchGroup.dispatchCode);
        if (Boolean(dispatch) && dispatchGroup && Util.isNonEmptyArray(dispatchGroup.orderIds)) {
            // set loading state
            this.setState({ creatingImage: true });
            try {
                const shipments: Map<string, IShipment> = Linq2.toDictionary(dispatch.shipments, s => s.orderId);
                const sorted = Util.isNonEmptyArray(dispatchGroup.orderIds) ? dispatchGroup.orderIds.sort((a, b) => a.localeCompare(b)) : [];
                const filtered: IShipment[] = sorted.map(id => shipments.get(id));
                const imageUrls = Linq2.selectMany(filtered, s => s.packages)
                                       .where(p => Boolean(p.labelUrl))
                                       .select(p => p.labelUrl)
                                       .toArray();
                if (!Util.isNonEmptyArray(imageUrls)) {
                    this.props.setErrorMessage('There are no shipping labels associated with this dispatch group');
                } else {
                    ShipmentService.printShippingLabels(imageUrls);
                }
            } catch (err) {
                this.props.setErrorMessage('Failed to create shipping labels');
            }
            // clear loading state
            this.setState({ creatingImage: false });
        }
    }

    private renderLabel = (url: string) => {
        return <div key={url}><img key={url} src={url} style={{margin: '0 3rem'}} /></div>;
    }

    private clearPrintDispatch = () => {
        if (this.mounted) {
            this.setState({ imageUrls: undefined });
        }
    }
    
    private renderPrintWindowPortal = () => {
        const { imageUrls } = this.state;
        return (
            <PrintWindowPortal onBeforeUnload={this.clearPrintDispatch}>
                <div style={{display: 'none'}}>{imageUrls.map(this.renderLabel)}</div>
            </PrintWindowPortal>
        );
    }

    private archiveDispatch = async () => {
        try {
            await this.props.updateDispatch({ ...this.state.archiveDispatch, state: StateEnum.ARCHIVED });
            this.props.setSuccessMessage('Dispatch Successfully updated');
        } catch (err) {
            this.props.setErrorMessage('Dispatch failed to update');
        }
        this.setState({ archiveDispatch: undefined });
    }

    private setArchiveDispatch = (archiveDispatch?: IDispatch) => () => {
        this.setState({ archiveDispatch });
    }

    private renderArchiveConfirmationModal(): React.ReactNode {
        return (
            <SimpleModal title="Archive Dispatch"
                         confirm={this.archiveDispatch}
                         cancel={this.setArchiveDispatch()}>
                <p>Are you sure you'd like to archive this dispatch?</p>
            </SimpleModal>
        );
    }

    private generateLogojetImage = async (dispatchGroup: IDispatchGroup, side: PrintSideEnum) => {
        // Set loading state
        this.setState({ creatingImage: true });
        try {
            const { orderIds } = dispatchGroup;
            const dispatch: IDispatch = this.props.dispatches.get(dispatchGroup.dispatchCode);
            const sorted = Util.isNonEmptyArray(orderIds) ? orderIds.sort((a, b) => a.localeCompare(b)) : [];
            const shipments: Map<string, IShipment> = Linq2.toDictionary(dispatch.shipments, s => s.orderId);
            const assignments: Map<string, IReleaseAssignment[]> = Linq2.where(sorted, id => shipments.has(id))
                                                                        .select(id => shipments.get(id))
                                                                        .where(s => Util.isNonEmptyArray(s.packages))
                                                                        .selectMany(s => s.packages)
                                                                        .where(p => Util.isNonEmptyArray(p.assignments))
                                                                        .selectMany(p => p.assignments)
                                                                        .groupBy(a => a.orderId)
                                                                        .toDictionary();

            const image = await PrintService.createLogojetImage(assignments, side);
            const url = await PrintService.buildPrintUrl(image, 'pdf', `${dispatchGroup.dispatchCode}_${Number(dispatchGroup.groupCode) + 1}_${side}.pdf`);
            Utilities.downloadFile(url);
            const imageProp = side === PrintSideEnum.front ? 'imageUri' : 'backImageUri';
            await this.props.updateDispatchGroup({ ...dispatchGroup, [imageProp]: url });
        } catch (err) {
            this.props.setErrorMessage('Failed to create logojet image');
        }
        // Clear loading state
        this.setState({ creatingImage: false });
    }

    private closePrintLogojetOptionsModal = () => {
        this.setState({ printLogojetModalOptions: undefined });
    }

    private printExistingLogojetImage = () => {
        const { group, side } = this.state.printLogojetModalOptions;
        const imageUri = side === PrintSideEnum.front ? group.imageUri : group.backImageUri;
        Utilities.downloadFile(imageUri);
        this.setState({ printLogojetModalOptions: undefined });
    }

    private createNewLogojetimage = async () => {
        const { group, side } = this.state.printLogojetModalOptions;
        await this.generateLogojetImage(group, side);
        this.setState({ printLogojetModalOptions: undefined });
    }

    private renderPrintLogojetOptionsModal = () => {
        const { printLogojetModalOptions } = this.state;
        if (Boolean(printLogojetModalOptions)) {
            return (
                <Dialog open={true} onClose={this.closePrintLogojetOptionsModal}>
                    <DialogTitle>{`Print Logojet Image (${printLogojetModalOptions.side})`}</DialogTitle>
                    <DialogContent style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '2rem' }}>
                        <Button variant="outlined"
                                color="primary"
                                disabled={this.state.creatingImage}
                                onClick={this.printExistingLogojetImage}>PRINT EXISTING IMAGE</Button>
                        <p style={{textAlign: 'center'}}>OR</p>
                        <Button variant="outlined"
                                color="primary"
                                disabled={this.state.creatingImage}
                                onClick={this.createNewLogojetimage}>GENERATE NEW IMAGE</Button>
                    </DialogContent>
                    <DialogActions>
                        <Button color="primary"
                                onClick={this.closePrintLogojetOptionsModal}>Cancel</Button>
                    </DialogActions>
                </Dialog>
            );
        }
    }

    private printLogojetImage = (dispatchGroup: IDispatchGroup, side: PrintSideEnum) => async () => {
        const imageUri = side === PrintSideEnum.front ? dispatchGroup.imageUri : dispatchGroup.backImageUri;
        if (Boolean(imageUri)) {
            this.setState({ printLogojetModalOptions: { group: dispatchGroup, side } });
            return;
        }
        
        this.generateLogojetImage(dispatchGroup, side);
    }

    private toggleExpandedDispatch = (expandedDispatch?: IDispatch) => () => {
        this.setState({ expandedDispatch });
    }

    private renderGroupPopover = (group: IDispatchGroup) => {
        const dispatch = this.props.dispatches.get(group.dispatchCode);
        const disabled = (dispatch.state === StateEnum.READY || dispatch.state === StateEnum.SHIPPABLE) || this.state.creatingImage;
        const key = `${group.dispatchCode}-${group.groupCode}`;
        return (
            <>
                <IconButton disabled={disabled} onClick={this.showMenu(key)}>
                    <MoreVert />
                </IconButton>
                <Popover open={this.state.groupOptionsKey === key}
                         anchorEl={this.state.menuAnchor}
                         anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                         transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                         onClose={this.hideMenu}>
                    <MenuItem onClick={this.setPrintDispatch(group)}>Print Shipping Labels</MenuItem>
                    <MenuItem onClick={this.printLogojetImage(group, PrintSideEnum.front)}>Print Logojet Front</MenuItem>
                    <MenuItem onClick={this.printLogojetImage(group, PrintSideEnum.back)}>Print Logojet Back</MenuItem>
                </Popover>
            </>
        );
    }

    private showMenu = (groupOptionsKey: string) => (e: React.MouseEvent<HTMLElement>) => {
        this.setState({ groupOptionsKey, menuAnchor: e.currentTarget });
    }

    private hideMenu = () => {
        this.setState({ groupOptionsKey: undefined, menuAnchor: undefined });
    }

    private renderRowChildren = (dispatch: IDispatch) => {
        const { expandedDispatch } = this.state;
        const { groups } = dispatch;
        if (Boolean(expandedDispatch) && expandedDispatch.dispatchCode === dispatch.dispatchCode) {
            return (
                <div style={{borderRight: `1px solid ${FCSConstants.grayLight}`, borderLeft: `1px solid ${FCSConstants.grayLight}`}}>
                    {Util.isNonEmptyArray(groups) && groups.map((group, i) => (
                        <div key={`dispatch-group-${group.groupCode}`}style={{display: 'flex', flexDirection: 'row', alignItems: 'center', padding: '0 1rem', justifyContent: 'space-between'}}>
                            <label>{`Group: ${i + 1} of ${groups.length}`}</label>
                            <div>{`${group.orderIds.length} Orders`}</div>
                            {this.renderGroupPopover(group)}
                        </div>
                    ))}
                    <div style={{padding: '.5rem', borderTop: `1px solid ${FCSConstants.grayLight}`, display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
                        <Button color="primary"
                                onClick={this.setShowInventory(dispatch)}>CHECK INVENTORY</Button>
                        <Button disabled={!(dispatch.state === StateEnum.READY || dispatch.state === StateEnum.SHIPPABLE)}
                                color="primary"
                                onClick={this.setCancelDispatch(dispatch)}>CANCEL DISPATCH</Button>
                    </div>
                </div>
            );
        }
        return null;
    }

    private renderStatus = (dispatch: IDispatch) => {
        const ready = dispatch.state === StateEnum.READY || dispatch.state === StateEnum.SHIPPABLE;
        return ready ? 'Ready' : <Typography color="primary"><strong>In Fulfillment</strong></Typography>;
    }

    private renderAction = (dispatch: IDispatch) => {
        const expanded = Boolean(this.state.expandedDispatch) ? this.state.expandedDispatch.dispatchCode === dispatch.dispatchCode : false;
        const isCompleted = Linq2.selectMany(dispatch.shipments, s => s.packages).all(p => p.state === StateEnum.COMPLETED);
        const isReady = dispatch.state === StateEnum.READY || dispatch.state === StateEnum.SHIPPABLE;
        return (
            <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
                <Button style={{marginRight: '1rem'}}
                        onClick={isCompleted ? this.completeDispatch(dispatch) : this.showModalFulfillDispatch(dispatch)}
                        disabled={!isReady && !isCompleted}
                        variant="contained"
                        color="primary">{isCompleted ? 'COMPLETE' : 'FULFILL'}</Button>
                <IconButton onClick={this.toggleExpandedDispatch(expanded ? undefined : dispatch)}>
                    {expanded ? <ExpandLess /> : <ExpandMore />}
                </IconButton>
            </div>
        );
    }

    public render(): React.ReactNode {
        const { cancelDispatchItem, showInventoryReleases, fulfillDispatchModalProps, archiveDispatch, imageUrls } = this.state;
        const { selectedClient, dispatches, activeDispatchStatus } = this.props;
        const clientDispatches = Linq2.where(dispatches.values(), d => d.clientCode === selectedClient).toArray();
        const incomplete = Linq2.where(clientDispatches, d => d.state === StateEnum.READY || d.state === StateEnum.FULFILLMENT || d.state === StateEnum.SHIPPABLE).toArray();
        const completed = Linq2.where(clientDispatches, d => d.state === StateEnum.COMPLETED).toArray();

        if (Boolean(activeDispatchStatus) && activeDispatchStatus.size) {
            activeDispatchStatus.forEach(s => {
                if (s.state !== JobStateEnum.COMPLETE && s.state !== JobStateEnum.FAILED) {
                    incomplete.push({ ...new Dispatch(), dispatchCode: s.dispatchCode });
                }
            });
        }

        const readyColumns: IDataTableColumn<IDispatch>[] = [
            {
                title: 'Dispatch ID',
                value: (item) => item.dispatchCode
            },
            {
                title: '# of Orders',
                value: (item) => {
                    if (activeDispatchStatus.has(item.dispatchCode)) {
                        return activeDispatchStatus.get(item.dispatchCode).totalShipments;
                    }
                    return Util.isNonEmptyArray(item.shipments) && item.shipments.length;
                }
            },
            {
                title: 'Date Created',
                value: (item) => {
                    const startTime = activeDispatchStatus.has(item.dispatchCode) ? activeDispatchStatus.get(item.dispatchCode).startedTime : item.beganAt;
                    return formatDateString(startTime);
                }
            },
            {
                title: 'Status',
                value: (item) => {
                    if (activeDispatchStatus.has(item.dispatchCode)) {
                        return 'Processing';
                    }
                    return this.renderStatus(item);
                }
            },
            {
                value: (item) => !activeDispatchStatus.has(item.dispatchCode) && this.renderAction(item)
            }
        ];

        const inFulfillmentColumns: IDataTableColumn<IDispatch>[] = [
            {
                title: 'Dispatch ID',
                value: (item) => item.dispatchCode
            },
            {
                title: '# of Orders',
                value: (item) => Util.isNonEmptyArray(item.shipments) && item.shipments.length
            },
            {
                title: 'Date Created',
                value: (item) => formatDateString(item.beganAt)
            },
            {
                title: 'Status',
                value: (item) => 'Completed'
            },
            {
                value: (item) => <Button style={{marginRight: '1rem'}}
                                         onClick={this.setArchiveDispatch(item)}
                                         variant="outlined"
                                         disabled={item.state !== StateEnum.COMPLETED}
                                         color="primary">ARCHIVE</Button>
            }
        ];

        return (
            <>
                {cancelDispatchItem !== undefined && this.renderCancelConfirmationModal()}
                {this.renderPrintLogojetOptionsModal()}
                {Util.isNonEmptyArray(showInventoryReleases) && <ModalCheckInventory confirm={this.setShowInventory()} orders={showInventoryReleases} />}
                {Boolean(archiveDispatch) && this.renderArchiveConfirmationModal()}
                {Util.isNonEmptyArray(imageUrls) && this.renderPrintWindowPortal()}
                {Boolean(fulfillDispatchModalProps) && <ModalFulfillDispatch {...fulfillDispatchModalProps} />}
                <DataTable placeholderText="No Dispatches"
                           title="Dispatches"
                           data={incomplete}
                           columns={readyColumns}
                           renderRowChildren={this.renderRowChildren} />
                <DataTable placeholderText="No Completed Dispatches"
                           title="Dispatches - Completed"
                           data={completed}
                           columns={inFulfillmentColumns} />
            </>
        );
    }
}
