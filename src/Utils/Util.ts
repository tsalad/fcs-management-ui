/**
 * 
 * @param query a string separated by the specified separator to query the object by
 * @param obj any data object that we can extract the value from using the query
 * @param separator the separator used to split the query string.
 * @returns any value that is retrieved from the object property specified by the query string.
 */
const getNestedValue = (query: string, obj: any, separator = '.'): any => {
    if (query.indexOf(separator) === -1) {
        return obj[query];
    }
    const separatorIndex = query.indexOf(separator);
    return getNestedValue(query.substring(separatorIndex + 1), obj[query.substring(0, separatorIndex)], separator);
};

/**
 * @param dateString an ISO date string that we want to convert to human readable text. Ex: 2019-07-29T00:00:00.000Z
 * @param showTime flag indicating whether or not to include time
 * @returns a human readable representation of the date
 */
const formatDateString = (dateString: string = '', showTime: boolean = false): string => {
    if (!dateString) {
        return;
    }
    // Need to replace "-" with "/" in date portion since Date constructor will subtract a day otherwise.
    const d = new Date(dateString.split('T')[0].replace(/-/g, '\/'));
    return `${d.toLocaleDateString()}${showTime ? ` ${new Date(dateString).toLocaleTimeString()}` : ''}`;
};

/**
 * @param address the address to format
 * @returns a human readable representation of the address object.
 */
const formatAddress = (address: IAddress): string => {
    if (!address) {
        return null;
    }
    return [ address.street, address.unit, address.locality, address.region, address.postalCode ].filter(i => Boolean(i)).join(', ');
};

export {
    getNestedValue,
    formatDateString,
    formatAddress
};
