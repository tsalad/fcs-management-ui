import { Constants } from 'o8-common';

export default class FCSConstants extends Constants {
    // Plugin name
    public static PLUGIN_ID: string = 'fcs-ui';

    // Menu items
    public static MENU_PRODUCTS: string = 'MENU_PRODUCTS';
    public static MENU_ORDERS: string = 'MENU_ORDERS';
    public static MENU_MANAGE: string = 'MENU_MANAGE';
    public static MENU_CLIENTS: string = 'MENU_CLIENTS';
    public static MENU_LOGISTICS: string = 'MENU_LOGISTICS';
    public static MENU_ACCOUNTS: string = 'MENU_ACCOUNTS';
    public static MENU_NOTIFICATIONS: string = 'MENU_NOTIFICATIONS';
    public static MENU_IMPORT_MANIFEST: string = 'MENU_IMPORT_MANIFEST';
    public static MENU_SUPERVISOR_DASHBOARD: string = 'MENU_SUPERVISOR_DASHBOARD';

    // Catalog Actions
    public static FETCH_ITEMS_SUCCESS: string = 'FETCH_ITEMS_SUCCESS';
    public static FETCH_ITEMS_FAILURE: string = 'FETCH_ITEMS_FAILURE';
    public static FETCH_CATEGORIES_SUCCESS: string = 'FETCH_CATEGORIES_SUCCESS';
    public static FETCH_CATEGORIES_FAILURE: string = 'FETCH_CATEGORIES_FAILURE';
    public static FETCH_PRODUCTS_SUCCESS: string = 'FETCH_PRODUCTS_SUCCESS';
    public static FETCH_PRODUCTS_FAILURE: string = 'FETCH_PRODUCTS_FAILURE';
    public static CREATE_PRODUCT_SUCCESS: string = 'CREATE_PRODUCT_SUCCESS';
    public static CREATE_PRODUCT_FAILURE: string = 'CREATE_PRODUCT_FAILURE';
    public static CREATE_PRODUCT_ITEM_SUCCESS: string = 'CREATE_PRODUCT_ITEM_SUCCESS';
    public static CREATE_PRODUCT_ITEM_FAILURE: string = 'CREATE_PRODUCT_ITEM_FAILURE';
    public static UPDATE_PRODUCT_ITEM_SUCCESS: string = 'UPDATE_PRODUCT_ITEM_SUCCESS';
    public static UPDATE_PRODUCT_ITEM_FAILURE: string = 'UPDATE_PRODUCT_ITEM_FAILURE';
    public static CREATE_CATEGORY_SUCCESS: string = 'CREATE_CATEGORY_SUCCESS';
    public static CREATE_CATEGORY_FAILURE: string = 'CREATE_CATEGORY_FAILURE';
    public static FETCH_PRODUCT_TAGS_SUCCESS: string = 'FETCH_PRODUCT_TAGS_SUCCESS';
    public static FETCH_PRODUCT_TAGS_FAILURE: string = 'FETCH_PRODUCT_TAGS_FAILURE';
    public static CREATE_TAG_LABEL_SUCCESS: string = 'CREATE_TAG_LABEL_SUCCESS';
    public static CREATE_TAG_LABEL_FAILURE: string = 'CREATE_TAG_LABEL_FAILURE';
    public static DELETE_PRODUCT_ITEM_IMAGE_SUCCESS: string = 'DELETE_PRODUCT_ITEM_IMAGE_SUCCESS';
    public static DELETE_PRODUCT_ITEM_IMAGE_FAILURE: string = 'DELETE_PRODUCT_ITEM_IMAGE_FAILURE';
    
    // Fulfillment Actions
    public static FETCH_ORDERS_PENDING_SUCCESS: string = 'FETCH_ORDERS_PENDING_SUCCESS';
    public static FETCH_ORDERS_PENDING_FAILURE: string = 'FETCH_ORDERS_PENDING_FAILURE';
    public static CREATE_RELEASE_SUCCESS: string = 'CREATE_RELEASE_SUCCESS';
    public static CREATE_RELEASE_FAILURE: string = 'CREATE_RELEASE_FAILURE';
    public static UPDATE_RELEASE_SUCCESS: string = 'UPDATE_RELEASE_SUCCESS';
    public static UPDATE_RELEASE_FAILURE: string = 'UPDATE_RELEASE_FAILURE';
    public static DELETE_RELEASE_ITEM_SUCCESS: string = 'DELETE_RELEASE_ITEM_SUCCESS';
    public static DELETE_RELEASE_ITEM_FAILURE: string = 'DELETE_RELEASE_ITEM_FAILURE';
    public static CREATE_RELEASE_ITEM_SUCCESS: string = 'CREATE_RELEASE_ITEM_SUCCESS';
    public static CREATE_RELEASE_ITEM_FAILURE: string = 'CREATE_RELEASE_ITEM_FAILURE';
    public static UPDATE_RELEASE_ITEM_SUCCESS: string = 'UPDATE_RELEASE_ITEM_SUCCESS';
    public static UPDATE_RELEASE_ITEM_FAILURE: string = 'UPDATE_RELEASE_ITEM_FAILURE';
    public static FETCH_DISPATCHES_SUCCESS: string = 'FETCH_DISPATCHES_SUCCESS';
    public static FETCH_DISPATCHES_FAILURE: string = 'FETCH_DISPATCHES_FAILURE';
    public static CREATE_DISPATCH_SUCCESS: string = 'CREATE_DISPATCH_SUCCESS';
    public static CREATE_DISPATCH_FAILURE: string = 'CREATE_DISPATCH_FAILURE';
    public static UPDATE_DISPATCH_SUCCESS: string = 'UPDATE_DISPATCH_SUCCESS';
    public static UPDATE_DISPATCH_FAILURE: string = 'UPDATE_DISPATCH_FAILURE';
    public static DELETE_DISPATCH_SUCCESS: string = 'DELETE_DISPATCH_SUCCESS';
    public static DELETE_DISPATCH_FAILURE: string = 'DELETE_DISPATCH_FAILURE';
    public static FETCH_ORDERS_FLAGGED_SUCCESS: string = 'FETCH_ORDERS_FLAGGED_SUCCESS';
    public static FETCH_ORDERS_FLAGGED_FAILURE: string = 'FETCH_ORDERS_FLAGGED_FAILURE';
    public static FETCH_RELEASES_FAILURE: string = 'FETCH_RELEASES_FAILURE';
    public static FETCH_RELEASES_SUCCESS: string = 'FETCH_RELEASES_SUCCESS';
    public static FETCH_ACTIVE_DISPATCH_STATUS_SUCCESS: string = 'FETCH_ACTIVE_DISPATCH_STATUS_SUCCESS';
    public static FETCH_ACTIVE_DISPATCH_STATUS_FAILURE: string = 'FETCH_ACTIVE_DISPATCH_STATUS_FAILURE';

    // Logistics Actions
    public static FETCH_CLIENTS_SUCCESS: string = 'FETCH_CLIENTS_SUCCESS';
    public static FETCH_CLIENTS_FAILURE: string = 'FETCH_CLIENTS_FAILURE';
    public static FETCH_DEPOTS_SUCCESS: string = 'FETCH_DEPOTS_SUCCESS';
    public static FETCH_DEPOTS_FAILURE: string = 'FETCH_DEPOTS_FAILURE';
    public static FETCH_STORAGE_SUCCESS: string = 'FETCH_STORAGE_SUCCESS';
    public static FETCH_STORAGE_FAILURE: string = 'FETCH_STORAGE_FAILURE';
    public static CREATE_DEPOT_SUCCESS: string = 'CREATE_DEPOT_SUCCESS';
    public static CREATE_DEPOT_FAILURE: string = 'CREATE_DEPOT_FAILURE';
    public static UPDATE_DEPOT_SUCCESS: string = 'UPDATE_DEPOT_SUCCESS';
    public static UPDATE_DEPOT_FAILURE: string = 'UPDATE_DEPOT_FAILURE';
    public static DELETE_DEPOT_SUCCESS: string = 'DELETE_DEPOT_SUCCESS';
    public static DELETE_DEPOT_FAILURE: string = 'DELETE_DEPOT_FAILURE';
    public static FETCH_STOCK_SUCCESS: string = 'FETCH_STOCK_SUCCESS';
    public static FETCH_STOCK_FAILURE: string = 'FETCH_STOCK_FAILURE';
    public static CREATE_STOCK_SUCCESS: string = 'CREATE_STOCK_SUCCESS';
    public static CREATE_STOCK_FAILURE: string = 'CREATE_STOCK_FAILURE';
    public static FETCH_EVENTS_SUCCESS: string = 'FETCH_EVENTS_SUCCESS';
    public static FETCH_EVENTS_FAILURE: string = 'FETCH_EVENTS_FAILURE';
    public static FETCH_PRODUCTION_LINES_SUCCESS: string = 'FETCH_PRODUCTION_LINES_SUCCESS';
    public static FETCH_PRODUCTION_LINES_FAILURE: string = 'FETCH_PRODUCTION_LINES_FAILURE';
    public static STOCK_MOVE_SUCCESS: string = 'STOCK_MOVE_SUCCESS';
    public static STOCK_MOVE_FAILURE: string = 'STOCK_MOVE_FAILURE';
    public static FETCH_SUPPLIERS_SUCCESS: string = 'FETCH_SUPPLIERS_SUCCESS';
    public static FETCH_SUPPLIERS_FAILURE: string = 'FETCH_SUPPLIERS_FAILURE';
    public static CREATE_SUPPLIER_SUCCESS: string = 'CREATE_SUPPLIER_SUCCESS';
    public static CREATE_SUPPLIER_FAILURE: string = 'CREATE_SUPPLIER_FAILURE';
    public static UPDATE_SUPPLIER_SUCCESS: string = 'UPDATE_SUPPLIER_SUCCESS';
    public static UPDATE_SUPPLIER_FAILURE: string = 'UPDATE_SUPPLIER_FAILURE';
    public static DELETE_SUPPLIER_SUCCESS: string = 'DELETE_SUPPLIER_SUCCESS';
    public static DELETE_SUPPLIER_FAILURE: string = 'DELETE_SUPPLIER_FAILURE';
    public static CREATE_CLIENT_SUCCESS: string = 'CREATE_CLIENT_SUCCESS';
    public static CREATE_CLIENT_FAILURE: string = 'CREATE_CLIENT_FAILURE';
    public static UPDATE_CLIENT_SUCCESS: string = 'UPDATE_CLIENT_SUCCESS';
    public static UPDATE_CLIENT_FAILURE: string = 'UPDATE_CLIENT_FAILURE';
    public static FETCH_SHIPPING_METHODS_SUCCESS: string = 'FETCH_SHIPPING_METHODS_SUCCESS';
    public static FETCH_SHIPPING_METHODS_FAILURE: string = 'FETCH_SHIPPING_METHODS_FAILURE';
    public static CREATE_STORAGE_SUCCESS: string = 'CREATE_STORAGE_SUCCESS';
    public static CREATE_STORAGE_FAILURE: string = 'CREATE_STORAGE_FAILURE';
    public static FETCH_WORKSTATION_SUCCESS: string = 'FETCH_WORKSTATION_SUCCESS';
    public static FETCH_WORKSTATION_FAILURE: string = 'FETCH_WORKSTATION_FAILURE';
    public static DELETE_STORAGE_SUCCESS: string = 'DELETE_STORAGE_SUCCESS';
    public static DELETE_STORAGE_FAILURE: string = 'DELETE_STORAGE_FAILURE';

    // Notifications Actions
    public static FETCH_PRODUCTS_REORDER_ALERT_SUCCESS: string = 'FETCH_PRODUCTS_REORDER_ALERT_SUCCESS';
    public static FETCH_PRODUCTS_REORDER_ALERT_FAILURE: string = 'FETCH_PRODUCTS_REORDER_ALERT_FAILURE';
    public static FETCH_PROBLEM_REPORTS_SUCCESS: string = 'FETCH_PROBLEM_REPORTS_SUCCESS';
    public static FETCH_PROBLEM_REPORTS_FAILURE: string = 'FETCH_PROBLEM_REPORTS_FAILURE';

    // Logojet Actions
    public static FETCH_TEMPLATES_SUCCESS: string = 'FETCH_TEMPLATES_SUCCESS';
    public static FETCH_TEMPLATES_FAILURE: string = 'FETCH_TEMPLATES_FAILURE';
    public static FETCH_LOGOJET_IMAGES_SUCCESS: string = 'FETCH_LOGOJET_IMAGES_SUCCESS';
    public static FETCH_LOGOJET_IMAGES_FAILURE: string = 'FETCH_LOGOJET_IMAGES_FAILURE';
    public static CREATE_LOGOJET_IMAGE_SUCCESS: string = 'CREATE_LOGOJET_IMAGE_SUCCESS';
    public static CREATE_LOGOJET_IMAGE_FAILURE: string = 'CREATE_LOGOJET_IMAGE_FAILURE';

    // Environment Actions
    public static FETCH_ENVIRONMENT_SUCCESS: string = 'FETCH_ENVIRONMENT_SUCCESS';
    public static FETCH_ENVIRONMENT_FAILURE: string = 'FETCH_ENVIRONMENT_FAILURE';

    // Application Actions
    public static SET_SNACKBAR_MESSAGE: string = 'SET_SNACKBAR_MESSAGE';
    public static CLEAR_SNACKBAR_MESSAGE: string = 'CLEAR_SNACKBAR_MESSAGE';
    public static MODAL_MANIFEST_IMPORT_TOGGLE: string = 'MODAL_MANIFEST_IMPORT_TOGGLE';

    // Parent Actions
    public static SET_CLIENT_MENUS: string = 'SET_CLIENT_MENUS';

    // TODO: This should be configurable.. currently this is the depot code for the FIC.
    public static depotCode: string = 'FIC';

    // ENV variables
    public static authorizationUri = 'security.oauth2.client.user-authorization-uri';
    public static porterUri = 'porter.fcs.url';
    public static porterUri2 = 'PORTER_FCS_URL';
    public static porterUri3 = 'porter_fcs_url';
    public static printUri = 'porter.print.url';
    public static printUri2 = 'PORTER_PRINT_URL';
    public static printUri3 = 'porter_print_url';

    public static category: string = '/catalog/category';
    public static item: string = '/catalog/item';

    // CSS Variables
    public static grayLight: string = '#E1E1E1';
    public static grayLighter: string = 'rgba(0, 0, 0, 0.07)';

    // Styles
    public static textFieldStyle: React.CSSProperties = {
        width: '100%',
        margin: '16px 1rem 8px 0'
    };

    public static headerStyle: React.CSSProperties = {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: '1rem',
        borderBottom: `1px solid ${FCSConstants.grayLight}`
    };

    public static buttonSpacing: React.CSSProperties = {
        margin: '0 .5rem'
    };

    // Used for the sticky footer in the list for select inputs
    public static listFooterStyle: React.CSSProperties =  { padding: '0', bottom: '0', backgroundColor: '#fff', borderTop: `1px solid ${FCSConstants.grayLight}` };
    public static listFooterButtonStyle: React.CSSProperties = { padding: '1rem', borderRadius: '0', width: '100%', justifyContent: 'space-between' };

    public static stateList: string[] = ['AL', 'AK', 'AS', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FM', 'FL', 'GA', 'GU', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MH', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'MP', 'OH', 'OK', 'OR', 'PW', 'PA', 'PR', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VI', 'VA', 'WA', 'WV', 'WI', 'WY'];

    // Common validation messages
    public static messageRequired: string = 'This is a required field.';

    // Number of spaces between orders in the logojet tray.
    public static traySpacing: number = 0;
}
