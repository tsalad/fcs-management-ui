import FCSConstants from './FCSConstants';

export default class Validators {
    public static isValidCode(value: string = ''): MC.IValidationResult {
        return { valid: (/^[A-Z0-9]{3,4}$/).test(value), message: 'Must be 3 or 4 characters and contain only numbers and letters' };
    }
    public static isValidSKU(value: string = ''): MC.IValidationResult {
        return { valid: (/^[A-Z0-9][A-Z0-9-_\\.]*$/).test(value), message: 'Sku must be composed of uppercase case letters, numbers, hyphens, underscores, and periods' };
    }
    public static isValidTagLabel(value: string = ''): MC.IValidationResult {
        if (value.length < 3) {
            return { valid: false, message: 'Minimum 3 characters.' };
        }
        return { valid: (/^[A-Z]+(_[A-Z]+)*$/).test(value), message: 'Minimum 3 characters and must be capital letters and "_", ex: ATTRIBUTE_TEST' };
    }

    public static isRequired(value: string | null | undefined = ''): MC.IValidationResult {
        if (typeof value === 'string') {
            value = value.trim();
        }
        return { valid: Boolean(value), message: FCSConstants.messageRequired }; 
    }

    public static isValidEmailAddress(value: string = ''): MC.IValidationResult {
        return {
            valid: value.length > 0 ? (/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/).test(value) : true,
            message: 'Please enter a valid email address.'
        };
    }

    public static isValidPhoneNumber(value: string = ''): MC.IValidationResult {
        return {
            valid: value.length > 0 ? (/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/).test(value) : true,
            message: 'Please enter a valid phone number.'
        };
    }
}
