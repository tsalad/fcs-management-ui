import { TemplateTypeEnum } from "./TemplateTypeEnum";

declare interface ITemplate {
    id: string;
    name: string;
    type: TemplateTypeEnum;
    maxItems: number;
    content: string;
    updatedAt?: string;
    createdAt?: string;
    supportedSlotTypes?: string[];
}