declare interface IMetaProps {
    displayName?: string;
    units?: string;
    min?: number;
    max?: number;
    required?: boolean;
}
