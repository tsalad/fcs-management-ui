import { OrderFlagReasonEnum } from './OrderFlagReasonEnum';

export interface IOrderFlag {
    id: string;
    createdAt: string;
    orderId: string;
    clientName: string;
    clientCode: string;
    packageCode: string;
    sku: string;
    assigneeId: string;
    assigneeIndex: string;
    addressName: string;
    shipBy: string;
    priority: string;
    reason: OrderFlagReasonEnum;
    status: string;
}