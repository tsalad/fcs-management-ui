import { EventCodeEnum } from "./EventCodeEnum";

export interface IStockInput {
    updatedAt?: string;
    storageCode: string;
    sku: string;
    eventCode: EventCodeEnum;
    adjustment: number;
}