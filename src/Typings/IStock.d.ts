declare interface IStock {
    updatedAt?: string;
    storageCode: string;
    sku: string;
    storedAt: string;
    quantity: number;
}