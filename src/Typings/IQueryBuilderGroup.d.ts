import { IQueryBuilderRule } from "./IQueryBuilderRule";
import { QueryBuilderOperation } from "./QueryBuilderOperation";

export interface IQueryBuilderGroup {
    type: QueryBuilderOperation;
    rules: (IQueryBuilderRule | IQueryBuilderGroup)[];
}