import { EventCodeEnum } from "./EventCodeEnum";

declare interface IStockEvent {
    fromStorageCode: string;
    toStorageCode: string;
    sku: string;
    eventCode: EventCodeEnum;
    createdAt: string;
    quantity: number;
}