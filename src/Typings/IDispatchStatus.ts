import { JobStateEnum } from './JobStateEnum';

export interface IDispatchStatus {
    dispatchCode: string;
    dispatchCreated: boolean;
    state: JobStateEnum;
    startedTime: string;
    completedTime: string;
    totalShipments: number;
    processedShipments: number;
    failedShipments: number;
    successfulShipments: number;
    message: string;
}
