declare interface IDispatchInput {
    dispatchCode: string;
    clientCode: string;
    priority: number;
    productionCode: string;
    orderIds: string[];
    autoGroupRequest: {
        slots: number;
        space: number;
    }
}
