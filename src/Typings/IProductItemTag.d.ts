declare interface IProductItemTag {
    updatedAt?: string;
    sku: string;
    label: string;
    value: string;
}