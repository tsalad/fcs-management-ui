declare interface IQueryBuilderDecoratorProps {
    fields?: string[];
    foreignKeyDisplayName?: string;
    foreignKeyPrototype?: any;
    suggestionFields?: string[];
}