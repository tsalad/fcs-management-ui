declare interface IClientShippingMethod {
    updatedAt?: string;
    method: IShippingMethod;
    preferencePriority: number;
    expedited: boolean;
}