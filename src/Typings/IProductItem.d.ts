declare interface IProductItem extends Record<string, any> {
    updatedAt?: string;
    categoryCode?: string;
    productCode: string;
    sku: string;
    gtin?: string;
    weightOunces: number;
    reorderThreshold: number;
    imageUri?: string;
    name: string;
    description?: string;
    suppliers: IProductItemSupplier[];
}