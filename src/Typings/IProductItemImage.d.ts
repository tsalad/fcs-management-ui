declare interface IProductItemImage {
    name?: string;
    metadata?: Record<string, string>;
    contentType: string;
    url?: string;
    timestamp: number;
    data: string;
}
