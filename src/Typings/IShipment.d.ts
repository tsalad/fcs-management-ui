import { StateEnum } from "./StateEnum";
import { IShipmentPackage } from "./IShipmentPackage";

declare interface IShipment {
    updatedAt?: string;
    beganAt: string;
    completedAt: string;
    archivedAt: string;
    state: StateEnum;
    priority: number;
    clientCode: string;
    orderId: string;
    carrierCode: string;
    serviceCode: string;
    toDepotCode: string;
    fulfillmentDepotCode: string;
    dispatchCode: string;
    toAddress: IAddress;
    fromAddress: IAddress;
    shipperAddress: IAddress;
    packages: IShipmentPackage[];
}