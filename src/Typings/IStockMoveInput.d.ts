declare interface IStockMoveInput {
    fromStorageCode: string;
    fromUpdatedAt: string;
    toStorageCode: string;
    toUpdatedAt: string;
    sku: string;
    quantity: number;
}