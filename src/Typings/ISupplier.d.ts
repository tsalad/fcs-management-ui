declare interface ISupplier {
    updatedAt?: string;
    supplierCode: string;
    name: string;
    address: IAddress;
}
