declare interface ICreateShipmentRequest {
    orderId: string;
    priority: number;
    fulfillmentDepotCode: string;
    serviceCode?: string;
    dispatchCode?: string;
    createPackages: boolean;
}
