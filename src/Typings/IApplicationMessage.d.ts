import { SnackbarActionStateEnum } from "../Components/SnackbarAction";

declare interface IApplicationMessage {
    id: string;
    type: SnackbarActionStateEnum;
    message: string;
}