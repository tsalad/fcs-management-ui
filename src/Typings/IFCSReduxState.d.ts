import { IStockEvent } from "./IStockEvent";
import { IDispatch } from "./IDispatch";
import { ITemplate } from "./ITemplate";
import { IRelease } from "./IRelease";
import { IProblemReport } from "./IProblemReport";
import { IApplicationMessage } from "./IApplicationMessage";
import { IOrderFlag } from "./IOrderFlag";
import { IDispatchStatus } from "./IDispatchStatus";

declare interface IFCSReduxState extends MC.IPluginState {
    selectedClient: string;
    previousMenu: string;
    catalog: {
        items: Map<string, IProductItem>;
        categories: Map<string, ICategory>;
        products: Map<string, IProduct>;
        tags: Map<string, ITagLabel>;
    };
    fulfillment: {
        dispatches: Map<string, IDispatch>;
        ordersFlagged: Map<string, IOrderFlag>;
        nonArchivedReleases: Map<string, IRelease>;
        activeDispatchStatus: Map<string, IDispatchStatus>;
    };
    logistics: {
        clients: Map<string, IClient>;
        depots: Map<string, IDepot>;
        storage: Map<string, IStorage>;
        stock: Map<string, IStock[]>;
        events: Map<string, IStockEvent[]>;
        productionLines: Map<string, IProductionLine>;
        suppliers: Map<string, ISupplier>;
        shippingMethods: Map<string, IShippingMethod>;
        workstations: Map<string, IWorkstation>;
    };
    notifications: {
        problemReports: Map<string, IProblemReport>;
        reorderAlerts: Map<string, IProductItem>;
    }
    environment: Record<string, any>;
    application: {
        messages: Map<string, IApplicationMessage>;
        showModalManifestImport: boolean;
    }
}
