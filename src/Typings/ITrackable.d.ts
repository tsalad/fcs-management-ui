declare interface ITrackable {
    major: number;
    minor: number;
    beaconId: string;
    mac: string;
    nfcId: string;
}