declare interface IShipmentPackageItem {
    sku: string;
    quantity: number;
}