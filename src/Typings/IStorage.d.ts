declare interface IStorage {
    updatedAt?: string;
    depotCode: string;
    storageCode: string;
    name: string;
    stock?: IStock[];
}