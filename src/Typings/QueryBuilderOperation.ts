export enum QueryBuilderOperation {
    And = 'And',
    Or = 'Or'
}
