declare interface IPrintImage {
    id: string;
    template: string;
    tray: string;
    space: number;
    renderUrl: string;
    createdAt: string;
}
