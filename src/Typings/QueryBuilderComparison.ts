export enum QueryBuilderComparison {
    eq = 'eq',
    gt = 'gt',
    gte = 'gte',
    contains = 'contains',
    lt = 'lt',
    lte = 'lte',
    regex = 'regex'
}
