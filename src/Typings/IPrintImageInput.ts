import { PrintSideEnum } from './PrintSideEnum';

export interface IPrintImageInput {
    template?: string;
    tray: string;
    space: number;
    side: PrintSideEnum;
    items: {
        orderId: string;
        metaInfo?: string;
        items: {
            hostId: string;
            text: { [key: string]: string }
            frontTemplate?: string;
            backTemplate?: string;
        }[]
    }[];
}
