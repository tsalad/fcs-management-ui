import { IssueTypeEnum } from "./IssueTypeEnum";
import { ReasonEnum } from "./ReasonEnum";

declare interface IProblemReport {
    id: string;
    createdAt: string;
    updatedAt: string;
    closedAt: string;
    storageCode: string;
    productItemSku: string;
    reason: ReasonEnum;
    issueType: IssueTypeEnum;
    comment: string;
}