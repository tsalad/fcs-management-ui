import { IQueryBuilderGroup } from "./IQueryBuilderGroup";

export interface IQueryBuilderProps {
    group: IQueryBuilderGroup;
    onChange(group: IQueryBuilderGroup): void;
    fields: MC.ISimpleDropdownValue[];
    showRemoveButton?: boolean;
    onRemoveClicked?(): void;
    style?: React.CSSProperties;
    showResetButton?: boolean;
    onResetClicked?(): void;
    showExportButton?: boolean;
    onExportClicked?(): void;
    // suggestionProviders?: Record<string, (value: string) => Promise<string[]>>;
}
