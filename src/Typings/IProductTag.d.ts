declare interface IProductTag {
    updatedAt?: string;
    productCode: string;
    label: string;
    value: string;
}