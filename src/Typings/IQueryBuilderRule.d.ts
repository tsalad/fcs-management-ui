import { QueryBuilderComparison } from "./QueryBuilderComparison";

export interface IQueryBuilderRule {
    not?: boolean;
    op?: QueryBuilderComparison;
    caseSensitive?: boolean;
    field: string;
    value: string;
}
