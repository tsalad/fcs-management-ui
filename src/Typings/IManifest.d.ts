declare interface IManifest {
    sku: string;
    createdAt: string;
    manufacturingDate: string;
    uid: number;
    major: number;
    minor: number;
    uuid: number;
}