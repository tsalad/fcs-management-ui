declare interface IWorkstation {
    updatedAt: string;
    storageCode: string;
    productionCode: string;
    operation: string;
    name: string;
    stock: IStock[]
}