import { StateEnum } from "./StateEnum";

declare interface IUpdateShipmentRequest {
    updatedAt: string;
    dispatchCode?: string;
    serviceCode: string;
    priority: number;
    state: StateEnum;
}