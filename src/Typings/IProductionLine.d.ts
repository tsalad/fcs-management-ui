declare interface IProductionLine {
    updatedAt?: string;
    depotCode: string;
    productionCode: string;
    name: string;
}