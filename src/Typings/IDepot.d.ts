declare interface IDepot {
    updatedAt?: string;
    depotCode: string;
    clientCode: string;
    name: string;
    address: IAddress;
}
