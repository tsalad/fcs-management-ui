declare interface IDispatchGroup {
    dispatchCode: string;
    groupCode: string;
    updatedAt?: string;
    imageUri: string;
    backImageUri: string;
    labelUri: string;
    orderIds: string[];
}