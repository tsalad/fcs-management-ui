declare interface IShippingMethod {
    name: string;
    carrierCode: string;
    updatedAt: string;
    serviceCode: string;
    international: boolean;
    domestic: boolean;
    multiPackage: boolean;
    cutoffTime: {
        hour: number;
        minute: number;
        second: number;
        nano: number;
    };
    transitDaysMin: number;
    transitDaysMax: number;
}