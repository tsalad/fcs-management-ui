declare interface IClient {
    updatedAt?: string;
    clientCode: string;
    name: string;
    address: IAddress;
}