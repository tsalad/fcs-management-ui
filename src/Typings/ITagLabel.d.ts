declare interface ITagLabel {
    updatedAt?: string;
    label: string;
}