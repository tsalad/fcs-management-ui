declare interface IAddress {
    name: string;
    phone: string;
    email: string;
    street?: string;
    unit?: string;
    locality?: string;
    region?: string;
    postalCode?: string;
    country?: string;
}