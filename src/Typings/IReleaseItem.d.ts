declare interface IReleaseItem {
    updatedAt?: Date;
    orderId?: string;
    sku: string;
    name?: string;
    quantity: number;
    assignments?: IReleaseAssignment[];
}