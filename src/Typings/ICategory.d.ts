declare interface ICategory {
    updatedAt?: string;
    categoryCode: string;
    name: string;
    description?: string;
}