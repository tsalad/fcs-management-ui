import { IShipment } from "./IShipment";
import { StateEnum } from "./StateEnum";

declare interface IRelease extends Record<string, any> {
    updatedAt?: string;
    orderId: string;
    clientCode: string;
    clientName: string;
    depotCode: string;
    depotName: string;
    dispatchCode: string;
    shipBy: string;
    arriveEarliest: string;
    arriveLatest: string;
    allowExpedite: boolean;
    state: StateEnum;
    address: IAddress;
    items: IReleaseItem[];
}