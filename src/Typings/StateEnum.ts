export enum StateEnum {
    SHIPPABLE = 'SHIPPABLE',
    PENDING = 'PENDING',
    READY = 'READY',
    FULFILLMENT = 'FULFILLMENT',
    COMPLETED = 'COMPLETED',
    ARCHIVED = 'ARCHIVED'
}
