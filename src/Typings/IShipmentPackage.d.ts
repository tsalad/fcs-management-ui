import { StateEnum } from "./StateEnum";

declare interface IShipmentPackage {
    updatedAt?: string;
    beganAt: string;
    completedAt: string;
    archivedAt: string;
    state: StateEnum;
    orderId: string;
    packageCode: string;
    trackingNumber: string;
    labelUrl: string;
    weightOunces: number;
    items: IShipmentPackageItem[];
    assignments: IShipmentPackageAssignment[];
}