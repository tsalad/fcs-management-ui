declare interface IProduct {
    updatedAt?: string;
    categoryCode: string;
    productCode: string;
    name: string;
    description?: string;
}
