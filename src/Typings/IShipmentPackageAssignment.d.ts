declare interface IShipmentPackageAssignment {
    orderId: string;
    sku: string;
    assigneeId: string;
    assigneeIndex: number;
    assigneeData: string;
    packageCode: string;
    uid: number;
    major: number;
    minor: number;
    uuid: string;
}