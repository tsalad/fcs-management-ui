declare interface IReleaseAssignment {
    orderId: string;
    sku: string;
    updatedAt: Date;
    assigneeId: string;
    assigneeIndex: number;
    assigneeData: string;
    assigneeFrontTemplate: string;
    assigneeBackTemplate: string;
    uid: number;
    major: number;
    minor: number;
    uuid: string;
}