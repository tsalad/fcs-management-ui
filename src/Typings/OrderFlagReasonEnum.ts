export enum OrderFlagReasonEnum {
    InvalidAddress = 'InvalidAddress',
    Unshippable = 'Unshippable',
    ShippingLabelDefect = 'ShippingLabelDefect',
    PackageItemPrintDefect = 'PackageItemPrintDefect',
    PackageItemHardwareDefect = 'PackageItemHardwareDefect',
    PackageItemMismatch = 'PackageItemMismatch',
    PackageItemBadManifest = 'PackageItemBadManifest',
    Other = 'Other'
}
