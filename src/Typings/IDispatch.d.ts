import { StateEnum } from "./StateEnum";
import { IShipment } from "./IShipment";

declare interface IDispatch {
    updatedAt?: string;
    beganAt: string;
    completedAt: string;
    archivedAt: string;
    dispatchCode: string;
    productionCode: string;
    clientCode: string;
    state: StateEnum;
    shipments: IShipment[];
    groups: IDispatchGroup[];
}
