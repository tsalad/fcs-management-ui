declare interface IProductItemSupplier {
    supplierCode: string;
    supplierSku: string;
}