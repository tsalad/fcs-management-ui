/// tslint:disable-next-line:no-reference
/// <reference types="o8-common" />

import ManagementConsolePlugin from 'o8-common/dist/ManagementConsolePlugin';
import * as path from 'path';
import * as webpack from 'webpack';

const APP_ENV = process.env.APP_ENV;
console.log(`APP_ENV: ${APP_ENV}`);

const name: string = 'porter-ui';
const context: string = path.resolve(__dirname, 'src');
const outputPath: string = path.resolve(__dirname, 'build');
const resolve: string = path.resolve(__dirname, 'node_modules');

const config: webpack.Configuration = {
    context,
    entry: './index.tsx',
    mode: APP_ENV === 'production' ? 'production' : 'development',
    output: {
        path: outputPath,
        publicPath: '',
        filename: 'index.js',
        devtoolModuleFilenameTemplate: `webpack:///${name}/[resource-path]`
    },
    resolve: {
        modules: [ resolve ],
        extensions: [ '.ts', '.tsx', '.js', 'jsx' ]
    },
    module: {
        rules: [
            { test: /\.tsx?$/, use: [ 'ts-loader' ] },
            { test: /\.(woff|woff2|ttf|otf|eot|svg|jpg|jpeg|png|webp)$/, use: [ 'url-loader' ] },
            { test: /\.css$/, use: [ 'style-loader', 'css-loader' ] }
        ]
    },
    plugins: [
        new ManagementConsolePlugin({
            // externals: [ '@material-ui/core' ],
            name
        })
    ],
    devtool: APP_ENV === 'production' ? undefined : 'source-map'
};

export default config;
